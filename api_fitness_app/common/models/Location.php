<?php namespace Kozmo\Common\Models;

use Model;

/**
 * Location Model
 */
class Location extends Model
{
    /**
    * @var implement Translate
    */
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['title'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_common_locations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
     public $hasMany = [
         'children' => ['Kozmo\Common\Models\Location', 'key' => 'location_id', 'order' => 'name asc']
     ];

     public $belongsTo = [
         'parent' => ['Kozmo\Common\Models\Location', 'key' => 'location_id']
     ];

     /**
      * Events
      */
     public function beforeSave()
     {
         if ($this->parent) {
             $this->level = $this->parent->level + 1;
         } else {
             $this->level = 1;
         }

         if (!$this->slug) {
             $this->slug = str_slug($this->name);
         }

         if ($this->isDirty('is_enabled')) {
             if ($this->children) {
                 foreach ($this->children as $location) {
                     $location->is_enabled = $this->is_enabled;
                     $location->save();
                 }
             }
         }
     }

     /**
      * Scopes
      */
     public function scopeIsEnabled($query)
     {
         return $query->where(['is_enabled' => 1]);
     }

     public function scopeCities($query)
     {
         return $query->where(['type' => 'city']);
     }

     public function scopeDefaultCountry($query)
     {
         return $query->where(['type' => 'country', 'is_default' => 1]);
     }

     public function scopeDefaultCity($query)
     {
         return $query->where(['type' => 'city', 'is_default' => 1]);
     }
}
