<?php namespace Kozmo\Common\Models;

use Model;
use Kozmo\Helpers\ImageHelper;

/**
 * Splash Model
 */
class Splash extends Model
{

    use \October\Rain\Database\Traits\Sortable;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_common_splashes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Fillable fields
     */
    protected $appends = ['image_path'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopePublished($query)
    {
      return $query->where('is_published', 1);
    }

    public function scopeLatest($query)
    {
      return $query->orderBy('sort_order', 'desc');
    }

    /**
    *   Attributes
    */
    public function getImagePathAttribute($value)
    {
      if (!$this->image) {
        return;
      }
      return ImageHelper::publicUrl($this->image);
    }
}
