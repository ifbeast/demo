<?php namespace Kozmo\Common\Components;

use Event;
use Session;
use Carbon\Carbon;
use Kozmo\Sport\Models\Room;
use Kozmo\Common\Models\QrCode;
use Kozmo\Common\Models\Scanner;
use Kozmo\Personal\Models\Checkin;
use October\Rain\Support\Collection;
use Cms\Classes\ComponentBase;

class ScannerComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Scanner Component',
            'description' => 'Scan qr codes'
        ];
    }

    public function defineProperties()
    {
        return [
            'scanner_id' => [
                'title'       => 'Scanner ID',
                'description' => 'Scanner ID'
            ],
            'room_id' => [
                'title'       => 'Room ID',
                'description' => 'Room ID'
            ],
            'auto_refresh' => [
                'title'       => 'Auto Refresh',
                'description' => 'Auto Refresh',
                'type'        => 'boolean',
                'default'     => '0'
            ],
            'auto_refresh_rate' => [
                'title'       => 'Auto Refresh Rate',
                'description' => 'Auto Refresh Rate',
                'type'        => 'integer',
                'default'     => '5'
            ],
        ];
    }

    public function init()
    {

    }

    public function onRun()
    {
        $scannerId = $this->property('scanner_id');
        $scanner = Scanner::enabled()->where('id', $scannerId)->first();
        $this->page['scanner'] = $scanner;

        if ($roomId = $this->property('room_id')) {
            if ($room = Room::find($roomId)) {
                $this->page['currentRoom'] = $room;
            }
        }
    }

    public function onScan()
    {
        $scannerId = $this->property('scanner_id');
        $scanner = Scanner::enabled()->where('id', $scannerId)->first();
        $this->page['scanner'] = $scanner;

        if ($roomId = $this->property('room_id')) {
            if ($room = Room::find($roomId)) {
                $this->page['currentRoom'] = $room;
            }
        }

        $target = '#info';
        switch ($scanner->type) {
            case 'in-out':
                $target = '#info';
            break;
            case 'only-in':
                $target = '#info';
            break;
            case 'info':
                $target = '@#info';
            break;
        }

        if (!$code = post('code')) {
            $this->page['currentState'] = 'error';
            return [
                'status' => 'error',
                $target => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }
        if (!$qrcode = QrCode::byCode($code)->first()) {
            $this->page['currentState'] = 'error';
            $this->page['currentError'] = 'Код '.$code.' не найден';
            return [
                'status' => 'error',
                $target => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }
        if (!$user = $qrcode->user()->enabled()->first()) {
            $this->page['currentState'] = 'error';
            $this->page['currentError'] = 'Аккаунт с кодом '.$code.' не найден';
            return [
                'status' => 'error',
                $target => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }

        $status = 'ok';
        switch ($scanner->type) {
            case 'in-out':
                if ($user->isFrozen) {
                    $user->setUnfreeze();
                }
                # check access
                if (!$user->hasSubscription) {
                    $this->page['currentState'] = 'error';
                    $this->page['currentError'] = 'Абонемент Sport для<br>'.$code.' не найден';
                    return [
                        'status' => 'error',
                        $target => $this->renderPartial('scanner/'.$scanner->type)
                    ];
                }
                # check out status today, then checkin
                # check in status today, then checkout
                if ($checkin = Checkin::in()->hour()->byCredentials($scanner->id, $user->id)->first()) {
                    $checkin->setStatusOut();
                    $status = 'out';
                    $this->page['currentMessage'] = $scanner->randomMessageOut;
                    Event::fire('check.out', [$checkin, true]);
                } else {
                    $checkin = new Checkin;
                    $checkin->user_id = $user->id;
                    $checkin->scanner_id = $scanner->id;
                    $checkin->setStatusIn();

                    $status = 'in';
                    $this->page['currentMessage'] = $scanner->randomMessageIn;
                    Event::fire('check.in', [$checkin, true]);
                }
                break;
            case 'only-in':
                if ($user->isFrozen) {
                    $user->setUnfreeze();
                }
                # check access
                if (!$user->getHasSubscriptionYoga()) {
                    $this->page['currentState'] = 'error';
                    $this->page['currentError'] = 'Абонемент Yoga для<br>'.$code.' не найден';
                    return [
                        'status' => 'error',
                        $target => $this->renderPartial('scanner/'.$scanner->type)
                    ];
                }
                if (!$roomId = post('room_id')) {
                    $this->page['currentState'] = 'error';
                    return [
                        'status' => 'error',
                        $target => $this->renderPartial('scanner/'.$scanner->type)
                    ];
                }
                if (!$room = Room::find($roomId)) {
                    $this->page['currentState'] = 'error';
                    return [
                        'status' => 'error',
                        $target => $this->renderPartial('scanner/'.$scanner->type)
                    ];
                }
                # check in status today
                if ($checkin = Checkin::in()->today()->byCredentials($scanner->id, $user->id)->byRoom($room->id)->first()) {

                    $status = 'already-in';
                    $this->page['currentMessage'] = $scanner->randomMessageOut;
                    Event::fire('check.out', [$checkin, true]);
                } else {
                    $checkin = new Checkin;
                    $checkin->user_id = $user->id;
                    $checkin->scanner_id = $scanner->id;
                    $checkin->room_id = $room->id;
                    $checkin->setStatusIn();

                    $status = 'in';
                    $this->page['currentMessage'] = $scanner->randomMessageIn;
                    Event::fire('check.in', [$checkin, true]);
                }
                break;
            // case 'only-out':
            //     if ($checkin = Checkin::in()->today()->byCredentials($scanner->id, $user->id)->first()) {
            //         $checkin->setStatusOut();
            //         $status = 'out';
            //         $this->page['currentMessage'] = $scanner->randomMessageOut;
            //         Event::fire('check.out', [$checkin]);
            //     } elseif ($checkin = Checkin::out()->today()->byCredentials($scanner->id, $user->id)->first()) {
            //         $status = 'already-out';
            //         $this->page['currentMessage'] = $scanner->randomMessageOut;
            //     } else {
            //         return [
            //             'status' => 'error',
            //             '#info' => $this->renderPartial('scanner/'.$scanner->type)
            //         ];
            //     }
            //     break;
            case 'info':
                if (!$user->hasSubscription) {
                    $this->page['currentState'] = 'error';
                    $this->page['currentError'] = $user->name.'&nbsp;'.$user->surname.' '.$user->persist_code.'<br>Нет активных абонементов';
                    return [
                        'status' => 'error',
                        $target => $this->renderPartial('scanner/'.$scanner->type)
                    ];
                }
                break;
        }

        $this->page['currentState'] = 'success';
        $this->page['currentUser'] = $user;
        return [
            'status' => $status,
            $target => $this->renderPartial('scanner/'.$scanner->type)
        ];
    }

    public function onRefresh()
    {
        $scannerId = $this->property('scanner_id');
        $scanner = Scanner::enabled()->where('id', $scannerId)->first();

        $this->page['scanner'] = $scanner;

        if ($roomId = $this->property('room_id')) {
            if ($room = Room::find($roomId)) {
                $this->page['currentRoom'] = $room;
            }
        }

        $this->page['currentState'] = 'default';
        return [
            'status' => 'ok',
            '#info' => $this->renderPartial('scanner/'.$scanner->type)
        ];
    }

    public function onClose()
    {
        $scannerId = $this->property('scanner_id');
        $scanner = Scanner::enabled()->where('id', $scannerId)->first();

        if (!$roomId = post('room_id')) {
            $this->page['currentState'] = 'error';
            return [
                'status' => 'error',
                '#info' => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }
        if (!$room = Room::find($roomId)) {
            $this->page['currentState'] = 'error';
            return [
                'status' => 'error',
                '#info' => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }
        $room->setClose();

        $this->page['scanner'] = $scanner;
        $this->page['currentState'] = 'default';
        return [
            'status' => 'ok',
            '#info' => $this->renderPartial('scanner/'.$scanner->type)
        ];
    }

    public function onNotify()
    {
        $scannerId = $this->property('scanner_id');
        $scanner = Scanner::enabled()->where('id', $scannerId)->first();

        $this->page['scanner'] = $scanner;
        $this->page['currentState'] = 'default';
        $lastCheckinIn = Session::get('checkins_in');
        $lastCheckinOut = Session::get('checkins_out');
        $checkinsIn = new Collection();
        $checkinsOut = new Collection();

        if (!$lastCheckinIn && !$lastCheckinOut) {
            $checkins = Checkin::where('updated_at', '>=', Carbon::now()->subMinutes(5))->get();
        } else {
            if ($lastCheckinIn) {
                $checkinsIn = Checkin::in()->where('updated_at', '>=', Carbon::now()->subMinutes(5))->where('id', '>', $lastCheckinIn)->get();
            } else {
                $checkinsIn = Checkin::in()->where('updated_at', '>=', Carbon::now()->subMinutes(5))->get();
            }
            if ($lastCheckinOut) {
                $checkinsOut = Checkin::out()->where('updated_at', '>=', Carbon::now()->subMinutes(5))->where('id', '>', $lastCheckinOut)->get();
            } else {
                $checkinsOut = Checkin::out()->where('updated_at', '>=', Carbon::now()->subMinutes(5))->get();
            }
            $checkins = $checkinsIn->merge($checkinsOut);
        }

        if (!$checkins->isEmpty()) {
            // if ($lastCheckin = $checkinsIn->last()) {
            //     Session::put('checkins_in', $lastCheckin->id);
            // }
            // if ($lastCheckin = $checkinsOut->last()) {
            //     Session::put('checkins_out', $lastCheckin->id);
            // }
            foreach ($checkins as $checkin) {
                if ($checkin->isStatusIn()) {
                    Session::put('checkins_in', $checkin->id);
                } elseif ($checkin->isStatusOut()) {
                    Session::put('checkins_out', $checkin->id);
                }
            }
            $this->page['checkins'] = $checkins;
            return [
                'status' => 'ok',
                '@#info' => $this->renderPartial('scanner/'.$scanner->type)
            ];
        }

        return [
            'status' => 'nothing',
            'message' => 'Nothing to update.'
        ];
    }
}
