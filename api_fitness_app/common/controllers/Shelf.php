<?php namespace Kozmo\Common\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Shelf Back-end Controller
 */
class Shelf extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Common', 'common', 'shelf');
    }
}
