<?php namespace Kozmo\Common\Updates;

use October\Rain\Database\Updates\Seeder;
use Kozmo\Common\Models\Location;

class SeedLocationsTable extends Seeder
{
    public function run()
    {
        /*
         * The countries and states table were previously seeded
         * by RainLab.User so this occurance is detected and halt.
         */
        if (Location::count() > 0) {
            return;
        }

        Location::insert([
            ['slug' => 'azerbaijan', 'title' => 'Азербайджан', 'level' => 1, 'type' => 'country', 'is_default' => 0],
            ['slug' => 'belarus', 'title' => 'Беларусь', 'level' => 1, 'type' => 'country', 'is_default' => 0],
            ['slug' => 'kazakhstan', 'title' => 'Казахстан', 'level' => 1, 'type' => 'country', 'is_default' => 1]
        ]);

        $az = Location::whereSlug('azerbaijan')->first();
        $az->setAttributeTranslated('title', 'Азәрбајҹан', 'az');
        $az->save();

        $cities = [
            ['title' => 'Агдам', 'translation' => 'Ağdam', 'level' => 2, 'type' => 'city'],
            ['title' => 'Агдаш', 'translation' => 'Ağdaş', 'level' => 2, 'type' => 'city'],
            ['title' => 'Агджабеди', 'translation' => 'Ağcabədi', 'level' => 2, 'type' => 'city'],
            ['title' => 'Аджигабул', 'translation' => 'Hacıqabul', 'level' => 2, 'type' => 'city'],
            ['title' => 'Акстафа', 'translation' => 'Ağstafa', 'level' => 2, 'type' => 'city'],
            ['title' => 'Астара', 'translation' => 'Astara', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ахсу', 'translation' => 'Ağsu', 'level' => 2, 'type' => 'city'],
            ['title' => 'Бабек', 'translation' => 'Babək', 'level' => 2, 'type' => 'city'],
            ['title' => 'Баку', 'translation' => 'Bakı', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['title' => 'Барда', 'translation' => 'Bərdə', 'level' => 2, 'type' => 'city'],
            ['title' => 'Бейлаган', 'translation' => 'Beyləqan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Белоканы', 'translation' => 'Balakən', 'level' => 2, 'type' => 'city'],
            ['title' => 'Билясувар', 'translation' => 'Biləsuvar', 'level' => 2, 'type' => 'city'],
            ['title' => 'Габала', 'translation' => 'Qəbələ', 'level' => 2, 'type' => 'city'],
            ['title' => 'Гёйгёль', 'translation' => 'Göygöl', 'level' => 2, 'type' => 'city'],
            ['title' => 'Геокчай', 'translation' => 'Göyçay', 'level' => 2, 'type' => 'city'],
            ['title' => 'Геранбой', 'translation' => 'Goranboy', 'level' => 2, 'type' => 'city'],
            ['title' => 'Гывраг', 'translation' => 'Gyvrag', 'level' => 2, 'type' => 'city'],
            ['title' => 'Гянджа', 'translation' => 'Gəncə', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дашкесан', 'translation' => 'Daşkəsən', 'level' => 2, 'type' => 'city'],
            ['title' => 'Джалилабад', 'translation' => 'Cəlilabad', 'level' => 2, 'type' => 'city'],
            ['title' => 'Джебраил', 'translation' => 'Cəbrayıl', 'level' => 2, 'type' => 'city'],
            ['title' => 'Джульфа', 'translation' => 'Culfa', 'level' => 2, 'type' => 'city'],
            ['title' => 'Евлах', 'translation' => 'Yevlax', 'level' => 2, 'type' => 'city'],
            ['title' => 'Закаталы', 'translation' => 'Zaqatala', 'level' => 2, 'type' => 'city'],
            ['title' => 'Зангелан', 'translation' => 'Zəngilan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Зердаб', 'translation' => 'Zərdab', 'level' => 2, 'type' => 'city'],
            ['title' => 'Имишли', 'translation' => 'İmişli', 'level' => 2, 'type' => 'city'],
            ['title' => 'Исмаиллы', 'translation' => 'İsmayıllı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Казах', 'translation' => 'Qazax', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кахи', 'translation' => 'Qax', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кедабек', 'translation' => 'Gədəbəy', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кельбаджар', 'translation' => 'Kəlbəcər', 'level' => 2, 'type' => 'city'],
            ['title' => 'Куба', 'translation' => 'Quba', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кубатлы', 'translation' => 'Qubadlı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кусары', 'translation' => 'Qusar', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кюрдамир', 'translation' => 'Kürdəmir', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лачин', 'translation' => 'Laçın', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ленкорань', 'translation' => 'Lənkəran', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лерик', 'translation' => 'Lerik', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мараза', 'translation' => 'Mərəzə', 'level' => 2, 'type' => 'city'],
            ['title' => 'Масаллы', 'translation' => 'Masallı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Нафталан', 'translation' => 'Naftalan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Нахичевань', 'translation' => 'Naxçıvan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Нефтечала', 'translation' => 'Neftçala', 'level' => 2, 'type' => 'city'],
            ['title' => 'Огуз', 'translation' => 'Oğuz', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ордубад', 'translation' => 'Ordubad', 'level' => 2, 'type' => 'city'],
            ['title' => 'Саатлы', 'translation' => 'Saatlı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сабирабад', 'translation' => 'Sabirabad', 'level' => 2, 'type' => 'city'],
            ['title' => 'Садарак', 'translation' => 'Sədərək', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сальяны', 'translation' => 'Salyan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Самух', 'translation' => 'Samux', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сиазань', 'translation' => 'Siyəzən', 'level' => 2, 'type' => 'city'],
            ['title' => 'Тертер', 'translation' => 'Tərtər', 'level' => 2, 'type' => 'city'],
            ['title' => 'Товуз', 'translation' => 'Tovuz', 'level' => 2, 'type' => 'city'],
            ['title' => 'Уджары', 'translation' => 'Ucar', 'level' => 2, 'type' => 'city'],
            ['title' => 'Физули', 'translation' => 'Füzuli', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ханкенди', 'translation' => 'Xankəndi', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хачмас', 'translation' => 'Xaçmaz', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ходжавенд', 'translation' => 'Xocavənd ', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ходжалы', 'translation' => 'Xocalı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хызы', 'translation' => 'Xızı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хырдалан', 'translation' => 'Xırdalan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шабран', 'translation' => 'Şabran', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шамкир', 'translation' => 'Şəmkir', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шарур', 'translation' => 'Şərur', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шахбуз', 'translation' => 'Şahbuz', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шеки', 'translation' => 'Şəki', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шемаха', 'translation' => 'Şamaxı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ширван', 'translation' => 'Şirvan', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шуша', 'translation' => 'Şuşa', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ярдымлы', 'translation' => 'Yardımlı', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сумгаит', 'translation' => 'SUMQAYIT', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мингечевир', 'translation' => 'MINGƏÇEVİR', 'level' => 2, 'type' => 'city'],
        ];

        foreach ($cities as $city) {
            $location = new Location;
            $location->title = $city['title'];
            $location->level = $city['level'];
            $location->type = $city['type'];
            if (isset($city['is_default']))
                $location->is_default = $city['is_default'];
            $location['location_id'] = $az->id;
            $location->save();

            $location->setAttributeTranslated('title', $city['translation'], 'az');
            $location->save();
        }

        $by = Location::whereSlug('belarus')->first();
        $by->children()->createMany([
            ['title' => 'Барановичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Белыничи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Береза', 'level' => 2, 'type' => 'city'],
            ['title' => 'Березино', 'level' => 2, 'type' => 'city'],
            ['title' => 'Берестовица', 'level' => 2, 'type' => 'city'],
            ['title' => 'Бешенковичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Бобруйск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Борисов', 'level' => 2, 'type' => 'city'],
            ['title' => 'Брагин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Браслав', 'level' => 2, 'type' => 'city'],
            ['title' => 'Брест', 'level' => 2, 'type' => 'city'],
            ['title' => 'Буда-Кошелево', 'level' => 2, 'type' => 'city'],
            ['title' => 'Быхов', 'level' => 2, 'type' => 'city'],
            ['title' => 'Верхнедвинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ветка', 'level' => 2, 'type' => 'city'],
            ['title' => 'Вилейка', 'level' => 2, 'type' => 'city'],
            ['title' => 'Витебск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Волковыск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Воложин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Вороново', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ганцевичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Глубокое', 'level' => 2, 'type' => 'city'],
            ['title' => 'Глуск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Гомель', 'level' => 2, 'type' => 'city'],
            ['title' => 'Горки', 'level' => 2, 'type' => 'city'],
            ['title' => 'Городок', 'level' => 2, 'type' => 'city'],
            ['title' => 'Гродно', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дзержинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Добруш', 'level' => 2, 'type' => 'city'],
            ['title' => 'Докшицы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дрибин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дрогичин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дубровно', 'level' => 2, 'type' => 'city'],
            ['title' => 'Дятлово', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ельск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жабинка', 'level' => 2, 'type' => 'city'],
            ['title' => 'Житковичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жлобин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жодино', 'level' => 2, 'type' => 'city'],
            ['title' => 'Зельва', 'level' => 2, 'type' => 'city'],
            ['title' => 'Иваново', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ивацевичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ивье', 'level' => 2, 'type' => 'city'],
            ['title' => 'Калинковичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Каменец', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кировск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Клецк', 'level' => 2, 'type' => 'city'],
            ['title' => 'Климовичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кличев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кобрин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Копыль', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кореличи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Корма', 'level' => 2, 'type' => 'city'],
            ['title' => 'Костюковичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Краснополье', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кричев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Круглое', 'level' => 2, 'type' => 'city'],
            ['title' => 'Крупки', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лельчицы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лепель', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лида', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лиозно', 'level' => 2, 'type' => 'city'],
            ['title' => 'Логойск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лоев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лунинец', 'level' => 2, 'type' => 'city'],
            ['title' => 'Любань', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ляховичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Малорита', 'level' => 2, 'type' => 'city'],
            ['title' => 'Марьина Горка', 'level' => 2, 'type' => 'city'],
            ['title' => 'Минск', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['title' => 'Минский район', 'level' => 2, 'type' => 'city'],
            ['title' => 'Миоры', 'level' => 2, 'type' => 'city'],
            ['title' => 'Могилев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мозырь', 'level' => 2, 'type' => 'city'],
            ['title' => 'Молодечно', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мосты', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мстиславль', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мядель', 'level' => 2, 'type' => 'city'],
            ['title' => 'Наровля', 'level' => 2, 'type' => 'city'],
            ['title' => 'Несвиж', 'level' => 2, 'type' => 'city'],
            ['title' => 'Новогрудок', 'level' => 2, 'type' => 'city'],
            ['title' => 'Новополоцк', 'level' => 2, 'type' => 'city'],
            ['title' => 'Октябрьский', 'level' => 2, 'type' => 'city'],
            ['title' => 'Орша', 'level' => 2, 'type' => 'city'],
            ['title' => 'Осиповичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Островец', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ошмяны', 'level' => 2, 'type' => 'city'],
            ['title' => 'Петриков', 'level' => 2, 'type' => 'city'],
            ['title' => 'Пинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Плещеницы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Полоцк', 'level' => 2, 'type' => 'city'],
            ['title' => 'Поставы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Пружаны', 'level' => 2, 'type' => 'city'],
            ['title' => 'Пуховичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Речица', 'level' => 2, 'type' => 'city'],
            ['title' => 'Рогачев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Россоны', 'level' => 2, 'type' => 'city'],
            ['title' => 'Светлогорск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Свислочь', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сенно', 'level' => 2, 'type' => 'city'],
            ['title' => 'Славгород', 'level' => 2, 'type' => 'city'],
            ['title' => 'Слоним', 'level' => 2, 'type' => 'city'],
            ['title' => 'Слуцк', 'level' => 2, 'type' => 'city'],
            ['title' => 'Смолевичи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сморгонь', 'level' => 2, 'type' => 'city'],
            ['title' => 'Солигорск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Старые Дороги', 'level' => 2, 'type' => 'city'],
            ['title' => 'Столбцы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Столин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Толочин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Узда', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ушачи', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хойники', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хотимск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Чаусы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Чашники', 'level' => 2, 'type' => 'city'],
            ['title' => 'Червень', 'level' => 2, 'type' => 'city'],
            ['title' => 'Чериков', 'level' => 2, 'type' => 'city'],
            ['title' => 'Чечерск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шарковщина', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шклов', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шумилино', 'level' => 2, 'type' => 'city'],
            ['title' => 'Щучин', 'level' => 2, 'type' => 'city']
        ]);

        $kz = Location::whereSlug('kazakhstan')->first();
        $kz->setAttributeTranslated('title', 'Қазақстан', 'kz');
        $kz->save();

        $cities = [
            ['title' => 'Абай', 'translation' => 'Абай', 'level' => 2, 'type' => 'city'],
            ['title' => 'Акколь', 'translation' => 'Ақкөл', 'level' => 2, 'type' => 'city'],
            ['title' => 'Аксай', 'translation' => 'Ақсай', 'level' => 2, 'type' => 'city'],
            ['title' => 'Аксу', 'translation' => 'Ақсу', 'level' => 2, 'type' => 'city'],
            ['title' => 'Актау', 'translation' => 'Ақтау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Актобе', 'translation' => 'Ақтөбе', 'level' => 2, 'type' => 'city'],
            ['title' => 'Алга', 'translation' => 'Алға', 'level' => 2, 'type' => 'city'],
            ['title' => 'Алматы', 'translation' => 'Алматы', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['title' => 'Аральск', 'translation' => 'Арал', 'level' => 2, 'type' => 'city'],
            ['title' => 'Аркалык', 'translation' => 'Арқалық', 'level' => 2, 'type' => 'city'],
            ['title' => 'Арысь', 'translation' => 'Арыс', 'level' => 2, 'type' => 'city'],
            ['title' => 'Астана', 'translation' => 'Астана', 'level' => 2, 'type' => 'city'],
            ['title' => 'Атбасар', 'translation' => 'Атбасар', 'level' => 2, 'type' => 'city'],
            ['title' => 'Атырау', 'translation' => 'Атырау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Аягоз', 'translation' => 'Аягөз', 'level' => 2, 'type' => 'city'],
            ['title' => 'Байконыр', 'translation' => 'Байқоңыр', 'level' => 2, 'type' => 'city'],
            ['title' => 'Балхаш', 'translation' => 'Балқаш', 'level' => 2, 'type' => 'city'],
            ['title' => 'Булаево', 'translation' => 'Булаев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Державинск', 'translation' => 'Державин', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ерейментау', 'translation' => 'Ерейментау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Есик', 'translation' => 'Есік', 'level' => 2, 'type' => 'city'],
            ['title' => 'Есиль', 'translation' => 'Есіл', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жанаозен', 'translation' => 'Жаңаөзен', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жанатас', 'translation' => 'Жаңатас', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жаркент', 'translation' => 'Жаркент', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жезказган', 'translation' => 'Жезқазған', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жем', 'translation' => 'Жем', 'level' => 2, 'type' => 'city'],
            ['title' => 'Жетысай', 'translation' => 'Жетісай', 'level' => 2, 'type' => 'city'],
            ['title' => 'Житикара', 'translation' => 'Жетіқара', 'level' => 2, 'type' => 'city'],
            ['title' => 'Зайсан', 'translation' => 'Зайсаң', 'level' => 2, 'type' => 'city'],
            ['title' => 'Зыряновск', 'translation' => 'Зыряновск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Казалинск', 'translation' => 'Қазалы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кандыагаш', 'translation' => 'Қандыағаш', 'level' => 2, 'type' => 'city'],
            ['title' => 'Капчагай', 'translation' => 'Қапшағай', 'level' => 2, 'type' => 'city'],
            ['title' => 'Караганда', 'translation' => 'Қарағанды', 'level' => 2, 'type' => 'city'],
            ['title' => 'Каражал', 'translation' => 'Қаражал', 'level' => 2, 'type' => 'city'],
            ['title' => 'Каратау', 'translation' => 'Қаратау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Каркаралинск', 'translation' => 'Қарқаралы', 'level' => 2, 'type' => 'city'],
            ['title' => 'Каскелен', 'translation' => 'Қаскелең', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кентау', 'translation' => 'Кентау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кокшетау', 'translation' => 'Көкшетау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Костанай', 'translation' => 'Қостанай', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кульсары', 'translation' => 'Құлсары', 'level' => 2, 'type' => 'city'],
            ['title' => 'Курчатов', 'translation' => 'Курчатов', 'level' => 2, 'type' => 'city'],
            ['title' => 'Кызылорда', 'translation' => 'Қызылорда', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ленгер', 'translation' => 'Ленгер', 'level' => 2, 'type' => 'city'],
            ['title' => 'Лисаковск', 'translation' => 'Лисаковск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Макинск', 'translation' => 'Макинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Мамлютка', 'translation' => 'Мамлют', 'level' => 2, 'type' => 'city'],
            ['title' => 'Павлодар', 'translation' => 'Павлодар', 'level' => 2, 'type' => 'city'],
            ['title' => 'Петропавловск', 'translation' => 'Петропавл', 'level' => 2, 'type' => 'city'],
            ['title' => 'Приозёрск', 'translation' => 'Приозер', 'level' => 2, 'type' => 'city'],
            ['title' => 'Риддер', 'translation' => 'Риддер', 'level' => 2, 'type' => 'city'],
            ['title' => 'Рудный', 'translation' => 'Рудный', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сарань', 'translation' => 'Саран', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сарканд', 'translation' => 'Сарқант', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сарыагаш', 'translation' => 'Сарыағаш', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сатпаев', 'translation' => 'Сәтбаев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Семей', 'translation' => 'Семей', 'level' => 2, 'type' => 'city'],
            ['title' => 'Сергеевка', 'translation' => 'Сергеев', 'level' => 2, 'type' => 'city'],
            ['title' => 'Серебрянск', 'translation' => 'Серебрянск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Степногорск', 'translation' => 'Степногор', 'level' => 2, 'type' => 'city'],
            ['title' => 'Степняк', 'translation' => 'Степняк', 'level' => 2, 'type' => 'city'],
            ['title' => 'Тайынша', 'translation' => 'Тайынша', 'level' => 2, 'type' => 'city'],
            ['title' => 'Талгар', 'translation' => 'Талғар', 'level' => 2, 'type' => 'city'],
            ['title' => 'Талдыкорган', 'translation' => 'Талдықорған', 'level' => 2, 'type' => 'city'],
            ['title' => 'Тараз', 'translation' => 'Тараз', 'level' => 2, 'type' => 'city'],
            ['title' => 'Текели', 'translation' => 'Текелі', 'level' => 2, 'type' => 'city'],
            ['title' => 'Темир', 'translation' => 'Темір', 'level' => 2, 'type' => 'city'],
            ['title' => 'Темиртау', 'translation' => 'Теміртау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Туркестан', 'translation' => 'Түркістан', 'level' => 2, 'type' => 'city'],
            ['title' => 'Уральск', 'translation' => 'Орал', 'level' => 2, 'type' => 'city'],
            ['title' => 'Усть-Каменогорск', 'translation' => 'Өскемен', 'level' => 2, 'type' => 'city'],
            ['title' => 'Ушарал', 'translation' => 'Үшарал', 'level' => 2, 'type' => 'city'],
            ['title' => 'Уштобе', 'translation' => 'Үштөбе', 'level' => 2, 'type' => 'city'],
            ['title' => 'Форт-Шевченко', 'translation' => 'Форт-Шевченко', 'level' => 2, 'type' => 'city'],
            ['title' => 'Хромтау', 'translation' => 'Хромтау', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шардара', 'translation' => 'Шардара', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шалкар', 'translation' => 'Шалқар', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шар', 'translation' => 'Шар', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шахтинск', 'translation' => 'Шахтинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шемонаиха', 'translation' => 'Шемонаиха', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шу', 'translation' => 'Шу', 'level' => 2, 'type' => 'city'],
            ['title' => 'Шымкент', 'translation' => 'Шымкент', 'level' => 2, 'type' => 'city'],
            ['title' => 'Щучинск', 'translation' => 'Щучинск', 'level' => 2, 'type' => 'city'],
            ['title' => 'Экибастуз', 'translation' => 'Екібастұз', 'level' => 2, 'type' => 'city'],
            ['title' => 'Эмба', 'translation' => 'Эмбі', 'level' => 2, 'type' => 'city']
        ];

        foreach ($cities as $city) {
            $location = new Location;
            $location->title = $city['title'];
            $location->level = $city['level'];
            $location->type = $city['type'];
            if (isset($city['is_default']))
                $location->is_default = $city['is_default'];
            $location['location_id'] = $kz->id;
            $location->save();

            $location->setAttributeTranslated('title', $city['translation'], 'kz');
            $location->save();
        }
    }
}
