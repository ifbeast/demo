<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateShelvesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_common_shelves', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('block')->nullable();
            $table->integer('row')->nullable();
            $table->integer('col')->nullable();
            $table->integer('user_id')->nullable();
            $table->boolean('is_enabled')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_common_shelves');
    }
}
