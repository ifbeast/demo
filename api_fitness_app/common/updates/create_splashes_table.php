<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSplashesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_common_splashes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->string('image');
            $table->string('video');
            $table->boolean('is_published')->default(0);
            $table->integer('sort_order')->default(0);
            $table->boolean('has_free_button')->default(0);
            $table->boolean('has_more_button')->default(0);
            $table->boolean('has_skip_button')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_common_splashes');
    }
}
