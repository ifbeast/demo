<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddPrintableQrcodesTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_common_qr_codes', function(Blueprint $table) {
           $table->boolean('is_printable')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_common_qr_codes', function(Blueprint $table) {
            $table->dropColumn('is_printable');
        });
    }
}
