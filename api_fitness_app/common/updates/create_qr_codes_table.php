<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateQrCodesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_common_qr_codes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('unique_string');
            $table->string('image_path');
            $table->timestamps();

            $table->unique('unique_string');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_common_qr_codes');
    }
}
