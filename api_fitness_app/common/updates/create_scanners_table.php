<?php namespace Kozmo\Common\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateScannersTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_common_scanners', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_enabled');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_common_scanners');
    }
}
