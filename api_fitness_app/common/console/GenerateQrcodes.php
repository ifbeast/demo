<?php namespace Kozmo\Common\Console;

use DB;
use Carbon\Carbon;
use ApplicationException;
use Kozmo\Common\Models\Shelf;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateQrcodes extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'generate:qrcodes';

    /**
     * @var string The console command description.
     */
    protected $description = 'Генерим qr коды.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        dd($this->option('amount'), $this->option('printable'));
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command opt  ions.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['amount', null, InputOption::VALUE_REQUIRED, 'Count of qr codes.', null],
            ['printable', null, InputOption::VALUE_REQUIRED, 'Printable option.', null],
        ];
    }

}
