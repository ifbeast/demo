<?php namespace Kozmo\Api\Routes;

use Carbon\Carbon;
use Kozmo\Api\Models\Settings;

use Kozmo\Sport\Models\Fresh;
use Kozmo\Sport\Models\Product;
use Kozmo\Sport\Models\Feature;
use Kozmo\Sport\Models\Room;
use Kozmo\Sport\Models\Trainer;
use Kozmo\Api\Classes\ResponseManager;

class SportRoutes
{
	const LIMIT_ROOMS_DAYS = 7;

	public static function products() {
		$data = Product::published()->byFree()->byType()->byPrice()->get();
		return ResponseManager::success($data);
	}

	public static function productsFree() {
		if (!$freeId = Settings::get('sport.free')) {
			return ResponseManager::error('default');
		}
		if (!$poduct = Product::find($freeId)) {
			return ResponseManager::error('default');
		}
		return ResponseManager::success($poduct);
	}

	public static function freshes() {
		$data = Fresh::published()->get();
		return ResponseManager::success($data);
	}

	public static function rooms() {
		$data = Room::published()->actual()->get();
		return ResponseManager::success($data);
	}

	public static function roomsMap() {
		$data = Room::published()->actual()->get()->groupBy(function($item, $key) {
			return $item['day'];
		});
		$roomsMap = [];
		$counter = 0;
		foreach ($data as $day => $rooms) {
			if ($counter >= self::LIMIT_ROOMS_DAYS) {
				break;
			}
			$counter = $counter + 1;
			$temp = ['day' => $day, 'data' => $rooms];
			$roomsMap[] = $temp;
		}
		return ResponseManager::success($roomsMap);
	}

	public static function roomsByDate($date = null) {
		if (!$date) {
			return ResponseManager::error('default');
		}
		if (!$date = Carbon::createFromFormat('Y-m-d', $date)) {
			return ResponseManager::error('default');
		}
		$data = Room::published()->byDate($date)->get();
		return ResponseManager::success($data);
	}

	public static function trainers() {
		$data = Trainer::published()->get();
		return ResponseManager::success($data);
	}

	public static function trainersByType($type = null) {
		if (!$type) {
			return ResponseManager::error('default');
		}
		$data = Trainer::published()->byType($type)->get();
		return ResponseManager::success($data);
	}
}
