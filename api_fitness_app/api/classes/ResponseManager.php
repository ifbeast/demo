<?php namespace Kozmo\Api\Classes;

use Log;
use View;
use Response;
use October\Rain\Database\Collection;

class ResponseManager
{
	public static $messages = [
		'default' => [
			'code' => 200
		]
	];

	public static $errors = [
		'default' => [
			'code'  =>  1000,
			'error' => 'Internal error.'
		],
		'empty' => [
			'code'  =>  2000,
			'error' => 'Nothing send.'
		],
		'api' => [
			'code'  =>  1010,
			'error' => 'Wrong api token.'
		],
		'auth' => [
			'code'  =>  1100,
			'error' => 'Auth token is invalid.'
		],
		'checkout' => [
			'code'  =>  1200,
			'error' => 'Payment checkout error.'
		],
		'payment_agent' => [
			'code'  =>  1201,
			'error' => 'Wrong payment agent.'
		],
		'phone-empty' => [
			'code'  =>  601,
			'error' => 'Empty phone number.'
		],
		'phone-bad-format' => [
			'code'  =>  602,
			'error' =>  'Bad phone number format.'
		],
		'phone-taken' => [
			'code'  =>  603,
			'error' =>  'The phone number is already in use.'
		],
		'sms-database' => [
			'code'  =>  611,
			'error' =>  'Error saving SMS Code to database.'
		],
		'sms-code-not-found' => [
			'code'  =>  612,
			'error' =>  'SMS Code not found.'
		],
		'sms-code-mark-used' => [
			'code'  =>  613,
			'error' =>  'Error SMS Code marking as used.'
		],
		'sms-code-empty' => [
			'code'  =>  614,
			'error' =>  'Empty SMS Code.'
		],
		'sms-code-generate' => [
			'code'  =>  621,
			'error' =>  'Cannot generate SMS Code often. Please, wait for a couple of minutes.'
		],
		'sms-send' => [
			'code'  =>  631,
			'error' =>  'An error occurred while sending message'
		],
		'user-registration' => [
			'code'  =>  641,
			'error' =>  'Error while registration of a new user.'
		],
		'user-not-found' => [
			'code'  =>  651,
			'error' =>  'User not found or not active'
		],
		'user-bad-format' => [
			'code'  =>  661,
			'error' =>  'User data has bad format'
		],
		'user-no-freeze' => [
			'code'  =>  662,
			'error' =>  'Nothing to freeze'
		],
		'qr-code' => [
			'code'  =>  670,
			'error' =>  'Error while generating QR codes.',
		],
		'qr-code-directory' => [
			'code'  =>  671,
			'error' =>  'Cannot create directory for QR codes.'
		],
		'qr-code-file' => [
			'code'  =>  672,
			'error' =>  'Cannot save QR code file.'
		],
		'qr-code-database' => [
			'code'  =>  673,
			'error' =>  'Cannot save QR code data to database.'
		],
		'qr-code-amount-empty' => [
			'code'  =>  674,
			'error' =>  'Empty amount.'
		],
		'qr-code-checkin-user-not-found' => [
			'code'  =>  675,
			'error' =>  'There is no user with the provided QR code.'
		],
		'qr-code-checkin-database-failure' => [
			'code'  =>  676,
			'error' =>  'Cannot check in the current user.'
		],
		'rooms_daily_limit' => [
			'code' => 	700,
			'error' =>  'Rooms daily limit exceeded for current user.'
		]
	];

	public static function error($key = null, $data = [])
	{
		if (!$key || !array_key_exists($key, self::$errors)){
			$error = self::$errors['default'];
		} else {
			$error = self::$errors[$key];
		}

		$error['status'] = false;
		if ($data instanceof Collection) {
			$error['data'] = $data;
		} else {
			$error['data'] = (isset($data[0]) || empty($data)) ? $data : [$data];
		}

		Log::info(json_encode($error));
		return Response::json($error)->header('Access-Control-Allow-Origin', '*')
			->header('Access-Control-Allow-Credentials', true)
			->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
			->header('Access-Control-Allow-Headers', 'Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With,auth-token, Auth-Token, api-token, Api-Token');
	}

	public static function success($data = [])
	{
		$message = self::$messages['default'];

		$message['status'] = true;
		if ($data instanceof Collection) {
			$message['data'] = $data;
		} else {
			$message['data'] = (isset($data[0]) || empty($data)) ? $data : [$data];
		}

		Log::info(json_encode($message));
		return Response::json($message)
			->header('Access-Control-Allow-Origin', '*')
			->header('Access-Control-Allow-Credentials', true)
			->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
			->header('Access-Control-Allow-Headers', 'Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With,auth-token, Auth-Token, api-token, Api-Token');
	}

	public static function view($view, $data = [])
	{
		Log::info('View: '.$view.'. With data: '.json_encode($data));
		return View::make($view, $data);
	}
}
