<?php
use Carbon\Carbon;

use Kozmo\Api\Middleware\CheckApiToken;
use Kozmo\Api\Middleware\CheckAuthToken;

use Kozmo\Api\Routes\MeRoutes;
use Kozmo\Api\Routes\CRMRoutes;
use Kozmo\Api\Routes\SportRoutes;
use Kozmo\Api\Routes\CommonRoutes;
use Kozmo\Api\Routes\PaymentRoutes;

use Illuminate\Http\Request;
use Kozmo\Api\Classes\AuthManager;
use Kozmo\Api\Classes\QrCode;

Route::group(['prefix' => '/api/v1'], function () {

    /**
     *  System
     */
    Route::any('/version', function () {
        return 'Version 1.0';
    });
    Route::any('/time', function () {
        return Carbon::now()->format('d.m.Y H:i:s');
    });

    /**
     *  Payment callbacks
     */
    Route::any('/payment/success/{agent}', function($agent) {
        return PaymentRoutes::success($agent);
    });
    Route::any('/payment/fail/{agent}', function($agent) {
        return PaymentRoutes::fail($agent);
    });
    Route::any('/payment/callback', function() {
        return PaymentRoutes::callback();
    });

    /**
     *  CRM webhooks
     */
    Route::any('/crm/deal/add', function() {
        return CRMRoutes::dealAdd();
    });
    Route::any('/crm/deal/get/{id}', function($id) {
        return CRMRoutes::dealGet($id);
    });
    Route::any('/crm/deal/updated', function() {
        return CRMRoutes::dealUpdated();
    });
    Route::any('/crm/contact/list/{phone}', function($phone) {
        return CRMRoutes::contactList($phone);
    });

    Route::middleware([CheckApiToken::class])->group(function () {
        /**
         * SMS Code
         */
        Route::post('/sms/confirm', function () {
            return AuthManager::instance()->smsConfirm();
        });
        Route::post('/sms/repeat', function () {
            return AuthManager::instance()->smsRepeat();
        });

        /**
         * Registration and authorization
         */
        Route::post('/user/signup', function () {
            return AuthManager::instance()->signUp();
        });
        Route::post('/user/signin', function () {
            return AuthManager::instance()->signIn();
        });
        /**
         *  Payment for web
         */
        Route::post('/user/checkout', function () {
            $data = AuthManager::instance()->signUp(true);
            if (!is_array($data)) {
                return $data;
            }
            return PaymentRoutes::checkout($data['user']);
        });

        /**
         * QrCode
         */
        Route::post('/qrcode/generate', function () {
            return QrCode::generate();
        });
        // Route::post('/qrcode/checkin', function () {
        //     return QrCode::checkInAndOut();
        // });
        // Route::post('/qrcode/checkout', function () {
        //     return QrCode::checkInAndOut();
        // });

        /**
         *  Content
         */
        Route::get('/splashes', function () {
            return CommonRoutes::splashes();
        });
        Route::get('/splashes/last', function () {
            return CommonRoutes::splashesLast();
        });
        Route::get('/contacts', function () {
            return CommonRoutes::contacts();
        });

        Route::get('/sport/products', function () {
            return SportRoutes::products();
        });
        Route::get('/sport/products/free', function () {
            return SportRoutes::productsFree();
        });
        Route::get('/sport/freshes', function () {
            return SportRoutes::freshes();
        });
        Route::get('/sport/rooms', function () {
            return SportRoutes::rooms();
        });
        Route::get('/sport/rooms/map', function () {
            return SportRoutes::roomsMap();
        });
        Route::get('/sport/rooms/byDate/{date}', function ($date) {
            return SportRoutes::roomsByDate($date);
        });
        Route::get('/sport/trainers', function () {
            return SportRoutes::trainers();
        });
        Route::get('/sport/trainers/byType/{type}', function ($type) {
            return SportRoutes::trainersByType($type);
        });
    });

    Route::middleware([CheckApiToken::class, CheckAuthToken::class])->group(function () {
        /**
         *  Payment
         */
        Route::post('/payment/checkout', function () {
            return PaymentRoutes::checkout();
        });

        /**
         *  Me
         */
        Route::get('/me', function () {
            return MeRoutes::me();
        });
        Route::post('/me/check', function () {
            return MeRoutes::meCheck();
        });
        Route::get('/me/fresh', function () {
            return MeRoutes::meFresh();
        });
        Route::get('/me/add/fresh/{id}', function ($id) {
            return MeRoutes::meAddFresh($id);
        });
        Route::get('/me/rooms', function () {
            return MeRoutes::meRooms();
        });
        Route::get('/me/rooms/map', function () {
            return MeRoutes::meRoomsMap();
        });
        Route::get('/me/toggle/room/{id}', function ($id) {
            return MeRoutes::meToggleRoom($id);
        });
        Route::get('/me/subs', function () {
            return MeRoutes::meSubs();
        });
        Route::get('/me/orders', function () {
            return MeRoutes::meOrders();
        });
        Route::get('/me/freeze', function () {
            return MeRoutes::meFreeze();
        });
    });
});
?>
