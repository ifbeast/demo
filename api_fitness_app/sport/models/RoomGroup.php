<?php namespace Kozmo\Sport\Models;

use Model;

/**
 * RoomGroup Model
 */
class RoomGroup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_sport_room_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
