<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddMiniImageFieldsToFreshesTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_freshes', function(Blueprint $table) {
            $table->string('image_mini')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_freshes', function(Blueprint $table) {
            $table->dropColumn('image_mini');
        });
    }
}
