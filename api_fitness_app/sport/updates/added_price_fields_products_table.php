<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTypeFieldsToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->string('type')->nullable();
            $table->integer('price')->default(0);
            $table->boolean('is_free')->default(0);
            $table->boolean('has_count')->default(0);
            $table->boolean('has_period')->default(0);
            $table->boolean('has_schedule')->default(0);
            $table->boolean('has_description')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('price');
            $table->dropColumn('is_free');
            $table->dropColumn('has_count');
            $table->dropColumn('has_period');
            $table->dropColumn('has_schedule');
            $table->dropColumn('has_description');
        });
    }
}
