<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFeaturesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_sport_features', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->boolean('is_published')->default(0);
            $table->integer('product_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_sport_features');
    }
}
