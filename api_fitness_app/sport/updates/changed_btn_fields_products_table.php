<?php namespace Kozmo\Sport\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ChangedBtnFieldsToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->renameColumn('has_description', 'btn_description');
            $table->boolean('btn_schedule')->default(0);
            $table->boolean('btn_signup')->default(0);
        });
    }

    public function down()
    {
        Schema::table('kozmo_sport_products', function(Blueprint $table) {
            $table->renameColumn('btn_description', 'has_description');
            $table->dropColumn('btn_schedule');
            $table->dropColumn('btn_signup');
        });
    }
}
