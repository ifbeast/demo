<?php namespace Kozmo\Payment\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveriesTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_payment_deliveries', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_payment_deliveries');
    }
}
