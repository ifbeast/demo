<?php namespace Kozmo\Payment\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_payment_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_model')->nullable();
            $table->string('product_type')->nullable();
            $table->string('product_title')->nullable();
            $table->integer('product_price')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_payment_orders');
    }
}
