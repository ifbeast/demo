<?php namespace Kozmo\Payment;

use Backend;
use System\Classes\PluginBase;

/**
 * Payment Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Payment',
            'description' => 'No description provided yet...',
            'author'      => 'Kozmo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'category'    => 'Payment',
                'label'       => 'Settings',
                'description' => 'Manage payment settings.',
                'icon'        => 'icon-plug',
                'class'       => 'Kozmo\Payment\Models\Settings',
                'order'       => 500,
                'keywords'    => 'api payment',
                'permissions' => ['kozmo.payment.setting']
            ]
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kozmo\Payment\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kozmo.payment.common' => [
                'tab' => 'Payment',
                'label' => 'Common permission'
            ],
            'kozmo.payment.setting' => [
                'tab' => 'Payment',
                'label' => 'Settings permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'payment' => [
                'label'       => 'Payment',
                'url'         => Backend::url('kozmo/payment/order'),
                'icon'        => 'icon-shopping-cart',
                'permissions' => ['kozmo.payment.*'],
                'order'       => 800,
                'sideMenu'    => [
                    'orders' => [
                        'label' => 'Orders',
                        'url'   => Backend::url('kozmo/payment/order'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.payment.*'],
                        'order' => 10
                    ],
                    'invoices' => [
                        'label' => 'Invoices',
                        'url'   => Backend::url('kozmo/payment/invoice'),
                        'icon'  => 'icon-circle-thin',
                        'permissions' => ['kozmo.payment.*'],
                        'order' => 20
                    ],
                    // 'deliveries' => [
                    //     'label' => 'Deliveries',
                    //     'url'   => Backend::url('kozmo/payment/delivery'),
                    //     'icon'  => 'icon-circle-thin',
                    //     'permissions' => ['kozmo.payment.*'],
                    //     'order' => 30
                    // ],
                ]
            ],
        ];
    }
}
