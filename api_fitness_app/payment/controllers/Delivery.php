<?php namespace Kozmo\Payment\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Delivery Back-end Controller
 */
class Delivery extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Payment', 'payment', 'delivery');
    }
}
