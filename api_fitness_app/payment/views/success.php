<!doctype html>
<html>
	<head>
		<title>Kozmo. Success</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900">
		<style>
			h1 {
				font-family: 'Roboto', sans-serif;
				font-weight: 200;
				font-size: 48px;
			}
		</style>
	</head>
	<body>
		<div style="display:table; width: 100%; height: 90vh;">
			<div style="display:table-cell; width: 100%; text-align: center; vertical-align: middle;">
			<h1>Спасибо</h1>
		</div>
		<script>
		setTimeout(function(){
			window.location.href = "uniwebview://success";
		}, 1000);
		</script>
	</body>
</html>
