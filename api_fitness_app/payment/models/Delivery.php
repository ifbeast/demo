<?php namespace Kozmo\Payment\Models;

use Model;

/**
 * Delivery Model
 */
class Delivery extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_payment_deliveries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
