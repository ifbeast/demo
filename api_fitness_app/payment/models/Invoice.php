<?php namespace Kozmo\Payment\Models;

use Model;
use Kozmo\Helpers\PaymentHelper;

/**
 * Invoice Model
 */
class Invoice extends Model
{
    const STATUS_FAIL = 'fail';
    const STATUS_ACCEPT = 'accept';
    const STATUS_WAITING = 'waiting';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kozmo_payment_invoices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
    *   Scopes
    */
    public function scopeWaiting($query)
    {
      return $query->where('status', self::STATUS_WAITING);
    }

    public function scopeAccept($query)
    {
      return $query->where('status', self::STATUS_ACCEPT);
    }

    public function scopeFail($query)
    {
      return $query->where('status', self::STATUS_FAIL);
    }

    public function scopeByKeys($query, $orderId, $wOrderId)
    {
        return $query->where('order_id', $orderId)
                    ->where('w_order_id', $wOrderId);
    }
    /**
    *   Events
    */
    public function beforeCreate()
    {
        $this->status = self::STATUS_WAITING;
    }

    /**
    *   Methods
    */
    public function getStatusOptions()
    {
        return [
            self::STATUS_FAIL => 'Отказан',
            self::STATUS_ACCEPT => 'Завершен',
            self::STATUS_WAITING => 'Ожидание'
        ];
    }

    public function getCurrenctOptions()
    {
        return PaymentHelper::getCurrencies();
    }

    public function setAccept()
    {
        $this->status = self::STATUS_ACCEPT;
        return $this->save();
    }

    public function setFail()
    {
        $this->status = self::STATUS_FAIL;
        return $this->save();
    }
}
