<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_model')->nullable();
            $table->string('product_title')->nullable();
            $table->string('product_type')->nullable();

            $table->boolean('is_free')->default(0);
            $table->boolean('has_fresh')->default(0);
            $table->boolean('has_count')->default(0);
            $table->boolean('has_period')->default(0);
            $table->boolean('has_schedule')->default(0);

            $table->string('is_enabled')->default(0);

            $table->integer('visits_limit')->nullable();
            $table->integer('period_limit')->nullable();

            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kozmo_personal_subscriptions');
    }
}
