<?php namespace Kozmo\Personal\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddPerPriceSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->integer('per_price')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kozmo_personal_subscriptions', function(Blueprint $table) {
            $table->dropColumn('per_price')->nullable();
        });
    }
}
