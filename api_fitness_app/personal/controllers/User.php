<?php namespace Kozmo\Personal\Controllers;

use Request;
use Redirect;
use BackendMenu;
use Kozmo\Personal\Models\User as UserModel;
use Kozmo\Common\Models\Shelf as ShelfModel;
use Kozmo\Common\Models\QrCode as QrCodeModel;
use Backend\Classes\Controller;

/**
 * User Back-end Controller
 */
class User extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Personal', 'personal', 'user');
    }

    public function onDetachQrCode()
    {
        $id = post('model');
        if (!$user = UserModel::find($id)) {
            return Redirect::refresh();
        }
        if (!$qrcode = $user->qr_code) {
            return Redirect::refresh();
        }
        $qrcode->user_id = null;
        $qrcode->save();
        return Redirect::refresh();
    }

    public function onDetachShelf()
    {
        $id = post('model');
        if (!$user = UserModel::find($id)) {
            return Redirect::refresh();
        }
        if (!$shelf = $user->shelf) {
            return Redirect::refresh();
        }
        $shelf->user_id = null;
        $shelf->save();
        return Redirect::refresh();
    }
}
