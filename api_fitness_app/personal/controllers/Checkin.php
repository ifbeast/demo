<?php namespace Kozmo\Personal\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Checkin Back-end Controller
 */
class Checkin extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kozmo.Personal', 'personal', 'checkin');
    }
}
