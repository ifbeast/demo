<?php namespace Kosmo\Billing\Traits;

use Exception;
use Carbon\Carbon;
use Kosmo\Billing\Models\Payment;

trait Payable
{
    public static function bootPayable()
    {
        static::extend(function($model) {
            if (!defined('static::STATUS_SUCCESS')) {
                throw new Exception(sprintf(
                    'You must implemetns IPayable interface property in %s to use the Payable trait.', get_called_class()
                ));
            }
            if (!property_exists(get_called_class(), 'paid_at')) {
                throw new Exception(sprintf(
                    'You must define a $paid_at property in %s to use the Payable trait.', get_called_class()
                ));
            }
            if (!in_array('paid_at', $model->dates)) {
                $model->dates[] = 'paid_at';
            }
            $model->bindEvent('model.beforeSave', function() use ($model) {
                $model->payableBeforeSave();
            });
        });
    }

    public function getPaymentAttribute($value = null)
    {
        return Payment::where('order_id', $this->id)
               ->where('order_model', '\\'.__class__)
               ->first();
    }

    public function payableBeforeSave()
    {
        if (empty($this->status)) {
            $this->status = static::STATUS_ACCEPT;
        }
        if ($this->status == static::STATUS_SUCCESS || $this->status == static::STATUS_CONFIRM) {
            if (empty($this->paid_at)) {
                $this->paid_at = Carbon::now();
            }
        }
    }

    public function getStatusOptions($keyValue = null)
    {
        return [
            static::STATUS_ACCEPT       => 'Ожидает оплаты',
            static::STATUS_SUCCESS      => 'Оплачен',
            static::STATUS_CONFIRM      => 'Подтвержден',
            static::STATUS_FAIL         => 'Ошибка',
            static::STATUS_EXPIRED      => 'Просрочен',
            static::STATUS_REFUND       => 'Возвращен',
            static::STATUS_REFUND_PART  => 'Возвращен частично',
            static::STATUS_INVALID      => 'Оформлен не верно',
        ];
    }

    public function getStatusName($status = null)
    {
        if (!$status) {
            $status = $this->status;
        }
        $list = $this->getStatusOptions();
        return isset($list[$status])? $list[$status] : $list[static::STATUS_INVALID];
    }

    public function isExpired()
    {
        if ($this->status == static::STATUS_ACCEPT)
            return $this->created_at->timestamp + static::EXPIRE_PERIOD < time();
        return false;
    }

    public static function canPay($id = null)
    {
        if (!empty($id)) {
            if (!$order = static::find($id)) {
                return;
            }
            return $order->isPayable();
        }
        return true;
    }

    public function isPayable()
    {
        if ($this->status != static::STATUS_ACCEPT) {
            return;
        }
        if ($this->isExpired()) {
            $this->status = static::STATUS_EXPIRED;
            $this->save();
            return;
        }
        return true;
    }

    public function setFail()
    {
        $this->status = static::STATUS_FAIL;
        $this->save();
    }

    public function setSuccess()
    {
        $this->status = static::STATUS_SUCCESS;
        $this->save();
    }

    public function setExpired()
    {
        $this->status = static::STATUS_EXPIRED;
        $this->save();
    }

    public function setConfirm()
    {
        $this->status = static::STATUS_CONFIRM;
        $this->save();
    }

    public function setRefund()
    {
        $this->status = static::STATUS_REFUND;
        $this->save();
    }

    public function scopeStatusSuccess($query)
    {
        return $query->where('status', static::STATUS_SUCCESS);
    }
}
