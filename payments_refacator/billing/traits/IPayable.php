<?php namespace Kosmo\Billing\Traits;

interface IPayable
{
    const STATUS_FAIL           = 0;
    const STATUS_REFUND         = 50;
    const STATUS_REFUND_PART    = 55;
    const STATUS_INVALID        = 60;
    const STATUS_EXPIRED        = 70;
    const STATUS_ACCEPT         = 100;
    const STATUS_SUCCESS        = 200;
    const STATUS_CONFIRM        = 300;
    const EXPIRE_PERIOD         = 259200; // 3 days
}
