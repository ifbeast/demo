<?php namespace Kosmo\Billing\Console;

use Log;
use Illuminate\Console\Command;
use Kosmo\Loyalty\Models\Order;
use Kosmo\Billing\Classes\PaymentKKB;
use Kosmo\Billing\Classes\GatewayManager;
use Symfony\Component\Console\Input\InputOption;

class Payment extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'billing:payment';

    /**
     * @var string The console command description.
     */
    protected $description = 'Billing confirmation.';

    /**
     * Execute the console command. 'billing:payment --order_id={id}'
     * @return void
     */
    public function fire()
    {
        if (!$auto = config('billing.methods.kkb.loyalty.auto')) {
            return;
        }

        if ($order_id = $this->option('order_id')) {
            Log::info('Billing command is running for order #'.$order_id);
            $this->info('Billing command is running for order #'.$order_id);
            try {
                PaymentKKB::process($order_id);
            } catch (\Throwable $t) {
                Log::info('ERROR: Cannot execute the auto payment task.');
                $this->info('ERROR: Cannot execute the auto payment task.');
            }
            Log::info('Order #'.$order_id.' is checked.');
            $this->info('Order #'.$order_id.' is checked.');
        } else {
            $orders = Order::statusSuccess()->get();
            Log::info('Billing command is running for orders '.count($orders));
            $this->info('Billing command is running for orders '.count($orders));
            $count = 0;
            foreach ($orders as $order) {
                try {
                    PaymentKKB::process($order->id);
                    $count = $count + 1;
                } catch (\Throwable $t) {
                    Log::info('ERROR: Cannot execute the auto payment task.');
                    $this->info('ERROR: Cannot execute the auto payment task.');
                }
            }
            Log::info($count.' from '.count($orders).' orders are checked.');
            $this->info($count.' from '.count($orders).' orders are checked.');
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            array('order_id', null, InputOption::VALUE_REQUIRED, 'Order ID of a purchase', null)
        ];
    }
}
