<?php namespace Kosmo\Billing\Models;

use Kosmo\Export\Classes\ExportModel;

class PaymentExport extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $users = Payment::all();
        $users->each(function($user) use ($columns) {
            $user->addVisible($columns);
        });
        return $users->toArray();
    }
}
?>
