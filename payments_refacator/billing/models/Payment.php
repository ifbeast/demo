<?php namespace Kosmo\Billing\Models;

use Model;
use ReflectionMethod;

/**
 * Payment Model
 */
class Payment extends Model implements \Kosmo\Billing\Traits\IPayable
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_billing_payments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['results_timestamp'];

    /**
     * @var array Relations
     */
    public $belongsTo = [];

    public function getOrderAttribute($value)
    {
        if(!$this->order_model || !$this->order_id) {
            return null;
        }
        if (!class_exists($this->order_model)) {
            return null;
        }
        if (!$order = call_user_func_array($this->order_model.'::find', [$this->order_id])) {
            return null;
        }
        return $order;
    }

    public function getStatusOptions($keyValue = null)
    {
        return [
            static::STATUS_ACCEPT       => 'Ожидает оплаты',
            static::STATUS_SUCCESS      => 'Оплачен',
            static::STATUS_CONFIRM      => 'Подтвержден',
            static::STATUS_FAIL         => 'Ошибка',
            static::STATUS_EXPIRED      => 'Просрочен',
            static::STATUS_REFUND       => 'Возвращен',
            static::STATUS_REFUND_PART  => 'Возвращен частично',
            static::STATUS_INVALID      => 'Оформлен не верно',
        ];
    }

    public function getStatusName($status)
    {
        return $this->getStatusOptions()[$status];
    }

    public function beforeSave()
    {
        if ($order = $this->order) {
            if ($this->status == self::STATUS_SUCCESS || $this->status == self::STATUS_CONFIRM) {
                if (empty($this->payment_amount) || empty($this->results_timestamp)) {
                    return false;
                }
                if (!empty($this->payment_reference)) {
                    $order->payment_reason = $this->payment_reference;
                }
                $order->paid_at = $this->results_timestamp;
            }
            $order->status = $this->status;
            $order->save();
        }
    }

    public function setFail()
    {
        $this->status = static::STATUS_FAIL;
        $this->save();
    }

    public function setSuccess()
    {
        $this->status = static::STATUS_SUCCESS;
        $this->save();
    }

    public function setExpired()
    {
        $this->status = static::STATUS_EXPIRED;
        $this->save();
    }

    public function setConfirm()
    {
        $this->status = static::STATUS_CONFIRM;
        $this->save();
    }

    public function setRefund()
    {
        $this->status = static::STATUS_REFUND;
        $this->save();
    }
}
