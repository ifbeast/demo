<?php namespace Kosmo\Billing\Classes;

use Mail;

class GatewayBase
{
    public $manager    = null;
    public $methodName = 'default';
    public $keyName    = 'default';
    public $configName = null;
    public $orderClass = null;
    public $settingsClass = null;

    public function __construct($keyName = null)
    {
        $this->manager = GatewayManager::instance();
        if ($keyName && config('billing.methods.'.$this->methodName.'.'.$keyName)) {
            $this->keyName = $keyName;
        }
        $this->configName = 'billing.methods.'.$this->methodName.'.'.$this->keyName;
        $this->orderClass = config($this->configName.'.order');
        $this->settingsClass = config($this->configName.'.settings');
        $this->initConfig($keyName);
    }

    public function initConfig($keyName = null)
    {

    }

    public function getVars($orderId = null)
    {
        return [];
    }

    public function getOrder($orderId = null)
    {
        if (!$orderId) {
            return;
        }
        if (!class_exists($this->orderClass)) {
            return;
        }
        if (!$order = call_user_func_array($this->orderClass.'::find', [$orderId])) {
            return;
        }
        return $order;
    }

    public function getSettings($key = null)
    {
        if (!$key) {
            return;
        }
        if (!class_exists($this->settingsClass)) {
            return;
        }
        if (!$value = call_user_func_array($this->settingsClass.'::get', [$key])) {
            return;
        }
        return $value;
    }

    public function callback($data = null)
    {
        return;
    }

    public function confirm($data = null)
    {
        return;
    }

    public function email($email = '', $name = '', $data = [])
    {
        if ($email) {
            $this->paymentEmail($email, $name, $data);
        }
        $this->reportEmail($data);
    }

    protected function paymentEmail($email, $name, $data)
    {
        if ($template = $this->getSettings('payment_template')) {
            try{
                Mail::send($template, $data, function($message) use($email, $name) {
                    $message->to($email, $name);
                });
            }catch(\Swift_TransportException $e){
                Log::debug('Payment mail send swift error');
            }catch(\Exception $e){
                Log::debug('Payment mail send error');
            }
        }
    }

    protected function reportEmail($data)
    {
        if($template = $this->getSettings('payment_report_template')) {
            if ($to = $this->getSettings('email')) {
                $copy = $this->getSettings('copy') ? $this->getSettings('copy') : null;
                try{
                    Mail::send($template, $data, function($message) use($to, $copy) {
                        $message->to($to, 'Delivery');
                        if ($copy) {
                            $message->bcc($copy, 'Manager');
                        }
                    });
                }catch(\Swift_TransportException $e){
                    Log::debug('Payment report mail send swift error');
                }catch(\Exception $e){
                    Log::debug('Payment report mail send error');
                }
            }
        }
    }

}
