<?php namespace Kosmo\Billing\Classes;

use Redirect;

class GatewayManager
{
    use \October\Rain\Support\Traits\Singleton;

    public function getFormUrl($page = null, $method = null, $orderId = null)
    {
        $url = '';
        $url = (!$page) ? $url : $url.'/'.$page;
        $url = (!$method) ? $url : $url.'/'.urlencode($method);
        $url = (!$orderId) ? $url : $url.'/'.urlencode($orderId);
        return $url;
    }

    public function getCallbackUrl($method = null, $key = null)
    {
        $url = config('billing.callback');
        $url = (!$method) ? $url : $url.'/'.urlencode($method);
        $url = (!$key) ? $url : $url.'/'.urlencode($key);
        return 'http://'.$_SERVER['HTTP_HOST'].$url;
    }

    public function getCallbackRouter()
    {
        return function ($method = null, $key = null)
        {
            if (!$method || !$key) {
                return;
            }
            if (!$gateway = $this->getGetway($method, $key)) {
                return;
            }
            $data = get();
            return $gateway->callback($data);
        };
    }

    public function getCallbackRoute()
    {
        $url = config('billing.callback');
        return $url.'/{method}/{key}';
    }

    public function getGetway($method = null, $key = null )
    {
        if(!$method || !$key) {
            return;
        }
        $methodClass = 'Kosmo\Billing\Classes\Gateway'.ucwords($method);
        if (!class_exists($methodClass)) {
            $methodClass = 'Kosmo\Billing\Classes\Gateway'.strtoupper($method);
            if (!class_exists($methodClass)) {
                return;
            }
        }
        return new $methodClass($key);
    }


}
