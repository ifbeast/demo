<?php namespace Kosmo\Billing\Classes;

use Log;
use Response;
use Kosmo\Billing\Models\Payment;

class GatewayCash extends GatewayBase
{
    public $methodName = 'cash';

    public function getVars($orderId = null)
    {
        $url = $this->getCallbackUrl($orderId);
        return [
            'url' => $url
        ];
    }

    public function getCallbackUrl($orderId)
    {
        $url = $this->manager->getCallbackUrl($this->methodName, $this->keyName);
        $url = $url.'?order='.urlencode($orderId);
        return $url;
    }

    public function callback($data = null)
    {
        if (!array_key_exists('order', $data)) {
            return Response::view('kosmo.billing::confirmationFail',['logo'=> config('client.logo')])->header('Content-Type', 'text/html; charset=UTF-8');
        } else {
            if (!$order = $this->getOrder($data['order'])) {
                return Response::view('kosmo.billing::confirmationFail',['logo'=> config('client.logo')])->header('Content-Type', 'text/html; charset=UTF-8');
            }
        }
        if (!$payment = $order->payment) {
            $payment = new Payment;
            if ($client = $order->client) {
                $payment->customer_mail  = $client->email;
                $payment->customer_name  = $client->fullname;
                $payment->customer_phone = $client->phone;
            }
            $payment->order_amount      = $order->amount;
            $payment->order_currency    = 'KZT';
            $payment->order_id          = $order->id;
            $payment->order_model       = $this->orderClass;
            $payment->payment_method    = $this->methodName;
            $payment->status            = Payment::STATUS_ACCEPT;
            $payment->save();
            $this->email($payment->customer_mail, $payment->customer_name, ['order' => $order, 'payment' => $payment]);
        }
        return Response::view('kosmo.billing::confirmationSuccess',['logo'=> config('client.logo')])->header('Content-Type', 'text/html; charset=UTF-8');
    }
}
