<?php namespace Kosmo\Billing\Classes;

use Log;
use Response;
use Kosmo\Billing\Models\Payment;
use Kosmo\Billing\Libs\Epay\KKB;
use Kosmo\Billing\Libs\Epay\KKBSign;
use Kosmo\Billing\Libs\Epay\KKBHelper;

class GatewayKKB extends GatewayBase
{
    public $methodName = 'kkb';
    public $configPath = null;
    public $autoCheck  = false;

    public function initConfig($keyName = null)
    {
        if (!$this->configName) {
            return;
        }
        $this->configPath = config('billing.test')
                          ? config('billing.methods.'.$this->methodName.'.testConfig')
                          : config($this->configName.'.config');
        $this->autoCheck  = config($this->configName.'.auto');
    }

    public function getVars($orderId = null)
    {
        if (!$orderId) {
            return;
        }
        $sign = $this->getSign($orderId);
        $url  = $this->getRedirectUrl('jsp/process/login.jsp');
        $urls = $this->getCallbackUrls($orderId);
        return [
            'sign' => $sign,
            'url'  => $url,
            'urls' => $urls
        ];
    }

    public function getRedirectUrl($custom = null)
    {
        $url = config('billing.test')
             ? config('billing.methods.'.$this->methodName.'.testUrl')
             : config('billing.methods.'.$this->methodName.'.redirectUrl');

        if ($custom) {
            $url = $url.'/'.$custom;
        }
        return $url;
    }

    public function getCallbackUrls($orderId)
    {
        return [
            'postback' => $this->getCallbackUrl($orderId, 'postback'),
    		'success'  => $this->getCallbackUrl($orderId, 'success'),
    		'failure'  => $this->getCallbackUrl($orderId, 'failure'),
        ];
    }

    public function getCallbackUrl($orderId, $action)
    {
        $url = $this->manager->getCallbackUrl($this->methodName, $this->keyName);
        $url = $url.'/?action='.urlencode($action);
        if ($orderId) {
            $url = $url.'&order='.urlencode($orderId);
        }
        return $url;
    }

    public function getSign($orderId)
    {
        if (!$order = $this->getOrder($orderId)) {
            return;
        }
        return KKB::process_request($order->id, 398, $order->total, $this->configPath);
    }

    public function callback($data = null)
    {
        $action = 'default';
        if (array_key_exists('action', $data)) {
            $action = $data['action'];
        }
        $actionMethod = 'action'.ucwords($action);
        if (!method_exists($this, $actionMethod)) {
            return;
        }
        return call_user_func([$this, $actionMethod], $data);
    }

    protected function actionDefault($data)
    {
        return $this->actionPostback($data);
    }

    protected function actionPostback($data)
    {
        if (!array_key_exists('order', $data)) {
            die('EMPTY');
        }
        if(!$order = $this->getOrder($data['order'])) {
            die('EMPTY');
        }
        $response = post('response', $default = null);
	    if (empty($response)) {
            die('EMPTY');
        }
        $result = KKB::process_response(stripslashes($response), $this->configPath);
        Log::debug('KKB Response Converted: '.print_r($result, 1));
        if (is_array($result)) {
            if (in_array("DOCUMENT", $result)) {
                $condition = config('billing.test') ? true : strpos($result['CHECKRESULT'], 'SIGN_GOOD') !== false;
                if ($condition) {
                    $payment                 = new Payment;
                    $payment                 = KKBHelper::fill($payment, $result);
                    $payment->order_model    = $this->orderClass;
                    $payment->payment_method = $this->methodName;
                    $payment->status         = Payment::STATUS_SUCCESS;

                    if (!$payment->save()) {
                        Log::debug('KKB Response result: CANTSAVE');
                        die('CANTSAVE');
                    }
                    $order = $payment->order;
                    $order->payment_reason = $payment->payment_reference;
                    $order->setSuccess();

                    $this->email($payment->customer_mail, $payment->customer_name, ['order' => $order, 'payment' => $payment]);
                    Log::debug('KKB Response result: OK');
                    echo '0';
                    die();
                }
                Log::debug('KKB Response result: NOSIGN');
                die('NOSIGN');
            }
            Log::debug('KKB Response result: NODOC');
            die('NODOC');
        }
        Log::debug('KKB Response result: EMPTY');
        die('EMPTY');
    }

    protected function actionSuccess($data)
    {
        return Response::view('kosmo.billing::paymentSuccess',['logo'=> config('client.logo')])->header('Content-Type', 'text/html; charset=UTF-8');
    }

    protected function actionFailure($data)
    {
        if (array_key_exists('order', $data)) {
            if($order = $this->getOrder($data['order'])) {
                $order->setFail();
            }
        }
		return Response::view('kosmo.billing::paymentFail',['logo'=> config('client.logo')])->header('Content-Type', 'text/html; charset=UTF-8');
    }
}
