<?php namespace Kosmo\Billing\Classes;

use Kosmo\Billing\Libs\Epay\kkbsignclass;
use Kosmo\Loyalty\Models\Order;
use Log;

class PaymentKKB
{
    public static $merchant_id;
    public static $merchant_name;
    public static $merchant_certificate_id;
    public static $merchant_sign_type;

    public static $order_id;

    public static $command_type;
    public static $reference;
    public static $approval_code;
    public static $amount;
    public static $currency_code;
    public static $reason;

    private static $private_key;
    private static $private_key_password;
    private static $public_key;

    public static $xml_template;
    public static $xml_command_template;
    public static $xml_check_order_status_template;
    public static $xml_command_order_template;

    public static $operation_type;

    public static $config = null;
    public static $gateway;

    public static $methodName = 'kkb';
    public static $keyName = 'keyName';

    public static $url;
    public static $check_link = 'jsp/remote/check.jsp?';
    public static $command_link = 'jsp/remote/command.jsp?';
    public static $xml_file;
    public static $xml_string;

    public static $status_response;
    public static $status_response_message;

    public static $complete_response;
    public static $complete_response_message;

    /**
     * Setup necessary configurations and define variables
     *
     * @return void
     */
    public static function setup()
    {
        self::beforeSetup();

        self::setupConfig();

        self::setupUrl();

        self::afterSetup();
    }

    /**
     * Actions that perform before setupConfig()
     *
     * @return void
     */
    public static function beforeSetup()
    {

    }

    /**
     * Actions that perform after setupConfig()
     *
     * @return void
     */
    public static function afterSetup()
    {
        self::checkIfPropsNotNull();
    }

    /**
     * Checks properties for null value
     *
     * @throws \Exception
     * @return void
     */
    public static function checkIfPropsNotNull()
    {
        /**
         * Let's prepare an array for each operation type
         */
        switch (self::$operation_type) {
            case 'complete' :
                $arr = [
                    self::$order_id,
                    self::$merchant_id,
                    self::$command_type,
                    self::$reference,
                    self::$approval_code,
                    self::$amount,
                    self::$currency_code,
                ];
                break;

            case 'refund' :
                $arr = [
                    self::$order_id,
                    self::$merchant_id,
                    self::$command_type,
                    self::$reference,
                    self::$approval_code,
                    self::$amount,
                    self::$currency_code,
                ];
                break;

            case 'reverse' :
                $arr = [
                    self::$order_id,
                    self::$merchant_id,
                    self::$command_type,
                    self::$reference,
                    self::$approval_code,
                    self::$amount,
                    self::$currency_code,
                    self::$reason
                ];
                break;

            default :
                $arr = [
                    self::$order_id,
                    self::$merchant_id
                ];
                break;
        }

        /**
         * Check array for null value
         */
        $result = self::checkIfNotNull($arr);

        /**
         * If at least one property is null,
         * throw an exception
         */
        if (!$result) {
            Log::debug('ERROR: Properties are not set.');
            throw new \Exception('ERROR: Properties are not set.', 500);
        }
    }

    /**
     * Checks items of a provided array for null value
     *
     * @param array $arr
     * @return bool
     */
    public static function checkIfNotNull(Array $arr)
    {
        foreach ($arr as $item) {
            if ($item === null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Setup visiting url
     *
     * @return void
     */
    public static function setupUrl()
    {
        switch (self::$operation_type) {
            case 'complete' :
                $url = self::$command_link;
                self::$command_type = 'complete';
                self::$xml_file = self::$config['XML_COMMAND_ORDER_TEMPLATE_FN'];
                break;

            case 'reverse' :
                $url = self::$command_link;
                self::$command_link = 'reverse';
                self::$xml_file = self::$config['XML_COMMAND_ORDER_TEMPLATE_FN'];
                break;

            case 'refund' :
                $url = self::$command_link;
                self::$command_type = 'refund';
                self::$xml_file = self::$config['XML_COMMAND_ORDER_TEMPLATE_FN'];
                break;

            default :
                $url = self::$check_link;
                self::$xml_file = self::$config['XML_CHECK_ORDER_STATUS_TEMPLATE_FN'];
                break;
        }

        self::setupXml();

        self::$url = self::$gateway->getRedirectUrl() . '/' . $url . self::$xml_string;
    }

    /**
     * Get visiting url
     *
     * @return mixed
     */
    public static function getUrl()
    {
        return self::$url;
    }

    /**
     * Setup main configurations and properties
     *
     * @throws \Exception
     * @return void
     */
    public static function setupConfig()
    {
        self::$gateway                          = GatewayManager::instance()->getGetway(self::$methodName, self::$keyName);
        self::$config                           = (self::$gateway->configPath ? parse_ini_file(self::$gateway->configPath) : null);

        if (self::$config === null) {
            Log::debug('Cannot load config file while setting up PaymentKKB.');
            throw new \Exception('ERROR: Cannot load config file.', 500);
        }

        self::$merchant_id                      = self::$config['MERCHANT_ID'];
        self::$merchant_name                    = self::$config['MERCHANT_NAME'];
        self::$merchant_certificate_id          = self::$config['MERCHANT_CERTIFICATE_ID'];
        self::$merchant_sign_type               = 'RSA';

        self::$private_key                      = self::$config['PRIVATE_KEY_FN'];
        self::$private_key_password             = self::$config['PRIVATE_KEY_PASS'];
        self::$public_key                       = self::$config['PUBLIC_KEY_FN'];

        self::$xml_template                     = self::$config['XML_TEMPLATE_FN'];
        self::$xml_command_template             = self::$config['XML_COMMAND_TEMPLATE_FN'];
        self::$xml_check_order_status_template  = self::$config['XML_CHECK_ORDER_STATUS_TEMPLATE_FN'];
        self::$xml_command_order_template       = self::$config['XML_COMMAND_ORDER_TEMPLATE_FN'];
    }

    /**
     * Setup XML template
     *
     * @throws \Exception
     * @return void
     */
    public static function setupXml()
    {
        if (! is_file(self::$xml_file)) {
            Log::debug('Cannot find the XML file to parse.');
            throw new \Exception('ERROR: Cannot find the XML file to parse.', 500);
        }

        $merchant = file_get_contents(self::$xml_file);

        $merchant = preg_replace('/\[MERCHANT_ID\]/',    self::$merchant_id,     $merchant);
        $merchant = preg_replace('/\[ORDER_ID\]/',       self::$order_id,        $merchant);
        $merchant = preg_replace('/\[COMMAND_TYPE\]/',   self::$command_type,    $merchant);
        $merchant = preg_replace('/\[REFERENCE\]/',      self::$reference,       $merchant);
        $merchant = preg_replace('/\[APPROVAL_CODE\]/',  self::$approval_code,   $merchant);
        $merchant = preg_replace('/\[AMOUNT\]/',         self::$amount,          $merchant);
        $merchant = preg_replace('/\[CURRENCY_CODE\]/',  self::$currency_code,   $merchant);
        $merchant = preg_replace('/\[REASON\]/',         self::$reason,          $merchant);

        $kkb = new kkbsignclass();
        $kkb->invert();
        $kkb->load_private_key(self::$config['PRIVATE_KEY_FN'], self::$config['PRIVATE_KEY_PASS']);

        $merchant_sign = '<merchant_sign type="'. self::$merchant_sign_type .'" cert_id="'. self::$merchant_certificate_id .'">';
        $merchant_sign .= $kkb->sign64($merchant);
        $merchant_sign .= '</merchant_sign>';

        $xml = '<document>'.$merchant.$merchant_sign.'</document>';

        $xml = urlencode($xml);

        self::$xml_string = $xml;
    }

    /**
     * Visit url and get XML response
     *
     * @return array|mixed
     * @throws \Exception
     */
    public static function curl($complete = null)
    {
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, self::$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output_raw = curl_exec($ch);
            curl_close($ch);

            if (self::$operation_type == 'complete' || $complete == 'complete') {
                $output = self::parseCompleteCurlResult($output_raw);
            } else {
                $output = self::parseStatusCurlResult($output_raw);
            }

        } catch (\Throwable $t) {
            Log::debug('ERROR: Cannot curl the link. '. $t->getMessage());
            throw new \Exception('ERROR: Cannot curl the link. ' . $t->getMessage(), 500);
        }

        return $output;
    }

    /**
     * Parse curl result for status check
     * and save it into an array
     *
     * @param String $xml
     * @return array
     * @throws \Exception
     */
    public static function parseStatusCurlResult($xml)
    {
        if (!$xml = simplexml_load_string($xml)) {
            Log::debug('ERROR: Cannot parse XML string.');
            throw new \Exception('ERROR: Cannot parse XML string.', 500);
        }

        $data = array();

        $bank = $xml->bank;
        $order = $bank->merchant->order->attributes();
        $response = $bank->response->attributes();

        $data['order_id']               = $order->id;
        $data['response_payment']       = $response->payment;
        $data['response_status']        = $response->status;
        $data['response_result']        = $response->result;
        $data['response_amount']        = $response->amount;
        $data['response_currencycode']  = $response->currencycode;
        $data['response_timestamp']     = $response->timestamp;
        $data['response_reference']     = $response->reference;
        $data['response_cardhash']      = $response->cardhash;
        $data['response_card_to']       = $response->card_to;
        $data['response_approval_code'] = $response->approval_code;
        $data['response_msg']           = $response->msg;
        $data['response_secure']        = $response->secure;
        $data['response_card_bin']      = $response->card_bin;
        $data['response_payername']     = $response->payername;
        $data['response_payermail']     = $response->payermail;
        $data['response_payerphone']    = $response->payerphone;
        $data['response_c_hash']        = $response->c_hash;
        $data['response_recur']         = $response->recur;
        $data['response_OrderID']       = $response->OrderID;
        $data['response_SessionID']     = $response->SessionID;

        self::defineStatusResponseMessage([
            'payment' => $response->payment,
            'status' => $response->status,
            'result' => $response->result,
            'msg' => $response->msg,
        ]);

        $data['status_response_message'] = self::$status_response_message;

        return $data;
    }

    /**
     * Parse curl result for complete payment
     * and save it into an array
     *
     * @param String $xml
     * @return array
     * @throws \Exception
     */
    public static function parseCompleteCurlResult($xml)
    {
        if (!$xml = simplexml_load_string($xml)) {
            Log::debug('ERROR: Cannot parse XML string.');
            throw new \Exception('ERROR: Cannot parse XML string.', 500);
        }

        $data = array();

        $bank = $xml->bank;
        $merchant = $bank->merchant;
        $payment = $merchant->payment->attributes();
        $response = $bank->response->attributes();

        $data['command_type']               = $merchant->command->attributes()->type;

        $data['payment_reference']          = $payment->reference;
        $data['payment_approval_code']      = $payment->approval_code;
        $data['payment_orderid']            = $payment->orderid;
        $data['payment_amount']             = $payment->amount;
        $data['payment_currency_code']      = $payment->currency_code;

        $data['reason']                     = $merchant->reason;

        $data['response_code']              = $response->code;
        $data['response_message']           = $response->message;
        $data['response_remaining_amount']  = $response->remaining_amount;

        return $data;
    }

    /**
     * Define status message by combination of response codes
     * made up from payment, status, and result attributes
     *
     * @param $status
     * @return string
     */
    public static function defineStatusResponseMessage($status)
    {
        switch ($status) {
            case ($status['msg'] == 'refund') :
                $definition = 'ORDER CANCELLED';
                break;
            case ($status['payment'] == 'false' && $status['status'] == '2') :
                $definition = 'ORDER CANCELLED';
                break;

            case ($status['payment'] == 'true' && $status['status'] == '2' && $status['result'] == '0') :
                $definition = 'SUCCESSFULLY COMPLETED';
                break;

            case ($status['payment'] == 'true' && $status['status'] == '0' && $status['result'] == '0') :
                $definition = 'NEW ORDER IS AWAITING FOR COMPLETE';
                break;

            case ($status['payment'] == 'false' && $status['status'] == 'X' && $status['result'] == 'X') :
                $definition = 'NON-STANDARD CASE';
                break;

            case ($status['payment'] == 'false' && $status['status'] == '7' && $status['result'] == '7') :
                $definition = 'REQUEST NOT REACHED / ORDER NOT FOUND';
                break;

            case ($status['payment'] == 'false' && $status['status'] == '8' && $status['result'] == '8') :
                $definition = 'REQUEST REACHED, BUT NO PAYMENT WAS DONE';
                break;

            case ($status['payment'] == 'false' && $status['status'] == '9' && $status['result'] == '9') :
                $definition = 'SYSTEM ERROR';
                break;

            default:
                $definition = 'ERROR: Definition of status response message could not be accomplished. Combinations are out of scope. ';
                $definition .= '[payment=' . $status['payment'] . ', status=' . $status['status'] . ', result=' . $status['result'] . ']';
        }

        self::$status_response_message = $definition;

        return $definition;
    }

    /**
     * Check order status
     *
     * @param int|null $order_id
     * @return array|mixed
     */
    public static function status($order_id = null)
    {
        self::$order_id = $order_id;

        self::setup();

        $data = self::curl('status');

        self::$status_response  = $data;

        self::$order_id         = $data['order_id'];
        self::$reference        = $data['response_reference'];
        self::$approval_code    = $data['response_approval_code'];
        self::$amount           = $data['response_amount'];
        self::$currency_code    = $data['response_currencycode'];
        // next process
        $data['command_type']   = 'complete';

        return $data;
    }

    /**
     * Complete payment
     *
     * @param array $args
     * @return bool
     * @throws \Exception
     */
    public static function complete($args = [
        'command_type'                => 'complete',
        'order_id'                    => null,
        'response_amount'             => null,
        'response_currencycode'       => null,
        'response_approval_code'      => null,
        'refereresponse_referencence' => null,
    ])
    {
        self::$command_type     = $args['command_type'];
        self::$reference        = $args['response_reference'];
        self::$approval_code    = $args['response_approval_code'];
        self::$order_id         = $args['order_id'];
        self::$amount           = $args['response_amount'];
        self::$currency_code    = $args['response_currencycode'];

        self::$operation_type   = 'complete';
        self::setup();

        $data = self::curl('complete');

        if ($data['response_code'] != '00') {
            Log::debug('ERROR: Payment complete is not accomplished. Reason: ' . $data['response_message']);
            throw new \Exception('ERROR: Payment complete is not accomplished. Reason: ' . $data['response_message'], 500);
        }

        return true;
    }

    /**
     * Check order status and complete payment
     *
     * @param int|null $order_id
     * @return bool
     * @throws \Exception
     */
    public static function process($order_id = null)
    {
        $status_data = self::status($order_id);

        if ($status_data['status_response_message'] == 'SUCCESSFULLY COMPLETED') {
            self::updateOrder($order_id);
            return true;
        }

        if ($status_data['status_response_message'] == 'ORDER CANCELLED') {
            self::cancelOrder($order_id);
            return true;
        }

        if ($status_data['status_response_message'] == 'NEW ORDER IS AWAITING FOR COMPLETE') {
            // Log::debug('ERROR: Order cannot be autocompleted. Status is not compatible.');
            // throw new \Exception('ERROR: Order cannot be autocompleted. Status is not compatible.', 500);
            if (self::complete($status_data)) {
                self::updateOrder($order_id);
                return true;
            }
        }
        Log::info('Order #'.$order_id.' status is "'.$status_data['status_response_message'].'"');
        return false;
    }

    /**
     * Update order table
     *
     * @param int $order_id
     * @return bool
     * @throws \Exception
     */
    public static function updateOrder($order_id)
    {
        try {

            if ($order = Order::find($order_id)) {
                if ($payment = $order->payment) {
                    $payment->setConfirm();
                }
                // $order->setConfirm();
            }
            return true;

        } catch (\Throwable $t) {
            Log::debug('ERROR: Cannot update order status.');
            throw new \Exception('ERROR: Cannot update order status.', 500);
        }
    }

    /**
     * Cancel order table
     *
     * @param int $order_id
     * @return bool
     * @throws \Exception
     */
    public static function cancelOrder($order_id)
    {
        try {

            if ($order = Order::find($order_id)) {
                if ($payment = $order->payment) {
                    $payment->setRefund();
                }
                // $order->setRefund();
            }
            return true;

        } catch (\Throwable $t) {
            Log::debug('ERROR: Cannot update order status.');
            throw new \Exception('ERROR: Cannot update order status.', 500);
        }
    }
}
