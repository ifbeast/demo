<?php namespace Kosmo\Billing\Components;

use Redirect;
use Kosmo\Billing\Classes\GatewayManager;
use Cms\Classes\ComponentBase;

class PaymentForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'PaymentForm Component',
            'description' => 'Render payment form'
        ];
    }

    public function defineProperties()
    {
        return [
            'method' => [
                'title'       => 'Payment method',
                'description' => 'The payment method.',
                'default'     => '{{ :method }}',
                'type'        => 'string'
            ],
            'key' => [
                'title'       => 'Config key',
                'description' => 'Config key.',
                'default'     => 'default',
                'type'        => 'string'
            ],
            'orderId' => [
                'title'       => 'Order ID',
                'description' => 'The URL route parameter used for looking up the order by its identifier.',
                'default'     => '{{ :orderId }}',
                'type'        => 'string'
            ],
        ];
    }

    public function onRun()
    {
        if (!$order = $this->getOrder()) {
            return Redirect::to('/');
        }
        if (!$order->isPayable()) {
            return Redirect::to('/');
        }
        $this->page['order'] = $order;
    }

    public function onRender()
    {
        $this->page['method']  = $this->property('method');
        $this->page['gateway'] = $this->getGetwayVars();
    }

    private function getOrder()
    {
        $method  = $this->property('method');
        $key     = $this->property('key');
        $orderId = $this->property('orderId');
        $gateway = GatewayManager::instance()->getGetway($method, $key);
        return $gateway->getOrder($orderId);
    }

    private function getGetwayVars()
    {
        $method  = $this->property('method');
        $key     = $this->property('key');
        $orderId = $this->property('orderId');
        $gateway = GatewayManager::instance()->getGetway($method, $key);
        return $gateway->getVars($orderId);
    }
}
