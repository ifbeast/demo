<?php namespace Kosmo\Billing\Components;

use Log;
use Mail;
use ReflectionClass;
use ReflectionMethod;
use Cms\Classes\ComponentBase;
use Kosmo\Content\Models\Package;
use Kosmo\Billing\Models\Payment;
use Kosmo\Loyalty\Models\Settings;
use Kosmo\Billing\Libs\Epay\KKB;
use Kosmo\Billing\Libs\Epay\KKBSign;

class OrderForm extends ComponentBase
{
    const KKB_CONFIG_PATH        = 'config.txt';
    const MESSAGE_STATUS_FAIL    = 'fail';
    const MESSAGE_STATUS_SUCCESS = 'success';
    const MESSAGE_STATUS_ACCEPT  = 'accept';

    public function init()
    {
        // OrderForm::KKB_CONFIG_PATH = config('epay') ? config('epay.path') : "/config/.kkb/config.txt";
    }

    public static function getUrls($order_id, $plugin){
        return [
            'postback' => OrderForm::getUrl($order_id, $plugin, 'postback'),
  			'success' => OrderForm::getUrl($order_id, $plugin, 'success'),
  			'failure' => OrderForm::getUrl($order_id, $plugin, 'failure'),
        ];
    }
    public static function getUrl($order_id, $plugin, $purpose){
        return 'http://'.$_SERVER['HTTP_HOST']."/payment/$purpose/$plugin/$order_id";
    }

    public function componentDetails()
    {
        return [
            'name'        => 'Форма Заказа',
            'description' => 'Форма заказа с персональными даными'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public static function submit($input)
    {
        $order = new Order;
        $order->name = $input['name'];
        $order->phone = $input['phone'];
        $order->email = $input['email'];
        if ($order->save()) {
            $package = Package::find($input['package_id']);
            if (empty($package))
                return '/message/'.OrderForm::MESSAGE_STATUS_FAIL.'/'.$order->id;
            $order->package_name = $package->name;
            $order->package_hashtag = $package->hashtag;
            $order->amount = $package->real_price;
            $order->status = Order::STATUS_ACCEPT;
            if ($order->save())
                return '/message/'.OrderForm::MESSAGE_STATUS_ACCEPT.'/'.$order->id.'/'.md5($order->id.Order::FORM_TOKEN);
        }
        return '/message/'.OrderForm::MESSAGE_STATUS_FAIL.'/'.$order->id;
    }

    public static function check($id, $token)
    {
        return true; //remove this when tokens are back to business
        return ($token == md5($id.Order::FORM_TOKEN));
    }

    public static function signAbstract($id, $token, $className)
    {
        $modelClass = new ReflectionClass($className);
        $modelToken = $modelClass->getConstant('FORM_TOKEN');
        $findMethod = new ReflectionMethod($className, 'find');


        if (!empty($id)
            //&& !empty($token)
            ) {
            //if ($token == md5($id.$modelToken)) {
                $order = $findMethod->invoke(null, $id);
                if (!empty($order)) {
                    $sign = KKB::process_request($order->id, 398, $order->amount, $_SERVER['DOCUMENT_ROOT'].OrderForm::KKB_CONFIG_PATH);
                    return $sign;
                }
            //}
        }
        return false;
    }

    public static function responseAbstract($response, $className)
    {
        Log::debug('KKB Response: '.$response);
        $result = KKB::process_response(stripslashes($response), $_SERVER['DOCUMENT_ROOT'].OrderForm::KKB_CONFIG_PATH);
        Log::debug('KKB Response Converted: '.print_r($result,1));
        if (is_array($result)) {
            if (in_array("DOCUMENT", $result)) {
                $condition = //(env('APP_ENV') == 'dev' ) ? true :
                        (strpos($result['CHECKRESULT'], 'SIGN_GOOD') !== false);
                if($condition) {
                    $fields = [
                        "CUSTOMER_MAIL"     => 'customer_mail',
                        "CUSTOMER_NAME"     => 'customer_name',
                        "CUSTOMER_PHONE"    => 'customer_phone',
                        "ORDER_AMOUNT"      => 'order_amount',
                        "ORDER_CURRENCY"    => 'order_currency',
                        "ORDER_ORDER_ID"    => 'order_id',
                        "PAYMENT_AMOUNT"    => 'payment_amount',
                        "PAYMENT_REFERENCE" => 'payment_reference',
                        "RESULTS_TIMESTAMP" => 'results_timestamp',
                    ];

                    $payment = new Payment;
                    $payment->order_model = $className;

                    foreach ($fields as $field => $attr) {
                        $payment->$attr = $result[$field];
                    }
                    $payment->results_timestamp = strtotime($payment->results_timestamp);
                    $payment->status = Payment::STATUS_SUCCESS;
                    if(!$payment->order){
                        $payment->save();
                        Log::debug('KKB Response result: NOORDER');
                        die('NOORDER:'.$payment->order_id.$payment->order_model);
                    }
                    $order = $payment->order;
                    $order->setSuccess();
                    $order->payment_reason = $payment->payment_reference;
                    $order->save();
                    if ($payment->save()) {
                        Log::debug('KKB Response result: OK');
                        // delete this later
                        $to = Settings::get('email');
                        $popupParams = [
                          'order' => $order,
                          'payment' => $payment
                        ];
                        try{
                            Mail::sendTo($to, 'kosmo.loyalty::mail.loyalty', $popupParams);
                            Mail::sendTo($order->customer_mail, 'kosmo.loyalty::mail.payment_client', $popupParams);
                        }catch(\Swift_TransportException $e){
                            Log::debug('Loyalty mail send swift');
                        }catch(\Exception $e){
                            Log::debug('Loyalty mail send');
                        }
                        echo '0';
                        die();
                    }
                    Log::debug('KKB Response result: CANTSAVE');
                    die('CANTSAVE');
                }
                Log::debug('KKB Response result: NOSIGN');
                die('NOSIGN');
            }
            Log::debug('KKB Response result: NODOC');
            die('NODOC');
        }
        Log::debug('KKB Response result: EMPTY');
        die('EMPTY');
    }

    public static function confirm()
    {
        return true;
    }

    public static function _verifyCaptcha(){
        $recaptcha_secret = config('recaptcha.private');
        $recaptcha_data = $_REQUEST['g-recaptcha-response'];
        $rcCheck = $cError = $result = false;

        if($recaptcha_data)
        {
            $cUrl = "https://www.google.com/recaptcha/api/siteverify";
            $cData = http_build_query([
                'secret' => $recaptcha_secret,
                'response' => $recaptcha_data
            ]);

            if(class_exists('HTTPRequest'))
            {
                $request = new HTTPRequest($cUrl, HTTP_METH_POST);
                $request->setRawPostData($cData);
                $request->send();
                $rawResp = $request->getResponseBody();

            }elseif(function_exists('curl_init')){
                $ch = curl_init($cUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $cData);
                $rawResp = curl_exec($ch);

                $cError = curl_error($ch);
            }

            if(!$rawResp && !$cError) //fallback
            {
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $cData
                    )
                );
                $context  = stream_context_create($opts);
                $rawResp = file_get_contents($cUrl, false, $context);
            }

            if(!$rawResp)
                return false;

            if(!$cError)
            {
                $result = json_decode($rawResp,true);

                $rcCheck = $result['success'] && $result['hostname'] == $_SERVER['HTTP_HOST'];
                return $rcCheck;
            }
        }
        return false;
    }

    public function _sendEmail($to, $tpl = '', $params = [])
  	{
  	    try{
  	        Mail::sendTo($to, $tpl, $params);
  	    }catch(\Swift_TransportException $e){

  	    }catch(\Exception $e){

  	    }
  	}
}
