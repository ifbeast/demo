<?php namespace Kosmo\Billing;

use Route;
use Backend;
use Kosmo\Billing\Classes\GatewayManager;
use System\Classes\PluginBase;

/**
 * Payment Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Billing',
            'description' => 'No description provided yet...',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-credit-card'
        ];
    }

    public function registerSchedule($schedule)
    {
        $schedule->command('billing:payment')->daily();
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kosmo\Billing\Components\PaymentForm' => 'paymentForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kosmo.payment.some_permission' => [
                'tab' => 'Billing',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'payment' => [
                'label'       => 'Billing',
                'url'         => Backend::url('kosmo/billing/payment'),
                'icon'        => 'icon-credit-card',
                'permissions' => ['kosmo.billing.*'],
                'order'       => 500,
            ],
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('billing.payment', 'kosmo\billing\Console\Payment');
    }

    public function boot()
    {
        if (!$route = GatewayManager::instance()->getCallbackRoute()) {
            return;
        }
        if (!$router = GatewayManager::instance()->getCallbackRouter()) {
            return;
        }
        Route::match(['get','post'], $route, $router);
    }

}
