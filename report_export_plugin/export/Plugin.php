<?php namespace Kosmo\Export;

use Backend;
use Kosmo\Export\Console\Reports;
use System\Classes\PluginBase;

/**
 * export Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Export',
            'description' => 'Export plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kosmo\Export\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
     public function registerPermissions()
     {
         return [
             'kosmo.export.common_perimission' => [
                 'tab' => 'export',
                 'label' => 'Some permission'
             ],
             'kosmo.export.export_report' => [
                 'tab' => 'export',
                 'label' => 'Export report'
             ]
         ];
     }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
     public function registerNavigation()
     {
         return [
             'export' => [
                 'label'       => 'Отчеты',
                 'url'         => Backend::url('kosmo/export/report'),
                 'icon'        => 'icon-folder-o',
                 'permissions' => ['kosmo.export.*'],
                 'order'       => 6,
                 'sideMenu'    => [
                     'user' => [
                         'label'     => 'Отчеты',
                         'icon'        => 'icon-folder-o',
                         'url'       => Backend::url('kosmo/export/report'),
                         'permissions' => ['kosmo.export.*'],
                     ],
                 ]
             ],
         ];
     }

     public function register()
     {
        $this->registerConsoleCommand('report.check', 'Kosmo\Export\Console\CheckReport');

        $all = Reports::all();
        foreach ($all as $report) {
            $this->registerConsoleCommand($report['command'], $report['className']);
        }
     }

     public function registerSchedule($schedule)
     {
        $schedule->command('report:check')->everyTenMinutes()->withoutOverlapping();
     }
}
