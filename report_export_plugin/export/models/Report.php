<?php namespace Kosmo\Export\Models;

use Model;
use Artisan;
use Kosmo\Export\Console\Reports;

/**
 * Report Model
 */
class Report extends Model
{
    use \October\Rain\Database\Traits\Validation;

    const STATUS_WAITING = 'wating';
    const STATUS_PROGRESS = 'progress';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_export_reports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Datable fields
     */
    protected $dates = ['begin_at', 'end_at'];

    /**
     * @var array Validation rules
     */
    protected $rules = [
        'type'      => 'required',
        'begin_at'  => 'required',
        'end_at'    => 'required'
    ];


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getStatusOptions()
    {
        return [
            self::STATUS_WAITING => 'Wating',
            self::STATUS_PROGRESS => 'Progress',
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_ERROR => 'Error'
        ];
    }

    public function beforeCreate()
    {
        $this->title = $this->getTypeOption($this->type);
        $this->begin_at = $this->begin_at->startOfDay();
        $this->end_at = $this->end_at->endOfDay();
        $this->status = self::STATUS_WAITING;

    }

    public function generate()
    {
        Artisan::call($this->type, ['report' => $this->id, 'begin_at' => $this->begin_at, 'end_at' => $this->end_at]);
    }

    public function setSuccess()
    {
        $this->status = self::STATUS_SUCCESS;
        $this->save();
    }

    public function setError()
    {
        $this->status = self::STATUS_ERROR;
        $this->save();
    }

    public function scopeWaiting($query)
    {
        return $this->where('status', self::STATUS_WAITING);
    }

    public function scopeProgress($query)
    {
        return $this->where('status', self::STATUS_PROGRESS);
    }

    public function getTypeOptions()
    {
        $collection = collect(Reports::all());
        $options = $collection->lists('description', 'name');
        return $options;
    }

    public function getTypeOption($type)
    {
        $options = $this->getTypeOptions();
        return (isset($options[$type]))? $options[$type]: 'Nothing';
    }
}
