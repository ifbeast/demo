<?php namespace Kosmo\Export\Classes;

use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Carbon\Carbon;

class ReportHelper
{
    use \October\Rain\Support\Traits\Singleton;

    protected $id;

    protected $name;

    protected $extension;

    protected $filename;

    protected function init()
    {
        $this->setFilename();
    }

    public function initiate($id, $name, $extension)
    {
        $this->id        = $id;
        $this->name      = $name;
        $this->extension = $extension;
        $this->setFilename();
    }

    public function setFilename()
    {
        $datetime = Carbon::now()->format('d.m.Y-H:i');
        $this->filename = $this->id.'_'.str_replace(':', '_', $this->name).'_'.$datetime.$this->extension;
        return $this->filename;
    }

    public function getPathTemp()
    {
        if (!$this->filename) {
            $this->setFilename();
        }
        return temp_path() . '/' . $this->filename;
    }

    public function getPathStorage()
    {
        if (!$this->filename) {
            $this->setFilename();
        }
        return storage_path('app/media/'.$this->filename);
    }

    public function getPathMedia()
    {
        if (!$this->filename) {
            $this->setFilename();
        }
        return '/storage/app/media/'.$this->filename;
    }

    public function createFileTemp()
    {
        $pathTemp = $this->getPathTemp();
        file_put_contents($pathTemp, '');
    }

    public function updateFileTemp($data = [])
    {
        $pathTemp = $this->getPathTemp();
        $file = fopen($pathTemp, 'a');
        foreach ($data as $row) {
            fputcsv($file, $row);
        }
        fclose($file);
    }

    public function movePathTemp()
    {
        $pathStorage = $this->getPathStorage();
        $pathTemp = $this->getPathTemp();
        copy($pathTemp, $pathStorage);
        @unlink($pathTemp);
    }

    public function convertTo()
    {
        $pathStorage = $this->getPathStorage();
        $data = file_get_contents($pathStorage);

        if (!$data) {
            return;
        }
        if (!$rowData = explode(PHP_EOL, $data)) {
            return;
        }

        $xls = new PHPExcel();
        $xls->getProperties()->setTitle($this->filename);
        $xls->setActiveSheetIndex(0);
        $xls->getDefaultStyle()->getFont()->setName('Arial');

        foreach ($rowData as $x => $rowString) {
            if (!$row = explode(',', $rowString)) {
                continue;
            }
            foreach ($row as $y => $value) {
                $xls->getActiveSheet()->setCellValue($this->coords($x, $y), trim(str_replace('"', '', $value)));
            }
        }
        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $this->extension = '.xlsx';
        $this->setFilename();
        $writer->save($this->getPathStorage());
        return true;
    }

    public function coords($x,$y)
    {
        return PHPExcel_Cell::stringFromColumnIndex($y).($x+1);
    }
}
