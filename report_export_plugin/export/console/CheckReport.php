<?php namespace Kosmo\Export\Console;

use DB;
use ApplicationException;
use Kosmo\Export\Models\Report;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckReport extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:check';

    /**
     * @var string The console command description.
     */
    protected $description = 'Запускаем генерацию отчетов по расписанию.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        if ($report = Report::progress()->first()) {
            return;
        }
        if ($report = Report::waiting()->first()) {
            $report->generate();
            tracelog('Report done '.$report->id);
            return;
        }
        tracelog('Console::CheckReport:: No reports are waiting');
        return;
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
