<?php namespace Kosmo\Export\Console;

class Reports
{
    public static function all() {
        return
        [
            [
                'name' => 'report:forecasts',
                'command' => 'report.forecasts',
                'className' => 'Kosmo\Export\Console\Reports\ForecastsReport',
                'description' => 'Отчет по прогнозам'
            ],
            [
                'name' => 'report:active_users',
                'command' => 'report.active_users',
                'className' => 'Kosmo\Export\Console\Reports\ActiveUsersReport',
                'description' => 'Отчет по активным пользователям'
            ],
            [
                'name' => 'report:delivery',
                'command' => 'report.delivery',
                'className' => 'Kosmo\Export\Console\Reports\DeliveryReport',
                'description' => 'Отчет по доставкам'
            ]
        ];
    }
}
