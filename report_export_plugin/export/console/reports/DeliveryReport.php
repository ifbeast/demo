<?php namespace Kosmo\Export\Console\Reports;

use DB;
use Kosmo\Quiz\Models\Winning;

class DeliveryReport extends BaseReport
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:delivery';

    /**
     * @var string The console command description.
     */
    protected $description = 'Отчет по доставкам.';

    public function generate()
    {
        $helper = $this->helper;
        $index = 0;
        DB::table('kosmo_delivery_deliveries')
            ->where('created_at', '>=', $this->begin_at->format('Y-m-d H:i'))
            ->where('created_at', '<', $this->end_at->format('Y-m-d H:i'))
            ->orderBy('created_at')
            ->chunk(100, function($deliveries) use($helper, &$index) {
                $csv = [];
                if (!$index) {
                    $csv[] = ['ID', 'Prize', 'Name', 'Surname', 'Phone', 'Zip', 'State', 'City', 'Street', 'House', 'Apartment', 'Comment'];
                }

                foreach ($deliveries as $delivery) {
                    $prizeName = null;
                    if ($winning = Winning::where('id', $delivery->winning_id)->first()) {
                        $prizeName = $winning->prize->name;
                    }

                    $csv[] = [
                        $delivery->id,
                        $prizeName,
                        $delivery->name,
                        $delivery->surname,
                        $delivery->phone,
                        $delivery->zip,
                        $delivery->state,
                        $delivery->city,
                        $delivery->street,
                        $delivery->house,
                        $delivery->apartment,
                        $delivery->comment,
                        $delivery->created_at
                    ];
                }

                $helper->updateFileTemp($csv);
                $index ++;
            });
    }
}
