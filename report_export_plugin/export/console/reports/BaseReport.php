<?php namespace Kosmo\Export\Console\Reports;

use DB;
use Carbon\Carbon;
use Kosmo\Export\Models\Report;
use Kosmo\Export\Classes\ReportHelper;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class BaseReport extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:base';

    /**
     * @var string The console command description.
     */
    protected $description = 'Пустой отчет.';

    /**
     *  Custom params
     */
    protected $helper;

    protected $repot_id;

    protected $begin_at;

    protected $end_at;

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->report_id = $this->argument('report');
        $this->begin_at  = $this->argument('begin_at');
        $this->end_at    = $this->argument('end_at');

        if (!$report = Report::find($this->report_id)) {
            $report->message = 'Report is not found.';
            $report->setError();
            return;
        }

        $this->helper = ReportHelper::instance();
        $this->helper->initiate($this->report_id, $this->name, '.csv');
        $this->helper->createFileTemp();
        $this->generate();
        $this->helper->movePathTemp();

        $report->file = $this->helper->getPathMedia();
        $report->setSuccess();
        return;
    }

    public function generate()
    {
        $data = [];
        $this->helper->updateFileTemp($data);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['report', InputArgument::REQUIRED, 'Report ID is required.'],
            ['begin_at', InputArgument::REQUIRED, 'Begin datetime is required.'],
            ['end_at', InputArgument::REQUIRED, 'End datetime is required.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
