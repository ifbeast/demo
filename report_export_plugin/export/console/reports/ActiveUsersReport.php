<?php namespace Kosmo\Export\Console\Reports;

use DB;
use Kosmo\Profile\Models\User as UserModel;
use Kosmo\Profile\Models\Forecast as ForecastModel;

class ActiveUsersReport extends BaseReport
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:active_users';

    /**
     * @var string The console command description.
     */
    protected $description = 'Отчет по активным пользователям.';

    public function generate()
    {
        $helper = $this->helper;
        $begin  = $this->begin_at;
        $limit  = $this->end_at;

        $index = 0;
        $start = $begin->copy()->startOfDay();
        $end = $begin->copy()->endOfDay();

        while($limit->gte($end)) {
            $usersCount = UserModel::where('created_at', '>=', $start)
                                    ->where('created_at', '<', $end)
                                    ->count();

            $groupped = ForecastModel::orderBy('created_at', 'asc')
                                    ->groupBy('user_id')
                                    ->lists('id', 'user_id');

            $madeForecastCount = ForecastModel::where('created_at', '>=', $start)
                                    ->where('created_at', '<', $end)
                                    ->whereIn('id', array_values($groupped))
                                    ->count();

            $csv = ($index)? []: [['DateTime', 'Users Registred', 'Made 1st Forecast']];
            $csv[] = [
                $start->format('d.m.Y'),
                $usersCount,
                $madeForecastCount
            ];

            $helper->updateFileTemp($csv);
            $start->addDay();
            $end->addDay();
        }
    }
}
