<?php namespace Kosmo\Export\Console\Reports;

use DB;

class ForecastsReport extends BaseReport
{
    /**
     * @var string The console command name.
     */
    protected $name = 'report:forecasts';

    /**
     * @var string The console command description.
     */
    protected $description = 'Отчет по прогнозам.';

    public function generate()
    {
        $helper = $this->helper;
        $index = 0;
        DB::table('kosmo_profile_forecasts')
            ->where('created_at', '>=', $this->begin_at->format('Y-m-d H:i'))
            ->where('created_at', '<', $this->end_at->format('Y-m-d H:i'))
            ->orderBy('id')
            ->chunk(100, function($forecasts) use($helper, &$index) {
                $csv = [];
                if (!$index) {
                    $csv[] = ['ID', 'Yellow', 'Red', 'User', 'Ticket', 'Channel', 'Status', 'Registred'];
                }

                foreach ($forecasts as $forecast) {
                    $userPhone = DB::table('kosmo_profile_users')
                        ->where('id', $forecast->user_id)
                        ->value('phone');

                    $ticketNumber = null;
                    $channel = null;

                    if ($forecast->ticket_id) {
                        $ticket = DB::table('kosmo_profile_tickets')
                            ->where('id', $forecast->ticket_id)
                            ->first();
                        if ($ticket) {
                            $ticketNumber = $ticket->number;
                            $channel = $ticket->type;
                        }
                    } else {
                        $channel = 'bonus';
                    }

                    $csv[] = [
                        $forecast->id,
                        $forecast->scores_yellow,
                        $forecast->scores_red,
                        $userPhone.' ',
                        $ticketNumber,
                        $channel,
                        $forecast->status,
                        $forecast->created_at
                    ];
                }

                $helper->updateFileTemp($csv);
                $index ++;
            });
    }
}
