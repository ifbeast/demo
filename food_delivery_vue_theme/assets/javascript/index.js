/*
 * Application
 */

$(document).tooltip({
    selector: "[data-toggle=tooltip]"
})

/*
 * Auto hide navbar
 */
jQuery(document).ready(function($){
    var $header = $('.navbar-autohide'),
        scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 150

    $(window).on('scroll', function(){
        if (!scrolling) {
            scrolling = true

            if (!window.requestAnimationFrame) {
                setTimeout(autoHideHeader, 250)
            }
            else {
                requestAnimationFrame(autoHideHeader)
            }
        }
    })

    function autoHideHeader() {
        var currentTop = $(window).scrollTop()

        // Scrolling up
        if (previousTop - currentTop > scrollDelta) {
            $header.removeClass('is-hidden')
        }
        else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
            // Scrolling down
            $header.addClass('is-hidden')
        }

        previousTop = currentTop
        scrolling = false
    }
});


var modals = [],
    corporatModal,
    // corporatSentModal,
    orderSuccessModal,
    okBtn;


jQuery(document).ready(function($){
    $(document).on('click', '[data-toggle=animate]', function(e) {
        e.preventDefault();
        var target = $(this).data('target'),
            animation = $(this).data('animate');

        console.log($('[data-id='+target+']'));
        console.log(animation);
        if ($('[data-id='+target+']').hasClass('hidden')) {
            $('[data-id='+target+']').addClass('animated '+animation).removeClass('hidden');
        } else {
            $('[data-id='+target+']').removeClass('animated '+animation).addClass('hidden');
        }
    });

    // Smooth scroll
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
    });

    if ($('.order-total__fixed').length > 0) {
    	$(window).on('scroll', function () {
    		var offset = $('.order-total__body').offset().top-$(window).outerHeight();
    		if ($(window).scrollTop() > offset) {
    			$('.order-total__fixed').removeClass('animated fadeInUp').addClass('animated fadeOutDown');
    		} else {
    			if ($('.order-total__fixed').hasClass('animated')) {
    				$('.order-total__fixed').removeClass('animated fadeOutDown').addClass('animated fadeInUp');
    			}
    		}
    	});
    }

    // Index page slider
    $('#slider').owlCarousel({
      loop:true,
      items: 1,
      autoplay:true,
      autoplayTimeout:2000,
      animateOut: 'fadeOut'
    });

    // mobile menu open-close;

    var navMain = document.querySelector('.main-nav');
    var navToggle = document.querySelector('.main-nav__toggle');
    var bonAppetit = document.querySelector('.page-header__bon-appetit-logo');

    navToggle.addEventListener('click', function() {
      if (navMain.classList.contains('main-nav--closed')) {
        navMain.classList.remove('main-nav--closed');
        navMain.classList.add('main-nav--opened');
        bonAppetit.classList.add('page-header__bon-appetit-logo--mob');
      } else {
        navMain.classList.add('main-nav--closed');
        navMain.classList.remove('main-nav--opened');
        bonAppetit.classList.remove('page-header__bon-appetit-logo--mob');
      }
    });

    $('#main-nav-bg').on('click', function() {
      $('.main-nav').addClass('main-nav--closed');
      $('.main-nav').removeClass('main-nav--opened');
    });


  var formClear = function(modal) {
      var forms  = $(modal).find('form');
      if (forms.length > 0) {
          forms[0].reset();
          forms.find('.error__custom').empty();
          forms.removeClass('show_error__ajax').removeClass('show_error__custom')
      }
  }


  var openModal= function(modal, onOpen, onClose) {
        if (!onOpen) onOpen = function() {};
        if (!onClose) onClose = function() {};
        return function(event) {
            if (event) event.preventDefault();
            var currentModal = $(modal).bPopup({
                amsl: 0,
                modalClose: true,
                closeClass: 'close',
                opacity: 0.8,
                positionStyle: 'absolute',
                transition: 'slideDown',
                onClose: function() {
                    modals = [];
                    formClear(modal);
                    onClose();
                },
                onOpen: onOpen
            });
            for (var i = 0; i < modals.length; i++) {
                modals[i].close();
            };
            modals.push(currentModal);
        };
  }
  //
  orderSuccessModal = openModal('#order-success');

  $(window).on('ajaxErrorMessage', function(e, message){
      e.preventDefault();
      console.log(message);
  });

  $("#contacts-mob").mask("+7 (999) 999-99-99");
  if ($('#contacts-form').length > 0) {
      $('#contacts-form').validator({
          custom:{},
          errors:{},
          feedback:{}
      });
      $('#contacts-form').on('click', '#contacts-form-submit', function(e) {
          e.preventDefault();
          $('#contacts-form').submit();
      });
      $('#contacts-form').on('submit', function (e) {
          console.log('feedback');
          if (e.isDefaultPrevented()) {
          } else {
              e.preventDefault();
              $('#contacts-form').request('onFeedback',{
                  success: function(data) {
                      openModal('#feedback-success-modal', null, function(){location.reload()} )(e);
                      this.success(data);
                  }
              });
          }
      });
  }

  corporatModal = openModal('#corp-modal');
  $("#corporat-mob").mask("+7 (999) 999-99-99");
  $('#corporat-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
    } else {
        e.preventDefault();
        $('#corporat-form').request('onLead',{
            success: function(data) {
                openModal('#corp-success-modal', null, function(){location.reload()} )(e);
                this.success(data);
            }
        });
    }
  });

});
