var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);

function init () {
    myMap = new ymaps.Map('map', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [43.243691, 76.938371], 
        zoom: 13
    }, {
        searchControlProvider: 'yandex#search'
    }),


    myMap.geoObjects.add(new ymaps.Placemark([43.233511, 76.954242], {
        balloonContent: '<div class="corners__map-balloon">БЦ Samal Towers, <br>ул Жолдасбекова 97, <br>+ 7 778 911 55 66</div>'

    }, {
        iconLayout: 'default#image',
        iconImageHref: 'themes/mamas/assets/images/map_label.svg',
        iconImageSize: [22, 28], // размер иконки
        iconImageOffset: [-11, -28], // позиция иконки

        balloonContentSize: [218, 95], // размер нашего кастомного балуна в пикселях
        balloonLayout: "default#imageWithContent", // указываем что содержимое балуна кастомная херь
        balloonImageHref: 'themes/mamas/assets/images/map_balloon.svg', // Картинка заднего фона балуна
        balloonImageOffset: [0, -78], // смещение балуна, надо подогнать под стрелочку
        balloonImageSize: [218, 95], // размер картинки-бэкграунда балуна
        balloonShadow: false
    })),

    myMap.geoObjects.add(new ymaps.Placemark([43.229872, 76.950424], {
        balloonContent: '<div class="corners__map-balloon">БЦ CDC-1, ул Фурманова 240,<br> + 7 777 835 99 11</div>'

    }, {
        iconLayout: 'default#image',
        iconImageHref: 'themes/mamas/assets/images/map_label.svg',
        iconImageSize: [22, 28], 
        iconImageOffset: [-11, -28], 

        balloonContentSize: [270, 76], 
        balloonLayout: "default#imageWithContent",
        balloonImageHref: 'themes/mamas/assets/images/map_balloon.svg',
        balloonImageOffset: [0, -65],
        balloonImageSize: [270, 76],
        balloonShadow: false
    })),

    myMap.geoObjects.add(new ymaps.Placemark([43.253850, 76.924858], {
        balloonContent: '<div class="corners__map-balloon">БЦ «Далич», <br>ул Толе би 101</div>'

    }, {
        iconLayout: 'default#image',
        iconImageHref: 'themes/mamas/assets/images/map_label.svg',
        iconImageSize: [22, 28], 
        iconImageOffset: [-11, -28], 

        balloonContentSize: [200, 76], 
        balloonLayout: "default#imageWithContent",
        balloonImageHref: 'themes/mamas/assets/images/map_balloon.svg',
        balloonImageOffset: [0, -65],
        balloonImageSize: [200, 76],
        balloonShadow: false
    })),

    myMap.geoObjects.add(new ymaps.Placemark([43.229386, 76.946651], {
        balloonContent: '<div class="corners__map-balloon">Вагончик Mamas, БЦ «Нурлы Тау», блок 2А</div>'

    }, {
        iconLayout: 'default#image',
        iconImageHref: 'themes/mamas/assets/images/map_label.svg',
        iconImageSize: [22, 28], 
        iconImageOffset: [-11, -28], 

        balloonContentSize: [220, 76], 
        balloonLayout: "default#imageWithContent",
        balloonImageHref: 'themes/mamas/assets/images/map_balloon.svg',
        balloonImageOffset: [0, -65],
        balloonImageSize: [220, 76],
        balloonShadow: false
    })),

    myMap.geoObjects.add(new ymaps.Placemark([43.226949, 76.944046], {
        balloonContent: '<div class="corners__map-balloon">АФЦ, Проспект Аль-Фараби 34, <br>блок А (здание Qazkom), <br>+ 7 778 411 55 66 </div>'

    }, {
        iconLayout: 'default#image',
        iconImageHref: 'themes/mamas/assets/images/map_label.svg',
        iconImageSize: [22, 28],
        iconImageOffset: [-11, -28], 

        balloonContentSize: [290, 95], 
        balloonLayout: "default#imageWithContent",
        balloonImageHref: 'themes/mamas/assets/images/map_balloon.svg', 
        balloonImageOffset: [0, -78],
        balloonImageSize: [290, 95], 
        balloonShadow: false
    }))


    ;


}