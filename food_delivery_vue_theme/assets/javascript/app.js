var FB, VK;

var CompaniesSelect = function(target, hidden, auto) {
	$(target).autocomplete({
		appendTo: $(target).parent('div'),
		lookup: companies,
		formatResult: function(item, value){
			result = item.company.title;
			result = (item.company.address)? result + ' - ' + item.company.address: result;
			result = (item.company.office)? result + ' - ' + item.company.office: result;
			return result;
		},
		onSelect: function (item) {
			if (auto) {
				app.company = item.company;
			}
			$(hidden).val(item.data);
		}
	});
};

var SuccessModal = function() {
	$('#order-success-modal').bPopup({
	    amsl: 0,
	    modalClose: true,
	    closeClass: 'close',
	    opacity: 0.8,
	    positionStyle: 'absolute',
	    transition: 'slideDown',
	    onClose: function() {
	        location.replace('/');
	    }
	});
};

var ProductCounter = {
	template: '#tpl-product-counter',
    props: {
        id: {type: Number, required: true},
		title: {type: String},
        count: {type: Number, default: 0},
        price: {type: Number, default: 0},
		available: {type: Boolean, default: true},
        emitCount: {type: Boolean, default: true},
        emitPrice: {type: Boolean, default: true},
    },
	data: function() {
		return {
            currentCount: this.count,
            currentPrice: this.price,
			currentTitle: this.title
        }
	},
	computed: {
		showTitle: function() {
			return !!this.title;
		},
		currentCountPrice: function() {
			return (this.currentCount)? this.currentCount+'x'+this.currentPrice: this.currentPrice;
		}
	},
	methods: {
		addCurrentCount: function() {
			this.currentCount += 1;
			if (this.id == 0) {
				return;
			}
			this.$emit('add', {id: this.id, count: 1, price: this.price, emitCount: this.emitCount, emitPrice: this.emitPrice});
		},
		removeCurrentCount: function() {
			this.currentCount -= 1;
			if (this.currentCount < 0) {
				this.currentCount = 0;
			} else {
				if (this.id == 0) {
					return;
				}
				this.$emit('remove', {id: this.id, count: 1, price: this.price, emitCount: this.emitCount, emitPrice: this.emitPrice});
			}
		}
	}
};

var CheckForm = {
	template: '#tpl-check-form',
	data: function() {
		return {
			count: 0,
			showForm: true,
			showFail: false,
			showSuccess: false
		};
	},
	methods: {
		confirm: function() {
			this.$emit('check', this.count);
		},
		back: function() {
			this.$emit('check', this.count);
			this.showForm = true;
			this.showFail = false;
			this.showSuccess = false;
		}
	},
    mounted: function() {
		var _component = this;
        setTimeout(function() {
            CompaniesSelect('#check-form-input', '#check-form-hidden', false);
			$('#check-form-submit').on('click', function(e) {
				e.preventDefault();
				if (!$('#check-form-hidden').val()) {
					return false;
				}
				$('#check-form').request('onCheckCompany', {
					success: function(data) {
						app.company = data.company;
						_component.count = data.count;

						if (data.count) {
							_component.showForm = false;
							_component.showFail = false;
							_component.showSuccess = true;
						} else {
							_component.showForm = false;
							_component.showFail = true;
							_component.showSuccess = false;
						}
						this.success(data);
					}
				});
			})
        }, 1000);
    }
}

var OrderForm = {
    template: '#tpl-order-form',
	data: function() {
		return {
			hideForm: false,
			hidePayments: true
		};
	},
	props: {
		products: {type: Array, default: function() { return []}},
		user: {type: Object, default: function() { return {}}},
		company: {type: Object, default: function() { return {}}},
	},
	watch: {
		products: function(value, oldValue) {
			console.log('Products', value);
		},
		user: function(value, oldValue) {
			console.log('User', value);
		},
		company: function(value, oldValue) {
			console.log('Company', value);
		}
	},
	methods: {
		changeHideParams: function(hideForm, hidePayments) {
			this.hideForm = hideForm;
			this.hidePayments = hidePayments;
		},
		initProducts: function() {
			var products = [];
			_.each(this.products, function(item) {
				products.push(item);
			})
			return products;
		},
		submitForm: function() {
			var _component = this;
			$('#order-form').off().on('submit', function (e) {
				if (e.isDefaultPrevented()) {
				} else {
					e.preventDefault();
					$('#order-form').request('onOrderSubmit',{
						data: {products: _component.initProducts()},
						success: function(data) {
							_component.changeHideParams(true, false);
							this.success(data);
						}
					});
				}
			});
			$('#order-form').validator('validate');
		},
		submitPayment: function(payment) {
			var _component = this;
			$('#order-form').request('onPaymentSubmit',{
				data: {products: _component.initProducts(), payment: payment},
				success: function(data) {
					_component.changeHideParams(true, false);
					this.success(data);
				}
			});
		},
		showForm: function() {
			this.changeHideParams(false, true);
		}
	},
    mounted: function() {
		var _products = this.initProducts();
        setTimeout(function() {
            CompaniesSelect('#order-form-company', '#order-form-hidden', true);
            // $('#order-form-phone').mask("+7 (999) 999-99-99");
			$('#order-form').validator();
        }, 1000);
    }
};

Vue.use(VueMask.VueMaskPlugin);
Vue.directive('mask', VueMask.VueMaskDirective);
Vue.component('product-counter', ProductCounter);
Vue.component('check-form', CheckForm);
Vue.component('order-form', OrderForm);

var app = new Vue({
	el: '#app',
	data: {
		days: [],
		user: {},
		company: {},
        products: [],
        companyCount: 0,
		canLoginSocials: true,
        canChangeLunches: true
	},
	watch: {
		products: function(value, oldValue) {
			console.log('App Products', value);
		},
		user: function(value, oldValue) {
			console.log('App User', value);
		},
		company: function(value, oldValue) {
			console.log('App Company', value);
		}
	},
    computed: {
        totalCount: function() {
            var total = 0;
            for (var i = 0; i < this.products.length; i++) {
                total += (this.products[i].emitCount)? this.products[i].count: 0;
            }
            return total;
        },
        totalPrice: function() {
            var total = 0;
            for (var i = 0; i < this.products.length; i++) {
                total += (this.products[i].emitPrice)? this.products[i].price: 0;
            }
            return total;
        },
        isOrderAvailable: function() {
            return (this.totalCount > 0) && ((this.totalCount + this.companyCount) >= 2);
        },
        canChangeFeatures: function() {
            return (this.totalCount > 0) && this.canChangeLunches;
        },
		hasFBAuth: function() {
			return !!this.user.social_fb_id;
		},
		hasVKAuth: function() {
			return !!this.user.social_vk_id;
		}
    },
	methods: {
		addDay: function() {
			var _component = this,
				days = this.days.length;
			console.log(days);
			$.request('onAddDay', {
				data: {days: days},
				success: function(data) {
					if (data.length) {
						_component.days.push(data);
					}
					this.success(data);
				}
			});
		},
		addProduct: function(product) {
            this.products.push(product);
		},
		removeProduct: function(product) {
            var find = _.findWhere(this.products, {id: product.id});
            if (!find) return;

            var index = _.indexOf(this.products, find);
            if (index == -1) return;

            this.products.splice(index, 1);
		},
		changeCompanyCount: function(count) {
			this.companyCount = count;
		},
		fbLogin: function() {
			if (FB) { FB.login() }
		},
		vkLogin: function() {
			if (VK) { VK.Auth.login() }
		},
		appLogin: function(data) {
			console.log('App Login', data);

			$.request('onLogin', {
				data: data,
				success: function(data) {
					if (data.success) {
						app.canLoginSocials = false;
					}
					if (data.user) {
						app.user = data.user;
					}
					if (data.company) {
						app.company = data.company;
					}
					this.success(data);
				}
			});
		},
		appLogout: function() {
			if (FB) { try {FB.logout()} catch(e) {console.log(e)} }
			if (VK) { try {VK.Auth.logout()} catch(e) {console.log(e)} }
			$.request('onLogout', {
				success: function(data) {
					if (data.success) {
						app.canLoginSocials = true;
						app.user = {};
					}
					this.success(data);
				}
			});
		}
	},
	created: function() {
		var _component = this;
		setTimeout(function(){
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			(function(id){
				var el = document.createElement("script");
				el.type = "text/javascript";
				el.src = "https://vk.com/js/api/openapi.js?150";
				el.async = true;
				document.getElementById(id).appendChild(el);
			}('vk_api_transport'));
		}, 1000);

		this.addDay();
	}
});

window.fbAsyncInit = function() {
	FB.init({
		appId            : '1501149576667435',
		autoLogAppEvents : true,
		xfbml            : true,
		version          : 'v2.10'
	});
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			app.appLogin({social: 'fb', uid: response.authResponse.userID});
		}
	});
	FB.Event.subscribe('auth.login', function(response) {
		if (response.status === 'connected') {
			app.appLogin({social: 'fb', uid: response.authResponse.userID});
		}
	});
};

window.vkAsyncInit = function() {
	VK.init({
		apiId: '6250574',
		status: true
	});
	VK.Observer.subscribe('auth.login', function(response) {
		if (response.session) {
			VK.Api.call('users.get', {}, function(r) {
				if(r.response) {
					app.appLogin({social: 'vk', uid: r.response[0].uid});
				}
			});
		}
	});
};
