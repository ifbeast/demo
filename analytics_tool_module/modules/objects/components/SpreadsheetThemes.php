<?php
class SpreadsheetThemes {
	
	private $instance 		= NULL;
	private $table 			= NULL;
	private $relatedTable 	= NULL;

	static function instance()
	{
		if(!$this->instance) $this->instance = new SpreadsheetThemes;
		return $this->$instance;
	}

	function __construct()
	{		
		global $TABLES;
		$this->table = $TABLES['dobject_themes'];
		$this->relatedTable = $TABLES['dobject_theme_colors'];
	}

	function showThemes()
	{		
		$themes = array();
		$query = db_query("	SELECT ot.ot_id as 'id', ot.ot_name as 'name'
							FROM `$this->table` AS ot
							ORDER BY name ASC ");
		while($row = db_fetch_assoc($query))
		{
			$themes[] = $row;
		}
		return $themes;
	}

	function showThemesTree()
	{
		$tree = array();
		$themes = $this->showThemes();
		foreach ($themes as $theme)
		{			
			$tree[] = array('li_attr' => array('id' => $theme['id']), 'type' => 'link', 'text' => $theme['name'], 'children'=> false);
		}
		return $tree;
	}	

	function showTheme($id)
	{
		$theme = array();
		$query = db_query("	SELECT ot.ot_name as 'name', ot.ot_header_font as 'headerFont', ot.ot_body_font as 'bodyFont'
							FROM `$this->table` AS ot
							WHERE ot.ot_id = '$id'
							ORDER BY name ASC ");
		while($row = db_fetch_assoc($query))
		{
			$theme['headerFont'] = $row['headerFont'];
			$theme['bodyFont'] = $row['bodyFont'];
		}
		$query = db_query("	SELECT otc.otc_id as 'id', otc.otc_name as 'name', otc.otc_value as 'value'
							FROM `$this->relatedTable` AS otc
							WHERE otc.ot_id = '$id'
							ORDER BY name ASC ");
		while($row = db_fetch_assoc($query))
		{
			$theme['colors'][] = array('id' => $row['id'], 'name' => $row['name'], 'value' => $row['value']);
		}
		return $theme;
	}

	function createTheme($name)
	{
		db_query(" INSERT INTO `$this->table` (ot_name) VALUES('$name') ");
		return db_insert_id();
	}

	function updateTheme($id, $params)
	{
		if (isset($params['name'])){
			$font = $params['name'];
			$query = db_query("UPDATE `$this->table` SET ot_name='$name' WHERE ot_id='$id'");
		}
		if (isset($params['bodyFont'])){
			$font = $params['bodyFont'];
			$query = db_query("UPDATE `$this->table` SET ot_body_font='$font' WHERE ot_id='$id'");
		}
		if (isset($params['headerFont'])){
			$font = $params['headerFont'];
			$query = db_query("UPDATE `$this->table` SET ot_header_font='$font' WHERE ot_id='$id'");
		}
		if (isset($params['colors']) && !empty($params['colors'])){
			$colors = array();
			foreach ($params['colors'] as $color)
			{				
				ksort($color);
				foreach ($color as $key => $value)
				{
					if (!is_numeric($value) && is_string($value))
						$color[$key] = "'".$value."'";
				}
				$colors[] = '('.$id.','.implode(',', array_values($color)).')';
			}
			$values = implode(',', $colors);
			$query = db_query("	INSERT INTO `$this->relatedTable` (ot_id, otc_id, otc_name, otc_value) VALUES $values
								ON DUPLICATE KEY UPDATE ot_id = VALUES(ot_id), otc_name = VALUES(otc_name), otc_value = VALUES(otc_value); ");
		}
		return $query;
	}

	function removeTheme($id)
	{
		$removedTheme = db_query(" DELETE FROM `$this->table` WHERE ot_id = '$id' ");
		$removedThemeColors = db_query(" DELETE FROM `$this->relatedTable` WHERE ot_id = '$id' ");
		return ($removedTheme && $removedThemeColors);
	}

}	

?>