<?php
class SpreadsheetStyles {

	private $instance 		= NULL;
	private $table 			= NULL;
	private $relatedTable	= NULL;

	static function instance()
	{
		if(!$this->instance) $this->instance = new SpreadsheetStyles;
		return $this->$instance;
	}

	function __construct()
	{		
		global $TABLES;
		$this->table = $TABLES['dobject_styles'];
		$this->relatedTable = $TABLES['dobject_style_attrs'];
	}

	function showStyles($parent = false)
	{
		$styles = array();
		$query = db_query("	SELECT os.os_id as 'id', os.os_pid as 'parent', os.os_name as 'name'
							FROM `$this->table` AS os ".(($parent !== false) ? "WHERE os.os_pid = $parent" : "")." ORDER BY name ASC ");

		while($row = db_fetch_assoc($query))
		{
			$styles[] = $row;
		}
		return $styles;
	}

	function showStylesTree($parent)
	{
		if (!$parent) $parent = 0;
		else $parent = (int)$parent;
		$tree = array();
		$styles = $this->showStyles($parent);
		foreach ($styles as $style)
		{			
			$tree[] = array('li_attr' => array('id' => $style['id']), 'type' => 'link', 'text' => $style['name'], 'parent' => $parent, 'children'=> true);
		}
		return $tree;
	}

	function showStyle($id)
	{
		return $this->getStyle($id);
	}

	function createStyle($name, $parent)
	{
		db_query(" INSERT INTO `$this->table` (os_name, os_pid) VALUES('$name','$parent') ");
		return db_insert_id();
	}

	function updateStyle($id, $params)
	{
		if (isset($params['parent'])){
			$font = $params['parent'];
			$query = db_query("UPDATE `$this->table` SET os_pid='$parent' WHERE os_id='$id'");
		}
		if (isset($params['name'])){
			$name = $params['name'];
			$query = db_query("UPDATE `$this->table` SET os_name='$name' WHERE os_id='$id'");
		}
		if (isset($params['attrs']) && !empty($params['attrs'])){
			$attrs = array();
			foreach ($params['attrs'] as $attr)
			{				
				ksort($attr);
				foreach ($attr as $key => $value)
				{
					if (!is_numeric($value) && is_string($value))
						$attr[$key] = "'".$value."'";
				}
				$attrs[] = '('.$id.','.implode(',', array_values($attr)).')';
			}
			$values = implode(',', $attrs);
			$query = db_query("	INSERT INTO `$this->relatedTable` (os_id, osa_id, osa_name, osa_type, osa_value) VALUES $values
								ON DUPLICATE KEY UPDATE os_id = VALUES(os_id), osa_name = VALUES(osa_name),  osa_type = VALUES(osa_type), osa_value = VALUES(osa_value); ");
		}
		return $query;
	}

	function removeStyle($id)
	{
		$removedStyle = db_query(" DELETE FROM `$this->table` WHERE os_id = '$id' ");
		$removedStyleAttrs = db_query(" DELETE FROM `$this->relatedTable` WHERE os_id = '$id' ");
		return ($removedStyle && $removedStyleAttrs);
	}

	function getStyle($id)
	{
		$style = array();
		$query = db_query("	SELECT osa.osa_id as 'id', osa.osa_name as 'name', osa.osa_value as 'value', osa.osa_type as 'type'
							FROM `$this->relatedTable` AS osa
							WHERE osa.os_id = '$id'
							ORDER BY name ASC ");
		while($row = db_fetch_assoc($query))
		{
			$style['attrs'][] = array('id' => $row['id'], 'name' => $row['name'], 'type' => $row['type'], 'value' => $row['value']);
		}
		return $style;
	}

	function getAncestorStyle($id)
	{
		$query = db_query(" SELECT osa.osa_id as 'id', osa.osa_name as 'name', osa.osa_value as 'value', osa.osa_type as 'type'
					FROM `$this->table` AS os
					INNER JOIN `$this->relatedTable` AS osa
					ON osa.os_id = os.os_pid
					WHERE os.os_id = '$id' 
					ORDER BY name ASC ");
		if (mysql_num_rows($query) > 0) {
			$style = array();
			while($row = db_fetch_assoc($query))
			{
				$style['attrs'][] = array('id' => $row['id'], 'name' => $row['name'], 'type' => $row['type'], 'value' => $row['value']);
			}
			return $style;
		} else {
			return false;
		}
	}

	function getCompiledStyle($id)
	{
		$style = array();
		$descendantStyle = $this->getStyle($id);
		$ancestorStyle = $this->getAncestorStyle($id);
		$style = $this->compileStyles([$ancestorStyle, $descendantStyle]);
		return $style;
	}

	function compileStyles($styles)
	{
		$compiledStyle = array();
		if ($styles && !empty($styles))
			foreach ($styles as $style) {
				if ($style && !empty($style)) {
					foreach ($style['attrs'] as $key => $attr) {
						$compiledStyle[$attr['name']] = $attr['value'];
					}	
				}
			}
		return $compiledStyle;
	}
}

?>