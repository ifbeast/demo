<?php

class SpreadsheetStyleAttrValues {
	// 
	// Style Attr Types
	// 
	const STYLE_ATTR_TYPE_COLOR		= 'color';
	const STYLE_ATTR_TYPE_FONT		= 'font';
	const STYLE_ATTR_TYPE_BORDER	= 'border';
	const STYLE_ATTR_TYPE_VALIGN	= 'vAling';
	const STYLE_ATTR_TYPE_HALIGN	= 'hAling';
	private $STYLE_ATTR_TYPES = [
		SpreadsheetStyleAttrValues::STYLE_ATTR_TYPE_COLOR 	=> 'Цвет',
		SpreadsheetStyleAttrValues::STYLE_ATTR_TYPE_FONT  	=> 'Шрифт',
		SpreadsheetStyleAttrValues::STYLE_ATTR_TYPE_BORDER 	=> 'Граница',
		SpreadsheetStyleAttrValues::STYLE_ATTR_TYPE_VALIGN 	=> 'Выранивание по вертикали',
		SpreadsheetStyleAttrValues::STYLE_ATTR_TYPE_HALIGN 	=> 'Выранивание по горизонтале',
	];
	// 
	// Horizontal alignment styles
	// 
	const HORIZONTAL_GENERAL	= 'general';
	const HORIZONTAL_LEFT		= 'left';
	const HORIZONTAL_RIGHT		= 'right';
	const HORIZONTAL_CENTER		= 'center';
	private $HORIZONTAL_ALIGNMENT = [
		SpreadsheetStyleAttrValues::HORIZONTAL_GENERAL 	=> 'По умолчанию',
		SpreadsheetStyleAttrValues::HORIZONTAL_LEFT 		=> 'По левому краю',
		SpreadsheetStyleAttrValues::HORIZONTAL_RIGHT 	=> 'По правому краю',
		SpreadsheetStyleAttrValues::HORIZONTAL_CENTER 	=> 'По центру',
	];
	// 
	// Vertical alignment styles
	//
	const VERTICAL_BOTTOM		= 'bottom';
	const VERTICAL_TOP			= 'top';
	const VERTICAL_CENTER		= 'center';
	private $VERTICAL_ALIGNMENT = [
		SpreadsheetStyleAttrValues::VERTICAL_BOTTOM 		=> 'По нижнему краю',
		SpreadsheetStyleAttrValues::VERTICAL_TOP 		=> 'По верхнему краю',
		SpreadsheetStyleAttrValues::VERTICAL_CENTER 		=> 'По центру',
	];

	private $instance 			= NULL;

	static function instance()
	{
		if(!$this->instance) $this->instance = new SpreadsheetStyleAttrValues;
		return $this->$instance;
	}

	function __construct()
	{		
	}
	
	function getStyleAttrValues()
	{
		return array(	'types'  => $this->STYLE_ATTR_TYPES,
		 				'hAling' => $this->HORIZONTAL_ALIGNMENT,
		 				'vAling' => $this->VERTICAL_ALIGNMENT );
	}
}

?>