// 
// Globals
// 
var observer 		= new Observer(); // Works with data table and store other ids
var helpers 		= new Helpers();  // Manipulate with DOM
var treeConfig 		= new TreeConfiguration(); 	// Storage of tree configaration	

var columnsTree, rowsTree, hiddenTree, groupsTree; // Headers trees
var indexesTree, indexesMathTree; // Add data trees
var classesTree, elementsTrees, unitsTree; // Data filters trees

var folderActions 	= new FolderActions();
var objectActions 	= new ObjectActions();
var exportActions 	= new ExportActions();
var headerActions 	= new HeaderActions();
var spreadActions 	= new SpreadsheetActions();
var linksActions 	= new LinksActions();
var themeActions 	= new ThemeActions();
var styleActions 	= new StyleActions();

var spread, sheet; // Spreadsheet and Active sheet

var _o = observer, 
	_h = helpers,
	_s = spread,
	_ss = sheet;

var _state; 		// Bbq state

// 
// Document Ready
// 
$(function() {
	$(window).bind( 'load', loadHandler);
	$(window).bind( 'hashchange', hashChangeHandler);
	// setTemplate('#t-open-plate', 'open', initOpenTemplate);
})

var loadHandler = function(){
	_state = $.bbq.getState();
	if (!_.isEmpty(_state)){
		if (!_.isUndefined(_state.object)){
			observer.setObjectId(_state.object);
			if (!_.isUndefined(_state.act))
				if (_state.act == 'edit') window.dispatchEvent(new Event("editMode"));
		}
	} else {
		setTemplate('#t-open-plate', 'open', initOpenTemplate);		
	}
}

function hashChangeHandler(){
	_state = $.bbq.getState();
	if (!_.isEmpty(_state)){
		if (!_.isUndefined(_state.object)){
			observer.setObjectId(_state.object);
			if (!_.isUndefined(_state.act))
				if (_state.act == 'edit') window.dispatchEvent(new Event("editMode"));
		}
	} else {
		console.log("openMode");
	}
}

function initOpenTemplate(){
	console.log("initOpenTemplate");
	$('#t-root > *').addClass('hide');
	$('#t-open-plate').removeClass('hide');
	$('#t-folders').treegrid({
		toolbar:[{
				text:'Новая папка',
				iconCls:'fa fa-folder-o',
				handler: createFolderAtRoot
			}
		],
		animate:true,
        fitColumns:true,
        fit:true,
        singleSelect:false,
        ctrlSelect:true,
		idField:'id',
		treeField:'name',
		url: '/?mod=objects&act=showfolderslist',
		method: 'get',
		onLoadSuccess: function(row) {
            	$('#t-folders').treegrid('enableDnd', row?row.id:null);
            },
        onBeforeEdit: function(row) {
	        	$('#t-folders').treegrid('disableDnd', row?row.id:null);
	        },
	    onEndEdit: function(index, row, changes) {
	    		if (!_.isEmpty(changes)){
		        	var params = {};
						params['name'] = changes.name;
					if (row.type == 'document'){
						var id = row.id;
			        	objectActions.update(id, params).done(function(data) {
			        		if (data) {
				        		showMessage("Изменения сохранены.");
			        		} else {
			        			showMessage("Не удалось сохранить изменения.");
			        		}
			        		$('#t-folders').treegrid('enableDnd', row?row.id:null);
			        	});
					}
					if (row.type == 'folder') {
		        		var id = row.id.replace(/F/, '');
			        	folderActions.update(id, params).done(function(data) {
			        		if (data) {
				        		showMessage("Изменения сохранены.");
			        		} else {
			        			showMessage("Не удалось сохранить изменения.");	
			        		}
			        		$('#t-folders').treegrid('enableDnd', row?row.id:null);
			        	});
					}
	    		}
	        },
	    onCancelEdit: function(index, row) {
	        	$('#t-folders').treegrid('enableDnd', row?row.id:null);
	        },
		onClickRow: function(row) {
				if (row.type == 'folder')
					$('#t-folders').treegrid('expand', row.id);
			},
		onDblClickRow: function(row) {
				if (row.type == 'document') openTable(row.id);	
			},
		onDragOver: function(targetRow, sourceRow) {
				if (targetRow.type == 'document') return false;
				if (targetRow.type == 'folder') $('#t-folders').treegrid('expand', targetRow.id);
			},
		onDrop: function(targetRow, sourceRow, point) {
				var params = {};
					params['parent'] = targetRow.id.replace(/F/, '');
				if (sourceRow.type == 'document'){
					var id = sourceRow.id;
		        	objectActions.update(id, params).done(function(data) {
		        		showMessage("Изменения сохранены.");
		        	});
				}
				if (sourceRow.type == 'folder') {
					var id = sourceRow.id.replace(/F/, '');
		        	folderActions.update(id, params).done(function(data) {
		        		showMessage("Изменения сохранены.");
		        	});
				}
			},
		onContextMenu: function(e,row) {
				e.preventDefault();
				$('#t-folders').treegrid('select', row.id);
				$('#t-folders-contextmenu').menu('show', {left: e.pageX, top: e.pageY});
			}
	});
	$('#t-folders-contextmenu').menu({
		onShow: function() {
			foldersContextMenuFilter($(this))
		},
		onClick: function(item) {
			foldersContextMenuHandler(item)
		},
	});
}

function foldersContextMenuFilter(menu)
{
	var tf = $('#t-folders'),
		selected = tf.treegrid('getSelected');
	menu.find('.only-folders').each(function(){
		tf.menu(selected.type == 'folder' ? 'enableItem' : 'disableItem', this);
	})
	menu.find('.only-documents').each(function(){
		tf.menu(selected.type == 'document' ? 'enableItem' : 'disableItem', this);
	})
}

function foldersContextMenuHandler(item)
{
	var tf = $('#t-folders'),
		selected = tf.treegrid('getSelected');
	switch(item.name)
	{
		case 'openTable':
			openTable(selected.id);
			break;
		case 'remove':
			if (selected.type == 'document'){
				$.messager.confirm('Подтверждение удаление', 'Подтвердите, что хотите удалить эту таблицу.', function(confirm){
	                if (confirm) removeTable();
	            });
			}
			if (selected.type == 'folder'){
				$.messager.confirm('Подтверждение удаление', 'Только пустая папка может быть удалена. Подтвердите, что хотите удалить эту папку?', function(confirm){
	                if (confirm) removeFolder();
	            });				
			}
			break;
		case 'rename':
			treegridBeginEdit(tf, selected.id);			
			break;
		case 'export':
			showMessage("Пока не работает");
			break;
		case 'createFolder':
			createFolder();
			break;
		case 'createTable':
			createTable();
			break;
		default:
			showMessage("Пока не работает");
	}
}

function createTable()
{
	var tf = $('#t-folders'),
		selected = tf.treegrid('getSelected'),
		name = 'Новая таблица',
		parent = (selected) ? selected.id : null; 
	var params = {};
		params['name'] = name;
		params['parent'] = (parent) ? parent.replace(/F/, '') : '0';
	objectActions.create(params).done(function(data){
		if(data) {
			tf.treegrid('append', {
				parent: parent,
				data: [{
					id: data.id,
					name: name,
					type: data.type,
					author: data.author,
					created: data.created,
					updated: data.updated
				}]
			});
			showMessage("Вы успешно создали таблицу.");
			openTable(data.id);
		} else {
			showMessage("Не удалось создать таблицу.");
		}
	});
}

function createFolderAtRoot()
{
	$('#t-folders').treegrid('unselectAll');
	createFolder();
}

function createFolder()
{
	var tf = $('#t-folders'),
		selected = tf.treegrid('getSelected'),
		name = 'Новая папка',
		parent = (selected) ? selected.id : null; 
	var params = {};
		params['name'] = name;
		params['parent'] = (parent) ? parent.replace(/F/, '') : '0';
	folderActions.create(params).done(function(data){
		if(data) {
			tf.treegrid('append', {
				parent: parent,
				data: [{
					id: data.id,
					name: name,
					type: data.type,
					author: data.author,
					created: data.created,
					updated: data.updated
				}]
			});
			showMessage('Вы успешно создали папку.');
			treegridBeginEdit(tf, data.id);
		} else {
			showMessage('Не удалось создать папку.');
		}
	});
}

function openTable(id)
{
	$.bbq.pushState({act: "edit", object: id}, 2);
}

function removeFolder()
{
	var tf = $('#t-folders'),
		selections = tf.treegrid('getSelections');
	_.each(selections, function(selection){
		var id = selection.id.replace(/F/, '');
		folderActions.remove(id).done( function(data) {
			if (data) {
				tf.treegrid('remove', selection.id);
				showMessage('Вы успешно удалили папку.');
			} else {
				showMessage('Не удалось удалить папку.');
			}
		});
	})
}

function removeTable()
{
	var tf = $('#t-folders'),
		selections = tf.treegrid('getSelections');
	_.each(selections, function(selection){
		var id = selection.id;
		objectActions.remove(id).done( function(data) {
			if (data) {
				tf.treegrid('remove', selection.id);
				showMessage('Вы успешно удалили таблицу.');
			} else {
				showMessage('Не удалось удалить таблицу.');
			}
		});
	})
}

function treegridBeginEdit(treegrid, id)
{
	treegrid.treegrid('beginEdit', id);
	$('input.datagrid-editable-input').focus().select().keydown( function(e){
		e = e || window.event;
		switch(e.keyCode){
			case 27: //Esc
				treegrid.treegrid('cancelEdit', id);
				break;
			case 13: //Enter
				treegrid.treegrid('endEdit', id);
				break;
		}
	});
}

function initEditTemplate()
{
	$('#t-root > *').addClass('hide');
	$('#t-edit-plate').removeClass('hide');
	setTemplate('#t-edit-plate', 'edit', function(target){
		console.log("initEditTemplate");
		$('#t-layout').layout();
		$('#t-layout-headers').layout();
		$('#t-layout-headers .easyui-tabs').tabs();
		$('#t-layout .easyui-menubutton').menubutton();
		$('#t-layout .easyui-linkbutton').linkbutton();
		$('#t-layout .easyui-splitbutton').splitbutton();
		$('#t-layout .easyui-datebox').datebox({
			months: ['Янв', 'Фев', 'Март', 'Апр', 'Мая', 'Июнь', 'Июль', 'Авн', 'Сен', 'Окт', 'Ноя', 'Дек'],
			formatter: function(date){
				var y = date.getFullYear();
	            var m = date.getMonth()+1;
	            var d = date.getDate();
	            // return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	            return y+'/'+(m<10?('0'+m):m);
			},
			parser: function(s){
				if (!s) return new Date();
	            var ss = (s.split('/'));
	            var y = parseInt(ss[0],10);
	            var m = parseInt(ss[1],10);
	            // var d = parseInt(ss[2],10);
	            var d = '01';
	            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
	                return new Date(y,m-1,d);
	            } else {
	                return new Date();
	            }
			}
		});

		$('#t-layout .easyui-accordion').accordion();
		
		// 
		// Spreadsheet
		// 
		var height = $('#t-layout').layout('panel', 'center').height();
		$("#t-spreadsheet").css({"height": height});
		$("#t-spreadsheet").wijspread({sheetCount:1}); 
		var spread = $("#t-spreadsheet").wijspread("spread"); 
		var sheet = spread.getActiveSheet();

		var id = observer.getObjectId();
		var params = {};
			params["needTableSize"] = true;
		spreadActions.show(id, params).done( function(data){
			if (data){
				helpers.spreadsheet.setTableSize(data.size);
				helpers.spreadsheet.fromJSON(spread, data, true);
				sheet = spread.getActiveSheet();
			} else {
				observer.setTableSize({});
				spread.fromJSON({});
				sheet = spread.getActiveSheet();
			}
		});

	 	spread.useWijmoTheme = true;
		spread.canUserDragDrop(true);
		sheet.selectionPolicy($.wijmo.wijspread.SelectionPolicy.MultiRange);
		spread.bind($.wijmo.wijspread.Events.SelectionChanged, function (e, info) {
			var selectionMode = helpers.spreadsheet.getSelectionMode();
			if (selectionMode == "default") {
				selectionChangedHandler(e, info);
			} else {
				var value = helpers.spreadsheet.getSelectionValue(info.sheet);
				console.log("Selection mode", helpers.spreadsheet.getSelectionMode(), value);
				helpers.spreadsheet.selectionTargetSetValue(value);
				helpers.spreadsheet.toggleOutSelectionModeSource();
				helpers.spreadsheet.setSelectionMode("default");
			}
		});
		
		$(".change-selection-mode.toggle-button").click(function() {
			if ($(this).hasClass("toggle-in")) {
				helpers.spreadsheet.toggleOutSelectionModeSource();
				helpers.spreadsheet.setSelectionMode("default");
			} else {
				spread.getActiveSheet().clearSelection();
				helpers.spreadsheet.setSelectionModeSource($(this).attr("id"));
				helpers.spreadsheet.toggleInSelectionModeSource();
				helpers.spreadsheet.setSelectionTarget($(this).data("target"));
				helpers.spreadsheet.setSelectionMode($(this).data("mode"));
			}
		});

		var selectionChangedHandler = function(e, info) {
			var selection = _.last(info.sheet.getSelections());
			var tableSize = helpers.spreadsheet.getTableSize();

	    	var backColor = sheet.getCell(selection.row, selection.col).backColor();
	    	var foreColor = sheet.getCell(selection.row, selection.col).foreColor();
			console.log(backColor, foreColor);
			
			helpers.form.hideAllWrappers();
			if (helpers.spreadsheet.inTableSize(selection)) {
				helpers.form.showWrapperFunction();
				helpers.form.setRadioAssignType("cells");
				var fields = [];
				var rowsRange = _.range(selection.row - tableSize.top, selection.row - tableSize.top + selection.rowCount);
				var colsRange = _.range(selection.col - tableSize.left, selection.col - tableSize.left + selection.colCount);
				_.each(rowsRange, function(row){
					_.each(colsRange, function(col){
						fields = fields.concat(observer.linksTable.getFieldsByOrder(col + "", row + ""));
					});
				});
				var functions 	= _.unique(_.pluck(fields, "func"));
				var formats 	= _.unique(_.pluck(fields, "format"));
				var suffixes 	= _.unique(_.pluck(fields, "suffix"));
				var precisions 	= _.unique(_.pluck(fields, "precision"));
				var hiddens 	= _.unique(_.map(_.pluck(fields, "hidden"), function(i){ if (i) return true; else return false; }));
				if (functions.length == 1) helpers.form.setInputFunction(_.first(functions));
				else helpers.form.setInputFunction("");
				if (formats.length == 1) helpers.form.setSelectFormat(_.first(formats));
				else helpers.form.setSelectFormat("");			
				if (suffixes.length == 1) helpers.form.setInputSuffix(_.first(suffixes));
				else helpers.form.setInputSuffix("");
				if (precisions.length == 1) helpers.form.setSelectPrecision(_.first(precisions));
				else helpers.form.setSelectPrecision("");
				if (hiddens.length == 1) helpers.form.setCheckboxHidden(_.first(hiddens));
				else helpers.form.setCheckboxHidden(false);

			} else if (helpers.spreadsheet.inHeadersSize(selection)){
		        if (selection.colCount == 1 && selection.rowCount == 1){
		        	helpers.tree.selectNodeByOrder($("#header-rows-tree"), selection.row - tableSize.top);
		        	helpers.tree.selectNodeByOrder($("#header-cols-tree"), selection.col - tableSize.left);
		        }
		        if (selection.colCount > 1 && selection.rowCount == 1){
		        	helpers.tree.selectNodeByOrder($("#header-rows-tree"), selection.row - tableSize.top);
		        	$("#header-cols-tree").jstree(true).deselect_all();
		        }
		        if (selection.colCount == 1 && selection.rowCount > 1){
		        	helpers.tree.selectNodeByOrder($("#header-cols-tree"), selection.col - tableSize.left);
		        	$("#header-rows-tree").jstree(true).deselect_all();
		        }
			}		
		}

		headerActions.show(observer.getObjectId()).done( function(data){
			if (data){
				initHeadersTrees(data);
			} else {
				initEmptyHeadersTrees();
			}
			initIndexesTree();
		});

		// $('#t-common-properties').propertygrid();
		// $('#t-additional-properties').propertygrid();
		var commonProperties = {},
			additionalProperties =  {},
			commonPropertiesList = 
			[
				{
					'id':'sequence',
					'name':'Конвертировать',
					'value':'',
					'group':'Данные',
					'editor':__INFOGEDIT_PROPERTYGRID_EDITORS.bool 
				},
				{
					'id':'subscription',
					'name':'Подпись',
					'value':'',
					'group':'Подписи',
					'editor':'textbox'
				},
				{
					'id':'source',
					'name':'Источник',
					'value':'',
					'group':'Подписи',
					'editor':'textbox'
				},
				{
					'id':'units',
					'name':'Единицы измерения',
					'value':'',
					'group':'Подписи',
					'editor':'textbox'
				},
				{
					'id':'theme',
					'name':'Тема',
					'value':'',
					'group':'Оформление',
					'editor': {
						'type':'combobox',
				        'options':{
							// 'url':'combobox_data.json',
						    'valueField':'id',
						    'textField':'text',
						    'data': [{id: 'java', text: 'Java'},
						    		{id: 'perl', text: 'Perl'},
						    		{id: 'ruby', text: 'Ruby'}]
				        }
					} 
				},
				{
					'id':'styles',
					'name':'Специальное',
					'value':'',
					'group':'Оформление',
					'editor':__INFOGEDIT_PROPERTYGRID_EDITORS.specialStyles
				},
				{
					'id':'sort',
					'name':'Сортировать',
					'value':'',
					'group':'Сортировка таблицы',
					'editor':__INFOGEDIT_PROPERTYGRID_EDITORS.bool 
				},
				{
					'id':'columnsSort',
					'name':'Столбец сортировки',
					'value':'',
					'group':'Сортировка таблицы',
					'editor':'numberbox'
				},
				{
					'id':'rowsSort',
					'name':'Строка исключения',
					'value':'',
					'group':'Сортировка таблицы',
					'editor': 'numberbox'
				},
				{
					'id':'order',
					'name':'Порядок',
					'value':'',
					'group':'Сортировка таблицы',
					'editor':__INFOGEDIT_PROPERTYGRID_EDITORS.sortDirectionShort
				},
			],
			additionalPropertiesList = [];
		$.extend(commonProperties, __INFOGEDIT_PROPERTYGRID_VARIANTS.easyui);
		$.extend(additionalProperties, __INFOGEDIT_PROPERTYGRID_VARIANTS.easyui);
		commonProperties.init('#t-common-properties', {
			toolbar: [{
				text: 'Сохранить',
				handler: function(){
					// var idtx = [];
					// $.each(commonProperties._dom.propertygrid('getRows'), function(){idtx.push(this.id)})
					// that._findPresets(idtx.join(','));
					// console.log(idtx);
				}
			}]
		});
		additionalProperties.init('#t-additional-properties');

		var emptyTreeBehaviour = {
	    	readyTree: 	function(e){},
	    	loadAll: function(e, data){},
	    	selectNode: function(e, data){},
	    	createNode: function(e, data){},
	    	removeNode: function(e, data){},
	    	renameNode: function(e, data){},
	    	moveNode: 	function(e, data){}
	    }

		var columnsTreeBehaviour = {
			readyTree: function(e){
				observer.initColumnsTreeLeafs(columnsTree);
				columnsTree.jstree(true).open_all();
			},
			loadAll: function(e, data){},    	
			selectNode: function(e, data){
				var text = "", format = "", suffix = "", precision = 0;
				var id  = helpers.tree.getIdOnSelect(data);
				var order = helpers.tree.getOrderById(id, columnsTree);
				var type = helpers.tree.getTypeOnSelect(data);
				observer.setLastSelected("column", order, type);
				helpers.form.hideAllWrappers();
				if (!_.isUndefined(data.event)) helpers.spreadsheet.selectColumn(spread, sheet, order);
				switch (type){
					case "index":
					case "link":
					case "function":
					case "date":
						if (!helpers.tree.hasChildrenOnSelect(data)){
							helpers.form.showWrapperFunction();
							helpers.form.setRadioAssignType("column");

							text = observer.linksTable.getFuncByOrderColumn(order);
							format = observer.linksTable.getFormatByOrderColumn(order);
							suffix = observer.linksTable.getSuffixByOrderColumn(order);
							precision = observer.linksTable.getPrecisionByOrderColumn(order);
							hidden = observer.linksTable.getHiddenByOrderColumn(order);
							
							helpers.form.setInputFunction(text);
							helpers.form.setSelectFormat(format);
							helpers.form.setInputSuffix(suffix);
							helpers.form.setSelectPrecision(precision);
							helpers.form.setCheckboxHidden(hidden);
						}
					break;
				}			
			},
			createNode: function(e, data){
				console.log("----------- On createNode Columns-----------");
				var newOrder = helpers.tree.getTreeLeafs(columnsTree);
				observer.reorderColumnsFull(newOrder);
				var id = helpers.tree.getIdOnCreate(data);
				var order = helpers.tree.getOrderById(id, columnsTree);
				switch (helpers.tree.getTypeOnCreate(data)){
					case "link":
					case "index":
						observer.addOrderColumnNeedAddField(id, order);
					break;
					case "date":
						var parent = helpers.tree.getParentWithType(data, ["index", "link"], columnsTree);
						var date = data.node.text;
						var step = helpers.tree.getParentStep(parent, columnsTree);
						data.node.li_attr["step"] = step;
						observer.addDate(parent, date, order);
					break;
					case "function":
						var parent = helpers.tree.getParentWithType(data, ["index", "link"], columnsTree);
						var func = "val(shift)";
						var step = helpers.tree.getParentStep(parent, columnsTree);
						data.node.li_attr["step"] = step;
						observer.addFunc(parent, func, order);
					break;
					case "field":
						var parent = helpers.tree.getParentWithType(data, ["index", "link"], columnsTree);
						var step = helpers.tree.getParentStep(parent, columnsTree);
						data.node.li_attr["step"] = step;
						observer.addField(parent, order);
					break;
				}
			},
			removeNode: function(e, data){
				console.log("----------- On removeNode Columns-----------");
				var id = helpers.tree.getIdOnRemove(data);
				var oldOrder = observer.getColumnsTreeLeafs();
				var order = helpers.tree.getOrderById(id, columnsTree, oldOrder);
				var bringParentBack = false;
				switch (helpers.tree.getTypeOnRemove(data)){
					case "link":
					case "index":
						observer.onRemoveColumn(id, order);
					break;
					case "date":
					case "function":
					case "field":
						var parent = helpers.tree.getParentOnRemove(data);
						var children = helpers.tree.getChildrenById(parent, columnsTree);
						if (_.isEmpty(children)) bringParentBack = true;
						observer.linksTable.removeColumns(order);
					break;				
				}
				var newOrder = helpers.tree.getTreeLeafs(columnsTree);
				observer.reorderColumnsFull(newOrder, bringParentBack);
				window.dispatchEvent(new Event("refreshSpreadsheet"));
			},
			renameNode: function(e, data){
				console.log("----------- On renameNode Columns -----------");
				switch (helpers.tree.getTypeOnRename(data)){
					case "date":
						var id = helpers.tree.getIdOnRename(data);
						var date = helpers.tree.getNameOnRename(data);
						var order = helpers.tree.getOrderById(id, columnsTree);
						observer.linksTable.setDateByOrderColumn(order, date);
					break;
				}
				window.dispatchEvent(new Event("refreshSpreadsheet"));
			},
			moveNode: 	function(e, data){
				console.log("----------- On moveNode Columns-----------", data);
				var newOrder = helpers.tree.getTreeLeafs(columnsTree);
				observer.reorderColumnsFull(newOrder);
			}
		}

		var rowsTreeBehaviour = {
			readyTree: function(e){
				observer.initRowsTreeLeafs(rowsTree);
				rowsTree.jstree(true).open_all();
			},
			loadAll: function(e, data){},		
			selectNode: function(e, data){
				var id  = helpers.tree.getIdOnSelect(data);
				var order = helpers.tree.getOrderById(id, rowsTree);
				var type = helpers.tree.getTypeOnSelect(data);
				helpers.form.hideAllWrappers();
				if (!_.isUndefined(data.event)) helpers.spreadsheet.selectRow(spread, sheet, order);
				switch (type){
					case "field":
						observer.setLastSelected("static-group", id, type);
						helpers.form.showWrapperStaticGroup();
						var func = observer.staticGroupAttributes.getRowFunc(id);
						console.log(id, func);
						helpers.form.setSelectFuncStaticGroup(func);
					break;
				}
			},
			createNode: function(e, data){
				console.log("----------- On createNode Rows-----------");
				var newOrder = helpers.tree.getTreeLeafs(rowsTree);
				observer.reorderRowsFull(newOrder);
				switch (helpers.tree.getTypeOnCreate(data)){
					case "link":
					case "index":
						var id = helpers.tree.getIdOnCreate(data);
						var order = helpers.tree.getOrderById(id, rowsTree);
						observer.addOrderRowNeedAddField(id, order);
					break;
					case "field":
						var id = helpers.tree.getIdOnCreate(data);
						observer.staticGroupAttributes.addGroupRow(id);
					break;
				}
			},
			removeNode: function(e, data){
				console.log("----------- On removeNode Rows-----------");
				var id = helpers.tree.getIdOnRemove(data);
				var oldOrder = observer.getRowsTreeLeafs();
				var order = helpers.tree.getOrderById(id, rowsTree, oldOrder);
				var bringParentBack = false;
				switch (helpers.tree.getTypeOnRemove(data)){
					case "link":
						observer.onRemoveRow(id, order);
					break;
					case "field":
						observer.staticGroupAttributes.removeGroupRow(id);
					break;
				}
				var newOrder = helpers.tree.getTreeLeafs(rowsTree);
				observer.reorderRowsFull(newOrder, bringParentBack);
				window.dispatchEvent(new Event("refreshSpreadsheet"));
			},
			renameNode: function(e, data){
				console.log("----------- On renameNode Rows-----------");
				var nodes = helpers.tree.getSelectedNodes(rowsTree);
				if (data.text != data.old && nodes.length == 1)
					window.dispatchEvent(new Event("refreshSpreadsheet"));
			},
			moveNode: 	function(e, data){
				console.log("----------- On moveNode Rows-----------");
				var newOrder = helpers.tree.getTreeLeafs(rowsTree);
				if (!helpers.tree.isEqualParentsOnMove(data)) {
					var level = helpers.tree.getNodeLevel(data);
					data.node.li_attr["level"] = level - 1;
				}
				observer.reorderRowsFull(newOrder);
			}
		}

		var hiddenTreeBehaviour = {
			readyTree: function(e){},
			loadAll: function(e, data){},    	
			selectNode: function(e, data){
				var id  = helpers.tree.getIdOnSelect(data);
				var type = helpers.tree.getTypeOnSelect(data);
				helpers.form.hideAllWrappers();
				switch(type){
					case "default":
					case "linkgroup":
						observer.setLastSelected("hidden", id, type);
						helpers.form.showWrapperNameSpecial();
						var nameSpecial = observer.linksHidden.getNameSpecialByLink(id);
						helpers.form.setInputNameSpecial(nameSpecial);
					break;
				}			
			},
			createNode: function(e, data){},
			removeNode: function(e, data){
				switch (helpers.tree.getTypeOnRemove(data)){
					case "default":
					case "linkgroup":
						var id = helpers.tree.getIdOnRemove(data);
						observer.linksHidden.removeLink(id);
					break;
				}
			},
			renameNode: function(e, data){},
			moveNode: function(e, data){}		
		}

		var groupsTreeBehaviour = {
			readyTree: function(e){},
			loadAll: function(e, data){},    	
			selectNode: function(e, data){
				var id  = helpers.tree.getIdOnSelect(data);
				var type = helpers.tree.getTypeOnSelect(data);
				helpers.form.hideAllWrappers();
				switch(type){
					case "default":
						observer.setLastSelected("group", id, type);
						var column = observer.groupAttributes.getGroupColumn();
						var min = observer.groupAttributes.getRowMin(id);
						var max = observer.groupAttributes.getRowMax(id)
						var func = observer.groupAttributes.getRowFunc(id);
						console.log("Group selected", column, min, max, func);
						helpers.form.showWrapperGroup();
						helpers.form.setInputColumnGroup(column);
						helpers.form.setInputMinGroup(min);
						helpers.form.setInputMaxGroup(max);
						helpers.form.setSelectFuncGroup(func);
					break;
				}
			},
			createNode: function(e, data){
				switch (helpers.tree.getTypeOnCreate(data)){
					case "default":
						var id = helpers.tree.getIdOnCreate(data);
						observer.groupAttributes.addGroupRow(id);
					break;
				}
			},
			removeNode: function(e, data){
				switch (helpers.tree.getTypeOnRemove(data)){
					case "default":
						var id = helpers.tree.getIdOnRemove(data);
						observer.groupAttributes.removeGroupRow(id);
					break;
				}
				if (helpers.tree.isEmptyTree(groupsTree))
					observer.groupAttributes.clearGroups();
			},
			renameNode: function(e, data){
				switch (helpers.tree.getTypeOnRename(data)){
					case "default":
						var id = helpers.tree.getIdOnCreate(data);
						var name = helpers.tree.getNameOnRename(data);
						observer.groupAttributes.setRowName(id, name);
					break;
				}
				window.dispatchEvent(new Event("refreshSpreadsheet"));
			},
			moveNode: function(e, data){}
		}

	    var initHeadersTrees = function(data) {
	    	if (columnsTree) columnsTree.jstree("destroy");
	    	if (rowsTree) rowsTree.jstree("destroy");
	    	if (hiddenTree) hiddenTree.jstree("destroy");
	    	if (groupsTree) groupsTree.jstree("destroy");

	    	columnsTree = new Tree({id: "header-cols", config: treeConfig.getConfig("header-cols-edit", data.columns), tools:{}, helper:{}, behavior: columnsTreeBehaviour});
	    	columnsTree.bind("select_node.jstree", function (e, data){
	    		$(this).jstree("open_node", data.node);	
	    	});
	    	
	    	rowsTree = new Tree({id: "header-rows", config: treeConfig.getConfig("header-rows-edit", data.rows), tools:{}, helper:{}, behavior: rowsTreeBehaviour});
	    	rowsTree.bind("select_node.jstree", function (e, data){
	    		$(this).jstree("open_node", data.node);
	    	});

	    	hiddenTree = new Tree({id: "header-hidden", config: treeConfig.getConfig("links-hidden-edit", data.hidden), tools:{}, helper:{}, behavior: hiddenTreeBehaviour});
	    	hiddenTree.bind("select_node.jstree", function (e, data){
	    		$(this).jstree("open_node", data.node);
	    	});

	    	groupsTree = new Tree({id: "header-groups", config: treeConfig.getConfig("headers-edit", data.groups), tools:{}, helper:{}, behavior: groupsTreeBehaviour});
	    	groupsTree.bind("select_node.jstree", function (e, data){
	    		$(this).jstree("open_node", data.node);	
	    	});

	    	observer.setColumnsTree(columnsTree);
	    	observer.setRowsTree(rowsTree);
	    	observer.setHiddenTree(hiddenTree);
	    	observer.setGroupsTree(groupsTree);
	    	observer.setHeaders(_.omit(data, "columns", "rows", "hidden", "groups", "lastDate"));
	    	find = find && find.length ? find : null;
	    	var commonPropertiesValues = observer.getCommonProperties(),
	    		additionalPropertiesValues = observer.getAdditionalProperties();
	    	console.log(commonPropertiesValues, additionalPropertiesValues);
	    	commonProperties.showPropertiesFor('Common', 'default', commonPropertiesValues, {}, find, commonPropertiesList);
	    	$(document).bind("objectPropertyChanged", function(e, data) {
	    		observer.setCommonProperties(data);
	    	});
	    	additionalProperties.showPropertiesFor('Additional', 'default', additionalPropertiesValues, {}, find, additionalProperties);
	    }
		var initEmptyHeadersTrees = function() {
			if (columnsTree) columnsTree.jstree("destroy");
			if (rowsTree) rowsTree.jstree("destroy");
			if (hiddenTree) hiddenTree.jstree("destroy");
			if (groupsTree) groupsTree.jstree("destroy");

			columnsTree = new Tree({id: "header-cols", config: treeConfig.getConfig("header-cols-edit", []), tools:{}, helper:{}, behavior: columnsTreeBehaviour});
			columnsTree.bind("select_node.jstree", function (e, data){	
				$(this).jstree("open_node", data.node);	
			});

			rowsTree = new Tree({id: "header-rows", config: treeConfig.getConfig("header-rows-edit", []), tools:{}, helper:{}, behavior: rowsTreeBehaviour});
			rowsTree.bind("select_node.jstree", function (e, data){	
				$(this).jstree("open_node", data.node);	
			});

			hiddenTree = new Tree({id: "header-hidden", config: treeConfig.getConfig("links-hidden-edit", []), tools:{}, helper:{}, behavior: hiddenTreeBehaviour});
			hiddenTree.bind("select_node.jstree", function (e, data){	
				$(this).jstree("open_node", data.node);	
			});
			
			groupsTree = new Tree({id: "header-groups", config: treeConfig.getConfig("headers-edit", []), tools:{}, helper:{}, behavior: groupsTreeBehaviour});
			groupsTree.bind("select_node.jstree", function (e, data){	
				$(this).jstree("open_node", data.node);	
			});

			observer.setColumnsTree(columnsTree);
			observer.setRowsTree(rowsTree);
			observer.setHiddenTree(hiddenTree);
			observer.setGroupsTree(groupsTree);

			observer.clearHeaders();
			helpers.spreadsheet.setTableSize({});
			spread.fromJSON({});
			sheet = spread.getActiveSheet();
			find = find && find.length ? find : null;
			var commonPropertiesValues = observer.getCommonProperties(),
				additionalPropertiesValues = observer.getAdditionalProperties();
			console.log(commonPropertiesValues, additionalPropertiesValues);
			commonProperties.showPropertiesFor('Common', 'default', commonPropertiesValues, {}, find, commonPropertiesList);
			additionalProperties.showPropertiesFor('Additional', 'default', additionalPropertiesValues, {}, find, additionalProperties);
		}
		
	    var indexesTreeBehaviour = {
	    	readyTree: function(e){},
	    	loadAll: function(e, data){},    	
	    	selectNode: function(e, data){
	    		switch(helpers.tree.getTypeOnSelect(data)){
	    			case "folder":
			    		helpers.tips.setState("indexSelected");
			    		var path = helpers.tree.getIdOnSelect(data).replace("node_", "");
			    		linksActions.show(path).done(function(data){
			    			if(data){
			    				console.log(data);
			    				if (!_.isEmpty(data.classes)){
					    			observer.setIndexId(data.indexId);
					    			observer.setElementsNames(data.elements);
					    			observer.setElementsMap(data.elementsMap);
			    			
					    			observer.setClassesNames(data.classes);
					    			observer.setClassesMap(data.classesMap);
					    			
					    			observer.initUnitsMap(data.unitsMap);
					    			observer.setLinksMap(data.linksMap);
					    			observer.setLinksSteps(data.linksSteps);
					    			observer.setLinksFilterUnits(data.units);
					    			
					    			$("#dialog-links-filter .dialog-tree-wrapper").each(function(i){
					    				i ++;
					    				if($.jstree.reference("elements-" + i + "-tree")) $.jstree.reference("elements-" + i + "-tree").destroy();
					    			}).addClass("hide");

					    			var counter = 1;
					    			_.each(data.elementsTrees, function(tree, classId){	
					    				$("#elements-" + counter + "-tree").parent(".dialog-tree-wrapper").removeClass("hide");
					    				$("#class-elements-" + counter).text(data.classes[classId]);
					    				new Tree({id: "elements-" + counter, config: treeConfig.getConfig("elements-checkbox", tree), tools:{}, helper:{}, behavior: emptyTreeBehaviour}); 
					    				++ counter;
					    			});

					    			if($.jstree.reference("units-tree")) $.jstree.reference("units-tree").destroy();
					    			$("#units-tree").parent(".dialog-tree-wrapper").removeClass("hide");
					    			new Tree({id: "units", config: treeConfig.getConfig("elements-checkbox", data.unitsTree), tools:{}, helper:{}, behavior: emptyTreeBehaviour});
					    			
					    			$("#links-checkbox-title").html("Найдено " + data.linksTreeCount + " записей");
				    				if(linksCheckboxTree) linksCheckboxTree.jstree("destroy");
					    			if (data.linksTreeCount <= 100){			    					
						    			linksCheckboxTree = new Tree({id: "links-checkbox", config: treeConfig.getConfig("links-checkbox", data.linksTree), tools:{}, helper:{}, behavior: emptyTreeBehaviour});
					    			}	    			
					    		} else {
					    			$("#links-checkbox-title").html("Записей не найдено");
				    				if(linksCheckboxTree) linksCheckboxTree.jstree("destroy");
					    		}
			    			}
			    		});
						
		    		break;
	    		}
	    	},
	    	createNode: function(e, data){},
	    	removeNode: function(e, data){},
	    	renameNode: function(e, data){},
	    	moveNode: 	function(e, data){}    	
	    }

	    var indexesMathTreeBehaviour = {
	    	readyTree: function(e){},
	    	loadAll: function(e, data){},    	
	    	selectNode: function(e, data){
	    		// var link = helpers.tree.getLinkOnSelect(data);
	    		switch(helpers.tree.getTypeOnSelect(data)){
	    			case "default":
	    				observer.setMathIndex(data.node);
	    			break;
	    		}
	    	},
	    	createNode: function(e, data){},
	    	removeNode: function(e, data){},
	    	renameNode: function(e, data){},
	    	moveNode: 	function(e, data){},
	    }

	    var initIndexesTree = function(){
	    	if(indexesTree) indexesTree.jstree("destroy");
			indexesTree = new Tree({id: "indexes", config: treeConfig.getConfig("indexes"), tools:{}, helper:{}, behavior: indexesTreeBehaviour});
			indexesTree.bind("select_node.jstree", function (e, data){	$(this).jstree("open_node", data.node);	});
			
			if(indexesMathTree) indexesMathTree.jstree("destroy");
			indexesMathTree = new Tree({id: "indexes-math", config: treeConfig.getConfig("indexes-math"), tools:{}, helper:{}, behavior: indexesMathTreeBehaviour});
			indexesMathTree.bind("select_node.jstree", function (e, data){	$(this).jstree("open_node", data.node);	});
		}
		// 
		// Event refreshSpreadsheet 
		//
		var refreshSpreadsheet = function(){
			_state = $.bbq.getState("act");
			var id = observer.getObjectId();
			var params = {};
				params["headers"] = observer.getHeaders();
				params["lastDate"] = helpers.form.getSelectLastDate();
				params["needTableSize"] = true;
			spreadActions.show(id, params).done(function(data){
				if (data){
					helpers.spreadsheet.setTableSize(data.size);
					helpers.spreadsheet.fromJSON(spread, data, true);
					sheet = spread.getActiveSheet();
				}
			});
		}
		window.addEventListener("refreshSpreadsheet", refreshSpreadsheet);
		// 
		// Menu buttons
		// 
		$("#btnSaveTableFull").click(function(){
			var id = observer.getObjectId();
			var template = helpers.spreadsheet.getTemplate(spread);		
			var linksTable = _.unique(_.pluck(observer.linksTable.getTable(), "link"));
			var linksHidden = _.map(_.keys(observer.linksHidden.getLinks()), function(link){return link.replace("l_", "");});
			var params = {};
				params["headers"] = observer.getHeaders();
				params["template"] = JSON.stringify(template);
				params["links"] = _.union(linksTable, linksHidden);
			objectActions.update(id,params).done(function(data){
				if (data) showMessage("Таблица успешно сохранена.");
				else showMessage("Не удалось сохранить таблицу.");
			});
		});
		$("#btnSaveTable").click(function(){
			var id = observer.getObjectId();
			var linksTable = _.unique(_.pluck(observer.linksTable.getTable(), "link"));
			var linksHidden = _.map(_.keys(observer.linksHidden.getLinks()), function(link){return link.replace("l_", "");});
			var params = {};
				params["headers"] = observer.getHeaders();
				params["links"] = _.union(linksTable, linksHidden);
			objectActions.update(id,params).done(function(data){
				if (data) showMessage("Таблица успешно сохранена.");
				else showMessage("Не удалось сохранить таблицу.");
			});
		});
		$('#btnEditClose').click(function() {
			closeEditTemplate();
		});
		$("#btnRefresh").click(function(){
			window.dispatchEvent(new Event("refreshSpreadsheet"));
		});
		$('#btnStyles').click(function() {
			$('#dialog-style-tools').removeClass('hide').dialog({
				closed: false,
				modal: false,
			});
			$('#dialog-style-tools .easyui-linkbutton').linkbutton().tooltip({
				content: function(){
					return $(this).data("tooltip-text");
				}
			});
		});
		$("#btnClearStyles").click(function(){
			$.messager.confirm('Подтверждение', 'Для назначения автостилей будет удалено текущее оформление. Подтвердите, что хотите использовать автостили.', function(confirm){
                if (confirm) {
                	var id = observer.getObjectId();
                	spreadActions.remove(id).done(function(data){
                		window.dispatchEvent(new Event("refreshSpreadsheet"));
                	});
                }
            });
		});

		$("#btnMergeCells").click(function(){
			helpers.spreadsheet.mergeCells(sheet);
		});
		$("#btnUnmergeCells").click(function(){
			helpers.spreadsheet.unmergeCells(sheet);
		});

		$("#btnTextAlignLeft").click(function(){
			helpers.spreadsheet.setTextAlignLeft(sheet);
		});
		$("#btnTextAlignCenter").click(function(){
			helpers.spreadsheet.setTextAlignCenter(sheet);
		});
		$("#btnTextAlignRight").click(function(){
			helpers.spreadsheet.setTextAlignRight(sheet);
		});

		$("#btnTextIndentIncrease").click(function(){
			helpers.spreadsheet.increaseIndent(sheet);
		});
		$("#btnTextIndentDecrease").click(function(){
			helpers.spreadsheet.decreaseIndent(sheet);
		});
	});
}
window.addEventListener("editMode", initEditTemplate);

function closeEditTemplate()
{
	$('#t-root > *').addClass('hide');
	$('#t-open-plate').removeClass('hide');
	$('#t-edit-plate').empty();
	$.bbq.removeState();
}

function setTemplate(target, template, callback)
{
	var url = '/?mod=objects&act=tpl';
	$.ajax({
		method: 'post',
		url: url,
		data: {
			tpl: template
		},
		success: function(data){
			var tpl = Handlebars.compile(data);
			$(target).html(tpl);
			resizeWnd();
			if(callback) callback(target);
		}
	});
}