function Table(){
	var _self = this;

	var _table = []; // table = [{id:id, aliasRow:x, aliasColumn:y, orderRow:x1, orderColumn:y1, link:value, date:value, func:value, nameSpecial:value},{...} ...]
	this.getTable = function(){
		return _table;
	}
	this.setTable = function(table){
		_table = table;
	}
	this.test = function(){
		console.log("----------- Test Table -----------");
		console.log("getColumns", this.getColumns());
		console.log("getRows", this.getRows());
	}
	// 
	// Check Fields
	// 
	var _hasField = function(field){
		return _hasFieldFull(_.omit(field, "orderColumn", "orderRow"));
	}
	var _hasFieldFull = function(field){
		if (_.findWhere(_table, _.omit(field, "nameSpecial", "suffix", "precision"))) return true;
		else return false;
	}
	this.hasField = function(field){
		return _hasField(field);
	}
	// 
	// Get All
	// 
	this.getColumns = function(){
		var row = _.first(_.pluck(_table, "aliasRow"));
		var columns = this.getColumnsByRow(row);
		return columns;
	}
	this.getRows = function(){
		var column = _.first(_.pluck(_table, "aliasColumn"));
		var rows = this.getRowsByColumn(column);
		return rows;
	}
	// 
	// Add Field
	//
	this.addField = function(field){
		// console.log("Add Field", field);
		var table = this.getTable();
		var hasField = _hasField(field);
		if (!hasField) table.push(field);
		return hasField;
	}
	this.addFieldFull = function(field){
		// console.log("Add Field Full", field);
		var table = this.getTable();
		var hasFieldFull = _hasFieldFull(field);
		if (!hasFieldFull) table.push(field);
		return hasFieldFull;
	}
	this.addFieldDate = function(field, date, orderColumn){
		var fieldWithDate = _.extend({}, _.omit(field, "date", "func"));
		fieldWithDate["date"] = date;
		fieldWithDate["orderColumn"] = orderColumn;
		// console.log("addFieldDate", fieldWithDate, date);
		return this.addFieldFull(fieldWithDate);
	}
	this.addFieldFunc = function(field, func, orderColumn){
		var fieldWithFunc = _.extend({}, _.omit(field, "date", "func"));
		fieldWithFunc["func"] = func;
		fieldWithFunc["orderColumn"] = orderColumn;
		// console.log("addFieldFunction", fieldWithFunc, func);
		return this.addFieldFull(fieldWithFunc);
	}
	this.addFieldDateFunc = function(field, date, func, orderColumn){
		var fieldWithFunc = _.extend({}, _.omit(field, "date", "func"));
		fieldWithFunc["date"] = date;
		fieldWithFunc["func"] = func;
		fieldWithFunc["orderColumn"] = orderColumn;
		return this.addFieldFull(fieldWithFunc);
	}
	this.addFieldEmpty = function(field, orderColumn){
		var fieldWithEmpty = _.extend({}, _.omit(field, "date", "func"));
		fieldWithEmpty["orderColumn"] = orderColumn;
		return this.addFieldFull(fieldWithEmpty);
	}
	// 
	// Remove Field
	// 
	this.removeField = function(field){
		var table = this.getTable();
		this.setTable(_.without(table, field));
	}
	this.removeColumns = function(order){
		var fields = this.getFieldsByOrderColumn(order);
		console.log("----------- getFieldsByOrderColumn -----------", order, fields);
		_.each(fields, function(field){
			_self.removeField(field);
		})
	}
	this.removeRows = function(order){
		var fields = this.getFieldsByOrderRow(order);
		console.log("----------- getFieldsByOrderRow -----------", order, fields);
		_.each(fields, function(field){
			_self.removeField(field);
		})
	}
	// 
	// Get Fields By Alias
	// 	
	this.getFieldsByParams = function(params){
		var fields = _.where(_table, params); 
		return fields;
	}
	this.getFieldsByRow = function(alias){
		var alias = alias.toString();
		var feilds = this.getFieldsByParams({"aliasRow": alias});
		return feilds;
	}
	this.getFieldsByColumn = function(alias){
		var alias = alias.toString();
		var fields = this.getFieldsByParams({"aliasColumn": alias});
		return fields;
	}
	this.getFieldsByAlias = function(aliasRow, aliasColumn){
		var aliasRow = aliasRow.toString();
		var aliasColumn = aliasColumn.toString();
		var feilds = this.getFieldsByParams({"aliasColumn": aliasColumn, "aliasRow": aliasRow});
		return feilds;
	}
	// 
	// Get Fields by Order
	// 
	this.getFieldsByOrderColumn = function(order){
		var fields = this.getFieldsByParams({"orderColumn": order});
		return fields;	
	}
	this.getFieldsByOrderRow = function(order){
		var fields = this.getFieldsByParams({"orderRow": order});
		return fields;
	}
	this.getFieldsByOrder = function(orderColumn, orderRow){
		var fields = this.getFieldsByParams({"orderColumn": orderColumn, "orderRow": orderRow});
		return fields;
	}

	this.getFirstOrderFieldsByColumn = function(alias){
		var first = _.first(_.pluck(_self.getFieldsByColumn(alias), "orderColumn"));
		var fields = this.getFieldsByOrderColumn(first);
		return fields;
	}
	this.getFirstOrderFieldsByRow = function(alias){
		var first = _.first(_.pluck(_self.getFieldsByRow(alias), "orderRow"));
		var fields = this.getFieldsByOrderRow(first);
		return fields;
	}
	// this.getColumnsByParams = function(params){
	// 	var columns = _.pluck(this.getFieldsByParams(params), "aliasColumn");
	// 	return columns;
	// }
	this.getColumnsByRow = function(alias){
		var columns = _.pluck(this.getFieldsByRow(alias), "aliasColumn");
		return columns;
	}

	// this.getRowsByParams = function(params){
	// 	var rows = _.pluck(this.getFieldsByParams(params), "aliasRow");
	// 	return rows;
	// }	
	this.getRowsByColumn = function(alias){
		var rows = _.pluck(this.getFieldsByColumn(alias), "aliasRow");
		return rows;
	}
	// 
	// Get Field by Link
	// 
	this.getFieldsByLink = function(link){
		var fields = this.getFieldsByParams({"link": link});
		return fields;
	}
	this.getAliasColumnByLink = function(link){
		var alias = _.first(_.pluck(this.getFieldsByLink(link), "aliasColumn"));
		return alias;
	}
	this.getAliasRowByLink = function(link){
		var alias = _.first(_.pluck(this.getFieldsByLink(link), "aliasRow"));
		return alias;	
	}
	// 
	// 
	// 
	this.getDateByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "date");
		var date = _.first(fields);
		return date;	
	}
	this.setDateByOrderColumn = function(order, date){
		var fields = this.getFieldsByOrderColumn(order);
		_.each(fields, function(field){
			field.date = date;
		})
		console.log(fields);
	}
	// 
	// 
	// 
	this.getFuncByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "func");
		var func = _.last(fields);
		return func;
	}
	this.setFuncByOrderColumn = function(order, func){
		var fields = this.getFieldsByOrderColumn(order);
		_.each(fields, function(field){
			field.func = func;
		})
	}
	this.setFuncByOrder = function(orderColumn, orderRow, func){
		var fields = this.getFieldsByOrder(orderColumn, orderRow);
		_.each(fields, function(field){
			field.func = func;
		})
	}
	// 
	// 
	// 
	this.getFormatByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "format");
		var precision = _.first(fields);
		return precision;
	}
	this.setFormatByOrderColumn = function(order, format){
		if (format){
			var fields = this.getFieldsByOrderColumn(order);
			_.each(fields, function(field){
				field.format = format;
			})			
		}
	}
	this.setFormatByOrder = function(orderColumn, orderRow, format){
		if (format){
			var fields = this.getFieldsByOrder(orderColumn, orderRow);
			_.each(fields, function(field){
				field.format = format;
			})
		}
	}
	// 
	// 
	// 
	this.getSuffixByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "suffix");
		var suffix = _.first(fields);
		return suffix;
	}
	this.setSuffixByOrderColumn = function(order, suffix){
		var fields = this.getFieldsByOrderColumn(order);
		_.each(fields, function(field){
			field.suffix = suffix;
		})
	}
	this.setSuffixByOrder= function(orderColumn, orderRow, suffix){
		var fields = this.getFieldsByOrder(orderColumn, orderRow);
		_.each(fields, function(field){
			field.suffix = suffix;
		})
	}
	// 
	// 
	// 
	this.getPrecisionByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "precision");
		var precision = _.first(fields);
		return precision;
	}
	this.setPrecisionByOrderColumn = function(order, precision){
		if (precision){
			var fields = this.getFieldsByOrderColumn(order);
			_.each(fields, function(field){
				field.precision = precision;
			})			
		}
	}
	this.setPrecisionByOrder = function(orderColumn, orderRow, precision){
		if (precision){
			var fields = this.getFieldsByOrder(orderColumn, orderRow);
			_.each(fields, function(field){
				field.precision = precision;
			})			
		}
	}
	// 
	// 
	// 
	this.getHiddenByOrderColumn = function(order){
		var fields = _.pluck(this.getFieldsByOrderColumn(order), "hidden");
		var hidden = _.first(fields);
		return hidden;
	}
	this.setHiddenByOrderColumn = function(order, hidden){
		var fields = this.getFieldsByOrderColumn(order);
		_.each(fields, function(field){
			field.hidden = hidden;
		})
	}
	// 
	// 
	// 
	this.getNameSpecialByOrderRow = function(order){
		var fields = _.pluck(this.getFieldsByOrderRow(order), "nameSpecial");
		var nameSpecial = _.first(fields);
		return nameSpecial;
	}
	this.setNameSpecialByOrderRow = function(order, nameSpecial){
		var fields = this.getFieldsByOrderRow(order);
		_.each(fields, function(field){
			field.nameSpecial = nameSpecial;
		})	
	}
	// 
	// 
	// 
	this.getFuncByOrderRow = function(order){
		var fields = _.pluck(this.getFieldsByOrderRow(order), "func");
		var func = _.first(fields);
		return func;
	}
	this.setFuncByOrderRow = function(order, func){
		var fields = this.getFieldsByOrderRow(order);
		_.each(fields, function(field){
			field.func = func;
		})
	}
	// 
	// Reorder
	// 
	this.reorderColumnsFull = function(newOrder, oldOrder, bringParentBack){		
		var changeTable = {};
		var difference = _.difference(_.pluck(newOrder, "id"), _.pluck(oldOrder, "id"));
		console.log("----------- reorderColumnsFull -----------", bringParentBack);
		// console.log("oldOrder", oldOrder);
		// console.log("newOrder", newOrder);
		// console.log("difference", difference);
		if (!_.isEmpty(oldOrder) && !_.isEmpty(newOrder)){
			_.each(oldOrder, function(value, key){
				var changeFields = _self.getFieldsByOrderColumn(key.toString());
				if (_.isEmpty(changeFields) && bringParentBack){
					_.each(difference, function(id){
						var changeFields = _self.getFieldsByOrderColumn("a_" + id);
						var newKey = key.toString();
						// console.log("bringParentBack changeFields", changeFields);
						changeTable[newKey] = changeFields;
					});
				} else {
					var newKey = _.indexOf(newOrder, _.findWhere(newOrder, {id: value["id"]})).toString();
					// console.log("changeFields", changeFields);
					if (key != newKey) {
						if (newKey == -1){
							if (oldOrder.length == newOrder.length) changeTable["a_" + value["id"]] = changeFields;
						} else changeTable[newKey] = changeFields;
					}
				}
			});
		}

		if (oldOrder.length < newOrder.length){
			console.log("Добавили новую вершину.");			
			if (!_.isEmpty(oldOrder)){
			} else console.log("Дерево было пустым.");
		}
		if (oldOrder.length == newOrder.length){
			console.log("Замена родителя на ребенка.");
			console.log("Изменение порядка.");
		}
		if (oldOrder.length > newOrder.length){
			console.log("Удалили элемент.");			
			if (_.isEmpty(newOrder)){
				console.log("Дерево стало пустым.");
			}
		}

		_.each(changeTable, function(fields, key){
			_.each(fields, function(field){
				field.orderColumn = key.toString();
			});
		});
	}
	this.reorderRowsFull = function(newOrder, oldOrder, bringParentBack){
		console.log("----------- reorderRowsFull -----------");
		var difference = _.difference(_.pluck(newOrder, "id"), _.pluck(oldOrder, "id"));
		var changeTable = {};
		if (!_.isEmpty(oldOrder) && !_.isEmpty(newOrder)){
			_.each(oldOrder, function(value, key){
				var changeFields = _self.getFieldsByOrderRow(key.toString());
				if (_.isEmpty(changeFields) && bringParentBack){
					_.each(difference, function(id){
						var changeFields = _self.getFieldsByOrderRow("a_" + id);
						var newKey = key.toString();
						// console.log("bringParentBack changeFields", changeFields);
						changeTable[newKey] = changeFields;
					});
				} else {
					var newKey = _.indexOf(newOrder, _.findWhere(newOrder, {id: value["id"]})).toString();
					if (key != newKey) {
						if (newKey == -1){
							if (oldOrder.length == newOrder.length) changeTable["a_" + value["id"]] = changeFields;
						} else changeTable[newKey] = changeFields;
					}
				}
			});
		}
		if (oldOrder.length < newOrder.length){
			console.log("Добавили новую вершину.");
			if (!_.isEmpty(oldOrder)){
			} else console.log("Дерево было пустым.");
		}
		if (oldOrder.length == newOrder.length){
			console.log("Замена родителя на ребенка или изменение порядка.");
		}
		if (oldOrder.length > newOrder.length){
			console.log("Удалили элемент.");
			if (_.isEmpty(newOrder)){
				console.log("Дерево было пустым.");
			}
		}
		_.each(changeTable, function(fields, key){
			_.each(fields, function(field){
				field.orderRow = key.toString();
			});
		});
	}
}

function Alias(){
	var _self = this;
	var _alias = {}; // alias = {item:[element, element, ...], ...}

	this.getAlias = function(){
		return _alias;
	}
	this.setAlias = function(alias){
		_alias = alias;
	}
	this.test = function(){
		console.log("----------- Test Alias -----------");
	}
	// 
	// 
	//
	var _hasItems = function(items){
		var aliasArray = _self.getAlias();
		var invertAliasArray = _.invert(aliasArray);
		if (invertAliasArray.hasOwnProperty(items)) return true;
		else return false;
	}
	var _newItemKey = function(){
		var aliasArray = _self.getAlias();
		var newItemKey = _.size(aliasArray) == 0 ? 0 : parseInt(_.last(_.keys(aliasArray))) + 1;
		return newItemKey.toString();
	}

	this.addItem = function(elements){
		// console.log("Add Items", elements);
		var item = {key:"", needDraw: true}; // key for column:{key:"", name:"", rel:""}, needDraw for drawNode
		var alias = this.getAlias();
		var invertAlias = _.invert(alias);		
		if (invertAlias.hasOwnProperty(elements)){
			item.key = invertAlias[elements];
			item.needDraw = false;
		} else {
			item.key = _newItemKey();
			alias[item.key] = elements;
			item.needDraw = true;
		}
		// console.log("Add Items", item);
		return item;
	}
	// 
	// 
	// 
	this.removeItem = function(item){
		var alias = this.getAlias();
		delete alias[item];
	}
	this.needElementDelete = function(element){
		var needElementDelete = true;
		if (this.getItemsByElement(element).length > 1) needElementDelete = false;
		return needElementDelete;
	}
	// 
	// 
	//
	this.getElementsByItem = function(item){
		var alias = this.getAlias();
		return alias[item];
	}
	this.getItemsByElement = function(element){
		var items = [];
		var alias = this.getAlias();
		_.each(alias, function(value, key){
			if (_.contains(value, element)) items.push(key);
		});
		return items;
	}
}

function Elements(){
	var _self = this;
	var _elements = {}; // elements = {id:{name:value, rel:"index"|"link"}, ...}

	this.getElements = function(){
		return _elements;
	}

	this.setElements = function(elements){
		_elements = elements;
	}
	// 
	// 
	// 
	var _hasElement = function(id){
		var elements = _self.getElements();
		if (elements.hasOwnProperty(id)) return true;
		else return false;
	}

	this.addElement = function(id, text, type){
		var hasElement = _hasElement(id);
		var elements = _self.getElements();
		if (!hasElement) elements[id] = {text: text, type: type};
		return hasElement;
	}
	// 
	// 
	// 
	this.removeElement = function(id){
		var elements = this.getElements();
		this.setElements(_.omit(elements, id));
	}
	// 
	// 
	// 
	this.getNameByIds = function(ids, filter, classes){
		var names = [];
		var elements = _self.getElements();
		if (ids.length > 1){
			_.each(ids, function(id){
				if (_hasElement(id)){
					if (_.contains(filter, classes[id])) names.push(elements[id].text);
				}
			});
			var name = names.join(" :: ");
		} else {
			var name = elements[ids[0]].text
		}
		return name;
	}
	this.getTypeByIds = function(ids){
		var types = [];
		var elements = _self.getElements();		
		_.each(ids, function(id){
			if (_hasElement(id)) types.push(elements[id].type);
		});
		var types = (_.uniq(types).indexOf("index") == -1)? "link":"index";
		return types;
	}
}

function Links(){
	var _self = this;	
	var _links = {}; //{link:{nameSpecial:value}, ...}
	var _prefix = "l_";

	this.getLinks = function(){
		return _links;
	}
	this.setLinks = function(links){
		_links = links;
	}
	// 
	// 
	// 
	var _hasLink = function(link){
		var links = _self.getLinks();
		if (links.hasOwnProperty(link)) return true;
		else return false;
	}

	this.addLink = function(link){
		var link = _prefix + link;
		var hasLink = _hasLink(link);
		var links = _self.getLinks();
		if (!hasLink) links[link] = {nameSpecial: ""};
		console.log(links, "is links an array", _.isArray(links));
		return hasLink;
	}
	this.removeLink = function(link){
		var link = _prefix + link;
		var links = this.getLinks();
		this.setLinks(_.omit(links, link));
	}
	// 
	// 
	// 
	this.getNameSpecialByLink = function(link){
		var link = _prefix + link;
		var links = this.getLinks();
		var hasLink = _hasLink(link);
		if (hasLink) return links[link].nameSpecial;
	}	
	this.setNameSpecialByLink = function(link, nameSpecial){
		var link = _prefix + link;
		var links = this.getLinks();
		var hasLink = _hasLink(link);
		if (hasLink) links[link].nameSpecial = nameSpecial;
	}
}

function Groups(){
	var _self = this;	
	var _groups = {rows: {}, column: ""}; //{rows:[{name:value, min:value, max:value, func:value}], column:value}
	var _prefix = "g_";

	this.clearGroups = function(){
		this.setGroups({rows: {}, column: ""});
	}
	this.getGroups = function(){
		// console.log(_.isObject(_groups.rows), _.isArray(_groups.rows));
		return _groups;	
	}
	this.setGroups = function(groups){
		_groups = groups;
		// console.log(_.isObject(_groups.rows), _.isArray(_groups.rows));
	}
	//
	// 
	// 
	this.getGroupColumn = function(){
		var groups = this.getGroups();
		return groups.column;
	}
	this.setGroupColumn = function(column){
		var groups = this.getGroups();
		groups.column = column;
	}
	// 
	// 
	// 
	this.getGroupRows = function(){
		var groups = this.getGroups();
		return groups.rows;
	}
	this.setGroupRows = function(rows){
		var groups = this.getGroups();
		groups.rows = rows;
	}
	// 
	// 
	//
	var _hasGroupRow = function(row){
		var rows = _self.getGroupRows();
		if (rows.hasOwnProperty(row)) return true;
		else return false;
	}
	// 
	// 
	//
	this.addGroupRow = function(row){
		var row = _prefix + row;
		var rows = this.getGroupRows();
		var hasGroupRow = _hasGroupRow(row);
		if (!hasGroupRow) rows[row] = {};
		return hasGroupRow;
	}
	this.removeGroupRow = function(row){
		var row = _prefix + row;
		var rows = this.getGroupRows();
		this.setGroupRows(_.omit(rows, row));	
	}
	// 
	// 
	// 
	var _getRowAttr = function(row, attr){
		var row = _prefix + row;
		var rows = _self.getGroupRows();
		if (_hasGroupRow(row)) return rows[row][attr];
	}
	var _setRowAttr = function(row, attr, value){
		var row = _prefix + row;
		var rows = _self.getGroupRows();
		if (_hasGroupRow(row)) return rows[row][attr] = value;
	}
	// 
	// 
	// 
	this.getRowName = function(row){
		return _getRowAttr(row, "name");
	}
	this.setRowName = function(row, name){
		_setRowAttr(row, "name", name);
	}
	// 
	// 
	// 
	this.getRowMin = function(row){
		return _getRowAttr(row, "min");
	}
	this.setRowMin = function(row, min){
		_setRowAttr(row, "min", min);
	}
	// 
	// 
	// 
	this.getRowMax = function(row){
		return _getRowAttr(row, "max");
	}
	this.setRowMax = function(row, max){
		_setRowAttr(row, "max", max);
	}
	// 
	// 
	// 
	this.getRowFunc = function(row){
		return _getRowAttr(row, "func");
	}
	this.setRowFunc = function(row, func){
		_setRowAttr(row, "func", func);
	}
	// 
	// 
	// 
	this.setRowMinMaxFunc = function(row, min, max, func){
		this.setRowMin(row, min);
		this.setRowMax(row, max);
		this.setRowFunc(row, func);
	}
}