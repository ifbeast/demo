function initFoldersTree(){
	$("#folders-tree")
	.jstree({
		"core": {
			"data": {
				"url": REQBASE + "?mod=objects&act=showfolders",
				"data": function (n) { 
					return { id: n.id ? n.id : 0}; 
		}}}

	})
	.bind("select_node.jstree", function (e, data) { selectNode(e, data) })
	.bind("create.jstree", function (e, data) {})
	.bind("remove.jstree", function (e, data) {})
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) {});

	var selectNode = function (e, data){
		
		$("#folders-tree").jstree("open_node", data.rslt.obj);

		$("#btnEditObject").addClass("hide");
		$("#state-export-group").addClass("hide");
		$("#state-edit-group").addClass("hide");

		switch (data.rslt.obj.attr("rel")){
			case "folder":
				console.log("folder");				
			break;
			case "linkgroup":
				console.log("linkgroup");
				obs.packageId = data.rslt.obj.attr("id");
				var stateHeader = data.args[0].innerText.trim();
				$("#state-header").text("Пакет: " + stateHeader);				
				$("#state-export-group").removeClass("hide");
			break;
			case "template":
				console.log("template");
				obs.objectId = data.rslt.obj.attr("id");
				var stateHeader = data.args[0].innerText.trim();
				$("#state-header").text("Объект: " + stateHeader);
				$("#btnEditObject").removeClass("hide");
				$("#state-export-group").removeClass("hide");
				$.ajax({
					url: REQBASE + "?mod=objects",
					dataType: "json",
					type: "post",
					data:{ 
						act: "showheaders",
						id: obs.objectId						
					},
					success: function(data){
						var headers = {}, cols = {}, rows = {};

						if (data){
							var json = JSON.parse(data);
							cols = json.columns;
							rows = json.rows;
							headers = _.omit(json, "columns", "rows");
						}
						
						initHeadersColsTree(cols);
						initHeadersRowsTree(rows);
						obs.init(headers);
					}
				});
				$.ajax({
					url: "/?mod=objects",
					dataType:'json',
					type:'post',
					data:{
						act:'generatetemplate',
						id: obs.objectId
						},
					success: function(data){
						if (data){
							spread.fromJSON(data);
							sheet = spread.getActiveSheet();
						}
					}
				});
			break;
		}
	};
}

function initIndexesTree(){
	$("#indexes-tree")
	.jstree({
		"core": {
			"data": {
				"url": REQBASE,
				"data": function (n) { 
					return { "act" : "children", "id" : n.id ? n.id.replace("node_","") : 0 }; 
		}}}

	})
	.bind("select_node.jstree", function (e, data) { selectNode(e, data) })
	.bind("create.jstree", function (e, data) {})
	.bind("remove.jstree", function (e, data) {})
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) {});

	var selectNode = function(e, data){
		$.ajax({
			url: REQBASE + "?mod=objects",
			dataType: "json",
			type: "post",
			data:{ 
				act: "showlinks",
				path: data.node.id.replace("node_","")
			},
			success: function(data){
				// console.log(data);
				// processChainLinks(data);
				// initLinksList(data);
				obs.indexId = data.indexId;
				obs.elements = data.elements;
				obs.elementsMap = data.elementsMap;
				obs.linksMap = data.linksMap;
				initLinksTree(data.tree);
				initLinksCheckboxTree({});
			}
		});
	}
}