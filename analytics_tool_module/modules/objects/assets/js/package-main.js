var obs = new Observer();
var spread, sheet;

 $( document ).ready(function() {
	
	// Layouting
	var layout = $("#main-layout").layout({fxSpeed: "slow"});
	layout.sizePane("west", 300);
	
	var packageLayout = $("#package-layout").layout({fxSpeed: "slow"});
	packageLayout.sizePane("north", 34);
	packageLayout.sizePane("west", 550);
	packageLayout.hide("west");

	var editLayout = $("#edit-layout").layout({fxSpeed: "slow"});
	editLayout.sizePane("west", 280);
	var spreadsheetLayout = $("#spreadsheet-layout").layout({fxSpeed: "slow"});
	spreadsheetLayout.sizePane("west", 250);

	// Tabs
	$(".tree-wrapper").tabs();

	// Trees
	initFoldersTree();
	initIndexesTree();

	// Dialog
	var dialog = $("#dialog-confirm").dialog({
		autoOpen: false,
		resizable: false,
		height: 140,
		modal: false,
		buttons: {
	        "Сохранить": function() {
	          	$( this ).dialog( "close" );
	          	layout.show("west");
				packageLayout.hide("west");
				$("#state-edit-group").addClass("hide");
				$("#spreadsheet-tools-edit-mode").addClass("hide");
				$("#btnEditObject").removeClass("hide");
				$("#state-export-group").removeClass("hide");
	        },
	        "Отменить": function() {
	          	$( this ).dialog( "close" );
	          	layout.show("west");
				packageLayout.hide("west");
				$("#state-edit-group").addClass("hide");
				$("#spreadsheet-tools-edit-mode").addClass("hide");
				$("#btnEditObject").removeClass("hide");
				$("#state-export-group").removeClass("hide");
	        }
      	}
    });
    
	// Buttons
	$("#btnEditObject").click(function(){
		console.log("Edit mode");
		layout.hide("west");
		packageLayout.show("west");

		$("#btnEditObject").addClass("hide");
		$("#state-export-group").addClass("hide");
		$("#state-edit-group").removeClass("hide");
		$("#spreadsheet-tools-edit-mode").removeClass("hide");
		
	});

	$("#btnSaveObject").click(function(){
		console.log("Object save");
	});
	
	$("#btnEditCancel").click(function(){
		console.log("Edit cancel");
		dialog.dialog("open");
	});
	
	$("#btnExport").click(function(){
		console.log("Export");
	});	

	// Tree tools
	$("#btnAddLinks").click(function(){
		var links = [];
		$("#links-checkbox-tree").jstree("get_selected", null, true).each(function(index){
			if ($(this).attr("rel") == "linkgroup"){
				links.push($(this).attr("id"));
			}
		});
		obs.addLinks(links);
	});

	$("#btnAddIndex").click(function(){
		obs.addIndex();
	});
	
	$("#btnMoveFromRowsToColumns").click(function(){
		var elements = [];
		$("#links-tree").jstree("get_selected", null, true).each(function(index){
			if ($(this).attr("rel") != "linkgroup"){
				elements.push($(this).attr("id"));
			} 			
			if ($(this).attr("rel") == "linkgroup"){
				$(this).find("li").each(function(){
					elements.push($(this).attr("id"));
				});
			}
		});		

		obs.moveFromRowsToColumns(elements);
	});

	$("#btnAddTimePoint").click(function(){
		console.log("Add now");
		var source;
		$("#header-cols-tree").jstree("get_selected", null, true).each(function(){
			source = $(this).attr("id");
		});
		obs.addDate(source, "0");
	});

	$("#btnAddTimePointMonth").click(function(){
		console.log("Add month ago");
		var source;
		$("#header-cols-tree").jstree("get_selected", null, true).each(function(){
			source = $(this).attr("id");
		});
		obs.addDate(source, "-1");
	});

	$("#btnAddTimePointYear").click(function(){
		console.log("Add year ago");
		var source;
		$("#header-cols-tree").jstree("get_selected", null, true).each(function(){
			source = $(this).attr("id");
		});
		obs.addDate(source, "-12");
	});

	$("#btnAddEmptyField").click(function(){
		console.log("Add empty");
		obs.drawColumn("field");
	});

	$("#btnAddFunction").click(function(){
		console.log("Add function");
		obs.drawColumn("function");
	});

	$("#btnSaveHeaders").click(function(){
		console.log("Save headers");

		$.ajax({
			url: REQBASE + "?mod=objects",
			dataType: "json",
			type: "post",
			data:{ 
				act: "saveheaders",
				id: obs.objectId,
				data: obs.getHeaders()
			},
			success: function(data){
				console.log(data);
			}
		});
	});

});