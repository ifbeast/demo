function Observer(){
	// 
	// Public variables
	// 
	this.packageId;
	this.objectId;
	this.indexId;
	this.indexName;
	
	this.elements;
	this.elementsMap;
	this.linksMap;

	this.aliasColumns = {};
	this.aliasRows = {};
	this.elementNames = {};
	this.linksTable = [];

	// 
	// Private variables
	// 
	var fieldTypes = { 	
						"index": 	{ "rel": "index",	 	"position": "after", 	"data": "",				"edit": true },
						"link": 	{ "rel": "link",	 	"position": "after", 	"data": "",				"edit": true },
						"date": 	{ "rel": "date",	 	"position": "last", 	"data": "0",			"edit": true }, 
						"field": 	{ "rel": "field",	 	"position": "last", 	"data": "Заголовок",	"edit": false}, 
						"function": { "rel": "function",	"position": "last", 	"data": "Функция",		"edit": true }
					};
	
	var treeColumns, treeRows;
	var treeColumnsOrder = [], treeRowsOrder = [];
	var needTreeColumnsReorder = false, needTreeRowsReorder = false;

	var self = this;

	console.log("Observer is on");

	// 
	// Init on document ready
	// 
	this.init = function(data){
		if (_.isEmpty(data)){
			this.linksTable = [];
			this.aliasColumns = {};
			this.aliasRows = {};
			this.elementNames = {};
		} else {
			this.linksTable = data.linksTable;
			this.aliasColumns = data.aliasColumns;
			this.aliasRows = data.aliasRows;
			this.elementNames = data.elementNames;
		}
		treeColumns = $("#header-cols-tree");
		treeRows = $("#header-rows-tree");
	}

	this.initPartial = function(){
		console.log("----------- Init Partial -----------");
		this.aliasColumns = {};
		this.aliasRows = {};
		this.linksTable = [];
		treeColumns.empty();
		treeRows.empty();
	}

	this.getHeaders = function(){
		var columns =  treeColumns.jstree("get_json", "-1");
		var rows =  treeRows.jstree("get_json", "-1");		
		var headers = JSON.stringify({"columns": columns, "rows": rows, "linksTable": this.linksTable, "aliasColumns": this.aliasColumns, "aliasRows": this.aliasRows, "elementNames": this.elementNames});
		return headers;
	}

	// 
	// Working with trees
	// 
	this.drawColumn = function(fieldType, data){
		drawField(treeColumns, fieldTypes[fieldType], data);
	}

	this.drawRow = function(fieldType, data){
		drawField(treeRows, fieldTypes[fieldType], data);
	}

	var drawField = function(tree, fieldType, data){
		console.log("----------- Draw Field -----------");
		if (_.isObject(data)){
			tree.jstree("create", null, fieldType.position, { "attr": { "id": data.id, "rel": fieldType.rel }, "data": data.name }, false, fieldType.edit);
		} else if (_.isString(data)){
			tree.jstree("create", null, fieldType.position, { "attr": { "id": "", "rel": fieldType.rel }, "data": data }, false, fieldType.edit);
		} else {
			tree.jstree("create", null, fieldType.position, { "attr": { "id": "", "rel": fieldType.rel }, "data": fieldType.data }, false, fieldType.edit);
		}
	}

	// 
	// Working with links
	// 
	this.addIndex = function(){
		var links = [];
		_.each(self.aliasRows, function(aliasRow){
			var elements = _.sortBy(_.union(aliasRow, [self.indexId]));
			_.each(self.linksMap, function(value, key){
				if (_.sortBy(_.keys(value)).join() == elements.join()){
					links.push(key);
				}
			});
		});
		this.addLinks(links);
	}

	this.addDate = function(source, date){
		var linkExisted = false;
		_.each(_.where(self.linksTable, {"aliasColumn": source}), function(link){
			var newLink = {}
			_.extend(newLink, link);
			newLink["date"] = date;	
			linkExisted = addLink(newLink);
		});
		console.log(linkExisted);
		this.drawColumn("date", {"id": "date" + date, "name": date}	);		
	}

	this.addLinks = function(links){
		_.each(links, function(value, key){
			var column, row;
			var columnElements = [];
			var rowElements = [];
			var date = "0";
			var fieldType;
			
			_.each(self.linksMap[value], function(value, key){
				if (key != self.indexId){
					rowElements.push(key);
				}
			});
			columnElements.push(self.indexId);
			column = self.addAliasColumns(columnElements, self.elements);
			row = self.addAliasRows(rowElements, self.elements);

			addLink({"aliasColumn": column, "aliasRow": row, "orderColumn": column, "orderRow": row, "link": value, "date": date});
		});
		needTreeColumnsReorder = false;
		needTreeRowsReorder = false;
	}

	var addLink = function(data){
		console.log(data);
		if (!_.findWhere(self.linksTable, _.omit(data, "orderColumn", "orderRow"))){
			console.log("----------- Add Link -----------");
			self.linksTable.push(data);			
			if (needTreeColumnsReorder){
				reorderLinks("Column", false, treeColumnsOrder);
			}
			if (needTreeRowsReorder){
				reorderLinks("Row", false, treeRowsOrder);
			}
		} else {
			console.log("Link already existed.");
			return false
		}		
		return true
	};

	this.addAliasRows = function(elements, names, onlyIndexes){
		var aliasKey = addAlias(this.aliasRows, elements, this.drawRow, names, onlyIndexes);
		return aliasKey;
	}

	this.addAliasColumns = function(elements, names, onlyIndexes){
		var aliasKey = addAlias(this.aliasColumns, elements, this.drawColumn, names, onlyIndexes);
		return aliasKey;
	}

	var addAlias = function(aliasArray, elements, draw, names, onlyIndexes){
		console.log("----------- Add Alias -----------");
		var aliasKey;
		var invertAliasArray = _.invert(aliasArray);
		if (invertAliasArray.hasOwnProperty(elements)){
			aliasKey = invertAliasArray[elements];
		} else {
			aliasKey = _.size(aliasArray) == 0 ? 0 : parseInt(_.last(_.keys(aliasArray))) + 1;
			aliasArray[aliasKey] = elements;
			
			_.extend(self.elementNames, _.pick(names, elements));

			if(!onlyIndexes){
				var fieldType = (elements.indexOf(self.indexId) < 0) ? "link" : "index";				
			} else {
				var fieldType = "index";
			}

			draw(fieldType, {"id": aliasKey, "name": _.map(elements, function(element){ return names[element] }).join(" :: ")});
		}

		return aliasKey.toString();
	}

	this.afterAddColumns = function(source, sourceKeys){
		console.log("----------- after Add Columns -----------");
		treeColumnsOrder = sourceKeys;
		needTreeColumnsReorder = true;

		if(source){
			console.log("Execute Reorder manualy");
			needTreeColumnsReorder = false;
			reorderLinks("Column", source, sourceKeys);
		}
	}

	this.afterAddRows = function(source, sourceKeys){
		console.log("----------- after Add Rows -----------");
		treeRowsOrder = sourceKeys;
		needTreeRowsReorder = true;
	}

	// 
	// Move links
	// 
	this.moveLinksColumns = function(source, sourceKeys, target, targetKeys){
		moveLinks("Column", source, sourceKeys, target, targetKeys);
	}

	this.moveLinksRows = function(source, sourceKeys, target, targetKeys){
		moveLinks("Row", source, sourceKeys, target, targetKeys);
	}

	var moveLinks = function(linksTableKey, source, sourceKeys, target, targetKeys){
		console.log("----------- Move Links -----------");
		console.log(self.linksTable);

		if(!source){
			console.log("Source is root");
			_.each(self.linksTable, function(value, key){
				console.log("been:" + value["order" + linksTableKey] + " become:" + sourceKeys.indexOf(value["alias" + linksTableKey]));
				value["order" + linksTableKey] = sourceKeys.indexOf(value["alias" + linksTableKey]).toString();
			})
		} else {
			console.log("Source is node");

			console.log("been:" + value["order" + linksTableKey] + " become:" + sourceKeys.indexOf(value["alias" + linksTableKey]));
		}
		console.log("Links after move!");
		console.log(self.linksTable);
	}

	this.moveFromRowsToColumns = function(elements){
		console.log("----------- Move From Rows To Columns -----------");
		var newAliasRows = deleteElementsFromAlias(self.aliasRows, elements);
		var newAliasColumns = addElementsToAlias(self.aliasColumns, elements);
		this.changeLinks(newAliasRows, newAliasColumns);
	}
	
	var aliasShouldStay = false;
	var deleteElementsFromAlias = function(aliasArray, elements){
		console.log("----------- Delete Elements From Alias -----------");
		var newAliasArray = {};
		var newAliasArrayGrouped = [];
		var newAliasArrayUnique = {};

		_.each(aliasArray, function(value, key){
			if (_.intersection(value, elements).length == 0){
				aliasShouldStay = true;
			};
			if (value.length > 1){
				newAliasArray[key] = _.difference(value, elements);	
			} else {
				newAliasArray[key] = value;	
			}
		});
		
		newAliasArrayGrouped = _.groupBy(newAliasArray);
		delete newAliasArrayGrouped[""];

		var counter = 0;
		_.each(newAliasArrayGrouped, function(value, key){
			newAliasArrayUnique[counter] = _.first(value);
			counter ++;
		})
		
		newAliasArray = newAliasArrayUnique;
		return newAliasArray;
	}

	var addElementsToAlias = function(aliasArray, elements){
		console.log("----------- Add Elements To Alias -----------");
		var newAliasArray = {};
		var counter = 0;

		if (aliasShouldStay){			
			_.each(aliasArray, function(alias, aliasKey){
				newAliasArray[counter] = alias;
				counter ++;	
			});
		}

		_.each(aliasArray, function(alias, aliasKey){
			_.each(elements, function(element, elementKey){
				var newAliasKey = parseInt(aliasKey) + parseInt(elementKey) + counter;
				newAliasArray[newAliasKey] = alias.concat(element);
			});
			counter ++;
		});
		return newAliasArray;
	}

	this.changeLinks = function(newAliasRows, newAliasColumns){
		console.log("----------- Change Links -----------");
		var column, row;
		var oldAliasColumns = {}, oldAliasRows = {}, oldLinksTable = [], newLinksMap = {};
		var date = 0;

		oldAliasColumns = this.aliasColumns;
		oldAliasRows = this.aliasRows;
		oldLinksTable = this.linksTable;		

		this.initPartial();

		_.each(newAliasColumns, function(newAliasColumn, newAliasColumnKey){
			column = self.addAliasColumns(newAliasColumn, self.elementNames, true);
		});
		_.each(newAliasRows, function(newAliasRow, newAliasRowKey){
			row = self.addAliasRows(newAliasRow, self.elementNames, true);
		});

		needTreeColumnsReorder = false;
		needTreeRowsReorder = false;

		_.each(oldAliasRows, function(rowValue, rowKey){
			_.each(oldAliasColumns, function(columnValue, columnKey){
				var link = _.pluck(_.where(oldLinksTable, {"aliasRow": rowKey, "aliasColumn": columnKey}), "link");
				newLinksMap[link] = _.sortBy(_.union(rowValue, columnValue));
			});
		});

		_.each(self.aliasRows, function(rowValue, rowKey){
			_.each(self.aliasColumns, function(columnValue, columnKey){
				var link = 0;
				var elements = _.sortBy(_.union(rowValue, columnValue));

				_.each(newLinksMap, function(value, key){
					if (value.join() == elements.join()){
						link = key;
					}
				});

				addLink({"aliasColumn": columnKey, "aliasRow": rowKey, "orderColumn": columnKey, "orderRow": rowKey, "link": link, "date": date});
			});
		});

	}

	// 
	// Delete links
	// 
	this.deleteLinksColumns = function(source, sourceKeys, aliasKeys){
		deleteAlias(this.aliasColumns,aliasKeys);
		deleteLinks("Column", aliasKeys);
		reorderLinks("Column", source, sourceKeys);
	}

	this.deleteLinksRows = function(source, sourceKeys, aliasKeys){
		deleteAlias(this.aliasRows, aliasKeys);
		deleteLinks("Row", aliasKeys);
		reorderLinks("Row", source, sourceKeys);
	}

	var deleteAlias = function(aliasArray, aliasKeys){
		console.log("----------- Delete Alias -----------");
		_.each(aliasKeys, function(aliasKey){
			// _.each(aliasArray[aliasKey], function(elementName, elementKey){
			// 	delete self.elementNames[elementKey];
			// });
			self.elementNames = _.omit(self.elementNames, aliasArray[aliasKey]);
			delete aliasArray[aliasKey];
		})
	}

	var deleteLinks = function(linksTableKey, aliasKeys){
		console.log("----------- Delete Links -----------");
		_.each(aliasKeys, function(aliasKey){
			if (linksTableKey == "Column"){
				var where = _.where(self.linksTable, {aliasColumn: aliasKey});
			}
			if (linksTableKey == "Row"){
				var where = _.where(self.linksTable, {aliasRow: aliasKey});				
			}
			_.each(where, function(value){
				self.linksTable = _.without(self.linksTable, value);
			})
		})		
	}

	var reorderLinks = function(linksTableKey, source, sourceKeys){
		console.log("----------- Reorder Link -----------");
		console.log(linksTableKey);
		console.log(source);
		console.log(sourceKeys);
		if(!source){
			_.each(self.linksTable, function(value, key){
				value["order" + linksTableKey] = sourceKeys.indexOf(value["alias" + linksTableKey]).toString();
			})
		} else {
			// if (linksTableKey == "Column"){
			// 	var where = _.where(self.linksTable, {aliasColumn: source});
			// }
			// if (linksTableKey == "Row"){
			// 	var where = _.where(self.linksTable, {aliasRow: source});				
			// }
			// _.each(where, function(value, key){				
			// 	value["order" + linksTableKey] = (sourceKeys.indexOf(source) + counter).toString();
			// 	counter ++;
			// })
			_.each(self.linksTable, function(value, key){
				if(value["alias" + linksTableKey] == source){
					value["order" + linksTableKey] = (parseInt(source) + sourceKeys.indexOf(value["date"].toString())).toString();
				}
			})
			console.log(self.linksTable);
		}
	}
	// 
	// Working with spreadsheet
	// 	
	
};