	var currentEditedObject = 0;
// Objects 
	$("#newObjectBtn").click(newObjectDialog);
	$("#saveObjectBtn").click(saveObjectContent);

	loadObjects();
	
	$('#objectcnt')
	.sortable({ axis: "y", handle: "b.handle" })
	.droppable({
		accept: ".linkchain",
		activeClass: "ui-state-hover",
		hoverClass: "ui-state-active",
		drop: function( event, ui ) {
			var drparent = $(ui.draggable).parents('.onelink');
			uiMonitorObjectLinkDOM({
						id:drparent.attr('rel'),
						title:$(ui.draggable).find('chainpart[element="'+$(ui.draggable).attr('last')+'"]').text()
					})
					.appendTo(this);
		}
	});

	// 
	// Objects
	// 
    function loadObjects()
	{
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'listobjects'
				},
			success:function(os){
				var ol = $('#objectlist');
				ol.html('');
				$.each(os,function(i,o){
					uiMonitorObjectDOM({id:o.id,title:o.name}).appendTo(ol);
				});
			}
		});
	}

	

	function loadObjectLinks(oid)
	{
		if((typeof oid) == 'object' || !oid)
			oid = $(this).attr('rel');
		if(!oid)
			oid = $(this).parents('.objitem').attr('rel');
		var _oid = oid;
		currentEditedObject = _oid;
		$('#saveObjectBtn').css('visibility','visible');
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'objectcnt',
				'id': oid
				},
			success:function(os){
				var ol = $('#objectcnt');
				$('#objectlist .objitem').removeClass('selected');
				$('#objectlist').find('.objitem[rel="'+os.object+'"]').addClass('selected');
				ol.html('');
				$.each(os.links,function(i,o){
					uiMonitorObjectLinkDOM({id:o[0],title:o[1],special:o[2]})
						.appendTo(ol);
				});
			}
		});
	}

	function saveObjectContent()
	{
		if(!currentEditedObject) return;
		var links = [];
		$('#objectcnt .objcntitem').each(function(){
			links.push({
					id:$(this).attr('rel'),
					title:$(this).attr('title'),
					special:$(this).attr('special'),
					});
		});
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'saveobject',
				'id': currentEditedObject,
				'links' : links
				},
			success:function(os){
				console.log("ajax success!");
				$('#objectcnt').effect('highlight');
			}
		});
	}

	function newObjectDialog(){
		var DLG,
			TXC = $('<textarea rows="3" cols="30"></textarea>'),
			TYPESEL=$('<select><option>....Ждите....</option></select>'),
			DEFTOTAL=$('<input type="text" placeholder="Имя для строки TOTAL"/>');

		DLG = $('<div></div>')
				.appendTo($('body'))
				.append('<div>Наименование</div>')
				.append(TXC)
				.append('<div>Тип объекта</div>')
				.append(TYPESEL)
				.append('<div>Имя для строки с суммой, если она не указана</div>')
				.append(DEFTOTAL)
				.css('text-align','left')
				.dialog({
						title: 'Новый объект',
						modal:true,
						buttons:{
							'Ok':function(){
								var newval = TXC.val(),
									ntype = TYPESEL.val(),
									ntotalname = DEFTOTAL.val();
								if(!newval)
								{
									alert("Надо ввести имя.");
									return;
								}
								$.ajax({
									type:'post',
									url:'?mod=objects',
									dataType:'json',
									data:{
										act:'newobject',
										newname:newval,
										newtype:ntype,
										deftotal:ntotalname
									},
									success:function(){loadObjects();}
								});
								$(this).dialog('destroy');

							},
							'Cancel':function(){
								$(this).dialog('destroy');
							}
						}

					});

		$.ajax({
			type:'get',
			url:'?mod=objects&act=objecttypes',
			dataType:'json',
			success:function(tps){
				TYPESEL.find('option').remove();
				$.each(tps,function(i,tp){
					TYPESEL.append('<option value="'+tp.id+'">'+tp.name+'</option>');
				});

			}
		});

		return false;
	}

	function uiMonitorObjectLinkDOM(params)
	{
	if(!params.special) params.special = '';
	var newo = $('<div></div>')
				.addClass('objcntitem')
				.attr('rel',params.id)
				.attr('special',params.special)
				.attr('title',params.title)
				.html('<b class="handle"></b><span>'+params.id+'</span><a>'+params.title+'</a><sup>'+params.special+'</sup>');

	$('<img src="/style/images/icons/table-delete-row.png" title="Удалить" class="moaction moaction_unlink">')
		.appendTo(newo)
		.click(function(){
			newo.remove();
		});

	$('<img src="/style/images/icons/blue-document-rename.png" title="Переименовать" class="moaction moactionrename">')
		.appendTo(newo)
		.click(function(){
			var _id = params.id,
				_newname = '',
			_dialog =
				$('<div class="monobjectcntrename"></div>')
					.append($('<textarea></textarea>').html($('.objcntitem[rel="'+params.id+'"]').attr('title')))
					.appendTo('body')
					.dialog({
						title: 'Переименовать поле',
						modal: true,
						buttons: {
							'Сохранить': function(){
								_newname = _dialog.find('textarea').val();
								$('.objcntitem[rel="'+params.id+'"]').attr('title',_newname)
									.find('a').html(_newname);
								$(this).dialog('destroy').remove();
							},
							'Отмена': function(){
								$(this).dialog('destroy').remove();
							}
						}
					});
			return false;
		});


	$('<img src="/style/images/icons/property.png" title="Переименовать" class="moaction">')
		.appendTo(newo)
		.click(function(){
			var _id = params.id,
				_newname = '',
			_dialog =
				$('<div class="monobjectcntprops"></div>')
					.append('<div></div>')
					.append($('<input type="text" rel="special">').val($('.objcntitem[rel="'+params.id+'"]').attr('special')))
					.appendTo('body')
					.dialog({
						title: 'Переименовать поле',
						modal: true,
						buttons: {
							'Сохранить': function(){
								_newname = _dialog.find('input[rel="special"]').val();
								$('.objcntitem[rel="'+params.id+'"]').attr('special',_newname)
									.find('sup').html(_newname);
								$(this).dialog('destroy').remove();
							},
							'Отмена': function(){
								$(this).dialog('destroy').remove();
							}
						}
					});
			return false;
		});


	return newo;
}

function uiMonitorObjectDOM(params)
{
	var o = $('<div></div>')
				.addClass('objitem')
				.attr('rel',params.id)
				.html('<a><span>'+params.title+'</span></a>')
				.click(loadObjectLinks);
	$('<img src="/style/images/icons/blue-document-rename.png" title="Переименовать" class="moaction moactionrename">')
		.appendTo(o.find('a'))
		.click(function(){
			var _id = params.id,
				_newname = '',
			_dialog =
				$('<div class="monobjectrename"></div>')
					.append($('<textarea></textarea>').html($('.objitem[rel="'+params.id+'"] span').html()))
					.appendTo('body')
					.dialog({
						title: 'Переименовать объект',
						modal: true,
						buttons: {
							'Сохранить': function(){
								_newname = _dialog.find('textarea').val();
								$(this).dialog('destroy').remove();
								$.ajax({
									url: "/?mod=objects",
									dataType:'json',
									type:'post',
									data:{
										act:'renameobject',
										id: _id,
										newname: _newname
										},
									success:function(os){
										if(!os) return;
										if(!os.result) return;
										$('.objitem[rel="'+params.id+'"] span').html(_newname);
									}
								});
							},
							'Отмена': function(){
								$(this).dialog('destroy').remove();
							}
						}
					});
			return false;
		});
	$('<img src="/style/images/icons/table.png" title="Показать таблицу" class="moaction moactiotable">')
		.appendTo(o.find('a'))
		.click(__objectResultViewHandler);

	return o;
}

function __objectResultViewHandler()
{
	var oid = $(this).parents('.objitem').attr('rel'), _dialog;
	function ___req(type)
	{
		$.ajax({
			url: "/?mod=objects",
			dataType:'text',
			type:'post',
			data:{
				act:'object'+type,
				id: oid,
				month: $('#_yy').val()+'/'+$('#_mm').val(),
				sort: $('#_sort').val(),
				style: $('#_style').val(),
				},
			success:function(os){
				_dialog.find('.content').html(os);
			}
		});
	}
	_dialog =
		$('<div class="monobjectresult"></div>')
			.append($('<span>Дата:</span> <input type="text" maxlength="4" size=4 id="_yy" placeholder="ГГГГ">'+
							'<input type="text" maxlength="2" size=2 id="_mm" placeholder="ММ"> | '+
							'<span>Сортировка:</span> <input type="text" maxlength="2" size="6" id="_sort" placeholder="колонка" /> | '+
							'<span>Стиль:</span> <input type="text" maxlength="2" size="6" id="_style" placeholder="номер" />'))
			.append('<div class="content"></div>')
			.appendTo('body')
			.dialog({
				title: 'Просмотреть объект',
				modal: true,
				width: 700,
				height:500,
				buttons: {
					'Таблица': function(){
						___req('table');
					},
					'Картинка': function(){

						_dialog
							.find('.content')
							.html('<img src="/?mod=objects&act=objectimage&id='+oid+'&month='+$('#_yy').val()+'/'+$('#_mm').val()+'&sort='+$('#_sort').val()+'&style='+$('#_style').val()+'">');
					},
					'Экспорт': function(){
						___req('export');
					},
				}
			});


	return false;
}