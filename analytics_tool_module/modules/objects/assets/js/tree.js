// 
// data = {id: "packages", config: TreeConfiguration.getConfig("packages"), tools:{}, helper:{}, behavior:{}}
// 
function Tree(data){
	var _id 		= data.id;
	var _config 	= data.config;
	var _helper 	= data.helper;
	var _tools 		= data.tools;
	var _behavior 	= data.behavior;
	
	if(!_config.core)
		_config.core = {};

	if(!_config.core.check_callback)
		_config.core.check_callback = function(){ return true; };
		
	if(_config.types){
		if(!_config.types['#'])
			_config.types['#'] = {valid_children: -1, max_depth: -1, max_children: -1};
	}
	
	var _tree = $("#" + _id + "-tree")
	.on("ready.jstree", function (e){
		_behavior.readyTree(e);
	})
	.on("load_all.jstree", function (e, data){
		_behavior.loadAll(e, data);
	})	
	.on("select_node.jstree", function (e, data){
		_behavior.selectNode(e, data);
	})
	.on("create_node.jstree", function (e, data){
		_behavior.createNode(e, data);
	})
	.on("delete_node.jstree", function (e, data){
		_behavior.removeNode(e, data);
	})
	.on("rename_node.jstree", function (e, data){
		_behavior.renameNode(e, data);
	})
	.on("move_node.jstree", function (e, data){
		_behavior.moveNode(e, data);
	})
	.jstree(_config);

	return _tree;
}
// 
// 
// 
TreeConfiguration = function(){
	var _config = {"types": {},	"plugins": [], "contextmenu": {}, "core": {}, "themes": {"stripes": true}, "checkbox": {}, "search": {}},
		_commonHelper = new CommonHelper(),
		_treeHelper = new TreeHelper();

		this.getConfig = function(tree, json_data) {
			var types, plugins, request, contextmenu, checkbox, check_callback;

			switch (tree) {
				case "indexes":
					types = _indexesTypes;
					plugins = _indexesPlugins;
					request = _indexesAjax;
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "indexes-math":
					types = _indexesTypes;
					plugins = _indexesPlugins;
					request = _indexesMathAjax;
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "packages":
					types = _packagesTypes;
					plugins = _packagesPlugins;
					request = _packagesAjax;
					contextmenu = _packagesContext;
					checkbox = {};
					check_callback = _packagesCheckCallBack;
					break;
				case "links-checkbox":
					types = _linksCheckboxTypes;
					plugins = _linksCheckboxPlugins;
					request = [];
					contextmenu = {};
					checkbox = _linksCheckboxCheckbox;
					check_callback = _defaultCheckCallBack;
					break;
				case "elements-checkbox":
					types = _linksCheckboxTypes;
					plugins = _elementsCheckboxPlugins;
					request = [];
					contextmenu = {};
					checkbox = _linksCheckboxCheckbox;
					check_callback = _defaultCheckCallBack;
					search_callback = _elementsCheckboxSearchCallBack;
					break;				
				case "links-hidden":
					types = _linksCheckboxTypes;
					plugins = _linksHiddenPluginsEdit;
					request = [];
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "links-hidden-edit":
					types = _linksCheckboxTypes;
					plugins = _linksHiddenPluginsEdit;
					request = [];
					contextmenu = _linksHiddenContext;
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "headers":
					types = _headerColsTypes;
					plugins = _headersPlugins;
					request = [];
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "headers-edit":
					types = _headerColsTypes;
					plugins = _headersPluginsEdit;
					request = [];
					contextmenu = _headersContext;
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "header-cols-edit":
					types = _headerColsTypes;
					plugins = _headersPluginsEdit;
					request = [];
					contextmenu = _headerColsContext;
					checkbox = {};
					check_callback = _headersCheckCallBack;
					break;
				case "header-rows-edit":
					types = _headerRowsTypes;
					plugins = _headersPluginsEdit;
					request = [];
					contextmenu = _headerRowsContext;
					checkbox = {};
					check_callback = _headersCheckCallBack;
					break;
				case "themes":
					types = _themesTypes;
					plugins = _themePlugins;
					request = _themesAjax;
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "styles":
					types = _stylesTypes;
					plugins = _stylesPlugins;
					request = _stylesAjax;
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
				case "empty":
					types = {};
					plugins = [];
					request = [];
					contextmenu = {};
					checkbox = {};
					check_callback = _defaultCheckCallBack;
					break;
			}

			_config.types = types;
			_config.plugins = plugins;
			_config.contextmenu = contextmenu;
			_config.checkbox = checkbox;
			_config.core.data = (json_data) ? json_data : request;
			_config.core.check_callback = check_callback;

			if (_.contains(plugins, "search")) _config.search.search_callback = search_callback;
			return _config;
		}
	
	// 
	// 
	// 
	var _indexesTypes = {
			"default": 		{"valid_children": [], "icon": "/style/images/icons/blue-document.png"},
			"folder": 		{"valid_children": [ "default", "folder", 'folderempty' ], "icon": "/style/images/icons/blue-folder.png"},
			"empty": 		{"valid_children": [ "default", "folder" ],	"icon": "/style/images/icons/blue-folder-broken.png"},
			// "group": 		{"valid_children": [ "default", "folder" ],	"icon": "/style/images/icons/blue-folder-share.png"},
			"group": 		{"valid_children": [ "default", "folder" ],	"icon": "/style/images/icons/drawer.png"},
			"linkgroup": 	{"valid_children": [ "default", "folder" ], "icon": "/style/images/icons/drawer.png"},
			"multilinks": 	{"valid_children": [ "default", "folder" ], "icon": "/style/images/icons/chart--plus.png"}
		
	};
	var _packagesTypes = {
			"#": 		{valid_children: -1, max_depth: -1, max_children: -1},
			"folder": 	{"valid_children": [ "folder", "package", "object" ], "icon": "/style/images/icons/blue-folder.png"},
			"package": 	{"valid_children": [ "folder", "package", "object" ], "icon": "/style/images/icons/drawer.png"},
			"object": 	{"valid_children": "none","icon": "/style/images/icons/blue-document-template.png"}
	};
	var _linksCheckboxTypes = {
			"#": 			{valid_children: -1, max_depth: -1, max_children: -1},
			"linkgroup": 	{"valid_children": [ "index", "link", "default" ], "icon": "/style/images/icons/drawer.png"},
			"index": 		{"valid_children": [], "icon": "/style/images/icons/medal.png"},
			"default": 		{"valid_children": [], "icon": "/style/images/icons/blue-document.png"},
			"link": 		{"valid_children": [], "icon": "/style/images/icons/blue-document.png"}
	};
	var _headerColsTypes = {
			"#": 			{valid_children: -1, max_depth: -1, max_children: -1},
			"index": 		{"valid_children": [ "link", "date", "function", "field" ],	"icon": "/style/images/icons/medal.png"},
			"link": 		{"valid_children": [ "link", "date", "function", "field" ],"icon": "/style/images/icons/blue-document.png"},
			"field": 		{"valid_children": [ "link", "index", "date", "function", "field" ],"icon": "/style/images/icons/blue-folder.png"},
			"date": 		{"valid_children": [], "icon": "/style/images/icons/clock.png"},
			"function": 	{"valid_children": [], "icon": "/style/images/icons/function.png"}
	};
	var _headerRowsTypes = {
			"#": 			{valid_children: -1, max_depth: -1, max_children: -1},
			"index": 		{"valid_children": [ "link", "date", "function", "field" ],	"icon": "/style/images/icons/medal.png"},
			"link": 		{"valid_children": [ ],"icon": "/style/images/icons/blue-document.png"},
			"field": 		{"valid_children": [ "link", "index", "date", "function" ],"icon": "/style/images/icons/blue-folder.png"},
			"date": 		{"valid_children": [], "icon": "/style/images/icons/clock.png"},
			"function": 	{"valid_children": [], "icon": "/style/images/icons/function.png"}
	};
	var _themesTypes = {
			"#": 			{valid_children: -1, max_depth: -1, max_children: -1},
			"link": 		{"valid_children": [ ],"icon": "/style/images/icons/blue-document.png"},
	};
	var _stylesTypes = {
			"#": 			{valid_children: -1, max_depth: -1, max_children: -1},
			"link": 		{"valid_children": [ "link" ],"icon": "/style/images/icons/blue-document.png"},
	};
	// 
	// 
	// 
	var _fullPlugins 			= ["checkbox", "contextmenu", "dnd", "search", "sort", "state", "types", "unique", "wholerow"];
	var _indexesPlugins 		= ["types", "wholerow"];
	var	_packagesPlugins 		= ["dnd", "types", "contextmenu", "wholerow"];
	var	_linksCheckboxPlugins	= ["types", "checkbox", "wholerow"];
	var _elementsCheckboxPlugins= ["types", "checkbox", "wholerow", "search"];
	var	_linksHiddenPlugins		= ["types", "wholerow"];
	var	_linksHiddenPluginsEdit	= ["types", "contextmenu", "wholerow"];
	var	_headersPlugins 		= ["types", "wholerow"];
	var	_headersPluginsEdit 	= ["dnd", "types", "contextmenu", "wholerow"];
	var	_themePlugins			= ["types", "contextmenu", "wholerow"];
	var _stylesPlugins			= ["types", "contextmenu", "wholerow"];
	
	// 
	// 
	// 
	var _indexesAjax 		= {"url": REQBASE, "data": function (n){return { "act" : "children", "id" : n.id ? n.id.replace("node_","") : 0}} };
	var _indexesMathAjax	= {"url": REQBASE, "data" : function (n) {return { "act" : "childrenmath", "id" : n.id ? n.id.replace("node_","") : 0 }} };
	var _packagesAjax 		= {"url": REQBASE + "?mod=objects", "data": function (n){return {"act" : "showfolders", id: n.id ? n.id.replace(/f_|p_/,"") : 0}} };
	var _themesAjax 		= {"url": REQBASE + "?mod=objects", "data": function (n){return {"act" : "showthemestree"}} };
	var _stylesAjax			= {"url": REQBASE + "?mod=objects", "data": function (n){return {"act" : "showstylestree", parent: n.id ? n.id : 0}} };

	// 
	// 
	//
	var _packagesContext 		= { "items": function(node){ 
										var menu = {
											"Edit Object":{"icon": "fa fa-file-excel-o", "label": "Редактировать таблицу", "action": function(data){
													window.dispatchEvent(new Event("editMode"));
												}, "separator_after": true
											},
											"Add Folder":{"icon": "fa fa-folder-open", "label": "Добавить папку", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													_treeHelper.drawNode(data.reference, {position: "last", type: "folder", edit: true});
												}
											},
											"Add Object":{"icon": "fa fa-file-excel-o", "label": "Добавить таблицу", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													_treeHelper.drawNode(data.reference, {position: "last", type: "object", edit: true});
												}, "separator_after": true
											},
											"Rename":{"icon": "fa fa-edit", "label": "Переименовать", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.edit(inst.get_node(data.reference));													
												}
											}, 
											"Remove":{"icon": 'fa fa-trash', "label": "Удалить", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.delete_node(inst.get_selected()); 
												}
											}
										}
										if (node.type !== "object") delete(menu["Edit Object"]);
										return menu;
									}, "show_at_node": false
								};
	var _linksHiddenContext 	= { "items": function(node){ 
										return {
											"Remove":{"icon" : 'fa fa-trash', "label":"Удалить", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.delete_node(inst.get_selected()); 
												}
											}
										} 
									}, "show_at_node": false
								};
	var _headerColsContext		= { "items": function(node){ 
										var menu = {
											"Add Formula":{"icon": "fa fa-superscript", "label": "Добавить формулу", "action": function(data){
														_commonHelper.openDialogAddFormula();
												}, 
												"separator_after": true
											},
											"Add Time":{"icon": "fa fa-clock-o", "label": "Добавить временную точку", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													_treeHelper.drawNode(data.reference, {position: "last", type: "date", edit: true}, {text: "0"});
												}
											},
											"Add Field":{"icon": "fa fa-square-o", "label": "Добавить пустое поле", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													_treeHelper.drawNode(data.reference, {position: "last", type: "field", edit: true}, {text: "Пустое поле"});
												}
											},
											"Add Func":{"icon": "fa fa-superscript", "label": "Добавить функцию", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													_treeHelper.drawNode(data.reference, {position: "last", type: "function", edit: true}, {text: "Функция"});
												}, "separator_after": true
											},
											"Rename":{"icon": "fa fa-edit", "label": "Переименовать", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.edit(inst.get_node(data.reference));
												}
											}, 
											"Remove":{"icon": 'fa fa-trash', "label": "Удалить", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.delete_node(inst.get_selected()); 
												}
											}
										} 
										if (node.type == "field"){
											delete (menu["Add Formula"]);
										}
										if (node.type == "date"){
											delete (menu["Add Formula"]);
											delete (menu["Add Time"]);
											delete (menu["Add Field"]);
											delete (menu["Add Func"]);
										}
										if (node.type == "function"){
											delete (menu["Add Formula"]);
											delete (menu["Add Time"]);
											delete (menu["Add Field"]);
											delete (menu["Add Func"]);
										}
										return menu;
									}, "show_at_node": false
								};
	var _headerRowsContext		= { "items": function(node){ 
										// console.log(node);
										return {
											"Add Static Group":{"icon": "fa fa-plus-square", "label": "Добавить группу", "action": function(data){
													var selected = _treeHelper.getSelectedIds(data.reference);
													var parent = _treeHelper.drawNode(data.reference, {position: "first", type: "field", edit: false}, {text: "Группа"}, "#");
													if (selected.length != 0) _treeHelper.moveNodeToParent(data.reference, parent);
													window.dispatchEvent(new Event("refreshSpreadsheet"));
												}, "separator_after": true
											},
											"Move To Begin":{"icon": "fa fa-sort-asc", "label": "Переместить в начало", "action": function(data){
													_treeHelper.moveNodeToParent(data.reference, node.parent);
													window.dispatchEvent(new Event("refreshSpreadsheet"));
												}
											},
											"Sort Alpha Desc":{"icon": "fa fa-sort-alpha-asc", "label": "Сортировать по возрастанию", "action": function(data){
													if (node.parent == "#" && node.type == "link") _treeHelper.sortAlphaAsc(data.reference, "#");
													else _treeHelper.sortAlphaAsc(data.reference, node);
													window.dispatchEvent(new Event("refreshSpreadsheet"));
												}
											},
											"Sort Alpha Asc":{"icon": "fa fa-sort-alpha-desc", "label": "Сортировать по убыванию", "action": function(data){
													if (node.parent == "#" && node.type == "link") _treeHelper.sortAlphaAsc(data.reference, "#");
													else _treeHelper.sortAlphaDesc(data.reference, node);
													window.dispatchEvent(new Event("refreshSpreadsheet"));
												}, "separator_after": true
											},
											"Rename":{"icon" : "fa fa-edit", "label":"Переименовать", "action": function(data){
													var inst = $.jstree.reference(data.reference),
														nodes = inst.get_selected(true);
													if (nodes.length > 1){														
														var names = _.pluck(nodes, "text"),
															renameScanResult = RenameTool.scan(names),
															renameToolInput;
														var _cancel = function(){
															// renameToolInput.tooltip('destroy').remove();
															renameToolInput.remove();
															return false;
														}
														var _apply = function(newName){
															var newNames = RenameTool.recombine(newName, renameScanResult[1]);
															_.each(nodes, function(node, key){
																inst.rename_node(node, newNames[key])
															})
															// renameToolInput.tooltip('destroy').remove();
															renameToolInput.remove();
															window.dispatchEvent(new Event("refreshSpreadsheet"));
														}
														renameToolInput = $('<input id="rename-tool-input" type="text" value="' + renameScanResult[0] + '" style="width: 100%; min-width: 100px">')
															.css('pointerEvents','auto')
															.prependTo($("#header-rows-tree"))
															.keydown(
																function(e){
																	e = e || window.event;
																	switch(e.keyCode){
																		case 27: //Esc
																			return _cancel();
																		case 9: //Tab
																		case 13: //Enter
																			return _apply($(this).val())
																	}
																}
															);															
															// .tooltip({
															// 	position: 'top', 
															// 	trackMouse: false, 	
															// 	content: "<b>{</b>в скобках<b>}</b> остались НЕ повторяющиеся части.<br />Они будут возвращены назад в результате<br />"+
															// 								"<b>Enter</b> или <b>Tab</b> для сохранения<br />"+
															// 								"<b>Esc</b> для отмены<br />"});
														_.defer(function(){ renameToolInput.focus().select() });
														renameToolInput.on("focusout", function(){_cancel()});
													} else {
														obj = inst.get_node(data.reference);
														inst.edit(obj);
													}
												}
											}, 
											"Remove":{"icon" : 'fa fa-trash', "label":"Удалить", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.delete_node( inst.get_selected());
												}
											}
										} 
									}, "show_at_node": false
								};
	var _headersContext 		= { "items": function(node){ 
										return {
											"Rename":{"icon": "fa fa-edit", "label": "Переименовать", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.edit(inst.get_node(data.reference));													
												}
											}, 
											"Remove":{"icon": 'fa fa-trash', "label": "Удалить", "action": function(data){
													var inst = $.jstree.reference(data.reference);
													inst.delete_node(inst.get_selected());
												}
											}
										} 
									}, "show_at_node": false
								};

	// 
	// 
	// 
	// var _linksCheckbox 			= {"visible": true, "three_state": false, "whole_node": true, "tie_selection": true};
	var _linksCheckboxCheckbox	= {"visible": true, "whole_node": true, "keep_selected_style": false};

	// 
	// 
	// 
	var _prevOperation;
	var _defaultCheckCallBack 	= function(){ return true };
	var _packagesCheckCallBack 	= function(operation, node, node_parent, node_position, more){
									if(operation === "delete_node" && !confirm("Вы уверены, что хотите удалить?")) return false;
									return true;
								};
	var _headersCheckCallBack 	= function(operation, node, node_parent, node_position, more){
									if (operation === "move_node"){
										if (more.is_multi) return false;
									}
									if(operation === "delete_node"){
										if (_prevOperation === "move_node"){
											console.log("DnD попытался удалить node!");
										}
										if (!_.isEmpty(node.children)){
											alert("Нельзя удалять элементы с вложениями.");
											return false;
										}
										if(!confirm("Вы уверены, что хотите удалить?")) return false;
										return true;
									}
									_prevOperation = operation;
									return true;
								};	

	var _elementsCheckboxSearchCallBack = function(str, node) {
		if (str.search('l_') != -1){
			str = str.replace('l_', '');
			return node.li_attr.level == str;
		} else {
			regexp = new RegExp(str, "i");
			return node.text.search(regexp) != -1;
		}
	}
}