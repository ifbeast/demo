Actions = function(actions){
	var _actionCreate, _actionShow, _actionUpdate, _actionRemove;

	if (actions.create)	_actionCreate = actions.create;
	if (actions.show)	_actionShow   = actions.show;
	if (actions.update)	_actionUpdate = actions.update;
	if (actions.remove)	_actionRemove = actions.remove;

	var _request = function(data){
		return $.ajax({
			url: REQBASE + "?mod=objects",	dataType: "json", type: "post",	data: data}).
			// done(function(data){ console.log("Request done", data); }). 
			fail(function(data){ 
				console.log("Request failed", data);
				showMessage("Произошла ошибка.");
			}).always(function(data){ 
				if (data) {
					if (data.error && data.reauth) {
						showMessage(data.error);
						window.open(location.origin, '_blank');
					}
					else {console.log("Request finished", data); }
				}
			});
	}
	
	var _actions = {
		create: function(params){ if (_actionCreate) return _request({act: _actionCreate, params: params}); else return false;},
		show: 	function(id){ if (_actionShow) return _request({act: _actionShow, id: id}); else return false;},
		update: function(id, params){ if (_actionUpdate) return _request({act: _actionUpdate, id: id, params: params}); else return false;},
		remove: function(id){ if (_actionRemove) return _request({act: _actionRemove, id: id}); else return false;},
		request: function(data){ return _request(data); },		
	}

	return _actions;
}

HeaderActions = function(){
	var actions = new Actions({show: "showheaders", update: "saveheaders"});
		actions.addFormula = function(id, params){
			return actions.request({act: "add-formula", id: id, params: params});
		}
	return actions;
}

SpreadsheetActions = function(){
	var actions = new Actions({update: "savetemplate", remove: "removetemplate"});
		actions["show"] = function(id, params){
			return actions.request({act: "showtemplate", id: id, params: params});
		}
	return actions;
}

FolderActions = function(){
	return new Actions({create: "createfolder", update: "updatefolder", remove: "removefolder"});
}


ObjectActions = function(){
	var actions = new Actions({create: "createobject", update: "updateobject", remove: "removeobject"});
		actions["clone"] = function(id, params){ 
			return actions.request({act: "cloneobject", id: id, params: params});
		}
	return actions;
}

LinksActions = function(){
	var actions = new Actions({show: "showlinks"});
		actions["showgrouped"] = function(id, params){
				return actions.request({act: "showlinksgrouped", id: id, params: params});
			};
		actions["showlinksfiltered"] = function(id, params){
				return actions.request({act: "showlinksfiltered", id: id, params: params});
			};
	return actions;
}

ExportActions = function(){
	this.clearForm = function(){
		$('#reqform').empty();
	}

	this.setForm = function(data){
		this.clearForm();
		_.each(data, function(value, name){
			$('#reqform').append($('<input>').attr({type:'hidden', name:name, value: value}));
		});
	}

	this.sendForm = function(){
		$('#reqform').submit();
	}

}

ThemeActions = function() {
	var actions = new Actions({create: "createtheme", show: "showtheme", update: "updatetheme", remove: "removetheme"});
	return actions;
}

StyleActions = function() {
	var actions = new Actions({create: "createstyle", show: "showstyle", update: "updatestyle", remove: "removestyle"});
	return actions;
}
