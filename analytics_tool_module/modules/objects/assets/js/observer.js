Observer = function(){
	var _self = this;
	// 
	// 
	// 
	var _treeHelper = new TreeHelper();
	// 
	// 
	// 
	var _folder;
	this.getFolderId = function(){
		return _folder;
	}
	this.setFolderId = function(id){
		_folder = id.replace("f_", "");
	}
	// 
	// 
	// 
	var _package;
	this.getPackageId = function(){
		return _package;
	}
	this.setPackageId = function(id){
		_package = id.replace("p_", "");
	}
	// 
	// 
	// 	
	var _object;
	this.getObjectId = function(){
		return _object;
	}
	this.setObjectId = function(id){
		_object = id.replace("o_", "");
	}
	// 
	// 
	// 
	var _index;
	this.getIndexId = function(){
		return _index;
	}
	this.setIndexId = function(id){
		_index = id;
	}
	// 
	// 
	// 
	var _mathIndex;
	this.getMathIndex = function(){
		return _mathIndex;
	}
	this.setMathIndex = function(id){
		_mathIndex = id;
	}
	// 
	// 
	// 
	var _lastSelected = {"tree":"", "order":"", "type":""};
	this.getLastSelected = function(){
		return _lastSelected;
	}
	this.setLastSelected = function(tree, order, type){
		_lastSelected.tree = tree;
		_lastSelected.order = order;
		_lastSelected.type = type;
	}
	// 
	// 
	// 
	var _lastSelectedColumn = {"order":"", "type":""};
	this.getLastSelectedColumn = function(){
		return _lastSelectedColumn;
	}
	this.setLastSelectedColumn = function(order, type){
		_lastSelectedColumn.order = order;
		_lastSelectedColumn.type = type;
	}
	// 
	// 
	// 
	var _lastSelectedRow = {"order":"", "type":""};
	this.getLastSelectedRow = function(){
		return _lastSelectedRow;
	}
	this.setLastSelectedRow = function(order, type){
		_lastSelectedRow.order = order;
		_lastSelectedRow.type = type;
	}
	// 
	// 
	// 
	var _links;
	this.getLinks = function(){
		return _links;
	}
	this.setLinks = function(links){
		_links = links;
	}
	// 
	// 
	// 	
	var _elementsNames;
	this.getElementsNames = function(){
		return _elementsNames;
	}
	this.setElementsNames = function(elementsNames){
		_elementsNames = elementsNames;
	}
	// 
	// 
	// 	
	var _elementsMap;
	this.getElementsMap = function(){
		return _elementsMap;
	}
	this.setElementsMap = function(elementsMap){
		_elementsMap = elementsMap;
	}
	// 
	// 
	// 	
	var _classesNames;
	this.getClassesNames = function(){
		return _classesNames;
	}
	this.getClassesNamesWithoutIndex = function(){
		var index = this.getIndexId();
		var classes = this.getClassesNames();
		return classes;
	}
	this.setClassesNames = function(classesNames){
		_classesNames = classesNames;
	}
	// 
	// 
	// 
	var _classesMap;
	this.getClassesMap = function(){
		return _classesMap;
	}
	this.setClassesMap = function(classesMap){
		_classesMap = classesMap;
	}
	// 
	// 
	// 
	var _linksAttrs;
	this.getLinksAttrs = function(){
		return _linksAttrs;
	}
	this.setLinksAttrs = function(linksAttrs){
		_linksAttrs = linksAttrs;
		var linksMap = {};
		_.each(linksAttrs, function(links){
    		_.extend(linksMap, links['links']);
    	});
		this.setLinksMap(linksMap);		
	}
	// 
	// 
	// 
	var _unitsMap;
	this.getUnitsMap = function(){
		return _unitsMap;
	}
	this.setUnitsMap = function(unitsMap){
		this.setUnitsMap(unitsMap);
		var linksMap = {};
		_.each(unitsMap, function(links){
    		_.extend(linksMap, links);
    	});
		this.setLinksMap(linksMap);	
	}
	this.initUnitsMap = function(unitsMap){
		_unitsMap = unitsMap;
	}
	// 
	// 
	// 
	var _linksMap;
	this.getLinksMap = function(){
		return _linksMap;
	}
	this.setLinksMap = function(linksMap){
		_linksMap = linksMap;
	}
	// 
	// 
	// 
	var _linksSteps;
	this.getLinksSteps = function(){
		return _linksSteps;
	}
	this.setLinksSteps = function(linksSteps){
		_linksSteps = linksSteps;
	}
	// 
	// 
	// 
	var _linksFilterUnits;
	this.getLinksFilterUnits = function(){
		return _linksFilterUnits;
	}
	this.setLinksFilterUnits = function(linksFilterUnits){
		_linksFilterUnits = linksFilterUnits;		
	}
	// 
	// 
	// 
	var _sequence;
	this.getSequence = function(){
		return _sequence;
	}
	this.setSequence = function(value){
		_sequence = value;
	}
	// 
	// 
	// 
	var _units;
	this.getUnits = function(){
		return _units;
	}
	this.setUnits = function(units){
		_units = units;
	}
	// 
	// 
	// 
	var _source;
	this.getSource = function(){
		return _source;
	}
	this.setSource = function(source){
		_source = source;
	}
	// 
	// 
	// 
	var _subscription;
	this.getSubscription = function(){
		return _subscription;
	}
	this.setSubscription = function(subscription){
		_subscription = subscription;
	}
	// 
	// 
	// 
	var _sortAttributes = {};
	this.getSortAttributes = function(){
		return _sortAttributes;
	}
	this.setSortAttributes = function(columnsSort, rowsSort, order){
		_sortAttributes = {columnsSort: columnsSort, rowsSort: rowsSort, order: order};
	}
	this.setSortAttribute = function(attr, value){
		_sortAttributes[attr] = value;	
	}
	// // 
	// // 
	// // 
	// var _groupAttributes = {};
	// this.getGroupAttributes = function(){
	// 	return _groupAttributes;
	// }
	// this.setGroupAttributes = function(columnsGroup, groups){
	// 	_groupAttributes = {columnsGroup: columnsGroup, groups: groups};
	// }
	// 
	// 
	// 
	var _styles= {};
	this.getStyles= function(){
		return _styles;
	}
	this.setStyles = function(styles){
		_styles = styles;
	}
	// 
	// 
	// 
	var _columnsTree;
	this.getColumnsTree = function(){
		return _columnsTree;
	}
	this.setColumnsTree = function(columnsTree){
		console.log("setColumnsTree");
		_columnsTree = columnsTree;
		// this.initColumnsTreeLeafs(columnsTree);
	}
	// 
	// 
	// 
	var _rowsTree;
	this.getRowsTree = function(){
		return _rowsTree;
	}
	this.setRowsTree = function(rowsTree){
		_rowsTree = rowsTree;
		// this.initRowsTreeLeafs(rowsTree);
	}
	// 
	// 
	// 
	var _hiddenTree;
	this.getHiddenTree = function(){
		return _hiddenTree;
	}
	this.setHiddenTree = function(hiddenTree){
		_hiddenTree = hiddenTree;
	}
	// 
	// 
	// 
	var _groupsTree;
	this.getGroupsTree = function(){
		return _groupsTree;
	}
	this.setGroupsTree = function(groupsTree){
		_groupsTree = groupsTree;
	}
	// 
	// 
	// 
	// var _tableSize;
	// this.getTableSize = function(){
	// 	return _tableSize;
	// }
	// this.setTableSize = function(tableSize){
	// 	_tableSize = tableSize;
	// }
	// 
	// 
	// 
	var _columnsTreeLeafs;
	this.getColumnsTreeLeafs = function(){
		return _columnsTreeLeafs;
	}
	this.setColumnsTreeLeafs = function(columnsTreeLeafs){
		_columnsTreeLeafs = columnsTreeLeafs;
	}
	this.initColumnsTreeLeafs = function(tree){
		if (!tree) tree = this.getColumnsTree();
		this.setColumnsTreeLeafs(_treeHelper.getTreeLeafs(tree));
	}
	// 
	// 
	// 
	var _rowsTreeLeafs;
	this.getRowsTreeLeafs = function(){
		return _rowsTreeLeafs;
	}
	this.setRowsTreeLeafs = function(rowsTreeLeafs){
		_rowsTreeLeafs = rowsTreeLeafs;
	}
	this.initRowsTreeLeafs = function(tree){
		if (!tree) tree = this.getRowsTree();
		this.setRowsTreeLeafs(_treeHelper.getTreeLeafs(tree));
	}
	// 
	// Headers
	// 
	this.linksTable 	 = new Table();    // []
	this.aliasColumns 	 = new Alias();    // {}
	this.aliasRows 		 = new Alias(); 	  // {}
	this.elementsNames 	 = new Elements(); // {}	
	this.linksHidden 	 = new Links();	  // {}
	this.groupAttributes = new Groups();   // {}
	this.staticGroupAttributes = new Groups();   // {}

	this.getHeaders = function(flat){
		if (!flat) flat = false;
		var headers = "";
		var columnsTree = this.getColumnsTree();
		var rowsTree = this.getRowsTree();
		var hiddenTree = this.getHiddenTree();
		var groupsTree = this.getGroupsTree();
		var linksTable = this.linksTable.getTable();
		var aliasColumns = this.aliasColumns.getAlias();
		var aliasRows = this.aliasRows.getAlias();
		var elementNames = this.elementsNames.getElements();
		var linksHidden = this.linksHidden.getLinks();
		var sequence = this.getSequence();
		var units = this.getUnits();
		var source = this.getSource();
		var subscription = this.getSubscription();
		var sortAttributes = this.getSortAttributes();
		var groupAttributes = this.groupAttributes.getGroups();
		var staticGroupAttributes = this.staticGroupAttributes.getGroups();
		var styles = this.getStyles();
		var columns =  columnsTree.jstree(true).get_json("#", {"no_state": true, "flat": flat});
		var rows =  rowsTree.jstree(true).get_json("#", {"no_state": true, "flat": true});
		var hidden = hiddenTree.jstree(true).get_json("#", {"no_state": true});
		var groups = groupsTree.jstree(true).get_json("#", {"no_state": true});
		if (!_.isEmpty(columns) && !_.isEmpty(rows) && !_.isEmpty(linksTable))
			headers = JSON.stringify({
				"columns": columns, 
				"rows": rows, 
				"hidden": hidden, 
				"groups": groups,

				"linksTable": linksTable,
				"aliasColumns": aliasColumns, 
				"aliasRows": aliasRows,
				"elementNames": elementNames,
				
				"linksHidden": linksHidden,
				"sortAttributes": sortAttributes,
				"groupAttributes": groupAttributes,
				"staticGroupAttributes": staticGroupAttributes,
				
				"sequence": sequence,
				"units": units,
				"source": source,
				"subscription": subscription,
				"styles": styles
			});
		else 
			console.log(columns, rows, linksTable);
		return headers;
	}
	this.setHeaders = function(headers){
		if(headers.linksTable) this.linksTable.setTable(headers.linksTable);
		else this.linksTable.setTable([]);
		if(headers.aliasColumns) this.aliasColumns.setAlias(headers.aliasColumns);
		else this.aliasColumns.setAlias({});
		if(headers.aliasRows) this.aliasRows.setAlias(headers.aliasRows);
		else this.aliasRows.setAlias({});
		if(headers.elementNames) this.elementsNames.setElements(headers.elementNames);
		else this.elementsNames.setElements({});

		if(!_.isEmpty(headers.linksHidden)) this.linksHidden.setLinks(headers.linksHidden);
		else this.linksHidden.setLinks({});
		if(!_.isEmpty(headers.groupAttributes))
			if(!_.isEmpty(headers.groupAttributes.rows)) 
				this.groupAttributes.setGroups(headers.groupAttributes);
			else this.groupAttributes.clearGroups();
		else this.groupAttributes.clearGroups();
		if(!_.isEmpty(headers.staticGroupAttributes))
			if(!_.isEmpty(headers.staticGroupAttributes.rows))
				this.staticGroupAttributes.setGroups(headers.staticGroupAttributes);
			else this.staticGroupAttributes.clearGroups();
		else this.staticGroupAttributes.clearGroups();
		if(!_.isEmpty(headers.sortAttributes)) this.setSortAttributes(headers.sortAttributes.columnsSort, headers.sortAttributes.rowsSort, headers.sortAttributes.order);
		else this.setSortAttributes("", "", "");
		
		if(headers.sequence) this.setSequence(headers.sequence);
		else this.setSequence("");
		if(headers.units) this.setUnits(headers.units);
		else this.setUnits("");
		if(headers.subscription) this.setSubscription(headers.subscription);
		else this.setSubscription("");
		if(!_.isEmpty(headers.styles)) this.setStyles(headers.styles);
		else this.setStyles({});
	}
	this.clearHeaders = function(){
		this.linksTable.setTable([]);
		this.aliasColumns.setAlias({});
		this.aliasRows.setAlias({});
		this.elementsNames.setElements({});
		
		this.linksHidden.setLinks({});
		this.groupAttributes.clearGroups();
		this.staticGroupAttributes.clearGroups();
		this.setSortAttributes("", "", "");
		
		this.setSequence("");
		this.setUnits("");
		this.setSubscription("");
	}
	this.getCommonProperties = function(){
		return {
			'id': _self.getObjectId(),
			'sequence': _self.getSequence(),
			'subscription': _self.getSubscription(),
			'source': _self.getSource(),
			'units': _self.getUnits(),
			'theme': undefined,
			'styles': _self.getStyles().special,
			'sort' : (!_.isEmpty(_self.getSortAttributes())) ? 0 : 1,
			'columnsSort': _self.getSortAttributes().columnsSort,
			'rowsSort': _self.getSortAttributes().rowsSort,
			'order': _self.getSortAttributes().order
		};
	}
	this.setCommonProperties = function(data){
		console.log(data);
		if (data.id == 'sequence') _self.setSequence(data.value)
		if (data.id == 'subscription') _self.setSubscription(data.value)
		if (data.id == 'source') _self.setSource(data.value)
		if (data.id == 'units') _self.setUnits(data.value)
		if (data.id == 'theme') console.log("Need theme setter")
		if (data.id == 'styles') _self.setStyles({special: data.value})
		if (data.id == 'sort') _self.setSortAttributes(data.columnsSort, data.rowsSort, data.order);

	}
	this.getAdditionalProperties = function(){
		return {};
	}
	this.test = function(){
		var treeColumns = this.getColumnsTree();
		var treeRows = this.getRowsTree();
		console.log("----------- Test Observer -----------");
		console.log("Table", this.linksTable.getTable());
		console.log("Columns", this.aliasColumns.getAlias());
		console.log("Rows", this.aliasRows.getAlias());
		console.log("Hidden", this.linksHidden.getLinks());
		console.log("Elements", this.elementsNames.getElements());
		// console.log("ColumnsTree", $(this.getColumnsTree()).attr("id"));
		// console.log("RowsTree", $(this.getRowsTree()).attr("id"));
		// console.log("ColumnsTreeJson", treeColumns.jstree(true).get_json("#", {"no_state": true}));
		// console.log("RowsTreeJson", treeRows.jstree(true).get_json("#", {"no_state": true}));
		console.log("ColumnsTreeLeafs", this.getColumnsTreeLeafs());
		console.log("RowsTreeLeafs", this.getRowsTreeLeafs());
		console.log("Units", this.getUnits());
		console.log("Subscription", this.getSubscription());
		console.log("Sort Attributes", this.getSortAttributes());
		console.log("Group Attributes", this.groupAttributes.getGroups());
		console.log("Static Group Attributes", this.staticGroupAttributes.getGroups());
		// this.linksTable.test();
		// this.aliasColumns.test();
		// this.aliasRows.test();
	}
	// 
	// Add
	//
	this.getLinksByIndex = function(){
		var links = [];
		var indexId = _self.getIndexId(); 
		var aliasColumns = JSON.parse(JSON.stringify(_self.aliasColumns.getAlias()));
		var aliasRows = _self.aliasRows.getAlias();
		var linksMap = _self.getLinksMap();
		var linksFiltred = _self.getLinks();
		_.each(aliasRows, function(aliasRow){
			var needAdd = false;
			_.each(linksFiltred, function(link){
				_.each(aliasRow, function(element){	
					if (linksMap[link].hasOwnProperty(element)) needAdd = true;
					else needAdd = false; 
				});
				if (needAdd) links.push(link);
			});
		});

		return _.uniq(links);
	}

	// this.addIndex = function(){
	// 	this.getNodesByLinks(this.getLinksByIndex());
	// }

	this.replaceIndex = function(source){
		var targetId = this.getIndexId();
		var targetName = this.getElementsNames()[targetId];
		var targetLinks = this.getLinksByIndex();
		var sourceLinks = _.uniq(_.pluck(_self.linksTable.getFieldsByColumn(source) ,"link"));
		var sourceElements = _self.aliasColumns.getElementsByItem(source);
		var sourceId, targetLink;

		_.each(sourceElements, function(elementId){
			var elementType  = _self.elementsNames.getTypeByIds([elementId]);
			if (elementType == "index") sourceId = elementId;
			else {				
				alert("Исходный показатель не найден.");	
			}
		});
		console.log("sourceId", sourceId);
		if (sourceId){
			console.log("Start replace");
			if (targetLinks.length >= sourceLinks.length){
				var linksMap = _self.getLinksMap();
				console.log(linksMap);
				var sourceAliasColumn = source;
				var sourceColumnElements = JSON.parse(JSON.stringify(_self.aliasColumns.getElementsByItem(sourceAliasColumn)));
				_.each(sourceLinks, function(sourceLink){
					var sourceAliasRow = _self.linksTable.getAliasRowByLink(sourceLink);
					var sourceRowElements = _self.aliasRows.getElementsByItem(sourceAliasRow);
					if (sourceColumnElements.indexOf(sourceId) != -1) sourceColumnElements[sourceColumnElements.indexOf(sourceId)] = targetId;
					var sourceUnionElements = _.sortBy(_.union(sourceColumnElements, sourceRowElements));
					_.each(linksMap, function(value, key){
						if (_.sortBy(_.keys(value)).join(",") == sourceUnionElements.join(",")) targetLink = key;
					});
					if (_.contains(targetLinks, targetLink)){
						var fields = _self.linksTable.getFieldsByLink(sourceLink);
						_.each(fields, function(field){
							field.link = targetLink;
						});
						console.log("Replaced links to", targetLink);
					}
				});
				_self.elementsNames.removeElement(sourceId);
				_self.elementsNames.addElement(targetId, targetName, "index");
				console.log(_self.elementsNames.getElements());
				if (sourceElements.indexOf(sourceId) != -1) sourceElements[sourceElements.indexOf(sourceId)] = targetId;
				console.log(_self.aliasColumns.getAlias());
				var nodeName = _self.elementsNames.getNameByIds(sourceElements);
				var columnsTree = _self.getColumnsTree();
				columnsTree.jstree(true).rename_node(source, nodeName);
			}
		}
		if (targetLinks.length < sourceLinks.length){ 
			console.log(targetLinks.length, targetLinks, sourceLinks.length, sourceLinks);
			alert("Заменяемых ссылок меньше, чем исходных");
		}
	}

	this.addDate = function(parent, date, orderColumn){
		// var fields = this.linksTable.getFieldsByColumn(parent);
		var fields = this.linksTable.getFirstOrderFieldsByColumn(parent);
		console.log("Add Date to Fields", fields);
		_.each(fields, function(field){
			var hasFieldWithDate = _self.linksTable.addFieldDate(field, date, orderColumn);
			// if(hasFieldWithDate) console.log("ALERT: Ссылка уже есть в таблице.", field, date);
			// else console.log("ALERT: Ссылку добавлена в таблицу.", field, date);
		})
	}

	this.addFunc = function(parent, func, orderColumn){
		// var fields = this.linksTable.getFieldsByColumn(parent);
		var fields = this.linksTable.getFirstOrderFieldsByColumn(parent);
		console.log("Add Func to Fields", fields);
		_.each(fields, function(field){
			var hasFieldWithFunс = _self.linksTable.addFieldFunc(field, func, orderColumn);
			// if(hasFieldWithFunс) console.log("ALERT: Ссылка уже есть в таблице.", field, func);
			// else console.log("ALERT: Ссылку добавлена в таблицу.", field, func);
		})
	}

	this.addField = function(parent, orderColumn){
		var fields = this.linksTable.getFirstOrderFieldsByColumn(parent);
		console.log("Add Field to Fields", fields);
		_.each(fields, function(field){
			var hasFieldWithFunс = _self.linksTable.addFieldEmpty(field, orderColumn);
			// if(hasFieldWithFunс) console.log("ALERT: Ссылка уже есть в таблице.", field);
			// else console.log("ALERT: Ссылку добавлена в таблицу.", field);
		})	
	}

	this.addHiddenLinks = function(nodes, links){
		var hiddenTree = this.getHiddenTree();
		_.each(links, function(link){
			var hasLink = _self.linksHidden.addLink(link);
			if (!hasLink){
				var parents = _.where(nodes, {"id": link, "parent": "#"});
				var childrenNames = _.pluck(_.where(nodes, {"parent": link}), "text");
				_.each(parents, function(node){
					_treeHelper.drawNode(_self.getHiddenTree(), {position: "after", type: node.type, edit: false}, {id: node.id, text: node.text + " :: " + childrenNames.join(" :: ")}, node.parent);
				});
			}
		});
	}

	this.addHiddenLink = function(node){
		var hiddenTree = this.getHiddenTree();
		var link = node.li_attr.link;
		var hasLink = _self.linksHidden.addLink(link);
		if (!hasLink){
			_treeHelper.drawNode(_self.getHiddenTree(), {position: "after", type: node.type, edit: false}, {id: link, text: node.text}, "#");
		}
	}
	// 
	// New Add Links
	// 
	this.getNodesByLinks = function(links, columnsClasses, rowsClasses, columnsNames, rowsNames){
		if (!links) links = this.getLinks();
		var linksSteps = this.getLinksSteps();
		_.each(links, function(link){
			var node = _self.getNodeByLink(link, columnsClasses, rowsClasses, columnsNames, rowsNames);
			var step = linksSteps[link];
			if (node.column.needDraw) _treeHelper.drawNode(_self.getColumnsTree(), {position: "after", type: node.column.type, edit: false}, {id: node.column.key, text: node.column.text, step: step});
			else {
				var order = _treeHelper.getOrderById(node.column.key, _self.getColumnsTree());
				_self.addOrderColumnNeedAddField(node.column.key, order);
			}
			if (node.row.needDraw) _treeHelper.drawNode(_self.getRowsTree(), {position: "after", type: node.row.type, edit: false}, {id: node.row.key, text: node.row.text, step: step});
			else {
				var order = _treeHelper.getOrderById(node.row.key, _self.getRowsTree());
				_self.addOrderRowNeedAddField(node.row.key, order);
			}
		});
		this.addNeedAddFields();
	}
	
	var _needAddFields = [];
	this.getNeedAddFields = function(){
		return _needAddFields;
	}	
	this.setNeedAddFields = function(needAddFields){
		_needAddFields = needAddFields;
	}
	this.pushNeedAddField = function(field){
		_needAddFields.push(field);
	}
	this.addOrderColumnNeedAddField = function(aliasColumn, orderColumn){
		var fields = _.where(_needAddFields, {"aliasColumn":aliasColumn.toString()});
		_.each(fields, function(field){
			field["orderColumn"] = orderColumn.toString();
		});
	}
	this.addOrderRowNeedAddField = function(aliasRow, orderRow){
		var fields = _.where(_needAddFields, {"aliasRow":aliasRow.toString()});
		_.each(fields, function(field){
			field["orderRow"] = orderRow.toString();
		});
	}
	this.addNeedAddFields = function(){
		var fields = this.getNeedAddFields();
		console.log("----------- addNeedAddFields -----------", fields);
		_.each(fields, function(field){
			_self.linksTable.addField(field);
		});
		this.setNeedAddFields([]);
		window.dispatchEvent(new Event("refreshSpreadsheet"));
	}

	this.getNodeByLink = function(link, columnsClasses, rowsClasses, columnsNames, rowsNames){
		var column = {key:"", text:"", type:"", needDraw:""},
			row = 	 {key:"", text:"", type:"", needDraw:""};
		var node = {column: column, row: row};
		var columnElements = [],
			rowElements = [];
		var linksMap = this.getLinksMap(),
			indexId = this.getIndexId(),
			elementsNames = _self.getElementsNames();
			elementsMap = _self.getElementsMap();
			classesMap =  _self.getClassesMap();
		_.each(linksMap[link], function(value, key){
			if (_.isEmpty(columnsClasses) || _.isEmpty(rowsClasses)){
				if (key == indexId) columnElements.push(key);
				else rowElements.push(key);
			} else {
				if (_.contains(columnsClasses, classesMap[key])) {
					if (key == indexId)	columnElements.unshift(key);
					else columnElements.push(key);
				}
				else rowElements.push(key);
			}
			var text = elementsNames[key];
			var type = (key == indexId) ? "index" : "link";
			_self.elementsNames.addElement(key, text, type);
		});
		_.extend(column, this.aliasColumns.addItem(columnElements));
		_.extend(row, this.aliasRows.addItem(rowElements));
		column.text = this.elementsNames.getNameByIds(columnElements, columnsNames, classesMap);
		row.text = this.elementsNames.getNameByIds(rowElements, rowsNames, classesMap);
		column.type = this.elementsNames.getTypeByIds(columnElements);
		row.type = this.elementsNames.getTypeByIds(rowElements);
		// var field = {"aliasColumn": column.key, "aliasRow": row.key, "link": link, "date": "0", "func": ""};
		var field = {"aliasColumn": column.key, "aliasRow": row.key, "link": link};
		if (!this.linksTable.hasField(field)) this.pushNeedAddField(field);
		return node;
	}	
	// 
	// Remove
	// 
	this.onRemoveColumn = function(item, order){
		console.log("----------- onRemoveColumn -----------", item, order);
		var elements = _self.aliasColumns.getElementsByItem(item);
		_.each(elements, function(element){
			if (_self.aliasColumns.needElementDelete(element)) _self.elementsNames.removeElement(element);
		});
		_self.aliasColumns.removeItem(item);
		_self.linksTable.removeColumns(order);
	}

	this.onRemoveRow = function(item, order){
		console.log("----------- onRemoveRow -----------", item, order);
		var elements = _self.aliasRows.getElementsByItem(item);
		_.each(elements, function(element){
			if (_self.aliasRows.needElementDelete(element)) _self.elementsNames.removeElement(element);
		});
		_self.aliasRows.removeItem(item);
		_self.linksTable.removeRows(order);
	}
	// 
	// Reorder
	//
	this.reorderColumnsFull = function(newOrder, bringParentBack){
		var oldOrder = this.getColumnsTreeLeafs();
		this.linksTable.reorderColumnsFull(newOrder, oldOrder, bringParentBack);
		this.setColumnsTreeLeafs(newOrder);
	}
	this.reorderRowsFull = function(newOrder, bringParentBack){
		var oldOrder = this.getRowsTreeLeafs();
		this.linksTable.reorderRowsFull(newOrder, oldOrder, bringParentBack);
		this.setRowsTreeLeafs(newOrder);
	}
}