$( document ).ready(function() {
	$("#spreadsheet").wijspread({sheetCount:1}); 
	spread = $("#spreadsheet").wijspread("spread"); 
	sheet = spread.getActiveSheet();

	var borderStyle = [
					"Empty",
					"Thin",
					"Medium",
					"Dashed",
					"Dotted",
					"Thick",
					"Double",
					"Hair",
					"MediumDashed",
					"DashDot",
					"MediumDashDot",
					"DashDotDot",
					"MediumDashDotDot",
					"SlanteDashDot"
					];
	
	var theme = {	
					header: {background: "#C00000", text: "#FFFFFF"}, 
					field: {background: "#FFFFFF", text: "#000000"}, 
					subfield: {background: "#FDE9D9", text: "#000000"}, 
					innerBorder: {color: "#E26B0A", style: "Thin"}, 
					outterBorder: {color: "#C00000", style: "Thin"}
				};

	// 
	// Spreadsheet toolbar
	//     
    $("#btnGenerate").click(btnGenerate_Click);
    $("#btnShow").click(btnShow_Click);
    $("#btnLoad").click(btnLoad_Click);
    $("#btnSave").click(btnSave_Click);
    $("#btnExportJson").click(btnExportJson_Click);	

    $("#btnMergeCells").click(btnMerge_Click);
    $("#btnUnmergeCells").click(btnUnMerge_Click);
    $("#btnTextIndentIncrease").click(btnTextIndentIncrease_Click);
    $("#btnTextIndentDecrease").click(btnTextIndentDecrease_Click);

    $("#btnSetHeader").click(btnSetHeader_Click);
    $("#btnSetField").click(btnSetField_Click);
    $("#btnSetSubField").click(btnSetSubField_Click);
    $("#btnClearColor").click(btnClearColor_Click);
    $("#btnSetInnerBorder").click(btnSetInnerBorder_Click);
    $("#btnSetOuterBorder").click(btnSetOuterBorder_Click);
    $("#btnClearBorder").click(btnClearBorder_Click);
	
	// 
	// Spreadsheet JS
	// 
	function btnGenerate_Click(event) { 
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'generatetemplate',
				id: obs.objectId
				},
			success: function(data){
				if (data){
					spread.fromJSON(data);
					sheet = spread.getActiveSheet();
				}
			}
		});
	}
	function btnShow_Click(event){

	}
	function btnSave_Click(event) {
		var template = spread.toJSON();
		if (template.sheets.Sheet1.data){
			_.each(template.sheets.Sheet1.data.dataTable, function(row){
				_.each(row, function(column){
					if (_.has(column, "value")){
						delete column.value;
					}
				})
			})			
		}
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'savetemplate',
				id: obs.objectId,
				template: JSON.stringify(template)
				},
			success: function(data){
				console.log("template saved " + data);
			}
		});
	}
	function btnLoad_Click(event) {
		$.ajax({
			url: "/?mod=objects",
			dataType:'json',
			type:'post',
			data:{
				act:'showtemplate',
				id: obs.objectId
				},
			success: function(data){
				if (data){
					spread.fromJSON(JSON.parse(data));
					// spread.fromJSON(data);
					sheet = spread.getActiveSheet();
				} else {
					console.log("nothing to load");
				}
			}
		});
	}
	function btnExportJson_Click(event) { 
		jsonStr = JSON.stringify(spread.toJSON());
		console.log(jsonStr);
	}

	function btnMerge_Click(event) { 
	    var sel = sheet.getSelections(); 
	    if (sel.length > 0) { 
	        sel = getActualCellRange(sel[sel.length - 1], sheet.getRowCount(), sheet.getColumnCount()); 
	        sheet.addSpan(sel.row, sel.col, sel.rowCount, sel.colCount); 
	    }
	} 
	function btnUnMerge_Click(event) { 
	    var sel = sheet.getSelections(); 
	    if (sel.length > 0) { 
	        sel = getActualCellRange(sel[sel.length - 1], sheet.getRowCount(), sheet.getColumnCount()); 
	        sheet.isPaintSuspended(true); 
	        for (var i = 0; i < sel.rowCount; i++) { 
	            for (var j = 0; j < sel.colCount; j++) { 
	                sheet.removeSpan(i + sel.row, j + sel.col); 
	            } 
	        } 
	        sheet.isPaintSuspended(false); 
	    } 
	}
	function btnTextIndentIncrease_Click(event){
		var sels = sheet.getSelections(); 
		for (var n = 0; n < sels.length; n++) { 
		    var range = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount()); 
		    for (var i = 0; i < range.rowCount; i++) { 
		        for (var j = 0; j < range.colCount; j++) { 
		            var cell = sheet.getCell(range.row + i, range.col + j);
		            if (!cell.textIndent()){
		            	cell.textIndent(1);
		            } else {
		            	cell.textIndent(cell.textIndent() + 1);
		            }
		        } 
		    }
		}
	}
	function btnTextIndentDecrease_Click(event){
		var sels = sheet.getSelections(); 
		for (var n = 0; n < sels.length; n++) { 
		    var range = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount()); 
		    for (var i = 0; i < range.rowCount; i++) { 
		        for (var j = 0; j < range.colCount; j++) { 
		            var cell = sheet.getCell(range.row + i, range.col + j);
		            if (!cell.textIndent()){
		            	cell.textIndent(0);
		            } else {
		            	cell.textIndent(cell.textIndent() - 1);
		            }
		        } 
		    }
		}
	}

	function btnSetHeader_Click(event) {
		setColorForCellRange(theme.header.background, theme.header.text, $.wijmo.wijspread.HorizontalAlign.center);

	}
	function btnSetField_Click(event) { 
		setColorForCellRange(theme.field.background, theme.field.text, $.wijmo.wijspread.HorizontalAlign.left);
	}
	function btnSetSubField_Click(event) { 
		setColorForCellRange(theme.subfield.background, theme.subfield.text, $.wijmo.wijspread.HorizontalAlign.left);
	}
	function btnClearColor_Click(event){
		clearColorForCellRange();
	}
	function btnSetInnerBorder_Click(event) {
	    var color = theme.innerBorder.color; 
	    var style = borderStyle.indexOf(theme.innerBorder.style); 
	    var lineBorder = new $.wijmo.wijspread.LineBorder(color, style); 
	  
	    var sels = sheet.getSelections(); 
	    for (var n = 0; n < sels.length; n++) { 
	        var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount()); 
	        sheet.setBorder(sel, lineBorder, {all: true}); 
	    } 
	}
	function btnSetOuterBorder_Click(event) {
		var color = theme.outterBorder.color; 
		var style = borderStyle.indexOf(theme.outterBorder.style);
		var lineBorder = new $.wijmo.wijspread.LineBorder(color, style); 
		
		var sels = sheet.getSelections(); 
		var selsArray = sels.toArray();

		 	// Top
		 	for (var n = selsArray[0].col; n < selsArray[0].col + selsArray[0].colCount; n++) {
		 		sheet.getCell(selsArray[0].row, n).borderTop(lineBorder);
		 		sheet.getCell(selsArray[0].row - 1, n).borderBottom(null);
		 	}
		 	// Right
		 	for (var n = selsArray[0].row; n < selsArray[0].row + selsArray[0].rowCount; n++) {
		 		sheet.getCell(n, selsArray[0].col + selsArray[0].colCount - 1).borderRight(lineBorder);
		 	}
		 	// Left
		 	for (var n = selsArray[0].row; n < selsArray[0].row + selsArray[0].rowCount; n++) {
		 		sheet.getCell(n, selsArray[0].col).borderLeft(lineBorder);
		 		sheet.getCell(n, selsArray[0].col - 1).borderRight(null);
		 	}
		 	// Bottom
		 	for (var n = selsArray[0].col; n < selsArray[0].col + selsArray[0].colCount; n++) {
		 		sheet.getCell(selsArray[0].row + selsArray[0].rowCount - 1, n).borderBottom(lineBorder);
		 	}
	}
	function btnClearBorder_Click(event) { 
		var sels = sheet.getSelections(); 
		for (var n = 0; n < sels.length; n++) { 
		    var sel = getActualCellRange(sels[n], sheet.getRowCount(), sheet.getColumnCount()); 
		    sheet.setBorder(sel, null, {all: true}); 
		}
	}
	function clearColorForCellRange(){
		var sel = sheet.getSelections();
		for (var i = 0; i < sel.length; i++) { 
	        var range = getActualCellRange(sel[i], sheet.getRowCount(), sheet.getColumnCount()); 
	        for (var i = 0; i < range.rowCount; i++) { 
	            for (var j = 0; j < range.colCount; j++) { 
	                sheet.getCell(range.row + i, range.col + j).clearStyleProperty("backColor");
	                sheet.getCell(range.row + i, range.col + j).clearStyleProperty("foreColor");
	            } 
	        } 
	    }
	    sheet.repaint();
	}
	function setColorForCellRange(backColor, foreColor, hAlign){
		var sel = sheet.getSelections();
		for (var i = 0; i < sel.length; i++) { 
	        var range = getActualCellRange(sel[i], sheet.getRowCount(), sheet.getColumnCount()); 
	        for (var i = 0; i < range.rowCount; i++) { 
	            for (var j = 0; j < range.colCount; j++) { 
	                sheet.getCell(range.row + i, range.col + j).backColor(backColor);
	                sheet.getCell(range.row + i, range.col + j).foreColor(foreColor);
	                sheet.getCell(range.row + i, range.col + j).font("12px Calibri").hAlign(hAlign); 
	            } 
	        } 
	    }
	}

	function getActualCellRange(cellRange, rowCount, columnCount) { 
	    if (cellRange.row == -1 && cellRange.col == -1) { 
	        return new $.wijmo.wijspread.Range(0, 0, rowCount, columnCount); 
	    } 
	    else if (cellRange.row == -1) { 
	        return new $.wijmo.wijspread.Range(0, cellRange.col, rowCount, cellRange.colCount); 
	    } 
	    else if (cellRange.col == -1) { 
	        return new $.wijmo.wijspread.Range(cellRange.row, 0, cellRange.rowCount, columnCount); 
	    } 

	    return cellRange; 
	}
});