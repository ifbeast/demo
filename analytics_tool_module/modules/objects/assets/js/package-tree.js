var defaultTreeNodeTypes = $.jstree.defaults.types = {
	// "max_depth": -2,
	// "max_children": -2,
	
		"default": {
			"valid_children": "none",
			max_children: -1,
			max_depth: -1,
			"icon":"/style/images/icons/blue-document.png"
		},
		"folder": {
			"valid_children": [ "default", "folder", 'folderempty' ],
			"icon": "/style/images/icons/blue-folder.png"
		},
		"empty": {
			"valid_children": [ "default", "folder" ],
			"icon": "/style/images/icons/blue-folder-broken.png"
		},
		"group": {
			"valid_children": [ "default", "folder" ],
			"icon": "/style/images/icons/blue-folder-share.png"
		},
		"linkgroup": {
			"valid_children": [ "default", "folder" ],
			"icon": "/style/images/icons/drawer.png"
		},
		"multilinks": {
			"valid_children": [ "default", "folder" ],
			"icon": "/style/images/icons/chart--plus.png"
		},
		"table": {
			"valid_children": "none",
			"icon": "/style/images/icons/blue-document-table.png"
		},
		"template": {
			"valid_children": "none",
			"icon": "/style/images/icons/blue-document-template.png"
		},

		"date": {
			"valid_children": [ "link", "index", "function", "field" ],
			"icon": "/style/images/icons/clock.png"
		},
		"function": {
			"valid_children": [ "link", "index", "date", "field" ],
			"icon": "/style/images/icons/function.png"
		},
		
		"index": {
			"valid_children": [ "link", "date", "function", "field" ],
			"icon": "/style/images/icons/medal.png"
		},
		"link": {
			"valid_children": [ "link", "index", "date", "function", "field" ],
			"icon": "/style/images/icons/blue-document.png"
		},
		"field": {
			"valid_children": [ "link", "index", "date", "function", "field" ],
			"icon": "/style/images/icons/blue-document-number.png"
		}
};

// var defaultTreePlugins = ["themes","json_data","ui","crrm","cookies","dnd","types","hotkeys","contextmenu"];
var defaultTreePlugins  = $.jstree.defaults.plugins = ["themes","ui","dnd","types","hotkeys", "contextmenu"];

$.jstree.defaults.core.check_callback = function(){
	return true;
}


//end