Helpers = function(){
	this.common = new CommonHelper();
	this.spreadsheet = new SpreadsheetHelper();
	this.tree = new TreeHelper();
	this.form = new FormHelper();
	this.tips = new Tips();
}
// 
// 
// 
CommonHelper = function(){
	var _self = this;
	// 
	// Modes&State
	// 
	var _needSaveTrigger = false;

	this.switchExportMode = function(layout, packageLayout, spreadsheetLayout){
		layout.show("west");
		layout.open("west");
		packageLayout.close("west");
		packageLayout.hide("west");
		spreadsheetLayout.close("west");
		spreadsheetLayout.close("east");
		spreadsheetLayout.hide("west");
		spreadsheetLayout.hide("east");
		$("#state-edit-group").addClass("hide");
		$("#spreadsheet-tools-edit-mode").addClass("hide");
		$(".header-tools").addClass("hide");
		$("#btnEditObject").removeClass("hide");

		if ($("#header-cols-tree").jstree(true)) $("#header-cols-tree").jstree(true).destroy();
		if ($("#header-rows-tree").jstree(true)) $("#header-rows-tree").jstree(true).destroy();
		if ($("#header-hidden-tree").jstree(true)) $("#header-hidden-tree").jstree(true).destroy();
		if ($("#header-groups-tree").jstree(true)) $("#header-groups-tree").jstree(true).destroy();
		if ($("#indexes-tree").jstree(true)) $("#indexes-tree").jstree(true).destroy();
		if ($("#indexes-math-tree").jstree(true)) $("#indexes-math-tree").jstree(true).destroy();
		if ($("#links-checkbox-tree").jstree(true)) $("#links-checkbox-tree").jstree(true).destroy();

		$.bbq.removeState("act");
	}

	this.switchEditMode = function(layout, packageLayout, spreadsheetLayout){
		layout.close("west");
		layout.hide("west");
		packageLayout.close("west");
		packageLayout.hide("west");
		spreadsheetLayout.open("west");
		spreadsheetLayout.show("east");
		spreadsheetLayout.open("east");
		$("#btnEditObject").addClass("hide");
		$("#state-export-group").addClass("hide");
		$("#state-edit-group").removeClass("hide");
		$("#spreadsheet-tools-edit-mode").removeClass("hide");
		$(".header-tools").removeClass("hide");		
		this.changeNeedSaveTrigger();
	}

	this.switchAddHeadersMode = function(layout, packageLayout, spreadsheetLayout){
		// packageLayout.show("west");
		// packageLayout.open("west");
		$("#dialog-add-data").dialog("open");
	}

	this.needSave = function(){
		return _needSaveTrigger;
	}

	this.changeNeedSaveTrigger = function(){		
		_needSaveTrigger = !_needSaveTrigger;
	}
 
	this.hideAllState = function(){
		$("#btnEditObject").addClass("hide");
		$("#state-export-group").addClass("hide");
		$("#state-edit-group").addClass("hide");
		$("#refresh-group").addClass("hide");
	}
	// 
	// Export
	// 
	this.showExportGroup = function(){
		$("#state-export-group").removeClass("hide");
	}

	this.showEditButton = function(){
		$("#btnEditObject").removeClass("hide");
	}

	this.setStateHeader = function(prefix, header){
		$("#state-header").text(prefix + " " + header);
	}

	this.showRefreshGroup = function(){
		$("#refresh-group").removeClass("hide");
	}

	this.setSelectExportDate = function(date){
		if (date) $("#export-date-select").attr("value", date.replace("/", "-"));
	}
	this.getSelectExportDate = function(){
		return $("#export-date-select").val();
	}
	// 
	// Dialog Save
	//
	this.openDialogSave = function(){		
	    $("#dialog-save-cofirm").dialog("open");
	} 
	//
	// Dialog Add Data
	// 
	this.openDialogAddData = function() {
		$("#dialog-add-data").dialog("open");
		$("#dialog-add-data .tree-wrapper").tabs();
		$("#btn-links-filter").click(function(){
			_self.openDialogLinksFilter();
		});
	}
	var _addDataModes = ["default", "hidden"];
	var _addDataMode = _addDataModes[0];
	this.getAddDataMode = function() {
		return _addDataMode;
	}
	this.setAddDataMode = function(mode) {
		if (_.isString(mode)) _addDataMode = mode;
		if (_.isNumber(mode)) _addDataMode = _addDataModes[mode];
	}
	// 
	// Dialog Add Links
	// 
	this.openDialogAddLinks = function(classes, message){
		$("#dialog-add-links-message").text(message);
		$("#columns-links-select").empty();
		$("#rows-links-select").empty();
		_.each(classes, function(name, id){
			$("#columns-links-select").append("<option value='" + id + "'>" + name + "</option>");
			$("#rows-links-select").append("<option value='" + id + "'>" + name + "</option>");
		})
		$("#columns-names-select").empty();
		$("#rows-names-select").empty();
	    $("#dialog-add-links").dialog("open");
	}
	this.getSelectColumns = function(){
		return $("#columns-links-select").val();
	}
	this.getSelectRows = function(){
		return $("#rows-links-select").val();
	}

	this.getSelectColumnsNames = function(){
		return $("#columns-names-select").val();
	}
	this.getSelectRowsNames = function(){
		return $("#rows-names-select").val();
	}
	// 
	// Dialog Links Filter
	// 
	this.openDialogLinksFilter = function(){
		$("#dialog-links-filter").dialog("open");
	};

	this.getCheckboxesLinksFilter = function(){
		var units = [];
		$("#dialog-links-filter #units input:checked").each(function(){
			units.push({id:$(this).val()});
		});
		return _.pluck(units, "id");
	}
	// this.initLinksUnitsSelect = function(units){
	// 	$("#links-units-select").empty();
	// 	_.each(units, function(unit){
	// 		$("#links-units-select").append("<option value='" + unit.id + "''>" + unit.name + "</option>");
	// 	});
	// }

	// 
	// Dialog Add Formula
	// 
	this.openDialogAddFormula = function() {
		$("#dialog-add-formula").dialog("open");
	}
	this.getSelectDates = function() {
		return $("#dates-select").val();
	}
	this.getSelectFormulas = function() {
		return funcs = $("#formulas-select").val();
	}
	// 
	// Dialog Themes
	// 
	this.openDialogEditThemes = function() {
		$("#dialog-edit-theme").dialog("open");
		$("#theme-tabs").tabs();
	}

}	
// 
// 
// 
SpreadsheetHelper = function(){
	var _self = this,
		_treeHelper = new TreeHelper();

	// 
	// 
	// 
	this.resize = function(spread, width, height) {
		$("#spreadsheet").css({"width": width, "height": height});
		spread.repaint();
	}
	// 
	// Template
	// 
	this.getTemplate = function(spread){
		spread.getActiveSheet().clearSelection();
		var template = spread.toJSON();
		if (template.sheets.Sheet1.data){
			_.each(template.sheets.Sheet1.data.dataTable, function(row){
				_.each(row, function(column){
					if (_.has(column, "value")) delete column.value;
					if (_.has(column, "style")){
						if (_.isEmpty(column.style)) delete column.style;
						if (_.isObject(column.style) && _.has(column.style, "name")) delete column.style.name;
					}
				})
			})
		}
		console.log(template);
		return template;
	}
	// 
	// Table Size
	// 
	var _tableSize = {};
	this.getTableSize = function() {
		return _tableSize;
	}
	this.setTableSize = function(tableSize) {
		_tableSize = tableSize;
	}

	var _inTableSize = function(selection) {
		if ((selection.col - _tableSize.left < 0) || (selection.row - _tableSize.top < 0) || (selection.col + selection.colCount > _tableSize.width) || (selection.row + selection.rowCount > _tableSize.height))
			return false;
		else 
			return true;
	}
	this.inTableSize = function(selection) {
		return _inTableSize(selection);
	}
	
	var _inHeadersSize = function(selection) {
		if ((selection.col < _tableSize.left) || (selection.row < _tableSize.top))
			return true;
		else 
			return false;
	}
	this.inHeadersSize = function(selection) {
		return _inHeadersSize(selection);
	}
	//
	// Expectation
	// 
	var _selectionModes = [
		{name: "default", 	callback: function(){ return false; }},
		{name: "value", 	callback: function(sheet){
			var selection = _.last(sheet.getSelections());
	    	var value = sheet.getValue(selection.row, selection.col) || "";
	    	if (!_.isEmpty(value) && value.search(/\d+/) != -1) {
	    		value = parseFloat(value.replace(" ", ""));
	    	}
			return value;
		}},
		{name: "column", 	callback: function(sheet){ 
			var selection = _.last(sheet.getSelections());
			var tableSize = _self.getTableSize();
			var column = selection.col - tableSize.left;
			return column; 
		}},
		{name: "row", 		callback: function(sheet){ 
			var selection = _.last(sheet.getSelections());
			var tableSize = _self.getTableSize();
			var row = selection.row - tableSize.top;
			return row;
		}},
		{name: "cell", 		callback: function(sheet){ return false; }},
		{name: "range", 	callback: function(sheet){ return false; }}
	];
	this.getSelectionModes = function() {
		return _selectionModes;
	}
	this.getSelectionModeByName = function(name) {
		var selectionModes = this.getSelectionModes();
		return _.findWhere(selectionModes, {name: name});
	}

	var _selectionModeSource;
	this.getSelectionModeSource = function() {
		return _selectionModeSource;
	}
	this.setSelectionModeSource = function(source) {
		_selectionModeSource = "#" + source;
	}
	this.toggleInSelectionModeSource = function() {
		var source = this.getSelectionModeSource();
		if (!$(source).hasClass("toggle-in")) $(source).addClass("toggle-in");
	}
	this.toggleOutSelectionModeSource = function() {
		var source = this.getSelectionModeSource();
		if ($(source).hasClass("toggle-in")) $(source).removeClass("toggle-in");
	}

	var _selectionMode = _selectionModes[0].name;
	this.getSelectionMode = function() {
		return _selectionMode;
	}
	this.setSelectionMode = function(mode) {
		if (_.isString(mode)) _selectionMode = mode;
		if (_.isNumber(mode)) _selectionMode = _selectionModes[mode].name;
	}

	var _selectionTarget;
	this.getSelectionTarget = function() {
		return _selectionTarget;
	}
	this.setSelectionTarget = function(target) {
		_selectionTarget = "#" + target;
	}
	this.selectionTargetSetValue = function(value, target) {
		if (!target) target = this.getSelectionTarget();
		$(target).val(value);
	}

	this.getSelectionValue = function(sheet, mode, target) {
		if (!mode) mode = this.getSelectionMode();
		if (!target) target = this.getSelectionTarget();
		var selectionMode = this.getSelectionModeByName(mode);
		var value = (selectionMode.callback(sheet) == 0) ? 0 : selectionMode.callback(sheet) || "";
		return value;
	}
	// 
	// From Json
	// 
	this.fromJSON = function(spread, data, needTableSize){
		if (_.isUndefined(needTableSize)) needTableSize = false;
		if (needTableSize) spread.fromJSON(data.table);
		else spread.fromJSON(data);
	}
	// 
	// Set Selections 
	// 
	var _addSelection = function(spread, sheet, row, column, rowCount, columnCount){
		sheet.clearSelection();
		sheet.setSelection(row, column, rowCount, columnCount);
	}
	this.selectColumn = function(spread, sheet, order){
		order = parseInt(order);
		// if (order >= 0) _addSelection(spread, sheet, _tableSize.top - 1, order  + _tableSize.left, _tableSize.height - _tableSize.top, 1);
		if (order >= 0) _addSelection(spread, sheet, _tableSize.top - 1, order  + _tableSize.left, 1, 1);
	}
	this.selectRow = function(spread, sheet, order){
		order = parseInt(order);
		if (order >= 0) _addSelection(spread, sheet, parseInt(order) + _tableSize.top, _tableSize.left - 1, 1, 1);
	}
	// 
	// Get Fields By Selection
	// 
	this.getFieldsBySelection = function(sheet) {
		var fields = [];
		var selection = _.last(sheet.getSelections());
		var columnsRange = (selection.colCount == 1) ? [selection.col - _tableSize.left] : _.range(selection.col - _tableSize.left, selection.col - _tableSize.left + selection.colCount);
		var rowsRange = (selection.rowCount == 1) ? [selection.row - _tableSize.top] : _.range(selection.row - _tableSize.top, selection.row - _tableSize.top + selection.rowCount);
		_.each(columnsRange, function(column){
			_.each(rowsRange, function(row){
				fields.push({orderColumn: column + "", orderRow: row + ""});
			})
		})
		return fields;
	}
	// 
	// Styles
	// 
	_borderStyle = ["Empty","Thin","Medium","Dashed","Dotted","Thick","Double","Hair","MediumDashed","DashDot","MediumDashDot","DashDotDot","MediumDashDotDot","SlanteDashDot"],
	_theme = {
		innerBorder:{color:"#E26B0A",style:"Thin"},
		outterBorder:{color:"#C00000",style:"Thin"}
	},
	// _namedStyles = [
	// 	{name: "Header 1", backColor: "#C00000", foreColor: "#FFFFFF", font: "bold 13px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.center, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Header 2", backColor: "#C00000", foreColor: "#FFFFFF", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.center, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Field 1", backColor: "#FFFFFF", foreColor: "#000000", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Field 2", backColor: "#FFFFFF", foreColor: "#000000", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.right, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Subfield 1", backColor: "#FDE9D9", foreColor: "#000000", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Subfield 2", backColor: "#FDE9D9", foreColor: "#000000", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.right, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
	// 	{name: "Subscription", backColor: "#FFFFFF", foreColor: "#000000", font: "italic 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#C00000", borderStyle: "Thin", wordWrap: true}
	// ];
	_namedStyles = [
		{name: "Header 1", backColor: "Accent 1 50", foreColor: "Background 1", font: "bold 13px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.center, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Header 2", backColor: "Accent 1 20", foreColor: "Background 1", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.center, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Field 1", backColor: "Background 1", foreColor: "Text 1", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Field 2", backColor: "Background 2", foreColor: "Text 2", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.right, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Subfield 1", backColor: "Accent 2", foreColor: "Background 2", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Subfield 2", backColor: "Accent 3", foreColor: "Background 2", font: "normal 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.right, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#E26B0A", borderStyle: "Thin", wordWrap: true},
		{name: "Subscription", backColor: "Accent 4", foreColor: "Background 2", font: "italic 12px Calibri", hAlign: $.wijmo.wijspread.HorizontalAlign.left, vAlign: $.wijmo.wijspread.VerticalAlign.center, borderColor: "#C00000", borderStyle: "Thin", wordWrap: true}
	];
	var _setStyleOnCell = function(sheet, cell, style){
		var styleName = sheet.getStyleName(cell.row, cell.col);
		if (!_.isEmpty(styleName)){
			var namedStyle = sheet.getNamedStyle(styleName);
			sheet.setStyleName(cell.row, cell.col, "");
		}
		if (namedStyle){
			_.each(namedStyle, function(value, key){
				if (_.isFunction(cell[key]) && !_.isUndefined(value)) cell[key](value);	
			});
		}
		if (style){
			_.each(style, function(value, key){
				if (_.isFunction(cell[key]) && !_.isUndefined(value)) cell[key](value); 
			});	
		}
	}
	var _clearStyleOnCell = function(cell){
		cell.clearStyleProperty("backColor");
		cell.clearStyleProperty("foreColor");
		cell.clearStyleProperty("font");
		cell.clearStyleProperty("hAlign");
		cell.clearStyleProperty("vAlign");
		cell.clearStyleProperty("textIndent");
	}
	// 
	// Range
	// 
	var _setStyleOnRange = function(sheet, style){
		var sels = sheet.getSelections();
	 	sheet.isPaintSuspended(true);
	    _.each(sels, function(sel){
	    	var rowRange = _.range(sel.row, sel.row + sel.rowCount);
	    	var colRange = _.range(sel.col, sel.col + sel.colCount);
	    	_.each(rowRange, function(row){
	    		_.each(colRange, function(col){
	    			var cell = sheet.getCell(row, col);
	    			if (_.isEmpty(style)) _clearStyleOnCell(cell);
	    			else _setStyleOnCell(sheet, cell, style);
	    		});
	    	});
	    })
	    sheet.isPaintSuspended(false);
	}
	var _clearStyleOnRange = function(sheet){
		_setStyleOnRange(sheet, {});
	}
	// 
	// Named Style
	// 	
	var _getStyleByName = function(styleName){
		var style = new $.wijmo.wijspread.Style();
		var namedStyle = _.findWhere(_namedStyles, {name: styleName});
		if (namedStyle){
			style.name = namedStyle.name;
			style.backColor = namedStyle.backColor;
			style.foreColor = namedStyle.foreColor;
			style.font = namedStyle.font;
			style.hAlign = namedStyle.hAlign;
			style.vAlign = namedStyle.vAlign;
			style.wordWrap = namedStyle.wordWrap;
			if (namedStyle.borderColor && namedStyle.borderStyle){
				var lineBorder = new $.wijmo.wijspread.LineBorder(namedStyle.borderColor, _borderStyle.indexOf(namedStyle.borderStyle));
				style.borderTop = lineBorder;
				style.borderRight = lineBorder;
				style.borderBottom = lineBorder;
				style.borderLeft = lineBorder;
			}
		}
		return style;
	}
	var _setNamedStyleOnRange = function(sheet, styleName){		
		if (!_.isEmpty(styleName)){
			var style = _getStyleByName(styleName);
			sheet.addNamedStyle(style);
		}
		sheet.isPaintSuspended(true);
		var sels = sheet.getSelections();
	    _.each(sels, function(sel){
	    	var rowRange = _.range(sel.row, sel.row + sel.rowCount);
	    	var colRange = _.range(sel.col, sel.col + sel.colCount);
	    	_.each(rowRange, function(row){
	    		_.each(colRange, function(col){
	    			sheet.setStyleName(row, col, styleName);
	    		});
	    	});
	    })
	    sheet.isPaintSuspended(false);
	}
	this.setNamedStyleOnRange = function(sheet, styleName){
		_setNamedStyleOnRange(sheet, styleName);
	}
	this.clearNamedStyleOnRange = function(sheet){
		_setNamedStyleOnRange(sheet, "");
	}
	// 
	// Merge, unmerge
	// 
	this.mergeCells = function(sheet){
		var sel = sheet.getSelections(); 
		if (sel.length > 0) { 
			sheet.isPaintSuspended(true);
			sel = sel[0];
		    sheet.addSpan(sel.row, sel.col, sel.rowCount, sel.colCount); 
		    sheet.isPaintSuspended(false); 
		}
	}
	this.unmergeCells = function(sheet){
		var sel = sheet.getSelections(); 
		if (sel.length > 0) { 
		    sheet.isPaintSuspended(true); 
		    sel = sel[0];
		    var rowRange = _.range(sel.row, sel.row + sel.rowCount);
	    	var colRange = _.range(sel.col, sel.col + sel.colCount);
	    	_.each(rowRange, function(row){
	    		_.each(colRange, function(col){
	    			sheet.removeSpan(row, col);
	    		});
	    	});
		    sheet.isPaintSuspended(false); 
		} 
	}
	// 
	// Borders
	// 
	// this.setInnerBorder = function(sheet) {
	//     var color = _theme.innerBorder.color;
	//     var style = _borderStyle.indexOf(_theme.innerBorder.style);
	//     var lineBorder = new $.wijmo.wijspread.LineBorder(color, style);
	//     var sels = sheet.getSelections();
	//     sheet.isPaintSuspended(true);
	//     _.each(sels, function(sel){
	//     	sheet.setBorder(sel, lineBorder, {all: true});
	//     })
	//     sheet.isPaintSuspended(false);
	// }
	this.setOuterBorder = function(sheet) {
		var lineBorder = new $.wijmo.wijspread.LineBorder(_theme.outterBorder.color, _borderStyle.indexOf(_theme.outterBorder.style));
		var sels = sheet.getSelections(); 
		sheet.isPaintSuspended(true);		
		_.each(sels, function(sel){
			topRange = {col:sel.col, colCount:sel.colCount, row:sel.row, rowCount:0};
			bottomRange = {col:sel.col, colCount:sel.colCount, row:sel.row + sel.rowCount, rowCount:0};
			leftRange = {col:sel.col, colCount:0, row:sel.row, rowCount:sel.rowCount};
			rightRange = {col:sel.col + sel.colCount, colCount:0, row:sel.row, rowCount:sel.rowCount};
			sheet.setBorder(topRange, lineBorder, {top: true});
			sheet.setBorder(bottomRange, lineBorder, {bottom: true});
			sheet.setBorder(leftRange, lineBorder, {left: true});
			sheet.setBorder(rightRange, lineBorder, {right: true});
		});
	 	sheet.isPaintSuspended(false);
	}
	this.clearBorder = function(sheet) { 
		var sels = sheet.getSelections(); 
		sheet.isPaintSuspended(true);
		_.each(sels, function(sel){
		    sheet.setBorder(sel, null, {all: true}); 
		});
		sheet.isPaintSuspended(false);
	}
	// 
	// Align
	// 
	this.setTextAlignLeft = function(sheet){
		var style = {};
		style["hAlign"] = $.wijmo.wijspread.HorizontalAlign.general;
		_setStyleOnRange(sheet, style);
	}
	this.setTextAlignRight = function(sheet){
		var style = {};
		style["hAlign"] = $.wijmo.wijspread.HorizontalAlign.right;
		_setStyleOnRange(sheet, style);
	}
	this.setTextAlignCenter = function(sheet){
		var style = {};
		style["hAlign"] = $.wijmo.wijspread.HorizontalAlign.center;
		_setStyleOnRange(sheet, style);
	}
	// 
	// Formatter
	// 
	this.setFormat = function(sheet, format){
		sheet.isPaintSuspended(true);
		var sels = sheet.getSelections();
	    _.each(sels, function(sel){
	    	var rowRange = _.range(sel.row, sel.row + sel.rowCount);
	    	var colRange = _.range(sel.col, sel.col + sel.colCount);
	    	_.each(rowRange, function(row){
	    		_.each(colRange, function(col){
	    			sheet.setFormatter(row, col, format);
	    		});
	    	});
	    })
	    sheet.isPaintSuspended(false);
	}
	// 
	// Indent
	// 
	var _changeIndent = function(sheet, change){
		var sels = sheet.getSelections();
	 	sheet.isPaintSuspended(true);
	    _.each(sels, function(sel){
	    	var rowRange = _.range(sel.row, sel.row + sel.rowCount);
	    	var colRange = _.range(sel.col, sel.col + sel.colCount);
	    	_.each(rowRange, function(row){
	    		_.each(colRange, function(col){
	    			var cell = sheet.getCell(row, col);
	    			if (!cell.textIndent()) cell.textIndent(1);
		            else cell.textIndent(cell.textIndent() + change);
	    		});
	    	});
	    })
	    sheet.isPaintSuspended(false);
	}	
	this.increaseIndent = function(sheet){
		_changeIndent(sheet, 1);
	}
	this.decreaseIndent = function(sheet){
		_changeIndent(sheet, -1);
	}
}
// 
// 
// 
FormHelper = function()
{
	var _self = this;
	// 
	// Show \ hide Function Wrapper
	// 	
	this.hideWrapperFunction = function(){
		this.setInputFunction("");
		$("#function-wrapper").addClass("hide");
	}
	this.showWrapperFunction = function(){
		$("#function-wrapper").removeClass("hide");
	}
	// 
	// Show \ hide Name Special Wrapper
	// 	
	this.hideWrapperNameSpecial = function(){
		this.setInputNameSpecial("");
		$("#name-special-wrapper").addClass("hide");
	}	
	this.showWrapperNameSpecial = function(){
		$("#name-special-wrapper").removeClass("hide");
	}
	// 
	// Show \ hide Group Wrapper
	// 
	this.hideWrapperGroup = function(){
		this.setInputColumnGroup("");
		this.setInputMinGroup("");
		this.setInputMaxGroup("");
		$("#group-wrapper").addClass("hide");
	}
	this.showWrapperGroup = function(){
		$("#group-wrapper").removeClass("hide");
	}
	// 
	// Show \ hide Group Wrapper
	// 
	this.hideWrapperStaticGroup = function(){
		$("#static-group-wrapper").addClass("hide");
	}
	this.showWrapperStaticGroup = function(){
		$("#static-group-wrapper").removeClass("hide");
	}
	// 
	// Hide All Wrappers
	// 
	this.hideAllWrappers = function(){
		this.hideWrapperFunction();
		this.hideWrapperNameSpecial();
		this.hideWrapperGroup();
		this.hideWrapperStaticGroup();
	}	
	// 
	// Select Themes
	// 
	this.setSelectThemes = function(value){
		$("#themes-select").val(value);
	}
	this.getSelectThemes = function(){
		return $("#themes-select").val();
	}
	// 
	// Select Styles Special
	// 
	this.setSelectStylesSpecial = function(value){
		$("#styles-special-select").val(value);
	}
	this.getSelectStylesSpecial = function(){
		return $("#styles-special-select").val();
	}
	// 
	// Select Sequence
	// 
	this.setSelectSequence = function(value){
		if (_.isUndefined(value) || _.isEmpty(value)) value = "true";
		$("#sequence-select").val(value);
	}
	this.getSelectSequence = function(){
		return $("#sequence-select").val();
	}
	// 
	// Input Units
	// 
	this.setInputUnits = function(text){
		$("#units-input").val(text);
	}
	this.getInputUnits = function(){
		return $("#units-input").val().trim();
	}
	// 
	// Input Source
	// 
	this.setInputSource = function(text){
		$("#source-input").val(text);
	}
	this.getInputSource = function(){
		return $("#source-input").val().trim();
	}
	// 
	// Input Subscription
	//
	this.setInputSubscription = function(text){
		$("#subscription-input").val(text);
	}
	this.getInputSubscription = function(){
		return $("#subscription-input").val().trim();
	}
	// Input Text
	// 
	this.setInputFunction = function(text){
		$("#function-input").funcbox("value", text);
	}
	this.getInputFunction = function(){
		return $("#function-input").funcbox("value");
	}
	// 
	// Input Name Special
	// 
	this.setInputNameSpecial = function(text){
		$("#name-special-input").val(text);
	}
	this.getInputNameSpecial = function(){
		return $("#name-special-input").val().trim();
	}
	// 
	// Radio Assign Type
	// 
	this.getRadioAssignType = function(){
		return $("#assign-type-radio :radio:checked").val();
	}
	this.setRadioAssignType = function(value){
		if (_.isUndefined(value) || _.isEmpty(value)) value = "column";
		$("#assign-type-radio :radio").each(function(){
			$(this).prop("checked", ($(this).val() == value));
		});
		$("#assign-type-radio").buttonset("refresh");
	}
	// 
	// Select Format
	// 
	this.setSelectFormat = function(value){
		if (_.isUndefined(value) || _.isEmpty(value)) value = "default";
		$("#format-select").val(value);
	}
	this.getSelectFormat = function(){
		return $("#format-select").val();
	}
	// 
	// Input Suffix
	// 
	this.setInputSuffix = function(text){
		$("#suffix-input").val(text);
	}
	this.getInputSuffix = function(){
		return $("#suffix-input").val().trim();
	}
	// 
	// Select Presicion
	// 
	this.setSelectPrecision = function(value){
		$("#precision-select").val(value);
	}
	this.getSelectPrecision = function(){
		return $("#precision-select").val();
	}
	// 
	// Input Column Group
	// 
	this.setInputColumnGroup = function(value){
		$("#group-column-input").val(value);
	}
	this.getInputColumnGroup = function(){
		return $("#group-column-input").val();
	}
	// 
	// Input Min Group
	// 
	this.setInputMinGroup = function(value){
		$("#group-min-input").val(value);
	}
	this.getInputMinGroup = function(){
		return $("#group-min-input").val();
	}
	// 
	// Input Max Group
	// 
	this.setInputMaxGroup = function(value){
		$("#group-max-input").val(value);
	}
	this.getInputMaxGroup = function(){
		return $("#group-max-input").val();
	}
	// 
	// Select Func Group
	// 
	this.setSelectFuncGroup = function(value){
		$("#group-func-select").val(value);
	}
	this.getSelectFuncGroup = function(){
		return $("#group-func-select").val();
	}
	// 
	// Checkbox Hidden Columns
	// 
	this.setCheckboxHidden = function(value){
		if (_.isUndefined(value)) value = false;
		$("#hidden-checkbox").prop("checked", value);
	}
	this.getCheckboxHidden = function(){
		var value = $("#hidden-checkbox").prop("checked");
		return value;
	}
	// 
	// Input Columns Sort
	//
	this.setInputColumnsSort = function(value){
		$("#sort-columns-input").val(value);	
	} 
	this.getInputColumnsSort = function(){
		return $("#sort-columns-input").val();
	}
	// 
	// Input Rows Sort
	// 
	this.setInputRowsSort = function(value){
		$("#sort-rows-input").val(value);	
	} 
	this.getInputRowsSort = function(){
		return $("#sort-rows-input").val();
	}
	// 
	// Select Order
	// 
	this.setSelectOrder = function(value){
		$("#sort-order-select").val(value);
	}
	this.getSelectOrder = function(){
		return $("#sort-order-select").val();
	}
	// 
	// Select Func Static Group
	// 
	this.setSelectFuncStaticGroup = function(value){
		$("#static-group-func-select").val(value);
	}
	this.getSelectFuncStaticGroup = function(){
		return $("#static-group-func-select").val();
	}
	// 
	// Last Date
	// 
	this.setLabelLastDate = function(date){
		$("#last-date-label").text("Данные доступны до " + date);
	}
	this.clearLabelLastDate = function(){
		$("#last-date-label").text("");	
	}
	this.setSelectLastDate = function(date){
		if (date) $("#last-date-select").datebox('setValue', date.replace("-", "/"));
	}
	this.getSelectLastDate = function(){
		var date = $("#last-date-select").datebox('getValue');
		return date.replace("/", "-");
	}
	this.clearSelectLastDate = function(){
		$("#last-date-select").datebox('setValue', '');	
	}
}
// 
// 
// 
TreeHelper = function(){
	var _self = this;
	// 
	// Common
	// 
	var _getRel = function(data){
		var rel = data.node.li_attr.rel;
		return rel;
	}
	var _getId = function(data){
		// var id = data.node.id.replace(/o_|c_|r_/,""); //o_ object, c_ column, r_ row
		var id = data.node.id;
		return id;
	}
	// 
	// 
	// 
	this.isEmptyTree = function(tree) {
		var json = tree.jstree(true).get_json("#", {"no_state": true, "flat": true});
		if (json.length == 0) return true;
		else return false;
	}
	// 
	// Open Node
	// 
	this.selectNodeByUrl = function(tree, url){
		tree.jstree(true).deselect_all();
		if (!tree.jstree(true).select_node(url)){
			var path = tree.jstree(true).get_path(url, false, true);
			_.each(path, function(id){
				if (id == url) tree.jstree(true).select_node(id);
				else tree.jstree(true).open_node(id);
			});
		}
	}
	// 
	// Sort Nodes
	// 
	var _sortAlpha = function(tree, parent, order, caseSensitivity){
		tree = $.jstree.reference(tree);
		if (_.isObject(parent)) parent = parent.id;
		var json = tree.get_json(parent, {"no_state": true, "flat": true});
		var names = _.pluck(_.where(json, {parent: parent}), "text");
		names = _.sortBy(names, function (name) {
			if (caseSensitivity) return name;
			else return name.toLowerCase();
		})
		if (order == "desc") names.reverse(); 
		_.each(names, function(name, order){
			var node = _.findWhere(json, {"text": name});
			tree.move_node(node, parent, order);			 
		})
	}

	this.sortAlphaAsc = function(tree, parent, caseSensitivity){
		if (_.isUndefined(caseSensitivity)) caseSensitivity = false;
		_sortAlpha (tree, parent, "asc", caseSensitivity);
	}

	this.sortAlphaDesc = function(tree, parent, caseSensitivity){
		if (_.isUndefined(caseSensitivity)) caseSensitivity = false;
		_sortAlpha (tree, parent, "desc", caseSensitivity);
	}

	var _moveNodeToParent = function(tree, parent){
		tree = ($.jstree.reference(tree)) ? $.jstree.reference(tree) : $.jstree.reference(tree.context);		
		tree.move_node(tree.get_selected(true), parent);	
	}

	this.moveNodeToParent = function(tree, parent){
		_moveNodeToParent(tree, parent);
	}

	this.moveNodeToBegin = function(tree){
		_moveNodeToParent(tree, "#");
	}

	// 
	// Select Node
	// 
	this.getRelOnSelect = function(data){
		return _getRel(data);
	}
	this.getIdOnSelect = function(data){
		return _getId(data);
	}
	this.getTypeOnSelect = function(data){
		var type = data.node.type;
		return type;
	}
	this.getNameOnSelect = function(data){
		return data.node.text;
	}
	this.getLinkOnSelect = function(data){
		return data.node.li_attr.link;	
	}
	this.hasChildrenOnSelect = function(data){
		return (!_.isEmpty(data.node.children));
	}
	// 
	// Create Node
	// 
	this.getRelOnCreate = function(data){
		return _getRel(data);		
	}
	this.getTypeOnCreate = function(data){
		var type = data.node.type;
		return type;
	}
	this.getNodeOnCreate = function(data){
		var node = data.node;
		return node;
	}
	this.getIdOnCreate = function(data){
		return _getId(data);
	}
	this.getNameOnCreate = function(data){
		var name = data.node.text;
		return name;
	}
	this.getParentOnCreate = function(data){
		var parent = data.parent;
		return parent;
	}
	// 
	// Last Added
	// 
	var _lastAddedIds = [];
	this.setGetLastAddedIds = function(){
		return _lastAddedIds;
	}
	this.setLastAddedIds = function(id){
		_lastAddedIds.push(id);
	}
	this.clearLastAddedIds = function(){
		_lastAddedIds = [];
	}
	var _cloneId;
	this.getCloneId = function(){
		return _cloneId;
	}
	this.setCloneId = function(id){
		_cloneId = id;
	}
	var _needClone = false;
	this.needClone = function(){
		return _needClone;
	}
	this.setNeedClone = function(needClone){
		_needClone = needClone;
	}
	// 
	// Renames Node
	//
	this.getRelOnRename = function(data){
		return _getRel(data);
	}
	this.getIdOnRename = function(data){
		return _getId(data);
	}
	this.getTypeOnRename = function(data){
		var type = data.node.type;
		return type;
	}
	this.getNameOnRename = function(data){
		var name = data.node.text;
		return name;
	}
	// 
	// Get Parent
	// 
	this.getParentOnRename = function(data){
		var parent = data.node.parent;
		return parent;
	}
	this.getParentWithType = function(data, type, tree){
		if (_.isString(type)) type = [type];
		var parent = "#";
		var parents = data.node.parents;
		_.each(parents, function(id){
			if (_.contains(type, _self.getParentType(id, tree))) parent = id;
		});
		return parent;
	}
	this.getParentType = function(parent, tree){
		var type = tree.jstree(true).get_node(parent).type;
		return type;
	}
	this.getParentStep = function(parent, tree){
		var step = tree.jstree(true).get_node(parent).li_attr.step;
		return step;
	}
	// 
	// Get Children
	// 
	this.getLastChild = function(parent, tree) {
		var child;
		var leafs = this.getTreeLeafs(tree);
		_.each(leafs, function(leaf){
			var path = tree.jstree(true).get_path(leaf.parent, false, true);
			if (_.contains(path, parent)) child = leaf.id;
		});
		return child;
	}
	this.getLastChildren = function(parents, tree) {
		var children = [];
		_.each(parents, function(parent) {
			children.push(_self.getLastChild(parent, tree));
		});
		return children;
	}
	this.getLastChildId = function(parent, tree) {
		var childId;
		var leafs = this.getTreeLeafs(tree);
		_.each(leafs, function(leaf, id){
			var path = tree.jstree(true).get_path(leaf.parent, false, true);
			if (_.contains(path, parent)) childId = id;
		});
		return childId;
	}
	this.getLastChildrenIds = function(parents, tree) {
		var children = [];
		_.each(parents, function(parent) {
			children.push(_self.getLastChildId(parent, tree));
		});
		return children;
	}
	// 
	// Remove Node
	//
	this.getIdOnRemove = function(data){
		return _getId(data);
	}
	this.getTypeOnRemove = function(data){
		var type = data.node.type;
		return type;
	}
	this.getParentOnRemove = function(data){
		var parent = data.parent;
		return parent;
	}
	this.getChildrenById = function(id, tree){
		var children = tree.jstree(true).get_node(id).children;
		return children;
	}
	// 
	// Move Node
	//
	this.getIdOnMove = function(data){
		return _getId(data);
	}
	this.getTypeOnMove = function(data){
		var type = data.node.type;
		return type;
	}
	this.getParentOnMove = function(data){
		var parent = data.parent;
		return parent;
	}
	this.getOldParentOnMove = function(data){
		var parent = data.old_parent;
		return parent;	
	}
	this.isEqualParentsOnMove = function(data){
		return (data.old_parent == data.parent);
	}
	this.getParentLevel = function(data){
		return data.instance.get_path(data.parent).length;
	}
	this.getNodeLevel = function(data){
		return data.instance.get_path(data.node).length;
	}
	this.getTreeLeafs = function(tree, includeNodes, noChildrenNodes, excludeNodes){
		var newOrder = [];
		if ($(tree).attr("id") == "header-cols-tree") {
			if (!includeNodes) includeNodes = ["date", "function"];
			if (!noChildrenNodes) noChildrenNodes = ["index", "link", "field"];
			if (!excludeNodes) excludeNodes = [];
		}
		if ($(tree).attr("id") == "header-rows-tree") {
			if (!includeNodes) includeNodes = ["link", "field"];
			if (!noChildrenNodes) noChildrenNodes = [];
			if (!excludeNodes) excludeNodes = [];
		}
		var json = tree.jstree(true).get_json("#", {"no_state": true, "flat": true});
		_.each(json, function(node){
			if (!_.contains(excludeNodes, node.type)) {
				if (_.contains(includeNodes, node.type)) 
					newOrder.push({id: node.id, type: node.type, parent: node.parent});
				if (_.contains(noChildrenNodes, node.type))
					if (_.isEmpty(tree.jstree(true).get_node(node.id).children))
						newOrder.push({id: node.id, type: node.type, parent: node.parent});
			}
		});
		return newOrder;
	}
	this.getOrderById = function(id, tree, leafs){
		if (!leafs) leafs = this.getTreeLeafs(tree);
		var order = _.indexOf(leafs, _.findWhere(leafs, {"id":id})).toString();
		return order;
	}

	var _needReorder = false;
	this.needReorder = function(){
		return _needReorder; 
	}
	this.setNeedReorder = function(needReorder){
		_needReorder = needReorder;
	}

	this.selectNodeByOrder= function(tree, order) {
		var leafs = this.getTreeLeafs(tree);
		tree.jstree(true).deselect_all();
		tree.jstree(true).select_node(leafs[order]);
	}
	// 
	// Get Selected
	// 
	var _getSelected = function(tree){
		var nodes = [];
		nodes = tree.jstree(true).get_selected(true);
		return nodes;
	}
	var _getSelectedIds = function(tree){
		var ids = [];
		ids = tree.jstree(true).get_selected();
		return ids;	
	}
	this.getSelectedNodes = function(tree){
		return _getSelected(tree);
	}
	this.getSelectedIds = function(tree){
		return _getSelectedIds(tree);
	}
	this.getSelectedTypes = function(tree){
		var rels = [];
		rels = _.pluck(_getSelected(tree), "type");
		return rels;
	}
	this.getSelectedNames = function(tree){
		var names = [];
		names = _.pluck(_getSelected(tree), "text");
		return names;
	}
	// 
	// Get Checked
	// 
	this.getCheckedNodes = function(tree){
		var nodes = [];
		nodes = tree.jstree(true).get_checked(true);
		return nodes;
	}
	var _needFilter = true;
	this.needFilter = function(){
		return _needFilter;
	}
	this.setNeedFilter = function(needFilter){
		_needFilter = needFilter;
	}
	// 
	// Get Links
	// 
	this.getSelectedLinks = function(tree){
		var links = [];
		links = _.pluck(_.where(_getSelected(tree), {type: "linkgroup"}), "id");		
		return links;	
	}
	this.getNodesByIds = function(ids, tree){
		var nodes = [];
		_.each(ids, function(id){
			nodes.push(tree.jstree(true).get_node(id));
		})
		return nodes;
	}
	this.getNodeById = function(id, tree){
		var node = tree.jstree(true).get_node(id);
		return node;
	}
	// 
	// Draw Node
	// 
	var _drawNode = function(tree, nodeParams, nodeData, parent){
		// tree, parent
		// nodeParams = {position: "last", type: "folder", edit: true}
		// nodeData = {id: "0", text: "Some text"}

		if (!parent) parent = "#";
		if (_.isObject(nodeParams) && _.isObject(nodeData)){
			var node = tree.create_node(parent, {"li_attr": {"id": nodeData.id, "step": nodeData.step}, "type": nodeParams.type, "text": nodeData.text}, nodeParams.position);
			if (node && nodeParams.edit) tree.edit(node);
		} else if (_.isObject(nodeParams) && _.isUndefined(nodeData)){
			var node = tree.create_node(parent, {"type": nodeParams.type}, nodeParams.position);
			if (node && nodeParams.edit) tree.edit(node);
		} else {
			var node = tree.create_node(parent, {}, "first");
			if (node) tree.edit(node);
		}
		return node;	
	}
	this.drawNode = function(tree, nodeParams, nodeData, parent){
		// var inst = $(tree.context).jstree(true);
		var inst = $.jstree.reference(tree);
		if (!parent) parent = _.first(inst.get_selected());
		return _drawNode(inst, nodeParams, nodeData, parent);
	}
}
// 
// 
// 
Tips = function(){
	var _self = this;
  	$("body").on("chardinJs:start", function(){
		console.log("chardinJs:start");
	});	
	$("body").on("chardinJs:stop", function(){
		console.log("chardinJs:stop");
		_self.removeData();
	});
	// 
	// Data
	// 
	var _begin = [
		{"id":"#packages-tab", "intro": "При нажатии снимает выделение с дерева пакетов.", "position": "right"},
		{"id":"#packages-tree", "intro": "<strong>Дерево пакетов.</strong><ol><li>Для вызова контекстного меню нажмите правой кнопкой.</li><li>Через контекстное можно переименовать или удалить элементы дерева.</li><li>Для изменения порядка перенесите любой элемент дерева на новое метосположение.</li><li>Все изменения сохраняются автоматически.</li></ol>", "position": "right"},
		{"id":"#btn-add-object", "intro": "Можно создать папки, пакеты или объекты.", "position": "right"},
	];
	var _packageSelected = [
		{"id":"#state-export-group", "intro": "Можно задать и дату экспортировать весь пакет.", "position": "bottom"},
		{"id":"#state-header", "intro": "Здесь отображается название выбранного элемента.", "position": "bottom"}
	];
	var _objectSelected = [
		{"id":"#btnEditObject", "intro": "Нажмите для перехода в режим редактирования объекта.", "position": "right"},
		{"id":"#header-rows-tree", "intro": "<strong>Деревья заголвоков (столбцов и строк).</strong><ol><li>Для вызова контекстного меню нажмите правой кнопкой.</li><li>Через контекстное можно переименовать или удалить элементы дерева.</li><li>Для изменения порядка перенесите любой элемент дерева на новое метосположение.</li></ol>", "position": "right"},
		{"id":"#refresh-group", "intro": "Выберите дату и нажмите обновить. Ниже в таблице отобразятся найденные данные.", "position": "bottom"},
		{"id":"#last-date-label", "intro": "Дата рассчитывается, как общая для всех показателей в таблице.", "position": "bottom"},
	];
	var _editMode = [
		{"id":"#indexes-tree", "intro": "<strong>Дерево показателей.</strong><ol><li>Выбрите показатель из дерева.</li></ol>", "position": "right"},
		{"id":"#state-edit-group", "intro": "Сохраняет все ваши изменения, кроме Оформления таблицы.", "position": "right"},
		{"id":"#spreadsheet-tools-edit-mode", "intro": "Используйте эти инструменты для оформления таблицы.", "position": "bottom"}
	];
	var _indexSelected = [
		{"id":"#links-tree", "intro": "<strong>Фильтр ссылок.</strong><br>Здесь отобржаются все связанные с показателем элементы.<ol><li>Выберите элемент. Галочками будет отмечено все, что с ним связано.</li><li>Уберите галочки, чтобы исключить ссылки, содержащие эти элементы.</li></ol>", "position": "left"},
		{"id":"#links-checkbox", "intro": "<strong>Дерево ссылок.</strong><br>Здесь отобржаются отфильтрованные ссылки, которые можно добавить в таблицу.", "position": "left"},
		{"id":"#links-tools", "intro": "Используйте эти инструменты для добавления показателей в таблицу.<br><strong>Добавить ссылки.</strong><ul><li>Добавит все выбранные ссылки из дерева ссылок.</li></ul><strong>Добавить показатель.</strong><ul><li>Добавит показатель по аналогии с данными, которые уже есть в таблице.</li></ul>", "position": "right"},
		{"id":"#headers-tools", "intro": "<strong>Добавить временную точку.</strong><ul><li>Добавляет дату в формате 0, -1, -12 и т.д. 0 - последняя возможная дата, а, например, -1 покажет данные за предыдущий месяц.</li></ul><strong>Добавить вычисляемую строку.</strong><ul><li>Можно назначить формулу в специальном формате. Например, val(0)/val(-12)*100</li></ul>", "position": "right"}
	];
	// 
	// States
	//   
	var _states = ["begin", "packageSelected", "objectSelected", "editMode", "indexSelected"];
	var _state = _states[0];

	this.getState = function(){
	return _state;
	}

	this.setState = function(state){
		var index = _states.indexOf(state);
		if (index != -1) _state = state;
	}
	// 
	// Set Data
	// 
	_setDataToElement = function(data){
	  $(data.id).attr("data-intro", data.intro);
	  $(data.id).attr("data-position", data.position);
	}
	_removeDataFromElement = function(data){
		$(data.id).removeAttr("data-intro");
		$(data.id).removeAttr("data-position");
	}

	this.setData = function(){
		var state = this.getState();
		switch(state){
			case _states[0]:
				_.each(_begin, function(data){
				  _setDataToElement(data);
				})
			break;
			case _states[1]:
				 _.each(_packageSelected, function(data){
				  _setDataToElement(data);
				})
			break;
			case _states[2]:
				_.each(_objectSelected, function(data){
				  _setDataToElement(data);
				})
			break;
			case _states[3]:
				_.each(_editMode, function(data){
					_setDataToElement(data);
				})
			break;
			case _states[4]:
				_.each(_indexSelected, function(data){
			  		_setDataToElement(data);
				})
			break;
		}
	}

	this.removeData = function(){
		var state = this.getState();
		switch(state){
			case _states[0]:
				_.each(_begin, function(data){
					_removeDataFromElement(data);
				})
			break;
			case _states[1]:
				_.each(_packageSelected, function(data){
					_removeDataFromElement(data);
				})
			break;
			case _states[2]:
				_.each(_objectSelected, function(data){
					_removeDataFromElement(data);
				})
			break;
			case _states[3]:
				_.each(_editMode, function(data){
					_removeDataFromElement(data);
				})
			break;
			case _states[4]:
				_.each(_indexSelected, function(data){
			  		_removeDataFromElement(data);
				})
			break;
		}
	}

	this.show = function(){
		this.setData();
		$("body").chardinJs("start");
	}
}