function initLinksTree(data){
	$("#links-tree")
	.jstree({
		"core": { 
			"html_titles": true,
			"data": data
		}

	})
	.bind("select_node.jstree", function (e, data) { selectNode(e, data) })
	.bind("loaded.jstree", function(e,data) {})
	.bind("create.jstree", function (e, data) {})
	.bind("remove.jstree", function (e, data) {})
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) {});

	var selectNode = function(e, data){
		if (data.node.li_attr.rel == "link" || data.node.li_attr.rel == "index"){
			var filter = [];
			$("#links-tree").jstree("get_selected", null, true).each(function(index){
				var id = $(this).attr("id");
				var links = obs.elementsMap[id];
				$("#links-tree").find("a").removeClass("jstree-linked");
				_.each(links, function(value, key){
					filter.push(value);
					_.each(obs.linksMap[value], function(value, key){
						if (key != id){
							$("#links-tree").find("li#"+key+">a").addClass("jstree-linked");
						}
					});
				});
			});
			$.ajax({
				url: REQBASE + "?mod=objects",
				dataType: "json",
				type: "post",
				data:{ 
					act: "showlinksgrouped",
					indexId: obs.indexId,
					filter: filter			
				},
				success: function(data){
					initLinksCheckboxTree(data.tree);
				}
			});

		}
	}
}

function initLinksCheckboxTree(data){
	$("#links-checkbox-tree")
	.jstree({
		// "plugins": _.union(defaultTreePlugins,["checkbox"]),
		"core": { 
			"html_titles": true,
			"data": data
		}
	})
	.bind("select_node.jstree", function (e, data) {})
	.bind("create.jstree", function (e, data) {})
	.bind("remove.jstree", function (e, data) {})
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) {});

	$("#links-checkbox-tab").click(function(){
		$("#links-checkbox-tree").jstree("select_all");
	});
}


function initHeadersColsTree(data){
	console.log("----------- Init Header Cols Tree -----------");
	$("#header-cols-tree")
	.jstree({
		"crrm": {"move": {"check_move": function(m){ return checkMove(m) } }}, //FIXME!!!

		"core": { 
			"html_titles": true,
			"data": data
		}
	})
	.bind("select_node.jstree", function (e, data) { selectNode(e, data) })
	.bind("create.jstree", function (e, data) { createNode(e,data) })
	.bind("remove.jstree", function (e, data) { removeNode(e,data) })
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) { moveNode(e, data) } );

	var selectNode = function(e, data){
		var id = parseInt(data.rslt.obj.attr("id")) + 1;

		sheet.clearSelection();
		sheet.addSelection(0,id, 1,1);
	}

	var createNode = function(e, data){
		var source, lastFieldType;
		var sourceKeys = [], fieldTypes = [], fieldNames = [];

		if(data.rslt.parent == -1){
			source = false; // root			
		} else {
			source = data.rslt.parent.attr("id");
		}

		var container = (source) ? $(data.rslt.parent) : $(data.inst.get_container());
		
		container.children("ul").children("li").each(function(){
			sourceKeys.push($(this).attr("id").replace("date",""));
			// fieldTypes.push($(this).attr("rel"));
			// fieldNames.push($(this).children("a").text().trim());
		});		

		// lastFieldType = $(data.rslt.obj).attr("rel")
		// obs.afterAddColumns(source, sourceKeys, fieldTypes, fieldNames, lastFieldType);
		obs.afterAddColumns(source, sourceKeys);
	}

	var checkMove = function(data){
		var result = true;

		if (data.cr != -1 && $(data.o).attr("rel") == "index"){
			$(data.cr).parents("li").each(function(){
				if($(this).attr("rel") == "index"){
					result = false;
				}
			});		
		}

		return result	
	}

	var moveNode = function(e, data){
		var source, target;
		var sourceKeys = [];
		var targetKeys = [];
		
		if(_.isNaN(parseInt(data.rslt.op.attr("id")))){
			source = false; // root
		} else {
			source = data.rslt.op.attr("id");
		}

		$(data.rslt.op).children("ul").children("li").each(function(){
			sourceKeys.push($(this).attr("id"));
		});

		if(data.rslt.cr == -1){
			target = false; // root			
		} else {
			target = data.rslt.cr.attr("id");			
		}

		if (source === target){
			targetKeys = sourceKeys;
		} else if(target){
			$(data.rslt.cr).children("ul").children("li").each(function(){
				targetKeys.push($(this).attr("id"));
			});
		} else {
			$(data.rslt.ot.get_container()).children("ul").children("li").each(function(){
				targetKeys.push($(this).attr("id"));
			});
		}

		obs.moveLinksColumns(source, sourceKeys, target, targetKeys);
	}

	var removeNode = function(e, data){
		var source;
		var sourceKeys = [];
		var aliasKeys = [];

		if(data.rslt.parent == -1){
			source = false; // root			
		} else {
			source = data.rslt.parent.attr("id");
		}

		if(source){
			$(data.rslt.parent).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		} else {
			$(data.inst.get_container()).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		}

		data.rslt.obj.each(function(){
			aliasKeys.push($(this).attr("id"));
		});

		obs.deleteLinksColumns(source, sourceKeys, aliasKeys);
	}

	$("#header-cols-tab").click(function(){
		$("#header-cols-tree").jstree("deselect_all");
	});
}

function initHeadersRowsTree(data){
	console.log("----------- Init Header Rows Tree -----------");

	$("#header-rows-tree")
	.jstree({
		"crrm": {"move": {"check_move": function(m){ return checkMove(m) } }},
		"core": { 
			"html_titles": true,
			"data": data
		}
	})
	.bind("select_node.jstree", function (e, data) { selectNode(e, data) })
	.bind("create.jstree", function (e, data) { createNode(e,data) })
	.bind("remove.jstree", function (e, data) { removeNode(e,data) })
	.bind("rename.jstree", function (e, data) {})
	.bind("move_node.jstree", function (e, data) { moveNode(e, data) } );

	var selectNode = function(e, data){
		var id = parseInt(data.rslt.obj.attr("id")) + 1;

		sheet.clearSelection();
		sheet.addSelection(id,0, 1,1);
	}

	var createNode = function(e, data){
		var source;
		var sourceKeys = [];

		if(data.rslt.parent == -1){
			source = false; // root			
		} else {
			source = data.rslt.parent.attr("id");
		}

		if(source){
			$(data.rslt.parent).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		} else {
			$(data.inst.get_container()).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		}

		obs.afterAddRows(source, sourceKeys);
	}

	var checkMove = function(data){
		var result = true;

		if (data.cr != -1 && $(data.o).attr("rel") == "index"){
			$(data.cr).parents("li").each(function(){
				if($(this).attr("rel") == "index"){
					result = false;
				}
			});		
		}

		return result	
	}

	var moveNode = function(e, data){
		var source, target;
		var sourceKeys = [];
		var targetKeys = [];
		
		if(_.isNaN(parseInt(data.rslt.op.attr("id")))){
			source = false; // root
		} else {
			source = data.rslt.op.attr("id");
		}

		$(data.rslt.op).children("ul").children("li").each(function(){
			sourceKeys.push($(this).attr("id"));
		});

		if(data.rslt.cr == -1){
			target = false; // root			
		} else {
			target = data.rslt.cr.attr("id");			
		}

		if (source === target){
			targetKeys = sourceKeys;
		} else if(target){
			$(data.rslt.cr).children("ul").children("li").each(function(){
				targetKeys.push($(this).attr("id"));
			});
		} else {
			$(data.rslt.ot.get_container()).children("ul").children("li").each(function(){
				targetKeys.push($(this).attr("id"));
			});
		}

		obs.moveLinksRows(source, sourceKeys, target, targetKeys);
	}

	var removeNode = function(e, data){
		var source;
		var sourceKeys = [];
		var aliasKeys = [];

		if(data.rslt.parent == -1){
			source = false; // root			
		} else {
			source = data.rslt.parent.attr("id");
		}

		if(source){
			$(data.rslt.parent).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		} else {
			$(data.inst.get_container()).children("ul").children("li").each(function(){
				sourceKeys.push($(this).attr("id"));
			});
		}

		data.rslt.obj.each(function(){
			aliasKeys.push($(this).attr("id"));
		});

		obs.deleteLinksRows(source, sourceKeys, aliasKeys);
	}

	$("#header-rows-tab").click(function(){
		$("#header-rows-tree").jstree("deselect_all");
	});
}

// function initLinksList(data){
// 	console.log(data);
// 	// var lastChain = data.lastChain;
// 	var lastChain = 0;
// 	var links = _.clone(data.links);
// 	var names = _.clone(data.eNames);
// 	console.log(links);
// 	console.log(names);
// 	var html = "<ul class='links-list'>";
// 	for (var link in links){
// 		html += "<li id='"+link+"'>";
// 		for (var chain in links[link].chains){
// 			if (chain != lastChain){
// 				html += "<span class='element'><i class='fa fa-link'></i> "+names[chain]+"</span>";
// 			}
// 		}
// 		// html += "<span class='units'>"+links[link].attrs.LEFT.value+"</span>";
// 		html += "</li>";
// 	}		
// 		html += "</ul>";
// 	$("#links-tree").empty().append(html);
// }

// function processChainLinks(data)
// {
// 	$('body').css({cursor:'default'});
// 	$('.forLinkOnly').css('display','inline-block');

// 	$('#linklist').html('<div class="linktable" ></div>');
// 	var LL = $('#linklist > .linktable'),CL=0,ns;
// 	for(var x in data.links)
// 	{
// 		var th = $('<div class="linkhead" rel=\"'+x+'\"><b>LINK'+x+'</b></div>').attr('nowrap',true),
// 			r = $('<div class="onelink" skipped=\"'+data.skipped+'\" rel=\"'+x+'\">').append(th).appendTo(LL).addClass('row'+(CL++%2+1)),
// 			chn = data.links[x].chains,
// 			attrs = data.links[x].attrs,
// 			props = data.links[x].props;

// 		for(var ag in props)
// 		{
// 			var ago = props[ag];
// 			ns = ag=='STEP'?'':'not';
// 			if(ago.length>0)
// 			th.append($('<small class="filterdata" target="props" key="'+ag+'" value="'+ago+'"><'+ns+'strong>'+shorten(ag,20)+': '+shorten(ago,30)+'</'+ns+'strong></small>').css('display','block'));
// 		}
// 		for(var ag in attrs)
// 		{
// 			var ago = attrs[ag];
// 			ns = ag=='LEFT'?'':'not';
// 			th.append($('<small class="filterdata" short="'+ago.short+'" target="attrs" key="'+ag+'" value="'+ago.value+'"><'+ns+'strong>'+shorten(ago.name,20)+': '+shorten(ago.value,30)+'</'+ns+'strong></small>').css('display','block'));
// 		}

// 		for(var ch in chn)
// 		{
// 			var lpname='',lastde = 0;
// 			for(var y in chn[ch])
// 			{
// 				if(lpname) lpname += "-&gt;<br />";
// 				lpname += '<span class="chainpart" element="'+chn[ch][y]+'">'+shorten(data.eNames[chn[ch][y]],30)+"</span><chainpart element='"+chn[ch][y]+"'>"+data.eNames[chn[ch][y]]+"</chainpart>";
// 				lastde = chn[ch][y];
// 			}
// 			$('<div class="linkchain" chain="'+ch+'" last="'+lastde+'">').html(lpname).appendTo(r).attr({valign:'top',nowrap:true})
// 				// .bind('click',linkTHload).attr('declassid',data.eClasses[lastde]).attr('declassname',data.classDef[data.eClasses[lastde]]);
// 		}
// 	}
	
	// LL.find('.linkhead > b').each(function(){
	// 	$(this)./*bind('click',linkTHload).*/append(
	// 		$(	'<div class="ablock"></div>')
	// 			.append(
	// 				uiSmlBtn('search','Загрузить в рабочую область').click(linkTHload)
	// 			)
	// 			.append(
	// 				uiSmlBtn('gear',"Свойства").click(linkTHprops)
	// 			)
	// 			.append(
	// 				uiSmlBtn('trash',"Удалить связь").click(linkTHdelete)
	// 			)
	// 	);
	// });
	// LL.find('.chainpart').mouseenter(function(){
	// 	$(this).append($('<button style="background:transparent; border:0; margin:0;padding:0; height:12px; width:13px;"><img src="/style/images/button_edit.png" align="right" title="rename it" /></button>').click(renameElementClick));
	// }).mouseleave(function(){$(this).find('button').remove()});

// 	//if($( "#midtabs" ).tabs( "option", "active" )==1)
// 	if($( "#midtabs >ul").find(".ui-state-active" ).prev().length==1)
// 		$('.linkchain').draggable({revert:true,helper:'clone',zIndex: 10000});
// }

// function shorten(tx,to)
// {
// 	if(!tx) return '';
// 	if(!to) to=15;
// 	if(tx.replace(/<.*>/,'').length>to)
// 		return '<span title="'+tx.replace(/<.*>/,'')+'" class="shortened">'+tx.replace(/<.*>/,'').substring(0,to-2)+'..</span>';
// 	return tx;
// }

// function linkTHload(lid,de,nextinqueue)
// {
// 	if(typeof(lid)!='number'){
// 		var pp = $(this).parents('.linkhead');
// 		if(pp.length==0)
// 			pp = $(this).parents('.onelink');
// 		if(pp.length==0)
// 			return;
// 		lid = pp.attr('rel');
// 	}
// 	if(!de)
// 		if($(this).attr('last')!='0')
// 			de = $(this).attr('last');
// 	$('body').css({cursor:'wait'});
// 	$.ajax({
// 		url: "/?mod=edit",
// 		dataType:'json',
// 		data:{
// 			act:'chart',
// 			id: 'l'+lid,
// 			de: de?de:''
// 			},
// 		success: function(data){
// 				$('body').css({cursor:'default'});
// 				var l = $('#linklist').find('.onelink[rel="'+lid+'"]'),
// 					cps = de?l.find('chainpart[element='+de+']').parent().find('chainpart'):$(l.find('.linkchain')[0]).find('chainpart'),
// 					title = $(cps[cps.length-1]).html(),
// 					group = nextinqueue?'FILL '+LINKLOADNUM:(cps.length>1?$(cps[cps.length-2]).html():null),
// 					groupkey = nextinqueue?BLANKTHRESHOLD+LINKLOADNUM:(cps.length>1?$(cps[cps.length-2]).attr('element'):null);

// 				$('.linkhead[rel="'+lid+'"]').find('.filterdata[short]').each(function(){
// 					var v = $(this).attr('short').replace(/\^([1-9])/,'<sup>$1</sup>');
// 					if(v!='')
// 						title+=' <span title="'+$(this).attr('value')+'">('+v+')</span>';
// 				});
// 				GRID.addRow(lid,data,title,group,groupkey);
// 				if(nextinqueue && nextinqueue<LINKLOADQUEUE.length){
// 					for(;nextinqueue<LINKLOADQUEUE.length;nextinqueue++)
// 					{
// 						if(LINKLOADQUEUE[nextinqueue][0]==-1) //blanks
// 							GRID.addRow(BLANKTHRESHOLD+(++BLANKSNUM),false,'------',nextinqueue?'FILL '+LINKLOADNUM:'BLANK',BLANKTHRESHOLD+LINKLOADNUM);
// 						else
// 						if(LINKLOADQUEUE[nextinqueue][0]!=false) break; //skipped
// 					}
// 					linkTHload( parseInt(LINKLOADQUEUE[nextinqueue][0]), parseInt(LINKLOADQUEUE[nextinqueue][1]), nextinqueue+1);
// 				}else{
// 					LINKLOADQUEUE=[];
// 				}
// 			},
// 		error: invalidChartResponce
// 	});
// 	return false;
// }