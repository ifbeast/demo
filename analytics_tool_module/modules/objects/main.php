<?php
if(!defined('PORTAL_INSIDE')) exit('figa');

// Include libs
include(PATH_BACKBONE.'ExcelPackageModel.php');
include(PATH_BACKBONE.'sar2.php');
include(THISMODPATH.'/components/SpreadsheetThemes.php');
include(THISMODPATH.'/components/SpreadsheetStyles.php');
include(THISMODPATH.'/components/SpreadsheetStyleAttrValues.php');

// $op = secMatch(PassedVal('op'));
// $dom = (int)PassedVal('dom');
// $host = (int)PassedVal('host');
// $inv = (int)PassedVal('inv');
// Routing
$act = secMatch(PassedVal('act'));

define('MOD_VIEW_PATH', '/tables');
define('MODCALL_LINK','index.php?mod='.$thismod['modname'].'&');
define('ACTCALL_LINK',MODCALL_LINK.'act='.$act.'&');

$nav_1 = 'Объекты';
$nav_1_link = MODCALL_LINK;

switch($act) {
	case 'showfolders': JsonRequestCheckAuth(); showFolders(); return;
	case 'showfolderslist': JsonRequestCheckAuth(); showFoldersList(); return;	
	case 'createfolder': JsonRequestCheckAuth(); createFolder(); return;
	case 'updatefolder': JsonRequestCheckAuth(); updateFolder(); return;
	case 'removefolder': JsonRequestCheckAuth(); removeFolder(); return;

	case 'createobject': JsonRequestCheckAuth(); createObject(); return;
	case 'cloneobject': JsonRequestCheckAuth(); cloneObject(); return;
	case 'updateobject': JsonRequestCheckAuth(); updateObject(); return;
	case 'removeobject': JsonRequestCheckAuth(); removeObject(); return;

	// case 'showcontent': JsonRequestCheckAuth(); showContent(); return;
	
	case 'showtemplate': JsonRequestCheckAuth(); showTemplate(); return;
	case 'savetemplate': JsonRequestCheckAuth(); saveTemplate(); return;
	case 'removetemplate': JsonRequestCheckAuth(); removeTemplate(); return;
	
	case 'showheaders': JsonRequestCheckAuth(); showHeaders(); return;
	case 'saveheaders': JsonRequestCheckAuth(); saveHeaders(); return;
	case 'add-formula': JsonRequestCheckAuth(); addHeaders(); return;

	case 'exportobject': JsonRequestCheckAuth(); exportObject(); return;
	case 'fullexportobject': JsonRequestCheckAuth(); fullExportObject(); return;
	case 'exportpackage': JsonRequestCheckAuth(); exportPackage(); return;

	case 'showlinks': JsonRequestCheckAuth(); showLinks(); return;
	case 'showlinksalt': JsonRequestCheckAuth(); showLinksAlt(); return;
	case 'showlinksgrouped': JsonRequestCheckAuth(); showLinksGrouped(); return;

	// case 'listobjects': JsonRequestCheckAuth(); listObjects(); return;
	// case 'newobject': JsonRequestCheckAuth(); createObjectAlt(); return;
	// case 'saveobject': JsonRequestCheckAuth(); saveObject(); return;
	// case 'renameobject': JsonRequestCheckAuth(); renameObject(); return;
	// case 'objecttypes': JsonRequestCheckAuth(); objectTypes(); return;
	
	// case 'objectcnt': JsonRequestCheckAuth(); objectLinks(); return;
	// case 'objecttable': JsonRequestCheckAuth(); objectTable(); return;
	// case 'objectimage': JsonRequestCheckAuth(); objectImage(); return;
	// case 'objectexport': JsonRequestCheckAuth(); objectExport(); return;
	case 'listbylinks':	JsonRequestCheckAuth(); OBJListByLinks();return;

	case 'showthemes': 		JsonRequestCheckAuth(); showThemes(); 	return;
	case 'showthemestree': 	JsonRequestCheckAuth(); showThemesTree(); 	return;
	case 'showtheme': 		JsonRequestCheckAuth(); showTheme(); 	return;
	case 'createtheme': 	JsonRequestCheckAuth(); createTheme(); 	return;
	case 'updatetheme': 	JsonRequestCheckAuth(); updateTheme(); 	return;
	case 'removetheme': 	JsonRequestCheckAuth(); removeTheme(); 	return;

	case 'showstyles': 		JsonRequestCheckAuth(); showStyles(); 	return;
	case 'showstylestree': 	JsonRequestCheckAuth(); showStylesTree(); 	return;
	case 'showstyle': 		JsonRequestCheckAuth(); showStyle(); 	return;
	case 'createstyle': 	JsonRequestCheckAuth(); createStyle(); 	return;
	case 'updatestyle': 	JsonRequestCheckAuth(); updateStyle(); 	return;
	case 'removestyle': 	JsonRequestCheckAuth(); removeStyle(); 	return;
	case 'getcompiledstyle': JsonRequestCheckAuth(); getCompiledStyle(); 	return;
	case 'getstylevalues': 	JsonRequestCheckAuth(); getStyleAttrValues(); 	return;

	case 'tpl':	getTemplate(); return;
}



// Auth check and show login form
$AUTH_DIE = 0;
userauthPage();
if($AUTH_DIE) return;
// if(!$CLIENT['cl_editor']) return;

// Draw default
switch ($act) {
	default: drawDefault();
}
// Define common functions
function JsonRequestCheckAuth()
{
	global $CLIENT;
	if(empty($CLIENT))
		die('{"error":"Ваша сессия истекла. Авторизуйтесь, пожалуйста.","reauth":true}');
	// if(!$CLIENT['cl_editor'])
		// die('{"error":"У вас нет привилегий для доступа сюда","reauth":true}');
}

// Default action
function drawDefault()
{
	putView(MOD_VIEW_PATH."/tables");
}

function getTemplate()
{
	$tpl = (PassedVal('tpl'));
	if(empty($tpl)) die('');
	echo putView(MOD_VIEW_PATH."/tables-".$tpl, [], true);
	die();
}

// Define action functions
function showFolders()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$model = new ExcelPackageModel;	
	$p = $model->showFolders($id);
	echo json_encode($p,JSON_STDPARAMS);

	die();
}

function showFoldersList()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$model = new ExcelPackageModel;	
	$p = $model->showFolders($id, false);
	echo json_encode($p,JSON_STDPARAMS);

	die();
}

function createFolder()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$params = PassedVal('params');
	$name = $params['name'];
	$parent = $params['parent'];
	$model = new ExcelPackageModel;	
	$p = $model->createFolder($name, $parent);
	echo json_encode($p,JSON_STDPARAMS);

	die();
}

function updateFolder()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$params = PassedVal('params');
	$model = new ExcelPackageModel;
	$p = $model->updateFolder($id, $params);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function removeFolder()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$model = new ExcelPackageModel;	
	$p = $model->removeFolder($id);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}
// 
// 
// 
function createObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$params = PassedVal('params');
	$name = $params['name'];
	$pid = $params['parent'];
	$model = new ExcelPackageModel;	
	$p = $model->createObject($name, $pid);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function cloneObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$params = PassedVal('params');
	$name = $params['name'];
	$model = new ExcelPackageModel;	
	$p = $model->cloneObject($id, $name);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function updateObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$params = PassedVal('params');
	$model = new ExcelPackageModel;	
	$p = $model->updateObject($id, $params);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function removeObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$model = new ExcelPackageModel;	
	$p = $model->removeObject($id);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}
// 
// 
// 
function exportObject()
{
	$id = PassedVal('id');
	$date = PassedVal('date');
	$model = new ExcelPackageModel;	
	$p = $model->exportObject($id, $date);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();	
}
function fullExportObject()
{
	$id = PassedVal('id');
	$date = PassedVal('date');
	$model = new ExcelPackageModel;	
	$p = $model->exportObject($id, $date, false);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();	
}
function exportPackage()
{
	$id = PassedVal('id');
	$date = PassedVal('date');
	$model = new ExcelPackageModel;	
	$p = $model->exportPackage($id, $date);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}
// 
// 
// 
function showTemplate()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$params = PassedVal('params');
	if ($params){
		// die(print_r($params));
		if (isset($params['headers'])) $headers = $params['headers'];
		if (isset($params['lastDate'])) $lastDate = $params['lastDate'];
		if (isset($params['replaceDotes'])) $replaceDotes = $params['replaceDotes'];
		if (isset($params['eraseHiddenColumns'])) $eraseHiddenColumns = $params['eraseHiddenColumns'];
		if (isset($params['needTableSize'])) $needTableSize = $params['needTableSize'];
		$p = ExcelPackageModel::instance()->showTemplate($id, $headers, $lastDate, $replaceDotes, $eraseHiddenColumns, $needTableSize);
		if (isset($params['responceType'])){
			if ($params['responceType'] == 'string') echo json_encode($p);
		} else echo json_encode($p,JSON_STDPARAMS);
	} else {
		$p = ExcelPackageModel::instance()->showTemplate($id);
		echo json_encode($p,JSON_STDPARAMS);
	}

	die();
}

function saveTemplate()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$params = PassedVal('params');
	$template = $params['template'];
	$p = ExcelPackageModel::instance()->saveTemplate($id, $template);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function removeTemplate()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$id = PassedVal('id');
	$p = ExcelPackageModel::instance()->removeTemplate($id);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function addHeaders()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$params = PassedVal('params');
	if (isset($params['selected'])) $selected = $params['selected'];
	if (isset($params['headers'])) $headers = $params['headers'];
	if (isset($params['dates'])) $dates = $params['dates'];
	if (isset($params['formulas'])) $formulas = $params['formulas'];
	if (isset($params['lastChildren'])) $lastChildren = $params['lastChildren'];
	$headers = ExcelPackageModel::instance()->addHeaders($id, $selected, $lastChildren, $headers, $dates, $formulas);
	$p = ExcelPackageModel::instance()->showTemplate($id, $headers, null, false, false, true);
	$p['headers'] = $headers;
	echo json_encode($p,JSON_STDPARAMS);
	die();
}

function showHeaders()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new ExcelPackageModel;
	$p = $model->showHeaders($id, true);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}

function saveHeaders()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$params = PassedVal('params');
	$p = ExcelPackageModel::instance()->saveHeaders($id, $params['headers'], $params['links']);	
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}
// 
// Object Themes
// 
function showThemes()
{
	header("Content-Type: text/json; charset=UTF-8");
	$model = new SpreadsheetThemes;	
	$p = $model->showThemes();
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function showThemesTree()
{
	header("Content-Type: text/json; charset=UTF-8");
	$model = new SpreadsheetThemes;	
	$p = $model->showThemesTree();
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function showTheme()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new SpreadsheetThemes;	
	$p = $model->showTheme($id);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function createTheme()
{
	header("Content-Type: text/json; charset=UTF-8");
	$name = PassedVal('name');
	$model = new SpreadsheetThemes;	
	$p = $model->createTheme($name);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function updateTheme()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$params = PassedVal('params');
	$model = new SpreadsheetThemes;	
	$p = $model->updateTheme($id, $params);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function removeTheme()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new SpreadsheetThemes;	
	$p = $model->removeTheme($id);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
// 
// Object Styles
// 
function showStyles()
{
	header("Content-Type: text/json; charset=UTF-8");
	$parent = PassedVal('parent');
	$model = new SpreadsheetStyles;	
	$p = $model->showStyles($parent);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function showStylesTree()
{
	header("Content-Type: text/json; charset=UTF-8");
	$parent = PassedVal('parent');
	$model = new SpreadsheetStyles;	
	$p = $model->showStylesTree($parent);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function showStyle()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new SpreadsheetStyles;	
	$p = $model->showStyle($id);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function createStyle()
{
	header("Content-Type: text/json; charset=UTF-8");
	$parent = PassedVal('parent');
	$name = PassedVal('name');
	$model = new SpreadsheetStyles;	
	$p = $model->createStyle($name, $parent);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function updateStyle()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$params = PassedVal('params');
	$model = new SpreadsheetStyles;	
	$p = $model->updateStyle($id, $params);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function removeStyle()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new SpreadsheetStyles;	
	$p = $model->createObject($id);
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
function getCompiledStyle()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = PassedVal('id');
	$model = new SpreadsheetStyles;	
	$p = $model->getCompiledStyle($id);
	echo json_encode($p,JSON_STDPARAMS);
	die();	
}
function getStyleAttrValues()
{
	header("Content-Type: text/json; charset=UTF-8");
	$model = new SpreadsheetStyleAttrValues;	
	$p = $model->getStyleAttrValues();
	echo json_encode($p,JSON_STDPARAMS);
	die();
}
// 
// 
//
function showLinks()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$path = PassedVal('id');
	$model = new ExcelPackageModel;	
	
	$pathParsed = $model->_parseTreePath($path);
	$elementId = $pathParsed['lastChain'];
	$classId = $pathParsed['lastClass'];
	$p = $model->showLinks($elementId, $classId);
	echo json_encode($p,JSON_STDPARAMS);

	die();
}

function showLinksAlt()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$path = PassedVal('path');
	$model = new ExcelPackageModel;	
	$node = $model->beforeShowLinksAlt($path);
	$sar = new sar2;
	$links = $sar->GetChainLinkList($node);
	$r = $model->afterShowLinksAlt($links, $node);

	die( json_encode($r,JSON_STDPARAMS) );
}

function showLinksGrouped()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$elementId = PassedVal('id');
	$params = PassedVal('params');
	$model = new ExcelPackageModel;	
	$p = $model->showLinksGroupByLink($elementId, $params);
	echo json_encode($p,JSON_STDPARAMS);

	die();
}
// 
// 
// 
function showContent()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$pid = PassedVal('pid');
	$model = new ExcelPackageModel;	
	$p = $model->showContent($pid);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function listObjects()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$model = new ExcelPackageModel;
	$p = $model->listObjects($id);
	echo json_encode($p,JSON_STDPARAMS);
	
	die();
}

function createObjectAlt()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$model = new ExcelPackageModel;
	$name = trim(PassedVal('newname'));
	$type = (int)PassedVal('newtype');

	$newId = ExcelPackageModel::createObject($name,$type);
	echo json_encode(
		array('id'=>$newId),JSON_STDPARAMS
	);
	
	die();
}

function renameObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	
	$model = new ExcelPackageModel;
	$id = (int)PassedVal('id');
	$name = trim(PassedVal('newname'));

	$succ = $model->renameObject($id,$name);
	
	echo json_encode(
		array('result'=>$succ,'id'=>$id),JSON_STDPARAMS
	);
	
	die();
}

function saveObject()
{
	header("Content-Type: text/json; charset=UTF-8");
	$id = (int)PassedVal('id');
	$links = PassedVal('links');
	ksort($links);
	$succ = ExcelPackageModel::saveObject($id,$links);
	
	echo json_encode(
		array('result'=>true,'id'=>$id),JSON_STDPARAMS
	);
	
	die();
}


function objectTypes()
{

	$r = ExcelPackageModel::getObjectTypes(true);
	echo json_encode(
		$r,JSON_STDPARAMS
	);
	
	die();
}

function objectImage()
{
	$o_id = (int)PassedVal('id');
	$base_month = trim(PassedVal('month'));
	$sort = (int)PassedVal('sort');
	$style = (int)PassedVal('style');
	
	ExcelPackageModel::objectImage($o_id,$base_month,$sort,null,$style);
}

function objectExport()
{
	$o_id = (int)PassedVal('id');
	$base_month = str_replace('/','-',trim(PassedVal('month')));
	$sort = (int)PassedVal('sort');
	$style = (int)PassedVal('style');
	
	$odata = ExcelPackageModel::getObject($o_id);
	$url = 'http://'.$_SERVER['HTTP_HOST'].'/object/'.$odata['o_key']."_s{$sort}_m{$base_month}_st{$style}.gif";
	$title = htmlspecialchars($odata['o_name']);
	echo "
	<table>
		<tr><th>Ссылка</th>
			<td><input value=\"".htmlspecialchars($url)."\" size=70></td></tr>
		<tr><th>BB-код</th>
			<td><input value=\"".htmlspecialchars('[img]'.$url.'[/img]')."\" size=70></td></tr>
		<tr><th>HTML-код</th>
			<td><input value=\"".htmlspecialchars("<img src=\"".htmlspecialchars($url)."\" title=\"$title\" alt=\"$title\" />")."\" size=70></td></tr>
	
	</table>";
	
	die();
}

function objectItemRename()
{
	$oid = (int)PassedVal('oid');
	$link = (int)PassedVal('link');
	$newname = trim(PassedVal('newname'));
	
}

function objectTable()
{
	$o_id = (int)PassedVal('id');
	$base_month = trim(PassedVal('month'));
	
	list($odata,$READYGRID,$columns,$DEFINITION,$oContent,$requireMonths) = ExcelPackageModel::getObjectData($o_id,$base_month);

	$outputlines = array(1=>'<tr><th></th>',2=>'<tr><th></th>');
	foreach($DEFINITION as $colGroup)
	{
		$outputlines[1] .= "<th colspan=\"".count($colGroup['months'])."\">".$colGroup['title']."</th>";
		//flatten columns for future 
	
		foreach($colGroup['months'] as $colDesc)
		{
			$title = '';
			if(is_array($colDesc))
				$title = $colDesc['title'];
			else
				$title = $colDesc;
		
		
			$outputlines[2] .= "<th>".($requireMonths[$title][1])."</th>";
		}
	
	}
	$outputlines[0] = "<tr><th colspan='".(1+count($columns))."'>$odata[o_name] $base_month</th></tr>";


	// GO!

	foreach($READYGRID as $Y => $Xs)
	{
		$line = "<th>".$oContent[$Y]['oc_title']."</th>";
	
		foreach($Xs as $X=>$V)
		{
		
			$line .= "<td >$V</td>";
		}
	
		$outputlines[] = "<tr>".$line."</tr>";
	}

	ksort($outputlines);
	?>
	
	<table border="1">
	<?
	foreach($outputlines as $line)
		echo $line;
	?>
	</table>
	<?

	die();

}

function OBJListByLinks()
{
	header("Content-Type: text/json; charset=UTF-8");
	$path = PassedVal('path');
	$resp = [];
	if($path)
	{
		$links = sar2::instance()->ExplodePathIntoLinks($path);
		$ids = [];
		foreach($links as $li => $ldata)
		{
			$rmap[$ldata['id']] = $li;
			$ids[] = $ldata['id'];
		}
	}
	if(empty($ids)) return $resp;
	
	$data = ExcelPackageModel::listObjectsByLinks($ids);
	//fix date representation
	foreach($data['objects'] as $_k=>$_v)
	{
		$data['objects'][$_k]['created'] = date('Y/m/d H:i', $_v['created']);
		$data['objects'][$_k]['updated'] = date('Y/m/d H:i', $_v['updated']);
		unset($data['objects'][$_k]['template']);
		unset($data['objects'][$_k]['params']);
	}
	foreach($data['linkmap'] as $thelink => $cols)
	{
		$resp['groups'][$thelink] = [
				'name' => $links[$rmap[$thelink]]['link_name'],
				'list'	=> $cols
			];
	}
	$resp['objects'] = $data['objects'];
	$resp['groups'] = array_values($resp['groups']);

	die(
		json_encode(
			$resp,
			JSON_STDPARAMS
		)
	);
}

function objectLinks()
{
	header("Content-Type: text/javascript; charset=UTF-8");
	
	$model = new ExcelPackageModel;
	$id = (int)PassedVal('id');

	$p = $model->getObjectLinks($id);
	
	if($p)
		$p = array(
				'object'=>$id,
				'links'=>$p
			);
	echo json_encode(
		$p
	);
	
	die();
}

?>