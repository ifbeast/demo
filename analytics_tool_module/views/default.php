<?
	// AddPageScript("/js/jquery-easyui/jquery.easyui.min.js");
	AddPageCSS("/js/jquery-easyui/themes/default/easyui.css");
	AddPageCSS("/js/jquery-easyui/themes/icon.css");

	AddPageScript("/js/jquery-easyui/src/jquery.linkbutton.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.parser.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.panel.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.tooltip.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.validatebox.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.messager.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.textbox.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.menu.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.combo.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.calendar.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.datebox.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.pagination.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.datagrid.js");
	AddPageScript("/js/jquery-easyui/src/jquery.propertygrid.js");
	AddPageScript("/modules/infogedit/assets/js/properties.js");

	AddPageScript("/js/jquery.layout.js");
	AddPageScript("/modules/objects/assets/js/chardinjs-min.js");

	AddPageScript("/js/wijmo/jquery.wijmo.wijspread.all.3.20143.15.min.js");
	AddPageCSS("/js/wijmo/css/theme/jquery-wijmo.css");
	AddPageCSS("/js/wijmo/css/jquery.wijmo.wijspread.3.20143.15.css");

	AddPageScript("/modules/objects/assets/js/combobox.js");
	AddPageScript("/modules/objects/assets/js/helpers.js");
	AddPageScript("/modules/objects/assets/js/tree.js");
	AddPageScript("/modules/objects/assets/js/actions.js");
	AddPageScript("/modules/objects/assets/js/headers.js");
	AddPageScript("/modules/objects/assets/js/observer.js");
	AddPageScript("/modules/objects/assets/js/main.js");

	AddPageCSS("/modules/objects/assets/css/main.css");
	AddPageCSS("/modules/objects/assets/css/chardinjs.css");
?>
<div id="main-layout" class="main-layout">
	<div id="dialog-save-cofirm" class="dialog">Сохранить изменения?</div>

	<div id="dialog-add-data" class="dialog">
		<div class="tree-wrapper">
			<ul style="border-right:0; border-top-right-radius:0; border-bottom-right-radius:0; margin-left: 1px; margin-right: -1px;">
				<li><a href="#indexes-tree"><i class="fa fa-sitemap"></i> Исходные</a></li>
				<li><a href="#indexes-math-tree"><i class="fa fa-gears"></i> Производные</a></li>
			</ul>
			<div id="indexes-tree" class="dialog-tree"></div>
			<div id="indexes-math-tree" class="dialog-tree"></div>
		</div>
		<div class="tree-wrapper">
			<ul style="border-left:0; border-top-left-radius:0; border-bottom-left-radius:0;">
				<li><a id="links-checkbox-title" href="#links-checkbox-tree-wrapper"><i class="fa fa-share-alt-square"></i> Результат</a></li>
			</ul>
			<div id="links-checkbox-tree-wrapper" style="padding:0;">
			  	<a href="javascript:void(0)" id="btn-links-filter" class="btn-flat-half"><i class="fa fa fa-filter"></i> Фильтровать</a>
			  	<a href="javascript:void(0)" id="btn-links-compare" class="btn-flat-half"><i class="fa fa fa-search"></i> Найти соотвествия</a>
			  	<div style="clear: both;"></div>
				<div id="links-checkbox-tree" class="dialog-tree"></div>
			</div>
		</div>
	</div>

	<div id="dialog-add-links" class="dialog">
		<table cellpadding="5">
			<tr>
				<td colspan="2"><div id="dialog-add-links-message"></div></td>
			</tr>
			<!-- <tr>
				<td colspan="2"><h3>Выберите как распределить заголовки:</h3></td>
			</tr> -->
			<tr>
				<td><label for="columns-links-select">Столбцы</label><br><br><select id="columns-links-select" style="min-width:220px" multiple></select></td>
				<td><label for="columns-names-select">Использовать в названии</label><br><br><select id="columns-names-select" style="min-width:220px" multiple></select></td>
			</tr>
			<tr>
				<td><label for="rows-links-select">Строки</label><br><br><select id="rows-links-select" style="min-width:220px" multiple></select></td>
				<td><label for="rows-names-select">Использовать в названии</label><br><br><select id="rows-names-select" style="min-width:220px" multiple></select></td>
			</tr>
		</table>
	</div>

	<div id="dialog-links-filter" class="dialog">
		<div class="dialog-tree-wrapper hide">
			<div class="dialog-tree-tools">
				<label id="class-elements-1" for="search-elements-1"></label>
				<select id="level-elements-1" style="width:100px;">
					<option value="all">Все уровни</option>
					<option value="1">Уровень 1</option>
					<option value="2">Уровень 2</option>
					<option value="3">Уровень 3</option>
					<option value="4">Уровень 4</option>
				</select>
				<input id="search-elements-1" placeholder="Поиск" type="text">
				<button id="btn-elements-1-select"><i class="fa fa-check-square-o"></i></button>
				<button id="btn-elements-1-deselect"><i class="fa fa-square-o"></i></button>
			</div>
			<div id="elements-1-tree" class="dialog-tree"></div>
		</div>
		<div class="dialog-tree-wrapper hide">
			<div class="dialog-tree-tools">
				<label id="class-elements-2" for="search-elements-2"></label>
				<select id="level-elements-2" style="width:100px;">
					<option value="all">Все уровни</option>
					<option value="1">Уровень 1</option>
					<option value="2">Уровень 2</option>
					<option value="3">Уровень 3</option>
					<option value="4">Уровень 4</option
				</select>
				<input id="search-elements-2" placeholder="Поиск" type="text">
				<button id="btn-elements-2-select"><i class="fa fa-check-square-o"></i></button>
				<button id="btn-elements-2-deselect"><i class="fa fa-square-o"></i></button>
			</div>
			<div id="elements-2-tree" class="dialog-tree"></div>
		</div>
		<div class="dialog-tree-wrapper hide">
			<div class="dialog-tree-tools">
				<label id="class-elements-3" for="search-elements-3"></label>
				<select id="level-elements-3" style="width:100px;">
					<option value="all">Все уровни</option>
					<option value="1">Уровень 1</option>
					<option value="2">Уровень 2</option>
					<option value="3">Уровень 3</option>
					<option value="4">Уровень 4</option
				</select>
				<input id="search-elements-3" placeholder="Поиск" type="text">
				<button id="btn-elements-3-select"><i class="fa fa-check-square-o"></i></button>
				<button id="btn-elements-3-deselect"><i class="fa fa-square-o"></i></button>
			</div>
			<div id="elements-3-tree" class="dialog-tree"></div>
		</div>
		<div class="dialog-units-wrapper">
			<div class="dialog-tree-tools">
				<label>Единицы измерения</label>
			</div>
			<div id="units-tree" class="dialog-tree"></div>
		</div>
			<!-- <div id="elements-4-tree" class="dialog-tree"></div> -->
			<!-- <fieldset id="units" class="attrvalues" style="float:left;"></fieldset>  -->
	</div>

	<div id="dialog-add-formula" class="dialog">
		<table cellpadding="5">
			<tr>
				<td><label for="dates-select">Даты</label></td>
				<td><label for="formulas-select">Формулы</label></td>
			</tr>
			<tr>
				<td>
					<select id="dates-select" style="width:200px; height:200px;" multiple>
						<option value="current-month-quarter" selected>Текущий месяц\квартал</option>
						<option value="prev-month-quarter">Предыдущий месяц\квартал</option>
						<option value="prev-year">Предыдущий год</option>
					</select>
				</td>
				<td>
					<select id="formulas-select" style="width:200px; height:200px;" multiple>
						<option value="default" selected>Всего</option>
						<option value="func-1-p">Рост в %</option>
						<option value="func-1">Рост</option>
						<option value="func-2-p">Доля от общего в %</option>
						<option value="func-2">Доля от общего в долях</option>
						<option value="func-3">Ренкинг</option>
					</select>
				</td>
			</tr>
		</table>
	</div>

	<div id="dialog-edit-theme" class="dialog">
		<!-- <div id="theme-property-grid"></div> -->
		<!-- <div id="theme-tabs" class="dialog-tree-wrapper">
			<ul>
				<li>
					<a id="themes-tab" href="#themes-tree-wrapper"><i class="fa fa-folder"></i> Темы</a>
					<a id="btn-refresh-themes-tree" href="javascript:void(0)" title="Обновить дерево" style="margin-left: -20px;"><i class="fa fa-refresh"></i></a>
				</li>
				<li>
					<a id="styles-tree-tab" href="#styles-tree-wrapper"><i class="fa fa-folder"></i> Стили</a>
					<a id="btn-refresh-styles-tree" href="javascript:void(0)" title="Обновить дерево" style="margin-left: -20px;"><i class="fa fa-refresh"></i></a>
				</li>
			</ul>
			<div id="themes-tree-wrapper" style="padding:0; widthL100%;" class="">
			  	<a href="javascript:void(0)" id="btn-add-theme" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить тему</a>
				<div id="themes-tree" class="dialog-tree"></div>
			</div>
			<div id="styles-tree-wrapper" style="padding:0; widthL100%;" class="">
				<div id="styles-tree" class="dialog-tree"></div>
			</div>
		</div>
		<div class="dialog-tree-wrapper">
			<div id="theme-property-grid"></div>
		</div> -->
	</div>
	<div class="ui-layout-west">
		<!--<div id="packages-tools" class="tools-wrapper left">
			<ul class="tools">
				 <li><a id="btn-add-folder" href="#" title="Добавить папку"><i class="fa fa-folder-open"></i></a></li>
				<li><a id="btn-add-package" href="#" title="Добавить пакет"><i class="fa fa-folder-open-o"></i></a></li>
				<li><a id="btn-add-object" href="#" title="Добавить объект"><i class="fa fa-file-excel-o"></i></a></li>
				<li><a id="btn-clone-object" href="#" title="Сделать копию объекта"><i class="fa fa-copy"></i></a></li>
				<li><a id="btn-refresh-packages-tree" href="#" title="Обновить дерево" style="color: green;"><i class="fa fa-refresh"></i></a></li>
			</ul>
		</div>-->
		<div id="packages" class="tree-wrapper">
			<ul>
				<li>
					<a id="packages-tab" href="#packages-tree-wrapper"><i class="fa fa-folder"></i> Пакеты</a>
					<a id="btn-refresh-packages-tree" href="javascript:void(0)" title="Обновить дерево" style="margin-left: -20px;"><i class="fa fa-refresh"></i></a>
				</li>
			</ul>
			<div id="packages-tree-wrapper" style="padding:0;" class="tree-wrapper">
			  	<a href="javascript:void(0)" id="btn-add-folder" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить папку</a>
				<div id="packages-tree" class="tree"></div>
			</div>
		</div>

	</div>

	<div id="package-layout" class="ui-layout-center">
		<div class="ui-layout-north">
			<div class="state-wrapper">
				<button id="btnEditObject" class="hide"><i class="fa fa-pencil"></i> Редактировать</button>
				<div id="state-edit-group" class="button-group hide">
					<button id="btnSaveObject"><i class="fa fa-check"></i> Сохранить</button>
					<button id="btnEditCancel">Назад</button>
				</div>
				<div id="state-export-group" class="button-group hide">
					<input id="export-date-select" type="month">
					<button id="btnExport"><i class="fa fa-coffee"></i> Экспорт</button>
				</div>
				<span id="state-header" class="header"></span>
				<button id="btn-test"  style="float: right;">Test</button>
				<button id="btn-help" style="float: right;"><i class="fa fa-question-circle"></i> Помощь</button>
			</div>
		</div>

		<div id="spreadsheet-layout" class="ui-layout-center">
			<div class="ui-layout-west">
				<div id="header-cols" class="tree-wrapper">
					<ul>
						<li><a id="header-cols-tab" href="#header-cols-tree-wrapper"><i class="fa fa-columns"></i> Столбцы</a></li>
						<li><a id="header-hidden-tab" href="#header-hidden-tree-wrapper"><i class="fa fa-share-alt-square"></i> Скрытые</a></li>
					</ul>
					<div id="header-cols-tree-wrapper" style="padding:0;" class="tree-wrapper">
					  	<a href="javascript:void(0)" id="btn-add-header" class="btn-flat header-tools hide"><i class="fa fa-plus-square"></i> Добавить данные</a>
						<div id="header-cols-tree" class="tree"></div>
					</div>
					<div id="header-hidden-tree-wrapper" style="padding:0;" class="tree-wrapper">
					  	<a href="javascript:void(0)" id="btn-add-hidden" class="btn-flat header-tools hide"><i class="fa fa-plus-square"></i> Добавить в скрытые</a>
						<div id="header-hidden-tree" class="tree"></div>
					</div>
				</div>

				<div id="header-rows" class="tree-wrapper with-tools">
					<ul>
						<li><a id="header-rows-tab" href="#header-rows-tree-wrapper"><i class="fa fa-table"></i> Строки</a></li>
						<li><a id="header-groups-tab" href="#header-groups-tree-wrapper"><i class="fa fa-th-list"></i> Эшелоны</a></li>
					</ul>
					<div id="header-rows-tree-wrapper" style="padding:0;" class="tree-wrapper">
						<a href="javascript:void(0)" id="btn-add-static-group" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить группу</a>
						<div id="header-rows-tree" class="tree"></div>
					</div>
					<div id="header-groups-tree-wrapper" style="padding:0;" class="tree-wrapper">
					  	<a href="javascript:void(0)" id="btn-add-group" class="btn-flat header-tools hide"><i class="fa fa-plus-square"></i> Добавить эшелон</a>
						<div id="header-groups-tree" class="tree"></div>
					</div>
				</div>
			</div>
			<div id="spreadsheet-tools-layout" class="ui-layout-center">
				<div class="ui-layout-north state-wrapper">
					<div id="refresh-group" class="button-group hide">
						<input id="last-date-select" type="month">
						<button id="btnRefresh"><i class="fa fa-refresh"></i> Обновить</button>
						<button id="btnExportObject"><i class="fa fa-coffee"></i> Экспорт</button>
						<button id="btnFullExportObject">полный</button>
						<span id="last-date-label" class="header"></span>
						<!-- <div id="formulaBar" contenteditable="true" spellcheck="false" style="border: 1px solid gray;height: 21px; width: 99%; overflow: hidden; font-size: 13px;font-family: arial,sans-serif;padding: 3px;margin-bottom: 5px;"></div>  -->
					</div>
				</div>
				<div class="ui-layout-center">
					<div id="spreadsheet"></div>
				</div>
			</div>
			<div class="ui-layout-east">
				<div id="theme-property-grid"></div>
				<div id="spreadsheet-tools" class="controls-wrapper">
					<h3><i class="fa fa-tags"></i> Общие</h3>
					<div id="export-titles">
						<table cellpadding="2">
							<tr>
								<td colspan="2">
									<label for="sequence-select" style="width:284px">Данные с накоплением</label><br>
									<select id="sequence-select" style="width:284px">
										<option value="true" selected>Конвертировать</option>
					                    <option value="false">Не конвертировать</option>
					                </select>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<label for="subscription-input">Подпись</label><br>
									<input id="subscription-input" type="text" style="width:275px">
								</td>
							</tr>
							<tr>
								<td><label for="source-input">Источник</label></td>
								<td><input id="source-input" type="text"></td>
							</tr>
							<tr>
								<td><label for="units-input">Единицы</label></td>
								<td><input id="units-input" type="text"></td>
							</tr>
							<tr>
								<td></td>
								<td><button id="btnSubscriptionInput"><i class="fa fa-check"></i> Применить</button></td>
							</tr>
						</table>
					</div>

					<h3><i class="fa fa-tags"></i> Стили</h3>
					<div id="style-tools">
						<div class="button-group">
							<select id="themes-select"  style="width:160px;">
								<option selected>По умолчанию</option>
			                    <option>Office</option>
			                    <option>Apex</option>
			                    <option>Aspect</option>
			                    <option>Concourse</option>
			                    <option>Civic</option>
			                    <option>Oriel</option>
			                    <option>Origin</option>
			                    <option>Paper</option>
			                    <option>Solstice</option>
			                    <option>Technic</option>
			                    <option>Trek</option>
			                    <option>Urban</option>
			                    <option>Verve</option>
			                    <option>Equity</option>
			                    <option>Flow</option>
			                    <option>Foundry</option>
			                    <option>Median</option>
			                    <option>Metro</option>
			                    <option>Module</option>
			                    <option>Opulent</option>
							</select>
							<button id="btnEditTheme" style="width:95px; margin-left:-5px;"><i class="fa fa-edit"></i> Редактор</button>
						</div>
						<!-- <div class="button-group">
							<button id="btnCreateTable"><i class="fa fa-table"></i> Таблица</button>
						</div> -->
						<div class="button-group">
							<button id="btnSetHeader1"><i class="fa fa-header"></i> Заголовок 1</button>
							<button id="btnSetHeader2"><i class="fa fa-header"></i> Заголовок 2</button>
							<button id="btnSetField1"><i class="fa fa-font"></i> Поле 1</button>
							<button id="btnSetField2"><i class="fa fa-font"></i> Поле 2</button>
							<button id="btnSetSubField1"><i class="fa fa-bold"></i> Особое 1</button>
							<button id="btnSetSubField2"><i class="fa fa-bold"></i> Особое 2</button>
							<button id="btnSetSubscription"><i class="fa fa-italic"></i> Подпись</button>
						</div>
						<div class="button-group">
							<button id="btnMergeCells"><i class="fa fa-th-large"></i> Объединить</button>
							<button id="btnUnmergeCells"><i class="fa fa-th"></i> Разъединить</button>
						</div>
						<div class="button-group">
							<button id="btnTextAlignLeft"><i class="fa fa-align-left"></i> Слева</button>
							<button id="btnTextAlignCenter"><i class="fa fa-align-center"></i> По центру</button>
							<button id="btnTextAlignRight"><i class="fa fa-align-right"></i> Справа</button>
							<button id="btnSetOuterBorder"><i class="fa fa-columns"></i> Границы</button>
						</div>
						<!-- <div class="button-group">	 -->
							<!-- <button id="btnSetInnerBorder"><i class="fa fa-table"></i> Внутренние</button> -->
						<!-- </div> -->
						<div class="button-group">
							<button id="btnTextIndentIncrease"><i class="fa fa-indent"></i> Отступ</button>
							<button id="btnTextIndentDecrease"><i class="fa fa-outdent"></i> Убрать отступ</button>
						</div>
						<div class="button-group">
							<button id="btnClearColor"><i class="fa fa-ban"></i> Убрать стили</button>
						</div>
						<div class="button-group">
							<!-- <button id="btnClearBorder"><i class="fa fa-eraser"></i> Убрать границы</button> -->
							<select id="styles-special-select"  style="width:160px;">
								<option value="default" selected>По умолчанию</option>
								<option value="level-even">Уровни четные</option>
								<option value="level-odd">Уровни нечетные</option>
								<option value="row-even">Строки четные</option>
								<option value="row-odd">Строки нечетные</option>
							</select>
							<button id="btnClearStyles" style="width:95px; margin-left:-5px;"><i class="fa fa-bolt"></i> Автостили</button>
						</div>
					</div>

					<h3><i class="fa fa-tags"></i> Сортировка</h3>
					<div id="post-sort">
						<table cellpadding="2">
							<tr>
								<td><label for="sort-columns-input">Столбец сортировки</label></td>
								<td><input id="sort-columns-input" type="number" min="-1" value="0"></td>
								<td><button id="sort-columns-button" class="change-selection-mode toggle-button" data-target="sort-columns-input" data-mode="column"><i class="fa fa-circle-o"></i></button></td>
							</tr>
							<tr>
								<td><label for="sort-rows-input">Строка исключения</label></td>
								<td><input id="sort-rows-input" type="number" min="0" value="0"></td>
								<td><button id="sort-rows-button" class="change-selection-mode toggle-button" data-target="sort-rows-input" data-mode="row"><i class="fa fa-circle-o"></i></button></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<select id="sort-order-select">
										<option value="desc">По убыванию</option>
										<option value="asc">По возрастанию</option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><button id="btnPostSort"><i class="fa fa-check"></i> Применить</button><a href="javascript:void(0)" id="btnPostSortCancel" class="btn-a">Отменить</a></td>
							</tr>
						</table>
					</div>

					<h3><i class="fa fa-tags"></i> Дополнительно</h3>
					<div id="additional">
						<div id="function-wrapper" class="hide">
							<table cellpadding="2">
								<tr>
									<td colspan="2">
										<div id="assign-type-radio">
										    <input type="radio" id="assign-column-radio" name="radio" value="column" checked><label for="assign-column-radio">Столбец</label>
										    <input type="radio" id="assign-cells-radio" name="radio" value="cells"><label for="assign-cells-radio">Ячейки</label>
									  	</div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<label for="function-input"><i class="fa fa-superscript"></i> Формула</label><br>
										<!-- <input id="function-input" type="text" style="width:275px"> -->
										<select id="function-input">
											<option value="">Выберите формулу</option>
										    <option value="val(shift)">Всего</option>
										    <option value="val(0)-val(shift)">Рост к итогу</option>
										    <option value="val(0)/val(shift)*100">Рост к итогу в %</option>
										    <option value="val(shift)/total(shift)">Доля от общего</option>
										    <option value="val(shift)/total(shift)*100">Доля от общего в %</option>
										    <option value="val(shift)/rcellx(n)">Доля от частного</option>
										    <option value="val(shift)/rcellx(n)*100">Доля от частного в %</option>
										    <option value="sortindex(n)">Ренкинг</option>
										    <option value="cell(m,n)">Абсолютная ячейка</option>
										    <option value="rcell(m,n)">Относительная ячейка</option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label for="format-select">Формат</label></td>
									<td>
										<select id="format-select">
											<option value="default" selected>По умолчанию</option>
											<option value="numerical">Числовой</option>
											<option value="percentage">Процентный</option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label for="suffix-input">Единицы</label></td>
									<td><input id="suffix-input" type="text"></td>
								</tr>
								<tr>
									<td><label for="precision-select">Точность</label></td>
									<td>
										<select id="precision-select">
										        <option value="0" selected>*</option>
										        <option value="1">*.0</option>
										        <option value="2">*.00</option>
										        <option value="3">*.000</option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label for="hidden-checkbox">Скрыть при экспорте</label></td>
									<td><input id="hidden-checkbox" type="checkbox" style="width:20px;"></td>
								</tr>
								<tr>
									<td></td>
									<td><button id="btnFunctionInput"><i class="fa fa-check"></i> Применить</button></td>
								</tr>
							</table>
						</div>
						<div id="name-special-wrapper" class="hide">
							<table cellpadding="2">
								<tr>
									<td><label for="name-special-input"><i class="fa fa-code"></i> Метка</label></td>
									<td><input id="name-special-input" type="text"></td>
								</tr>
								<tr>
									<td></td>
									<td><button id="btnNameSpecialInput"><i class="fa fa-check"></i> Применить</button></td>
								</tr>
							</table>
						</div>
						<div id="group-wrapper" class="hide">
							<table cellpadding="2">
								<tr>
									<td><label for="group-column-input">Столбец</label></td>
									<td><input id="group-column-input" type="number" min="0" value="0"></td>
									<td><button id="group-column-button" class="change-selection-mode toggle-button" data-target="group-column-input" data-mode="column"><i class="fa fa-circle-o"></i></button></td>
								</tr>
								<tr>
									<td><label for="group-max-input">Максимум</label></td>
									<td><input id="group-max-input" type="number" step="0.1"></td>
									<td><button id="group-max-button" class="change-selection-mode toggle-button" data-target="group-max-input" data-mode="value"><i class="fa fa-circle-o"></i></button></td>
								</tr>
								<tr>
									<td><label for="group-min-input">Минимум</label></td>
									<td><input id="group-min-input" type="number" step="0.1"></td>
									<td><button id="group-min-button" class="change-selection-mode toggle-button" data-target="group-min-input" data-mode="value"><i class="fa fa-circle-o"></i></button></td>
								</tr>
								<tr>
									<td><label for="group-func-select">Функция</label></td>
									<td>
										<select id="group-func-select">
											<option value="empty">Пусто</option>
											<option value="sum" selected>Сумма</option>
											<option value="avg">Среднее</option>
										</select>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><button id="btnGroupInput"><i class="fa fa-check"></i> Применить</button></td>
								</tr>
							</table>
						</div>
						<div id="static-group-wrapper" class="hide">
							<table cellpadding="2">
								<tr>
									<td><label for="static-group-func-select">Функция</label></td>
									<td>
										<select id="static-group-func-select">
											<option value="empty" selected>Пусто</option>
											<option value="sum">Сумма</option>
											<option value="avg">Среднее</option>
										</select>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><button id="btnStaticGroupInput"><i class="fa fa-check"></i> Применить</button></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<form id="reqform" method="post" action="/?mod=objects"  target="_blank"></form>
	</div>
</div>
