<div id="t-root" style="height:100%;">
	<div id="t-welcome" class="hide fillrest"></div>
	<div id="t-open-plate" class="hide fillrest"></div>
	<div id="t-tpls-plate" class="hide fillrest"></div>
	<div id="t-edit-plate" class="hide fillrest"></div>
</div>
<?php
	AddPageScript("/js/handlebars-v2.0.0.js");

	AddPageScript("/js/jquery-easyui/jquery.easyui.min.js");
	AddPageCSS("/js/jquery-easyui/themes/default/easyui.css");
	AddPageCSS("/js/jquery-easyui/themes/icon.css");

	AddPageScript("/js/jquery-easyui/plugins/jquery.datagrid.js");
	AddPageScript("/js/jquery-easyui/src/jquery.propertygrid.js");
	AddPageScript("/js/jquery-easyui/plugins/jquery.treegrid-dnd.js");

	AddPageScript("/js/wijmo/jquery.wijmo.wijspread.all.3.20143.15.min.js");
	AddPageCSS("/js/wijmo/css/theme/jquery-wijmo.css");
	AddPageCSS("/js/wijmo/css/jquery.wijmo.wijspread.3.20143.15.css");

	AddPageScript("/modules/infogedit/assets/js/properties.js");
	AddPageScript("/modules/objects/assets/js/combobox.js");
	AddPageScript("/modules/objects/assets/js/helpers.js");
	AddPageScript("/modules/objects/assets/js/tree.js");
	AddPageScript("/modules/objects/assets/js/actions.js");
	AddPageScript("/modules/objects/assets/js/headers.js");
	AddPageScript("/modules/objects/assets/js/observer.js");
	AddPageScript("/modules/objects/assets/js/main.js");
	AddPageCSS("/modules/objects/assets/css/main.css");
?>
