<div id="t-layout" class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north', split:false" style="height:38px; padding: 5px;">
		<a href="javascript:void(0)" class="easyui-menubutton" data-options="plain:true, menu:'#file-menu'">Файл</a>
		<input id="last-date-select" type="text" class="easyui-datebox">
		<a id="btnRefresh" href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true, iconCls:'fa fa-refresh'"> Обновить</a>
		<a id="btnClearStyles" href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true, iconCls:'fa fa-magic'"> Автостили</a>
		<a id="btnStyles" href="javascript:void(0)" class="easyui-linkbutton" data-options="plain:true, iconCls:'fa fa-paint-brush'"> Оформление</a>
		<div id="file-menu">
		    <div id="btnSaveTableFull" data-options="iconCls:'fa fa-save'">Сохранить</div>
		    <div id="btnSaveTable" data-options="iconCls:'fa fa-save'">Сохранить без оформления</div>
		    <div class="menu-sep"></div>
		    <div id="btnExportObject" data-options="iconCls:'fa fa-file-excel-o'">Экспорт в .xls</div>
		    <div class="menu-sep"></div>
		    <div id="btnEditClose" data-options="">Закрыть</div>
		</div>
	</div>
	<div data-options="region:'south', split:true,  title:'Статус', collapsed:true" style="height:100px;"></div>
    <div data-options="region:'west', split:true, title:'Заголовки', collapsible:true, border:false" style="width:20%;">
    	<div id="t-layout-headers" class="easyui-layout" data-options="fit:true">
	        <div data-options="region:'north', split:true" style="height:400px;">
	        	<div class="easyui-tabs"  data-options="border:false, plain:true, fit:true">
				    <div title="Столбцы" style="overflow-y:auto; overflow-x:hidden;">
						<a href="javascript:void(0)" id="btn-add-header" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить данные</a>
						<div id="header-cols-tree" class="tree"></div>
				    </div>
				    <div title="Скрытые" style="overflow-y:auto; overflow-x:hidden;">
						<a href="javascript:void(0)" id="btn-add-hidden" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить в скрытые</a>
						<div id="header-hidden-tree" class="tree"></div>
				    </div>
				</div>				
	        </div>
			<div data-options="region:'center'">
				<div class="easyui-tabs" data-options="border:false, plain:true, fit:true">
				    <div title="Строки" style="overflow-y:auto; overflow-x:hidden;">
				    	<a href="javascript:void(0)" id="btn-add-static-group" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить группу</a>
						<div id="header-rows-tree" class="tree"></div>
				    </div>
				    <div title="Эшелоны" style="overflow-y:auto; overflow-x:hidden;">
						<a href="javascript:void(0)" id="btn-add-group" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить эшелон</a>
						<div id="header-groups-tree" class="tree"></div>
				    </div>
				</div>
			</div>
    	</div>
    </div>
    <div data-options="region:'center'" style="overflow-y:hidden">    	
    	<div id="t-spreadsheet" style="width:100%;"></div>
    </div>
	<div data-options="region:'east', split:true, collapsible:false" style="width:20%;">
		<div id="t-accordion" class="easyui-accordion" data-options="fit:true, border:false">
		    <div title="Общие свойства"  data-options="selected:true" style="overflow-y:auto;">
		        <div id="t-common-properties"></div>
		    </div>
		    <div title="Дополнительные свойства" style="overflow-y:auto;">
		        <div id="t-additional-properties"></div>
		    </div>
		</div>
	</div>
</div>

<div id="dialog-add-data" class="easyui-window hide" title="Добавление данных" style="width:600px; height:600px;">
	<div class="tree-wrapper">
		<ul style="border-right:0; border-top-right-radius:0; border-bottom-right-radius:0; margin-left: 1px; margin-right: -1px;">
			<li><a href="#indexes-tree"><i class="fa fa-sitemap"></i> Исходные</a></li>
			<li><a href="#indexes-math-tree"><i class="fa fa-gears"></i> Производные</a></li>
		</ul>
		<div id="indexes-tree" class="dialog-tree"></div>
		<div id="indexes-math-tree" class="dialog-tree"></div>
	</div>
	<div class="tree-wrapper">
		<ul style="border-left:0; border-top-left-radius:0; border-bottom-left-radius:0;">
			<li><a id="links-checkbox-title" href="#links-checkbox-tree-wrapper"><i class="fa fa-share-alt-square"></i> Результат</a></li>
		</ul>
		<div id="links-checkbox-tree-wrapper" style="padding:0;">
		  	<a href="javascript:void(0)" id="btn-links-filter" class="btn-flat-half"><i class="fa fa fa-filter"></i> Фильтровать</a>
		  	<a href="javascript:void(0)" id="btn-links-compare" class="btn-flat-half"><i class="fa fa fa-search"></i> Найти соотвествия</a>
		  	<div style="clear: both;"></div>
			<div id="links-checkbox-tree" class="dialog-tree"></div>
		</div>
	</div>
</div>

<div id="dialog-add-links" class="easyui-window hide" title="Определение заголовков" style="width:600px; height:600px;">
	<table cellpadding="5">
		<tr>
			<td colspan="2"><div id="dialog-add-links-message"></div></td>
		</tr>
		<!-- <tr>
			<td colspan="2"><h3>Выберите как распределить заголовки:</h3></td>
		</tr> -->
		<tr>
			<td><label for="columns-links-select">Столбцы</label><br><br><select id="columns-links-select" style="min-width:220px" multiple></select></td>
			<td><label for="columns-names-select">Использовать в названии</label><br><br><select id="columns-names-select" style="min-width:220px" multiple></select></td>
		</tr>
		<tr>
			<td><label for="rows-links-select">Строки</label><br><br><select id="rows-links-select" style="min-width:220px" multiple></select></td>
			<td><label for="rows-names-select">Использовать в названии</label><br><br><select id="rows-names-select" style="min-width:220px" multiple></select></td>
		</tr>
	</table>
</div>

<div id="dialog-links-filter" class="easyui-window hide" title="Фильтр данных" style="width:600px; height:600px;">
	<div class="dialog-tree-wrapper hide">
		<div class="dialog-tree-tools">
			<label id="class-elements-1" for="search-elements-1"></label>
			<select id="level-elements-1" style="width:100px;">
				<option value="all">Все уровни</option>
				<option value="1">Уровень 1</option>
				<option value="2">Уровень 2</option>
				<option value="3">Уровень 3</option>
				<option value="4">Уровень 4</option>
			</select>
			<input id="search-elements-1" placeholder="Поиск" type="text">
			<button id="btn-elements-1-select"><i class="fa fa-check-square-o"></i></button>
			<button id="btn-elements-1-deselect"><i class="fa fa-square-o"></i></button>
		</div>
		<div id="elements-1-tree" class="dialog-tree"></div>
	</div>
	<div class="dialog-tree-wrapper hide">
		<div class="dialog-tree-tools">
			<label id="class-elements-2" for="search-elements-2"></label>
			<select id="level-elements-2" style="width:100px;">
				<option value="all">Все уровни</option>
				<option value="1">Уровень 1</option>
				<option value="2">Уровень 2</option>
				<option value="3">Уровень 3</option>
				<option value="4">Уровень 4</option					
			</select>
			<input id="search-elements-2" placeholder="Поиск" type="text">
			<button id="btn-elements-2-select"><i class="fa fa-check-square-o"></i></button>
			<button id="btn-elements-2-deselect"><i class="fa fa-square-o"></i></button>
		</div>
		<div id="elements-2-tree" class="dialog-tree"></div>
	</div>
	<div class="dialog-tree-wrapper hide">
		<div class="dialog-tree-tools">
			<label id="class-elements-3" for="search-elements-3"></label>
			<select id="level-elements-3" style="width:100px;">
				<option value="all">Все уровни</option>
				<option value="1">Уровень 1</option>
				<option value="2">Уровень 2</option>
				<option value="3">Уровень 3</option>
				<option value="4">Уровень 4</option
			</select>
			<input id="search-elements-3" placeholder="Поиск" type="text">
			<button id="btn-elements-3-select"><i class="fa fa-check-square-o"></i></button>
			<button id="btn-elements-3-deselect"><i class="fa fa-square-o"></i></button>
		</div>
		<div id="elements-3-tree" class="dialog-tree"></div>
	</div>
	<div class="dialog-units-wrapper">
		<div class="dialog-tree-tools">
			<label>Единицы измерения</label>
		</div>
		<div id="units-tree" class="dialog-tree"></div>
	</div>
		<!-- <div id="elements-4-tree" class="dialog-tree"></div> -->
		<!-- <fieldset id="units" class="attrvalues" style="float:left;"></fieldset>  -->
</div>

<div id="dialog-add-formula" class="easyui-window hide" title="Мастер формул" style="width:600px; height:600px;">
	<table cellpadding="5">
		<tr>
			<td><label for="dates-select">Даты</label></td>
			<td><label for="formulas-select">Формулы</label></td>
		</tr>
		<tr>
			<td>
				<select id="dates-select" style="width:200px; height:200px;" multiple>
					<option value="current-month-quarter" selected>Текущий месяц\квартал</option>
					<option value="prev-month-quarter">Предыдущий месяц\квартал</option>
					<option value="prev-year">Предыдущий год</option>
				</select>
			</td>
			<td>
				<select id="formulas-select" style="width:200px; height:200px;" multiple>
					<option value="default" selected>Всего</option>
					<option value="func-1-p">Рост в %</option>
					<option value="func-1">Рост</option>
					<option value="func-2-p">Доля от общего в %</option>
					<option value="func-2">Доля от общего в долях</option>
					<option value="func-3">Ренкинг</option>
				</select>
			</td>
		</tr>
	</table>
</div>

<div id="dialog-edit-theme" class="easyui-window hide" title="Редактор тем" style="width:600px; height:600px;">
	<!-- <div id="theme-property-grid"></div> -->
	<!-- <div id="theme-tabs" class="dialog-tree-wrapper">
		<ul>
			<li>
				<a id="themes-tab" href="#themes-tree-wrapper"><i class="fa fa-folder"></i> Темы</a>
				<a id="btn-refresh-themes-tree" href="javascript:void(0)" title="Обновить дерево" style="margin-left: -20px;"><i class="fa fa-refresh"></i></a>
			</li>
			<li>
				<a id="styles-tree-tab" href="#styles-tree-wrapper"><i class="fa fa-folder"></i> Стили</a>
				<a id="btn-refresh-styles-tree" href="javascript:void(0)" title="Обновить дерево" style="margin-left: -20px;"><i class="fa fa-refresh"></i></a>
			</li>
		</ul>
		<div id="themes-tree-wrapper" style="padding:0; widthL100%;" class="">
		  	<a href="javascript:void(0)" id="btn-add-theme" class="btn-flat"><i class="fa fa-plus-square"></i> Добавить тему</a>
			<div id="themes-tree" class="dialog-tree"></div>
		</div>
		<div id="styles-tree-wrapper" style="padding:0; widthL100%;" class="">
			<div id="styles-tree" class="dialog-tree"></div>
		</div>
	</div>
	<div class="dialog-tree-wrapper">
		<div id="theme-property-grid"></div>
	</div> -->
</div>

<div id="dialog-style-tools" class="easyui-window hide" title="Стили" style="width:110px; height:300px;">
	<div style="padding:3px 5px;">
        <a id='btnMergeCells' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Объеденить" data-options="iconCls:'fa fa-th-large'"></a>
        <a id='btnUnmergeCells' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Разъединить" data-options="iconCls:'fa fa-th'"></a>
    </div>
	<div style="padding:3px 5px;">
        <a id='btnTextAlignLeft' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="По левому краю" data-options="iconCls:'fa fa-align-left'"></a>
        <a id='btnTextAlignCenter' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="По центру" data-options="iconCls:'fa fa-align-center'"></a>
        <a id='btnTextAlignRight' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="По правому краю" data-options="iconCls:'fa fa-align-right'"></a>
    </div>
    <div style="padding:3px 5px;">
        <a id='btnTextIndentIncrease' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Увеличить отступ" data-options="iconCls:'fa fa-indent'"></a>
        <a id='btnTextIndentDecrease' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Убрать отступ" data-options="iconCls:'fa fa-outdent'"></a>
    </div>
    <div style="padding:3px 5px;">
        <a id='btnMergeCells' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Нормальный текст" data-options="iconCls:'fa fa-font'"></a>
        <a id='btnMergeCells' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Жирный текст" data-options="iconCls:'fa fa-bold'"></a>
        <a id='btnMergeCells' href="javascript:void(0)" class="easyui-linkbutton" data-tooltip-text="Текст курсивом" data-options="iconCls:'fa fa-italic'"></a>
    </div>
</div>