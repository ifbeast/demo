<table class="easyui-treegrid" id="t-folders" title="Доступные таблицы" style="width:100%; height:500px;">
	<thead>
		<tr>
			<th data-options="field:'id',width:50">Номер</th>
			<th data-options="field:'name',width:400,editor:'text'">Наименование</th>
			<th data-options="field:'author',width:100">Автор</th>
			<th data-options="field:'created',width:120,align:'right'">Создан</th>
			<!-- <th data-options="field:'lasteditor',width:120,align:'right'">Изменен автором</th> -->
			<th data-options="field:'updated',width:120,align:'right'">Изменен</th>
		</tr>
	</thead>
</table>

<div id="t-folders-contextmenu" class="easyui-menu" style="width:150px; display: none;">
	<div data-options="name:'openTable'" class="only-documents">Открыть</div>
	<!-- <div class="menu-sep"></div>
	<div data-options="name:'newitem',iconCls:'fa fa-asterisk'" class="onlyfolders">Создать здесь</div>
	<div class="menu-sep"></div>
	<div data-options="name:'properties'" class="onlyitems">Свойства</div>
	<div data-options="name:'refresh',iconCls:'fa fa-refresh'" class="onlyfolders">Обновить</div> -->
	<div class="menu-sep"></div>
	<div data-options="name:'rename',iconCls:'fa fa-edit'">Переименовать</div>
	<div data-options="name:'export',iconCls:'fa fa-file-excel-o'">Экспорт</div>
	<div class="menu-sep"></div>
	<div data-options="name:'createFolder',iconCls:'fa fa-folder-o'" class="only-folders">Новая папка</div>
	<div data-options="name:'createTable',iconCls:'fa fa-file-o'" class="only-folders">Новая таблица</div>
	<div class="menu-sep"></div>
	<div data-options="name:'remove',iconCls:'fa fa-remove'">Удалить</div>
</div>